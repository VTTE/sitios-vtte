<?php
/*
Plugin Name: Tags
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: EventTags Taxonomy
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

function registerEventTagsTaxonomy(){
	register_taxonomy(
		'event_tags',
		array( 'event' ),
		array(
			'label' => _x('Tags', 'event_tags', 'custom_tax_event_tags'),
			'labels' => array(
				'name' => _x('Tags', 'event_tags', 'custom_tax_event_tags'),
				'singular_name' => _x('Tag', 'event_tags', 'custom_tax_event_tags'),
				'menu_name' => _x('Tags', 'event_tags', 'custom_tax_event_tags'),
				'all_items' => _x('Tags', 'event_tags', 'custom_tax_event_tags'),
				'edit_item' => _x('Editar Tag', 'event_tags', 'custom_tax_event_tags'),
				'view_item' => _x('Ver Tag', 'event_tags', 'custom_tax_event_tags'),
				'update_item' => _x('Actualizar Tag', 'event_tags', 'custom_tax_event_tags'),
				'add_new_item' => _x('Agregar nuevo Tag', 'event_tags', 'custom_tax_event_tags'),
				'new_item_name' => _x('Nuevo nombre del Tag', 'event_tags', 'custom_tax_event_tags'),
				'parent_item' => _x('Tag padre', 'event_tags', 'custom_tax_event_tags'),
				'parent_item_colon' => _x('Tag padre:', 'event_tags', 'custom_tax_event_tags'),
				'search_items' => _x('Buscar Tags', 'event_tags', 'custom_tax_event_tags'),
				'popular_items' => _x('Tags populares', 'event_tags', 'custom_tax_event_tags'),
				'separate_items_with_commas' => _x('Separar los Tags con comas', 'event_tags', 'custom_tax_event_tags'),
				'add_or_remove_items' => _x('Agregar o eliminar Tags', 'event_tags', 'custom_tax_event_tags'),
				'choose_from_most_used' => _x('Elegir entre los Tags más usados', 'event_tags', 'custom_tax_event_tags'),
				'not_found' => _x('No se han encontrado Tags', 'event_tags', 'custom_tax_event_tags')
			),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_admin_column' => false,
			'hierarchical' => false,
			'query_var' => false,
			'rewrite' => array( 'slug' => __('tipo', 'cpt_event_tags'), 'with_front' => true, 'hierarchical' => true ),
			'sort' => true
		)
	);
}
add_action('init', 'registerEventTagsTaxonomy');