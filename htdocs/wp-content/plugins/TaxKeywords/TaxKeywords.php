<?php
/*
Plugin Name: Palabras clave
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: TaxKeywords Taxonomy
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

function registerTaxKeywordsTaxonomy(){
	register_taxonomy(
		'tax_keywords',
		array( 'article' ),
		array(
			'label' => _x('Palabras clave', 'tax_keywords', 'custom_tax_tax_keywords'),
			'labels' => array(
				'name' => _x('Palabras clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'singular_name' => _x('Palabra clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'menu_name' => _x('Palabras clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'all_items' => _x('Palabras clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'edit_item' => _x('Editar Palabra clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'view_item' => _x('Ver Palabra clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'update_item' => _x('Actualizar Palabra clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'add_new_item' => _x('Agregar nueva Palabra clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'new_item_name' => _x('Nuevo nombre de la Palabra clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'parent_item' => _x('Palabra clave padre', 'tax_keywords', 'custom_tax_tax_keywords'),
				'parent_item_colon' => _x('Palabra clave padre:', 'tax_keywords', 'custom_tax_tax_keywords'),
				'search_items' => _x('Buscar Palabras clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'popular_items' => _x('Palabras clave populares', 'tax_keywords', 'custom_tax_tax_keywords'),
				'separate_items_with_commas' => _x('Separar las Palabras clave con comas', 'tax_keywords', 'custom_tax_tax_keywords'),
				'add_or_remove_items' => _x('Agregar o eliminar Palabras clave', 'tax_keywords', 'custom_tax_tax_keywords'),
				'choose_from_most_used' => _x('Elegir entre las Palabras clave más usadas', 'tax_keywords', 'custom_tax_tax_keywords'),
				'not_found' => _x('No se han encontrado Palabras clave', 'tax_keywords', 'custom_tax_tax_keywords')
			),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_admin_column' => false,
			'hierarchical' => false,
			'query_var' => false,
			'rewrite' => array( 'slug' => __('palabras-clave', 'cpt_tax_keywords'), 'with_front' => true, 'hierarchical' => true ),
			'sort' => true
		)
	);
}
add_action('init', 'registerTaxKeywordsTaxonomy');