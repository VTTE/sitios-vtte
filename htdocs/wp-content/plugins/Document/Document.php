<?php
/*
Plugin Name: Documentos
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: 
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class DocumentPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'document';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Documentos', 'document', 'cpt_document'),
			'labels' => array(
				'name' => _x('Documentos', 'document', 'cpt_document'),
				'singular_name' => _x('Documento', 'document', 'cpt_document'),
				'add_new' => _x('Agregar nuevo Documento', 'document', 'cpt_document'),
				'all_items' => _x('Documentos', 'document', 'cpt_document'),
				'add_new_item' => _x('Agregar nuevo Documento', 'document', 'cpt_document'),
				'edit_item' => _x('Editar Documento', 'document', 'cpt_document'),
				'new_item' => _x('Nuevo Documento', 'document', 'cpt_document'),
				'view_item' => _x('Ver Documento', 'document', 'cpt_document'),
				'search_items' => _x('Buscar Documentos', 'document', 'cpt_document'),
				'not_found' => _x('No se han encontrado Documentos', 'document', 'cpt_document'),
				'not_found_in_trash' => _x('No se han encontrado Documentos en la papelera', 'document', 'cpt_document'),
				'parent_item_colon' => _x('Documento padre', 'document', 'cpt_document'),
				'menu_name' => _x('Documentos', 'document', 'cpt_document')
			),
			'description' => _x('', 'document', 'cpt_document'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-media-text',
			'capability_type' => array( 'document', 'documentos' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('documentos', 'cpt_document'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => false
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('DocumentPostType', 'activatePlugin') );
// register post type
add_action('init', array('DocumentPostType', 'registerPostType'));

class DocumentQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'document';
	}
	protected function setDecorator(){
		return 'DocumentObject';
	}
}

class DocumentObject extends Model\PostObject{
	// controller methods
}

class DocumentMeta extends Model\PostMeta{
    protected function setId(){
        return 'document';
    }
    protected function setDataModel(){
        return array(
        	new Model\PostMetaData(
                'author',
                'Autor',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'download',
                'Descargable',
                '\GutenPress\Forms\Element\InputFile',
                array(
                )
            ),
            new Model\PostMetaData(
                'url',
                'Url descargable',
                '\GutenPress\Forms\Element\InputText',
                array(
                    'description' => 'Si se especifica la url, se descarta el archivo descargable de arriba'
                )
            )
        );
    }
}
new Model\Metabox( 'DocumentMeta', 'Información del Documento', 'document', array('priority' => 'high') );