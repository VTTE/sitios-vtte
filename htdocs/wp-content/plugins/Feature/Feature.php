<?php
/*
Plugin Name: UTEM Destacados
Plugin URI: http://vtte.utem.cl
Description: Plugin de destacados de portada Para UTEM
Version: 1.0
Author: hugo.solar
Author URI: http://hugo.solar
*/
use \GutenPress\Model as Model;

class FeaturePostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'utemfeature';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Destacados', 'feature', 'cpt_feature'),
			'labels' => array(
				'name' => _x('Destacados', 'feature', 'cpt_feature'),
				'singular_name' => _x('Destacado', 'feature', 'cpt_feature'),
				'add_new' => _x('Agregar nuevo Destacado', 'feature', 'cpt_feature'),
				'all_items' => _x('Destacados', 'feature', 'cpt_feature'),
				'add_new_item' => _x('Agregar nuevo Destacado', 'feature', 'cpt_feature'),
				'edit_item' => _x('Editar Destacado', 'feature', 'cpt_feature'),
				'new_item' => _x('Nuevo Destacado', 'feature', 'cpt_feature'),
				'view_item' => _x('Ver Destacado', 'feature', 'cpt_feature'),
				'search_items' => _x('Buscar Destacados', 'feature', 'cpt_feature'),
				'not_found' => _x('No se han encontrado Destacados', 'feature', 'cpt_feature'),
				'not_found_in_trash' => _x('No se han encontrado Destacados en la papelera', 'feature', 'cpt_feature'),
				'parent_item_colon' => _x('Destacado padre', 'feature', 'cpt_feature'),
				'menu_name' => _x('Destacados', 'feature', 'cpt_feature')
			),
			'description' => _x('', 'feature', 'cpt_feature'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-format-image',
			'capability_type' => array( 'destacado', 'destacados' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => false,
			'query_var' => true,
			'can_export' => false
		);
	}
	  public static function addThumbnailColumn( $cols ){
        $cols1 = array_slice($cols, 0, 1);
        $cols2 = array_slice($cols, 1);
        $custom_col = array('thumbnail' => 'Foto');
        return $cols1 + $custom_col + $cols2;
    }
    public static function showThumbnailColumn( $col_name, $post_id ){
        if ( $col_name === 'thumbnail' ) {
            echo get_the_post_thumbnail( $post_id, 'landscape-small' );
        }
    }
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('FeaturePostType', 'activatePlugin') );
// register post type
add_action('init', array('FeaturePostType', 'registerPostType'));
add_filter('manage_utemfeature_posts_columns', array('FeaturePostType', 'addThumbnailColumn'));
add_action('manage_utemfeature_posts_custom_column', array('FeaturePostType', 'showThumbnailColumn'), 10, 2);

class FeatureQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'utemfeature';
	}
	protected function setDecorator(){
		return 'FeatureObject';
	}
}

class FeatureObject extends Model\PostObject{
	// controller methods
}

class FeatureMeta extends Model\PostMeta{
    protected function setId(){
        return 'feature';
    }
    protected function setDataModel(){
        return array(
        	
            new Model\PostMetaData(
                'subtitle',
                'Subtitulo',
                '\GutenPress\Forms\Element\InputText'
            ),
            new Model\PostMetaData(
                'button_text',
                'Texto botón',
                '\GutenPress\Forms\Element\InputText'
            ),
            new Model\PostMetaData(
                'button_url',
                'URL Botón',
                '\GutenPress\Forms\Element\InputText'
            )
          );
    }
}
new Model\Metabox( 'FeatureMeta', 'Información del destacado', 'utemfeature', array('priority' => 'high') );