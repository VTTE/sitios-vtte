<?php
/*
Plugin Name: Techonology
Plugin URI: http://vtte.utem.cl
Description: Plugin post type para tecnologias de centros de investigacion
Version: 1
Author: Hugo Solar
Author URI: http://hugo.solar
License: GPL2
*/
use \GutenPress\Model as Model;

class TechnologyPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'technology';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Tecnologías', 'technology', 'cpt_technology'),
			'labels' => array(
				'name' => _x('Tecnologías', 'technology', 'cpt_technology'),
				'singular_name' => _x('Tecnología', 'technology', 'cpt_technology'),
				'add_new' => _x('Agregar nuevo Tecnología', 'technology', 'cpt_technology'),
				'all_items' => _x('Tecnologías', 'technology', 'cpt_technology'),
				'add_new_item' => _x('Agregar nuevo Tecnología', 'technology', 'cpt_technology'),
				'edit_item' => _x('Editar Tecnología', 'technology', 'cpt_technology'),
				'new_item' => _x('Nueva Tecnología', 'technology', 'cpt_technology'),
				'view_item' => _x('Ver Tecnología', 'technology', 'cpt_technology'),
				'search_items' => _x('Buscar Tecnologías', 'technology', 'cpt_technology'),
				'not_found' => _x('No se han encontrado Tecnologías', 'technology', 'cpt_technology'),
				'not_found_in_trash' => _x('No se han encontrado Tecnologías en la papelera', 'technology', 'cpt_technology'),
				'parent_item_colon' => _x('Tecnología padre', 'technology', 'cpt_technology'),
				'menu_name' => _x('Tecnologías', 'technology', 'cpt_technology')
			),
			'description' => _x('', 'technology', 'cpt_technology'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-clipboard',
			'capability_type' => array( 'technology', 'technologies' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('tecnologia', 'cpt_technology'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => true
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('TechnologyPostType', 'activatePlugin') );
// register post type
add_action('init', array('TechnologyPostType', 'registerPostType'));

class TechnologyQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'technology';
	}
	protected function setDecorator(){
		return 'TechnologyObject';
	}
}

class TechnologyObject extends Model\PostObject{
	// controller methods
}

class TechnologyMeta extends Model\PostMeta{
    protected function setId(){
        return 'technology';
    }
    protected function setDataModel(){
        return array(
			new Model\PostMetaData(
				'status',
				'Estado',
				'\GutenPress\Forms\Element\InputText',
				array(
					'placeholder' => 'ej: En ejecución'
				)
			),
			new Model\PostMetaData(
				'gallery',
				'Galería de imagenes',
				'\GutenPress\Forms\Element\WPGallery'
			)

        );
    }
}
new Model\Metabox( 'TechnologyMeta', 'Información del tecnologia', 'technology', array('priority' => 'high') );