<?php
/*
Plugin Name: Programas
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: 
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class ProgramPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'program';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Programas', 'program', 'cpt_program'),
			'labels' => array(
				'name' => _x('Programas', 'program', 'cpt_program'),
				'singular_name' => _x('Programa', 'program', 'cpt_program'),
				'add_new' => _x('Agregar nuevo Programa', 'program', 'cpt_program'),
				'all_items' => _x('Programas', 'program', 'cpt_program'),
				'add_new_item' => _x('Agregar nuevo Programa', 'program', 'cpt_program'),
				'edit_item' => _x('Editar Programa', 'program', 'cpt_program'),
				'new_item' => _x('Nuevo Programa', 'program', 'cpt_program'),
				'view_item' => _x('Ver Programa', 'program', 'cpt_program'),
				'search_items' => _x('Buscar Programas', 'program', 'cpt_program'),
				'not_found' => _x('No se han encontrado Programas', 'program', 'cpt_program'),
				'not_found_in_trash' => _x('No se han encontrado Programas en la papelera', 'program', 'cpt_program'),
				'parent_item_colon' => _x('Programa padre', 'program', 'cpt_program'),
				'menu_name' => _x('Programas', 'program', 'cpt_program')
			),
			'description' => _x('', 'program', 'cpt_program'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-welcome-learn-more',
			'capability_type' => array( 'program', 'programs' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('programas', 'cpt_program'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => false
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('ProgramPostType', 'activatePlugin') );
// register post type
add_action('init', array('ProgramPostType', 'registerPostType'));

class ProgramQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'program';
	}
	protected function setDecorator(){
		return 'ProgramObject';
	}
}

class ProgramObject extends Model\PostObject{
	// controller methods
}

class ProgramMeta extends Model\PostMeta{
    protected function setId(){
        return 'program';
    }
    protected function setDataModel(){
        return array(
			new Model\PostMetaData(
				'short_title',
				'Título corto',
				'\GutenPress\Forms\Element\InputText',
				array()
			),
        	new Model\PostMetaData(
                'init',
                'Fecha inicio',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
        	new Model\PostMetaData(
                'time',
                'Jornada',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'duration',
                'Duración',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'download',
                'Resumen',
                '\GutenPress\Forms\Element\InputFile',
                array(
                )
            ),
            new Model\PostMetaData(
                'featured',
                'Programa destacado',
                '\GutenPress\Forms\Element\InputCheckbox',
                array(
                	'options' => array(
                		'1' => 'Si'
                		),
                    'description' => 'Si es un programa destacado aparecerá en los apartados de "Destacamos"'
                )
			),
			new Model\PostMetaData(
				'apply_url',
				'Link postulación',
				'\GutenPress\Forms\Element\InputText',
				array()
			),
        );
    }
}
new Model\Metabox( 'ProgramMeta', 'Información del Programa', 'program', array('priority' => 'high') );