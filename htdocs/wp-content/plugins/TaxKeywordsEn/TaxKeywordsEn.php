<?php
/*
Plugin Name: Keywords
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: TaxKeywordsEn Taxonomy
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

function registerTaxKeywordsEnTaxonomy(){
	register_taxonomy(
		'tax_keywords_en',
		array( 'article' ),
		array(
			'label' => _x('Keywords', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
			'labels' => array(
				'name' => _x('Keywords', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'singular_name' => _x('Keyword', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'menu_name' => _x('Keywords', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'all_items' => _x('Keywords', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'edit_item' => _x('Editar Keyword', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'view_item' => _x('Ver Keyword', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'update_item' => _x('Actualizar Keyword', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'add_new_item' => _x('Agregar nuevo Keyword', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'new_item_name' => _x('Nuevo nombre del Keyword', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'parent_item' => _x('Keyword padre', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'parent_item_colon' => _x('Keyword padre:', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'search_items' => _x('Buscar Keywords', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'popular_items' => _x('Keywords populares', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'separate_items_with_commas' => _x('Separar los Keywords con comas', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'add_or_remove_items' => _x('Agregar o eliminar Keywords', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'choose_from_most_used' => _x('Elegir entre los Keywords más usados', 'tax_keywords_en', 'custom_tax_tax_keywords_en'),
				'not_found' => _x('No se han encontrado Keywords', 'tax_keywords_en', 'custom_tax_tax_keywords_en')
			),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_admin_column' => false,
			'hierarchical' => false,
			'query_var' => false,
			'rewrite' => array( 'slug' => __('keywords', 'cpt_tax_keywords_en'), 'with_front' => true, 'hierarchical' => true ),
			'sort' => true
		)
	);
}
add_action('init', 'registerTaxKeywordsEnTaxonomy');