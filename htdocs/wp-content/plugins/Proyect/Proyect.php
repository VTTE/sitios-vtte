<?php
/*
Plugin Name: Proyectos
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: 
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class ProyectPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'proyect';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Proyectos', 'proyect', 'cpt_proyect'),
			'labels' => array(
				'name' => _x('Proyectos', 'proyect', 'cpt_proyect'),
				'singular_name' => _x('Proyecto', 'proyect', 'cpt_proyect'),
				'add_new' => _x('Agregar nuevo Proyecto', 'proyect', 'cpt_proyect'),
				'all_items' => _x('Proyectos', 'proyect', 'cpt_proyect'),
				'add_new_item' => _x('Agregar nuevo Proyecto', 'proyect', 'cpt_proyect'),
				'edit_item' => _x('Editar Proyecto', 'proyect', 'cpt_proyect'),
				'new_item' => _x('Nuevo Proyecto', 'proyect', 'cpt_proyect'),
				'view_item' => _x('Ver Proyecto', 'proyect', 'cpt_proyect'),
				'search_items' => _x('Buscar Proyectos', 'proyect', 'cpt_proyect'),
				'not_found' => _x('No se han encontrado Proyectos', 'proyect', 'cpt_proyect'),
				'not_found_in_trash' => _x('No se han encontrado Proyectos en la papelera', 'proyect', 'cpt_proyect'),
				'parent_item_colon' => _x('Proyecto padre', 'proyect', 'cpt_proyect'),
				'menu_name' => _x('Proyectos', 'proyect', 'cpt_proyect')
			),
			'description' => _x('', 'proyect', 'cpt_proyect'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-clipboard',
			'capability_type' => array( 'proyecto', 'proyectos' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('proyectos', 'cpt_proyect'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => true
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('ProyectPostType', 'activatePlugin') );
// register post type
add_action('init', array('ProyectPostType', 'registerPostType'));

class ProyectQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'proyect';
	}
	protected function setDecorator(){
		return 'ProyectObject';
	}
}

class ProyectObject extends Model\PostObject{
	// controller methods
}

class ProyectMeta extends Model\PostMeta{
    protected function setId(){
        return 'program';
    }
    protected function setDataModel(){
        return array(
        	new Model\PostMetaData(
                'career',
                'Carrera',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
				'featured',
				'Es destacado?',
				'\GutenPress\Forms\Element\InputCheckbox',
				array(
					'description' => "Si se marca como destacado, aparecerá en primera posición mas grande en la portada",
					'options' => array(
						'1' => 'Si',
					),
				)
			),
			new Model\PostMetaData(
				'date',
				'Fecha inicio',
				'\GutenPress\Forms\Element\UIDate',
				array(
					'required' => 'required'
				)
			),
			new Model\PostMetaData(
				'date_end',
				'Fecha término',
				'\GutenPress\Forms\Element\UIDate',
				array()
			),
			new Model\PostMetaData(
				'location',
				'Ubicación del proyecto',
				'\GutenPress\Forms\Element\InputText',
				array()
			),
			new Model\PostMetaData(
				'beneficiary',
				'Beneficiario',
				'\GutenPress\Forms\Element\InputText',
				array()
			),
			new Model\PostMetaData(
				'status',
				'Estado',
				'\GutenPress\Forms\Element\InputText',
				array(
					'placeholder' => 'ej: En ejecución'
				)
			),
			new Model\PostMetaData(
				'download',
				'Proyecto descargable',
				'\GutenPress\Forms\Element\InputFile',
				array()
			),
			new Model\PostMetaData(
				'gallery',
				'Galería de imagenes',
				'\GutenPress\Forms\Element\WPGallery'
			)

        );
    }
}
new Model\Metabox( 'ProyectMeta', 'Información del Proyecto', 'proyect', array('priority' => 'high') );