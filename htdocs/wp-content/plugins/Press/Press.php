<?php
/*
Plugin Name: Apariciones en la prensa
Description: Post type de apariciones en la prensa (Editorial)
Version: 1
Author: hugo.solar
Author URI: http://hugo.solar
*/
use \GutenPress\Model as Model;

class PressPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'press';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Apariciones en la prensa', 'press', 'cpt_press'),
			'labels' => array(
				'name' => _x('Apariciones en la prensa', 'press', 'cpt_press'),
				'singular_name' => _x('Aparición en la prensa', 'press', 'cpt_press'),
				'add_new' => _x('Agregar nueva Aparición en la prensa', 'press', 'cpt_press'),
				'all_items' => _x('Apariciones en la prensa', 'press', 'cpt_press'),
				'add_new_item' => _x('Agregar nueva Aparición en la prensa', 'press', 'cpt_press'),
				'edit_item' => _x('Editar Aparición en la prensa', 'press', 'cpt_press'),
				'new_item' => _x('Nueva Aparición en la prensa', 'press', 'cpt_press'),
				'view_item' => _x('Ver Aparición en la prensa', 'press', 'cpt_press'),
				'search_items' => _x('Buscar Apariciones en la prensa', 'press', 'cpt_press'),
				'not_found' => _x('No se han encontrado Apariciones en la prensa', 'press', 'cpt_press'),
				'not_found_in_trash' => _x('No se han encontrado Apariciones en la prensa en la papelera', 'press', 'cpt_press'),
				'parent_item_colon' => _x('Aparición en la prensa padre', 'press', 'cpt_press'),
				'menu_name' => _x('Apariciones en la prensa', 'press', 'cpt_press')
			),
			'description' => _x('', 'press', 'cpt_press'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-media-document',
			'capability_type' => array( 'press', 'press' ),
			'hierarchical' => true,
			'supports' => array( 'title' ),
			'has_archive' => true,
			'rewrite' => false,
			'query_var' => true,
			'can_export' => false
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('PressPostType', 'activatePlugin') );
// register post type
add_action('init', array('PressPostType', 'registerPostType'));

class PressQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'press';
	}
	protected function setDecorator(){
		return 'PressObject';
	}
}

class PressObject extends Model\PostObject{
	// controller methods
}

class PressMeta extends Model\PostMeta{
    protected function setId(){
        return 'press';
    }
    public function get_publications() {
    	$return_publications = array('' => 'Ninguno');
    	$publications = new WP_Query(array(
    		'post_type' => 'books',
    		'posts_per_page' => -1,
    		'post_status' => 'publish'
    		));
    	if ($publications->have_posts()) {
    		foreach ($publications->posts as $loc) {
    			$return_publications[$loc->ID] = $loc->post_title;
    		}
    	}
    	return $return_publications;
    }
    protected function setDataModel(){
        return array(
        	new Model\PostMetaData(
                'medio',
                'Medio',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
        	new Model\PostMetaData(
                'url',
                'Url',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'publication',
                'Asociar a Publicación',
                '\GutenPress\Forms\Element\Select',
                array(
                	'options' => $this->get_publications()
                )
            )
        );
    }
}
new Model\Metabox( 'PressMeta', 'Información de Prensa', 'press', array('priority' => 'high') );