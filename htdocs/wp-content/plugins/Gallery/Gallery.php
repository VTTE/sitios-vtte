<?php
/*
Plugin Name: Galerias
Plugin URI: http://aquipresente.cl
Description: Plugin de galerías de imagen para funcionamiento del sitio (no desactivar)
Version: 1
Author: Hugo Solar
Author URI: http://hugo.solar
*/
use \GutenPress\Model as Model;

class GalleryPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'gallery';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Galerías', 'gallery', 'cpt_gallery'),
			'labels' => array(
				'name' => _x('Galerías', 'gallery', 'cpt_gallery'),
				'singular_name' => _x('Galeria', 'gallery', 'cpt_gallery'),
				'add_new' => _x('Agregar nueva Galeria', 'gallery', 'cpt_gallery'),
				'all_items' => _x('Galerías', 'gallery', 'cpt_gallery'),
				'add_new_item' => _x('Agregar nueva Galeria', 'gallery', 'cpt_gallery'),
				'edit_item' => _x('Editar Galeria', 'gallery', 'cpt_gallery'),
				'new_item' => _x('Nueva Galeria', 'gallery', 'cpt_gallery'),
				'view_item' => _x('Ver Galeria', 'gallery', 'cpt_gallery'),
				'search_items' => _x('Buscar Galerías', 'gallery', 'cpt_gallery'),
				'not_found' => _x('No se han encontrado Galerías', 'gallery', 'cpt_gallery'),
				'not_found_in_trash' => _x('No se han encontrado Galerías en la papelera', 'gallery', 'cpt_gallery'),
				'parent_item_colon' => _x('Galeria padre', 'gallery', 'cpt_gallery'),
				'menu_name' => _x('Galerías', 'gallery', 'cpt_gallery')
			),
			'description' => _x('', 'gallery', 'cpt_gallery'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-format-gallery',
			'capability_type' => array( 'gallery', 'galeries' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('galerias', 'cpt_gallery'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => false
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('GalleryPostType', 'activatePlugin') );
// register post type
add_action('init', array('GalleryPostType', 'registerPostType'));

class GalleryQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'gallery';
	}
	protected function setDecorator(){
		return 'GalleryObject';
	}
}

class GalleryObject extends Model\PostObject{
	// controller methods
}

class GalleryMeta extends Model\PostMeta{
    protected function setId(){
        return 'portafolio';
    }
    protected function setDataModel(){
        return array(
        	new Model\PostMetaData(
                'gallery',
                'Galería de imagenes',
                '\GutenPress\Forms\Element\WPGallery'
            )
          );
    }
}
new Model\Metabox( 'GalleryMeta', 'Definir Galería', 'gallery', array('priority' => 'high') );