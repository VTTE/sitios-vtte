<?php
/*
Plugin Name: Servicios
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: 
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class ServicePostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'service';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Servicios', 'service', 'cpt_service'),
			'labels' => array(
				'name' => _x('Servicios', 'service', 'cpt_service'),
				'singular_name' => _x('Servicio', 'service', 'cpt_service'),
				'add_new' => _x('Agregar nuevo Servicio', 'service', 'cpt_service'),
				'all_items' => _x('Servicios', 'service', 'cpt_service'),
				'add_new_item' => _x('Agregar nuevo Servicio', 'service', 'cpt_service'),
				'edit_item' => _x('Editar Servicio', 'service', 'cpt_service'),
				'new_item' => _x('Nuevo Servicio', 'service', 'cpt_service'),
				'view_item' => _x('Ver Servicio', 'service', 'cpt_service'),
				'search_items' => _x('Buscar Servicios', 'service', 'cpt_service'),
				'not_found' => _x('No se han encontrado Servicios', 'service', 'cpt_service'),
				'not_found_in_trash' => _x('No se han encontrado Servicios en la papelera', 'service', 'cpt_service'),
				'parent_item_colon' => _x('Servicio padre', 'service', 'cpt_service'),
				'menu_name' => _x('Servicios', 'service', 'cpt_service')
			),
			'description' => _x('', 'service', 'cpt_service'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-universal-access-alt',
			'capability_type' => array( 'service', 'services' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('servicios', 'cpt_service'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => false
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('ServicePostType', 'activatePlugin') );
// register post type
add_action('init', array('ServicePostType', 'registerPostType'));

class ServiceQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'service';
	}
	protected function setDecorator(){
		return 'ServiceObject';
	}
}

class ServiceObject extends Model\PostObject{
	// controller methods
}