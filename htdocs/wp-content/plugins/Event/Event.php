<?php
/*
Plugin Name: Actividades
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: 
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class EventPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'event';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Actividades', 'event', 'cpt_event'),
			'labels' => array(
				'name' => _x('Actividades', 'event', 'cpt_event'),
				'singular_name' => _x('Actividad', 'event', 'cpt_event'),
				'add_new' => _x('Agregar nuevo Actividad', 'event', 'cpt_event'),
				'all_items' => _x('Actividades', 'event', 'cpt_event'),
				'add_new_item' => _x('Agregar nuevo Actividad', 'event', 'cpt_event'),
				'edit_item' => _x('Editar Actividad', 'event', 'cpt_event'),
				'new_item' => _x('Nuevo Actividad', 'event', 'cpt_event'),
				'view_item' => _x('Ver Actividad', 'event', 'cpt_event'),
				'search_items' => _x('Buscar Actividades', 'event', 'cpt_event'),
				'not_found' => _x('No se han encontrado Actividades', 'event', 'cpt_event'),
				'not_found_in_trash' => _x('No se han encontrado Actividades en la papelera', 'event', 'cpt_event'),
				'parent_item_colon' => _x('Actividad padre', 'event', 'cpt_event'),
				'menu_name' => _x('Actividades', 'event', 'cpt_event')
			),
			'description' => _x('', 'event', 'cpt_event'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-calendar',
			'capability_type' => array( 'event', 'events' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('actividades', 'cpt_event'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => true
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('EventPostType', 'activatePlugin') );
// register post type
add_action('init', array('EventPostType', 'registerPostType'));

class EventQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'event';
	}
	protected function setDecorator(){
		return 'EventObject';
	}
	public static function filterQuery( $query ){
		if (!is_single()):
			if ( ( $query->is_post_type_archive('event') || ( isset($query->query['post_type']) && $query->query['post_type'] == 'event' ) ) && !is_admin() ) {
				$query->set('meta_query', array( array(
					'key'     => 'event_date',
					'value'   => date('Y-m-d'),
					'compare' => '>=',
					'type'    => 'DATE'
				) ));
				$query->set('meta_key', 'event_date');
				$query->set('orderby', 'meta_value');
				$query->set('order', 'ASC');
			}
		endif;
		return $query;
	}
}
//add_action('pre_get_posts', array('EventQuery', 'filterQuery'));

class EventObject extends Model\PostObject{
	// controller methods
}

class PresentacionMeta extends Model\PostMeta{
    protected function setId(){
        return 'event';
    }
    protected function setDataModel(){
        return array(
			new Model\PostMetaData(
				'status',
				'Estado',
				'\GutenPress\Forms\Element\Select',
				array(
					'options' => array(
						'1' => 'Normal',
						'2' => 'Cancelada'
					)
				)
			),
            new Model\PostMetaData(
                'date',
                'Fecha',
                '\GutenPress\Forms\Element\UIDate',
                array(
                    'required' => 'required'
                )
            ),
            new Model\PostMetaData(
                'date_end',
                'Fecha término',
                '\GutenPress\Forms\Element\UIDate',
                array(
                )
            ),
            new Model\PostMetaData(
                'hour',
                'Hora inicio',
                '\GutenPress\Forms\Element\InputText',
                array(
                    'placeholder' => '11:00 AM'
                )
            ),
            new Model\PostMetaData(
                'hour_end',
                'Hora término',
                '\GutenPress\Forms\Element\InputText',
                array(
                    'placeholder' => '11:00 AM'
                )
            ),
           new Model\PostMetaData(
                'location',
                'Lugar',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
           new Model\PostMetaData(
                'campus',
                'Campus',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
           new Model\PostMetaData(
                'address',
                'Dirección',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
           new Model\PostMetaData(
                'program',
                'Programa',
                '\GutenPress\Forms\Element\InputFile',
                array(
                )
            )
        );
    }
}
new Model\Metabox( 'PresentacionMeta', 'Información de la actividad', 'event',array('priority' => 'high', 'context' => 'side') );