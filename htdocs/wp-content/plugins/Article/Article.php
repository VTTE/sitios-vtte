<?php
/*
Plugin Name: Artículos
Plugin URI: http://vtte.utem.cl
Description: Plugin para activar tipo de contenido Artículos
Version: 1
Author: Hugo Solar
Author URI: http://hugo.solar
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class ArticlePostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'article';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Artículos', 'article', 'cpt_article'),
			'labels' => array(
				'name' => _x('Artículos', 'article', 'cpt_article'),
				'singular_name' => _x('Artículo', 'article', 'cpt_article'),
				'add_new' => _x('Agregar nuevo Artículo', 'article', 'cpt_article'),
				'all_items' => _x('Artículos', 'article', 'cpt_article'),
				'add_new_item' => _x('Agregar nuevo Artículo', 'article', 'cpt_article'),
				'edit_item' => _x('Editar Artículo', 'article', 'cpt_article'),
				'new_item' => _x('Nuevo Artículo', 'article', 'cpt_article'),
				'view_item' => _x('Ver Artículo', 'article', 'cpt_article'),
				'search_items' => _x('Buscar Artículos', 'article', 'cpt_article'),
				'not_found' => _x('No se han encontrado Artículos', 'article', 'cpt_article'),
				'not_found_in_trash' => _x('No se han encontrado Artículos en la papelera', 'article', 'cpt_article'),
				'parent_item_colon' => _x('Artículo padre', 'article', 'cpt_article'),
				'menu_name' => _x('Artículos', 'article', 'cpt_article')
			),
			'description' => _x('', 'article', 'cpt_article'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-format-aside',
			'capability_type' => array( 'article', 'artculos' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('articulos', 'cpt_article'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => false
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('ArticlePostType', 'activatePlugin') );
// register post type
add_action('init', array('ArticlePostType', 'registerPostType'), 11);

class ArticleQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'article';
	}
	protected function setDecorator(){
		return 'ArticleObject';
	}
}

class ArticleObject extends Model\PostObject{
	// controller methods
}

class ArticleMeta extends Model\PostMeta{
   protected function setId(){
        return 'article';
    }
    public function get_person() {
    	$return_person = array('' => 'Ninguno');
    	$person = new WP_Query(array(
    		'post_type' => 'person',
    		'posts_per_page' => -1,
    		'post_status' => 'publish'
    		));
    	if ($person->have_posts()) {
    		foreach ($person->posts as $loc) {
    			$return_person[$loc->ID] = $loc->post_title;
    		}
    	}
    	return $return_person;
    }
    protected function setDataModel(){
        return array(
            new Model\PostMetaData(
                'not_article',
                'es un artículo?',
                '\GutenPress\Forms\Element\InputCheckbox',
                array(
                    'description' => 'Si se marca esta opción esta entrada no aparecerá en la portada en la seccion de articulos',
                    'options' => array(
                        '1' => 'No es un articulo',
                    ),
                )
                ),
            new Model\PostMetaData(
                'person',
                'Asociar autor(es)',
                '\GutenPress\Forms\Element\SelectMultiple',
                array(
                	'options' => $this->get_person()
                )
            ),
            new Model\PostMetaData(
                'download',
                'Descarga articulo',
                '\GutenPress\Forms\Element\InputFile',
                array(
                	
                )
            ),
            new Model\PostMetaData(
                'only_download',
                'Sólo descarga',
                '\GutenPress\Forms\Element\InputCheckbox',
                array(
                    'description' => 'Si se marca esta opción, el articulo sólo tendrá el enlace al PDF',
                    'options' => array(
                        '1' => 'Sí',
                    ),
                )
            )
          );
    }
}
new Model\Metabox( 'ArticleMeta', 'Asociar autor', 'article', array('priority' => 'high') );

class DublinCoreMeta extends Model\PostMeta{
   protected function setId(){
        return 'dublin';
    }
    protected function setDataModel(){
    	global $post;
        return array(
        	new Model\PostMetaData(
                'title',
                'Título',
                '\GutenPress\Forms\Element\InputText',
                array(
                	'value' => get_the_title($post->ID),
                	'readonly' => 'readonly'
                )
            ),
            new Model\PostMetaData(
                'alt_title',
                'Título Alternativo',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'author_email',
                'Email de Autor',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'keyword_es',
                'Palabra clave español',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'keyword_en',
                'Palabra clave inglés',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'abstract_es',
                'Resumen Español',
                '\GutenPress\Forms\Element\WPEditor',
                array(
                	'media_buttons' => false,
                	'textarea_rows' => 7
                )
            ),
            new Model\PostMetaData(
                'abstract_en',
                'Resumen Ingles',
                '\GutenPress\Forms\Element\WPEditor',
                array(
                	'media_buttons' => false,
                	'textarea_rows' => 7
                )
            ),
            new Model\PostMetaData(
                'date_created',
                'Fecha Aceptación',
                '\GutenPress\Forms\Element\UIDate',
                array(
                )
            ),
            new Model\PostMetaData(
                'date_issued',
                'Fecha Recibido',
                '\GutenPress\Forms\Element\UIDate',
                array(
                )
            ),
            new Model\PostMetaData(
                'editor_name',
                'Nombre editor Corporativo',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'content_type',
                'Tipo',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'format',
                'Formato',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'url',
                'Url Artículo',
                '\GutenPress\Forms\Element\InputText',
                array(
                	'value' => get_permalink($post->ID),
                	'readonly' => 'readonly'
                )
            ),
            new Model\PostMetaData(
                'language',
                'Lenguaje',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'edition',
                'Edición',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'magazine_issn',
                'ISSN de la revista',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'edition_number',
                'Número de la edición',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'edition_volume',
                'Volumen de la edición',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'magazine_url',
                'URL de la revista',
                '\GutenPress\Forms\Element\InputText',
                array(
                	'value' => get_bloginfo('url'),
                	'readonly' => 'readonly'
                )
            ),
            new Model\PostMetaData(
                'license',
                'Licencia',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'article_citation',
                'Citación de artículo',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'title_citation',
                'Citación de título',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'author_citation',
                'Citación de autor',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'year_citation',
                'Citación año de publicación',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'magazine_citation',
                'Citación nombre de la revista',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'issn_citation',
                'Citación de ISSN',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'volume_citation',
                'Citación de volumen',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'number_citation',
                'Citación de Número',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'initial_page_citation',
                'Citación página inicial',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'last_page_citation',
                'Citación última página',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'pdf_url_citation',
                'Citación PDF Url',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'orcid',
                'ORCID',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
             new Model\PostMetaData(
                'doi',
                'DOI',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            )
          );
    }
}
new Model\Metabox( 'DublinCoreMeta', 'Cabeceras Dublin Core y Google Academic', 'article', array('priority' => 'high') );