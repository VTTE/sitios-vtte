<?php
/*
Plugin Name: Puntos de venta
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: 
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class PointsPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'points';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Puntos de venta', 'points', 'cpt_points'),
			'labels' => array(
				'name' => _x('Puntos de venta', 'points', 'cpt_points'),
				'singular_name' => _x('Punto de venta', 'points', 'cpt_points'),
				'add_new' => _x('Agregar nuevo Punto de venta', 'points', 'cpt_points'),
				'all_items' => _x('Puntos de venta', 'points', 'cpt_points'),
				'add_new_item' => _x('Agregar nuevo Punto de venta', 'points', 'cpt_points'),
				'edit_item' => _x('Editar Punto de venta', 'points', 'cpt_points'),
				'new_item' => _x('Nuevo Punto de venta', 'points', 'cpt_points'),
				'view_item' => _x('Ver Punto de venta', 'points', 'cpt_points'),
				'search_items' => _x('Buscar Puntos de venta', 'points', 'cpt_points'),
				'not_found' => _x('No se han encontrado Puntos de venta', 'points', 'cpt_points'),
				'not_found_in_trash' => _x('No se han encontrado Puntos de venta en la papelera', 'points', 'cpt_points'),
				'parent_item_colon' => _x('Punto de venta padre', 'points', 'cpt_points'),
				'menu_name' => _x('Puntos de venta', 'points', 'cpt_points')
			),
			'description' => _x('', 'points', 'cpt_points'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-location-alt',
			'capability_type' => array( 'point', 'points' ),
			'hierarchical' => true,
			'supports' => array( 'title' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('puntos-de-venta', 'cpt_points'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => true
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('PointsPostType', 'activatePlugin') );
// register post type
add_action('init', array('PointsPostType', 'registerPostType'));

class PointsQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'points';
	}
	protected function setDecorator(){
		return 'PointsObject';
	}
}

class PointsObject extends Model\PostObject{
	// controller methods
}

class PointsMeta extends Model\PostMeta{
    protected function setId(){
        return 'point';
    }
    protected function setDataModel(){
        return array(
        	new Model\PostMetaData(
                'address',
                'Dirección',
                '\GutenPress\Forms\Element\InputText',
                array(
                    'required' => 'required'
                )
            ),
            new Model\PostMetaData(
                'phone',
                'Teléfono',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'city',
                'Ciudad',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
			new Model\PostMetaData(
	                'map',
	                'Mapa',
	                '\GutenPress\Forms\Element\Textarea',
	                array(
	                	'cols' => 40,
	                	'description' => 'Pegar el embed del mapa con tamaño de 370x200'
	                	)
	            ),
		);
    }
}
new Model\Metabox( 'PointsMeta', 'Información de puntos de venta', 'points', array('priority' => 'high') );