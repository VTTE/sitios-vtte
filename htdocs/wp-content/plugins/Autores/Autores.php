<?php
/*
Plugin Name: VTTE Editorial Autores(as)
Plugin URI: http://editorial.utem.cl
Description: Taxonomia de autores
Version: 1
Author: Hugo Solar
Author URI: http://hugo.solar
License: A "Slug" license name e.g. GPL2
 */
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;

function registerAutoresTaxonomy()
{
    register_taxonomy(
        'tax_autores',
        array('books'),
        array(
            'label' => _x('Autores(as)', 'autores', 'custom_tax_autores'),
            'labels' => array(
                'name' => _x('Autores(as)', 'autores', 'custom_tax_autores'),
                'singular_name' => _x('Autor(a)', 'autores', 'custom_tax_autores'),
                'menu_name' => _x('Autores(as)', 'autores', 'custom_tax_autores'),
                'all_items' => _x('Autores(as)', 'autores', 'custom_tax_autores'),
                'edit_item' => _x('Editar Autor(a)', 'autores', 'custom_tax_autores'),
                'view_item' => _x('Ver Autor(a)', 'autores', 'custom_tax_autores'),
                'update_item' => _x('Actualizar Autor(a)', 'autores', 'custom_tax_autores'),
                'add_new_item' => _x('Agregar nuevo Autor(a)', 'autores', 'custom_tax_autores'),
                'new_item_name' => _x('Nuevo nombre del Autor(a)', 'autores', 'custom_tax_autores'),
                'parent_item' => _x('Autor(a) padre', 'autores', 'custom_tax_autores'),
                'parent_item_colon' => _x('Autor(a) padre:', 'autores', 'custom_tax_autores'),
                'search_items' => _x('Buscar Autores(as)', 'autores', 'custom_tax_autores'),
                'popular_items' => _x('Autores(as) populares', 'autores', 'custom_tax_autores'),
                'separate_items_with_commas' => _x('Separar los Autores(as) con comas', 'autores', 'custom_tax_autores'),
                'add_or_remove_items' => _x('Agregar o eliminar Autores(as)', 'autores', 'custom_tax_autores'),
                'choose_from_most_used' => _x('Elegir entre los Autores(as) más usados', 'autores', 'custom_tax_autores'),
                'not_found' => _x('No se han encontrado Autores(as)', 'autores', 'custom_tax_autores')
            ),
            'public' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => false,
            'rewrite' => array('slug' => __('autores', 'cpt_autores'), 'with_front' => true, 'hierarchical' => true),
            'sort' => true
        )
    );
    register_taxonomy(
        'tax_directoras',
        array('books'),
        array(
            'label' => _x('Directoras(os)', 'directoras', 'custom_tax_directoras'),
            'labels' => array(
                'name' => _x('Directoras(os)', 'directoras', 'custom_tax_directoras'),
                'singular_name' => _x('Directora(o)', 'directoras', 'custom_tax_directoras'),
                'menu_name' => _x('Directoras(os)', 'directoras', 'custom_tax_directoras'),
                'all_items' => _x('Directoras(os)', 'directoras', 'custom_tax_directoras'),
                'edit_item' => _x('Editar Directora(o)', 'directoras', 'custom_tax_directoras'),
                'view_item' => _x('Ver Directora(o)', 'directoras', 'custom_tax_directoras'),
                'update_item' => _x('Actualizar Directora(o)', 'directoras', 'custom_tax_directoras'),
                'add_new_item' => _x('Agregar nuevo Directora(o)', 'directoras', 'custom_tax_directoras'),
                'new_item_name' => _x('Nuevo nombre del Directora(o)', 'directoras', 'custom_tax_directoras'),
                'parent_item' => _x('Directora(o) padre', 'directoras', 'custom_tax_directoras'),
                'parent_item_colon' => _x('Directora(o) padre:', 'directoras', 'custom_tax_directoras'),
                'search_items' => _x('Buscar Directoras(os)', 'directoras', 'custom_tax_directoras'),
                'popular_items' => _x('Directoras(os) populares', 'directoras', 'custom_tax_directoras'),
                'separate_items_with_commas' => _x('Separar los Directoras(os) con comas', 'directoras', 'custom_tax_directoras'),
                'add_or_remove_items' => _x('Agregar o eliminar Directoras(os)', 'directoras', 'custom_tax_directoras'),
                'choose_from_most_used' => _x('Elegir entre los Directoras(os) más usados', 'directoras', 'custom_tax_directoras'),
                'not_found' => _x('No se han encontrado Directoras(os)', 'directoras', 'custom_tax_directoras')
            ),
            'public' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => false,
            'rewrite' => array('slug' => __('directoras', 'cpt_directoras'), 'with_front' => true, 'hierarchical' => true),
            'sort' => true
        )
    );
    register_taxonomy(
        'tax_coordinadoras',
        array('books'),
        array(
            'label' => _x('Coordinadora(os)', 'coordinadoras', 'custom_tax_coordinadoras'),
            'labels' => array(
                'name' => _x('Coordinadora(os)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'singular_name' => _x('Coordinadora(o)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'menu_name' => _x('Coordinadora(os)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'all_items' => _x('Coordinadora(os)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'edit_item' => _x('Editar Coordinadora(o)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'view_item' => _x('Ver Coordinadora(o)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'update_item' => _x('Actualizar Coordinadora(o)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'add_new_item' => _x('Agregar nuevo Coordinadora(o)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'new_item_name' => _x('Nuevo nombre del Coordinadora(o)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'parent_item' => _x('Coordinadora(o) padre', 'coordinadoras', 'custom_tax_coordinadoras'),
                'parent_item_colon' => _x('Coordinadora(o) padre:', 'coordinadoras', 'custom_tax_coordinadoras'),
                'search_items' => _x('Buscar Coordinadora(os)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'popular_items' => _x('Coordinadora(os) populares', 'coordinadoras', 'custom_tax_coordinadoras'),
                'separate_items_with_commas' => _x('Separar los Coordinadora(os) con comas', 'coordinadoras', 'custom_tax_coordinadoras'),
                'add_or_remove_items' => _x('Agregar o eliminar Coordinadora(os)', 'coordinadoras', 'custom_tax_coordinadoras'),
                'choose_from_most_used' => _x('Elegir entre los Coordinadora(os) más usados', 'coordinadoras', 'custom_tax_coordinadoras'),
                'not_found' => _x('No se han encontrado Coordinadora(os)', 'coordinadoras', 'custom_tax_coordinadoras')
            ),
            'public' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => false,
            'rewrite' => array('slug' => __('coordinadoras', 'cpt_coordinadoras'), 'with_front' => true, 'hierarchical' => true),
            'sort' => true
        )
    );
    register_taxonomy(
        'tax_redactoras',
        array('books'),
        array(
            'label' => _x('Redactoras(es)', 'redactoras', 'custom_tax_redactoras'),
            'labels' => array(
                'name' => _x('Redactoras(es)', 'redactoras', 'custom_tax_redactoras'),
                'singular_name' => _x('Redactora(o)', 'redactoras', 'custom_tax_redactoras'),
                'menu_name' => _x('Redactoras(es)', 'redactoras', 'custom_tax_redactoras'),
                'all_items' => _x('Redactoras(es)', 'redactoras', 'custom_tax_redactoras'),
                'edit_item' => _x('Editar Redactora(o)', 'redactoras', 'custom_tax_redactoras'),
                'view_item' => _x('Ver Redactora(o)', 'redactoras', 'custom_tax_redactoras'),
                'update_item' => _x('Actualizar Redactora(o)', 'redactoras', 'custom_tax_redactoras'),
                'add_new_item' => _x('Agregar nuevo Redactora(o)', 'redactoras', 'custom_tax_redactoras'),
                'new_item_name' => _x('Nuevo nombre del Redactora(o)', 'redactoras', 'custom_tax_redactoras'),
                'parent_item' => _x('Redactora(o) padre', 'redactoras', 'custom_tax_redactoras'),
                'parent_item_colon' => _x('Redactora(o) padre:', 'redactoras', 'custom_tax_redactoras'),
                'search_items' => _x('Buscar Redactoras(es)', 'redactoras', 'custom_tax_redactoras'),
                'popular_items' => _x('Redactoras(es) populares', 'redactoras', 'custom_tax_redactoras'),
                'separate_items_with_commas' => _x('Separar los Redactoras(es) con comas', 'redactoras', 'custom_tax_redactoras'),
                'add_or_remove_items' => _x('Agregar o eliminar Redactoras(es)', 'redactoras', 'custom_tax_redactoras'),
                'choose_from_most_used' => _x('Elegir entre los Redactoras(es) más usados', 'redactoras', 'custom_tax_redactoras'),
                'not_found' => _x('No se han encontrado Redactoras(es)', 'redactoras', 'custom_tax_redactoras')
            ),
            'public' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => false,
            'rewrite' => array('slug' => __('redactoras', 'cpt_redactoras'), 'with_front' => true, 'hierarchical' => true),
            'sort' => true
        )
    );
    register_taxonomy(
        'tax_editoras',
        array('books'),
        array(
            'label' => _x('Editoras(es)', 'editoras', 'custom_tax_editoras'),
            'labels' => array(
                'name' => _x('Editoras(es)', 'editoras', 'custom_tax_editoras'),
                'singular_name' => _x('Editora(o)', 'editoras', 'custom_tax_editoras'),
                'menu_name' => _x('Editoras(es)', 'editoras', 'custom_tax_editoras'),
                'all_items' => _x('Editoras(es)', 'editoras', 'custom_tax_editoras'),
                'edit_item' => _x('Editar Editora(o)', 'editoras', 'custom_tax_editoras'),
                'view_item' => _x('Ver Editora(o)', 'editoras', 'custom_tax_editoras'),
                'update_item' => _x('Actualizar Editora(o)', 'editoras', 'custom_tax_editoras'),
                'add_new_item' => _x('Agregar nuevo Editora(o)', 'editoras', 'custom_tax_editoras'),
                'new_item_name' => _x('Nuevo nombre del Editora(o)', 'editoras', 'custom_tax_editoras'),
                'parent_item' => _x('Editora(o) padre', 'editoras', 'custom_tax_editoras'),
                'parent_item_colon' => _x('Editora(o) padre:', 'editoras', 'custom_tax_editoras'),
                'search_items' => _x('Buscar Editoras(es)', 'editoras', 'custom_tax_editoras'),
                'popular_items' => _x('Editoras(es) populares', 'editoras', 'custom_tax_editoras'),
                'separate_items_with_commas' => _x('Separar los Editoras(es) con comas', 'editoras', 'custom_tax_editoras'),
                'add_or_remove_items' => _x('Agregar o eliminar Editoras(es)', 'editoras', 'custom_tax_editoras'),
                'choose_from_most_used' => _x('Elegir entre los Editoras(es) más usados', 'editoras', 'custom_tax_editoras'),
                'not_found' => _x('No se han encontrado Editoras(es)', 'editoras', 'custom_tax_editoras')
            ),
            'public' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => false,
            'rewrite' => array('slug' => __('editoras', 'cpt_editoras'), 'with_front' => true, 'hierarchical' => true),
            'sort' => true
        )
    );
    register_taxonomy(
        'tax_organizadoras',
        array('books'),
        array(
            'label' => _x('Organizadoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
            'labels' => array(
                'name' => _x('Organizadoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'singular_name' => _x('Organizadora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'menu_name' => _x('Organizadoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'all_items' => _x('Organizadoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'edit_item' => _x('Editar Organizadora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'view_item' => _x('Ver Organizadora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'update_item' => _x('Actualizar Organizadora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'add_new_item' => _x('Agregar nuevo Organizadora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'new_item_name' => _x('Nuevo nombre del Organizadora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'parent_item' => _x('Organizadora(o) padre', 'organizadoras', 'custom_tax_organizadoras'),
                'parent_item_colon' => _x('Organizadora(o) padre:', 'organizadoras', 'custom_tax_organizadoras'),
                'search_items' => _x('Buscar Organizadoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'popular_items' => _x('Organizadoras(es) populares', 'organizadoras', 'custom_tax_organizadoras'),
                'separate_items_with_commas' => _x('Separar los Organizadoras(es) con comas', 'organizadoras', 'custom_tax_organizadoras'),
                'add_or_remove_items' => _x('Agregar o eliminar Organizadoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'choose_from_most_used' => _x('Elegir entre los Organizadoras(es) más usados', 'organizadoras', 'custom_tax_organizadoras'),
                'not_found' => _x('No se han encontrado Organizadoras(es)', 'organizadoras', 'custom_tax_organizadoras')
            ),
            'public' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => false,
            'rewrite' => array('slug' => __('organizadoras', 'cpt_organizadoras'), 'with_front' => true, 'hierarchical' => true),
            'sort' => true
        )
    );
    register_taxonomy(
        'tax_compiladoras',
        array('books'),
        array(
            'label' => _x('Compiladoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
            'labels' => array(
                'name' => _x('Compiladoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'singular_name' => _x('Compiladora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'menu_name' => _x('Compiladoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'all_items' => _x('Compiladoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'edit_item' => _x('Editar Compiladora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'view_item' => _x('Ver Compiladora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'update_item' => _x('Actualizar Compiladora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'add_new_item' => _x('Agregar nuevo Compiladora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'new_item_name' => _x('Nuevo nombre del Compiladora(o)', 'organizadoras', 'custom_tax_organizadoras'),
                'parent_item' => _x('Compiladora(o) padre', 'organizadoras', 'custom_tax_organizadoras'),
                'parent_item_colon' => _x('Compiladora(o) padre:', 'organizadoras', 'custom_tax_organizadoras'),
                'search_items' => _x('Buscar Compiladoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'popular_items' => _x('Compiladoras(es) populares', 'organizadoras', 'custom_tax_organizadoras'),
                'separate_items_with_commas' => _x('Separar los Compiladoras(es) con comas', 'organizadoras', 'custom_tax_organizadoras'),
                'add_or_remove_items' => _x('Agregar o eliminar Compiladoras(es)', 'organizadoras', 'custom_tax_organizadoras'),
                'choose_from_most_used' => _x('Elegir entre los Compiladoras(es) más usados', 'organizadoras', 'custom_tax_organizadoras'),
                'not_found' => _x('No se han encontrado Compiladoras(es)', 'organizadoras', 'custom_tax_organizadoras')
            ),
            'public' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => false,
            'rewrite' => array('slug' => __('compiladoras', 'cpt_compiladoras'), 'with_front' => true, 'hierarchical' => true),
            'sort' => true
        )
    );
}
add_action('init', 'registerAutoresTaxonomy');
/*
	Add meta data to taxonomies
	Edition details like name, image, year and other things
 */
add_action('admin_enqueue_scripts', 'tax_admin_enqueue');

function tax_admin_enqueue()
{
    global $pagenow;
    if ((($pagenow == 'edit-tags.php') || ($pagenow == 'term.php')) && ($_GET['taxonomy'] == 'tax_autores')) {
        wp_enqueue_media();
    }
}
/** TAX AUTORES */
add_action('tax_autores_edit_form_fields', 'add_edition_group_field', 10, 2);
function add_edition_group_field($term, $taxonomy)
{
    $tax_image = get_term_meta($term->term_id, 'tax_image', true);
    $form = new Forms\MetaboxForm('edition-settings');
    $form->addElement(new Element\WPImage(
        _x('Imagen de Autor', 'tax settings fields', 'vtte'),
        'tax_image',
        array(
            'value' => isset($tax_image) ? $tax_image : ''
        )
    ));
    echo $form;
}
add_action('edited_tax_autores', 'save_tax_autores', 10, 2);

function save_tax_autores($term_id, $tt_id)
{
    $fields = array(
        'tax_image'
    );
    foreach ($fields as $field) {
        if (isset($_POST[$field])) {
            update_term_meta($term_id, $field, $_POST[$field]);
        } else {
            update_term_meta($term_id, $field, null);
        }
    }
}

/**TAX DIRECTORAS */
add_action('tax_directoras_edit_form_fields', 'add_edition_group_field', 10, 2);
add_action('edited_tax_directoras', 'save_tax_autores', 10, 2);

add_action('tax_coordinadoras_edit_form_fields', 'add_edition_group_field', 10, 2);
add_action('edited_tax_coordinadoras', 'save_tax_autores', 10, 2);

add_action('tax_redactoras_edit_form_fields', 'add_edition_group_field', 10, 2);
add_action('edited_tax_redactoras', 'save_tax_autores', 10, 2);

add_action('tax_editoras_edit_form_fields', 'add_edition_group_field', 10, 2);
add_action('edited_tax_editoras', 'save_tax_autores', 10, 2);

add_action('tax_organizadoras_edit_form_fields', 'add_edition_group_field', 10, 2);
add_action('edited_tax_organizadoras', 'save_tax_autores', 10, 2);

add_action('tax_compiladoras_edit_form_fields', 'add_edition_group_field', 10, 2);
add_action('edited_tax_compiladoras', 'save_tax_autores', 10, 2);
