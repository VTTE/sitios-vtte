<?php
/*
Plugin Name: Temáticas
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: TaxTopics Taxonomy
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

function registerTaxTopicsTaxonomy(){
	register_taxonomy(
		'tax_topics',
		array( 'books' ),
		array(
			'label' => _x('Temáticas', 'tax_topics', 'custom_tax_tax_topics'),
			'labels' => array(
				'name' => _x('Temáticas', 'tax_topics', 'custom_tax_tax_topics'),
				'singular_name' => _x('Temática', 'tax_topics', 'custom_tax_tax_topics'),
				'menu_name' => _x('Temáticas', 'tax_topics', 'custom_tax_tax_topics'),
				'all_items' => _x('Temáticas', 'tax_topics', 'custom_tax_tax_topics'),
				'edit_item' => _x('Editar Temática', 'tax_topics', 'custom_tax_tax_topics'),
				'view_item' => _x('Ver Temática', 'tax_topics', 'custom_tax_tax_topics'),
				'update_item' => _x('Actualizar Temática', 'tax_topics', 'custom_tax_tax_topics'),
				'add_new_item' => _x('Agregar nuevo Temática', 'tax_topics', 'custom_tax_tax_topics'),
				'new_item_name' => _x('Nuevo nombre del Temática', 'tax_topics', 'custom_tax_tax_topics'),
				'parent_item' => _x('Temática padre', 'tax_topics', 'custom_tax_tax_topics'),
				'parent_item_colon' => _x('Temática padre:', 'tax_topics', 'custom_tax_tax_topics'),
				'search_items' => _x('Buscar Temáticas', 'tax_topics', 'custom_tax_tax_topics'),
				'popular_items' => _x('Temáticas populares', 'tax_topics', 'custom_tax_tax_topics'),
				'separate_items_with_commas' => _x('Separar los Temáticas con comas', 'tax_topics', 'custom_tax_tax_topics'),
				'add_or_remove_items' => _x('Agregar o eliminar Temáticas', 'tax_topics', 'custom_tax_tax_topics'),
				'choose_from_most_used' => _x('Elegir entre los Temáticas más usados', 'tax_topics', 'custom_tax_tax_topics'),
				'not_found' => _x('No se han encontrado Temáticas', 'tax_topics', 'custom_tax_tax_topics')
			),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_admin_column' => false,
			'hierarchical' => true,
			'query_var' => false,
			'rewrite' => array( 'slug' => __('tematica', 'cpt_tax_topics'), 'with_front' => true, 'hierarchical' => true ),
			'sort' => true
		)
	);
}
add_action('init', 'registerTaxtopicsTaxonomy');