<?php
/*
Plugin Name: Sellos
Description: Sellos Taxonomy
Version: 1
Author: Hugo Solar
Author URI: http://hugo.solar
License: A "Slug" license name e.g. GPL2
*/

function registerSellosTaxonomy(){
	register_taxonomy(
		'sellos',
		array( 'post', 'event' ),
		array(
			'label' => _x('Sellos', 'sellos', 'custom_tax_sellos'),
			'labels' => array(
				'name' => _x('Sellos', 'sellos', 'custom_tax_sellos'),
				'singular_name' => _x('Sello', 'sellos', 'custom_tax_sellos'),
				'menu_name' => _x('Sellos', 'sellos', 'custom_tax_sellos'),
				'all_items' => _x('Sellos', 'sellos', 'custom_tax_sellos'),
				'edit_item' => _x('Editar Sello', 'sellos', 'custom_tax_sellos'),
				'view_item' => _x('Ver Sello', 'sellos', 'custom_tax_sellos'),
				'update_item' => _x('Actualizar Sello', 'sellos', 'custom_tax_sellos'),
				'add_new_item' => _x('Agregar nuevo Sello', 'sellos', 'custom_tax_sellos'),
				'new_item_name' => _x('Nuevo nombre del Sello', 'sellos', 'custom_tax_sellos'),
				'parent_item' => _x('Sello padre', 'sellos', 'custom_tax_sellos'),
				'parent_item_colon' => _x('Sello padre:', 'sellos', 'custom_tax_sellos'),
				'search_items' => _x('Buscar Sellos', 'sellos', 'custom_tax_sellos'),
				'popular_items' => _x('Sellos populares', 'sellos', 'custom_tax_sellos'),
				'separate_items_with_commas' => _x('Separar los Sellos con comas', 'sellos', 'custom_tax_sellos'),
				'add_or_remove_items' => _x('Agregar o eliminar Sellos', 'sellos', 'custom_tax_sellos'),
				'choose_from_most_used' => _x('Elegir entre los Sellos más usados', 'sellos', 'custom_tax_sellos'),
				'not_found' => _x('No se han encontrado Sellos', 'sellos', 'custom_tax_sellos')
			),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_admin_column' => true,
			'hierarchical' => true,
			'query_var' => false,
			'rewrite' => array( 'slug' => __('sellos', 'cpt_sellos'), 'with_front' => true, 'hierarchical' => true ),
			'sort' => true
		)
	);
}
add_action('init', 'registerSellosTaxonomy');