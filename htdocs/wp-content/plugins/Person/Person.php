<?php
/*
Plugin Name: Personas
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: 
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class PersonPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'person';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Personas', 'person', 'cpt_person'),
			'labels' => array(
				'name' => _x('Personas', 'person', 'cpt_person'),
				'singular_name' => _x('Persona', 'person', 'cpt_person'),
				'add_new' => _x('Agregar nueva Persona', 'person', 'cpt_person'),
				'all_items' => _x('Personas', 'person', 'cpt_person'),
				'add_new_item' => _x('Agregar nueva Persona', 'person', 'cpt_person'),
				'edit_item' => _x('Editar Persona', 'person', 'cpt_person'),
				'new_item' => _x('Nueva Persona', 'person', 'cpt_person'),
				'view_item' => _x('Ver Persona', 'person', 'cpt_person'),
				'search_items' => _x('Buscar Personas', 'person', 'cpt_person'),
				'not_found' => _x('No se han encontrado Personas', 'person', 'cpt_person'),
				'not_found_in_trash' => _x('No se han encontrado Personas en la papelera', 'person', 'cpt_person'),
				'parent_item_colon' => _x('Persona padre', 'person', 'cpt_person'),
				'menu_name' => _x('Personas', 'person', 'cpt_person')
			),
			'description' => _x('', 'person', 'cpt_person'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-admin-users',
			'capability_type' => array( 'person', 'persons' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
			'has_archive' => false,
			'rewrite' => false,
			'query_var' => true,
			'can_export' => true
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('PersonPostType', 'activatePlugin') );
// register post type
add_action('init', array('PersonPostType', 'registerPostType'));

class PersonQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'person';
	}
	protected function setDecorator(){
		return 'PersonObject';
	}
}

class PersonObject extends Model\PostObject{
	// controller methods
}

class PersonMeta extends Model\PostMeta{
    protected function setId(){
        return 'person';
    }
    protected function setDataModel(){
        return array(
            new Model\PostMetaData(
                'grade',
                'Grado académico',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'mail',
                'Email',
                '\GutenPress\Forms\Element\InputEmail',
                array(
                )
            )
        );
    }
}
new Model\Metabox( 'PersonMeta', 'Información de la persona', 'person', array('priority' => 'high') );