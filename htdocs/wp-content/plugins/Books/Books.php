<?php
/*
Plugin Name: Libros
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: 
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class BooksPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'books';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Libros', 'books', 'cpt_books'),
			'labels' => array(
				'name' => _x('Libros', 'books', 'cpt_books'),
				'singular_name' => _x('Libro', 'books', 'cpt_books'),
				'add_new' => _x('Agregar nuevo Libro', 'books', 'cpt_books'),
				'all_items' => _x('Libros', 'books', 'cpt_books'),
				'add_new_item' => _x('Agregar nuevo Libro', 'books', 'cpt_books'),
				'edit_item' => _x('Editar Libro', 'books', 'cpt_books'),
				'new_item' => _x('Nuevo Libro', 'books', 'cpt_books'),
				'view_item' => _x('Ver Libro', 'books', 'cpt_books'),
				'search_items' => _x('Buscar Libros', 'books', 'cpt_books'),
				'not_found' => _x('No se han encontrado Libros', 'books', 'cpt_books'),
				'not_found_in_trash' => _x('No se han encontrado Libros en la papelera', 'books', 'cpt_books'),
				'parent_item_colon' => _x('Libro padre', 'books', 'cpt_books'),
				'menu_name' => _x('Libros', 'books', 'cpt_books')
			),
			'description' => _x('', 'books', 'cpt_books'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-book-alt',
			'capability_type' => array( 'book', 'books' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('publicaciones', 'cpt_books'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => true
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('BooksPostType', 'activatePlugin') );
// register post type
add_action('init', array('BooksPostType', 'registerPostType'));

class BooksQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'books';
	}
	protected function setDecorator(){
		return 'BooksObject';
	}
}

class BooksObject extends Model\PostObject{
	// controller methods
}

class UploadMeta extends Model\PostMeta{
    protected function setId(){
        return 'upload';
    }
    protected function setDataModel(){
        return array(
        	new Model\PostMetaData(
                'pdf',
                'Subir PDF',
                '\GutenPress\Forms\Element\InputFile',
                array(
                )
            ),
            new Model\PostMetaData(
                'url',
                'Escribir Url',
                '\GutenPress\Forms\Element\InputText',
                array(
                	'description' => 'Se utiliza en caso de que el pdf se encuentre en otra ubicación o pueda haber sido subido por otra vía al servidor. En caso de escribir la URL se sobreescribirá el PDF que se subió'
                )
            )
        );
	}
}
new Model\Metabox( 'UploadMeta', 'Versión digital', 'books', array('priority' => 'high') );
class PublicationMeta extends Model\PostMeta{
    protected function setId(){
        return 'book';
    }
    protected function setDataModel(){
        return array(
			new Model\PostMetaData(
				'author_type',
				'Tipo de Autoría',
				'\GutenPress\Forms\Element\Select',
				array(
					'options' => array(
						'' => 'Seleccionar',
						'Autor(es)' => 'Autor(es)',
						'Editor(es)' => 'Editor(es)',
						'Compilador(es)' => 'Compilador(es)',
						'Director(es)' => 'Director(es)',
						'Coordinador(es)' => 'Coordinador(es)'
					)
				)
			),
        	new Model\PostMetaData(
                'isbn',
                'ISBN',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'edition_date',
                'Fecha de Edición',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'coedition',
                'Coedición',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
			new Model\PostMetaData(
	                'author',
	                'Autor(es)',
	                '\GutenPress\Forms\Element\InputTextMultiple'
	            ),
			new Model\PostMetaData(
	                'editor',
	                'Editor(es)',
	                '\GutenPress\Forms\Element\InputTextMultiple'
	            ),
			new Model\PostMetaData(
	                'compiler',
	                'Compilador(es)',
	                '\GutenPress\Forms\Element\InputTextMultiple'
	            ),
			new Model\PostMetaData(
	                'director',
	                'Director(es)',
	                '\GutenPress\Forms\Element\InputTextMultiple'
	            ),
			new Model\PostMetaData(
	                'coordinator',
	                'Coordinador(es)',
	                '\GutenPress\Forms\Element\InputTextMultiple'
	            ),
			new Model\PostMetaData(
	                'pages',
	                'Número de páginas',
	                '\GutenPress\Forms\Element\InputText'
	            ),
			new Model\PostMetaData(
	                'size',
	                'Tamaño edición',
	                '\GutenPress\Forms\Element\InputText'
	            ),
			new Model\PostMetaData(
	                'weight',
	                'Peso edición',
	                '\GutenPress\Forms\Element\InputText'
				),
			new Model\PostMetaData(
	                'gallery',
	                'Galería',
	                '\GutenPress\Forms\Element\WPGallery'
	            )	
		);
    }
}
new Model\Metabox( 'PublicationMeta', 'Información de la publicación', 'books', array('priority' => 'high') );

class MagazineMeta extends Model\PostMeta{
    protected function setId(){
        return 'magazine';
    }
    protected function setDataModel(){
        return array(
        	new Model\PostMetaData(
                'issnd',
                'ISSN (Digital)',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
            ),
            new Model\PostMetaData(
                'issni',
                'ISSN (Impresa)',
                '\GutenPress\Forms\Element\InputText',
                array(
                )
			),
			new Model\PostMetaData(
				'issnl',
				'ISSN-L',
				'\GutenPress\Forms\Element\InputText',
				array()
			),
			new Model\PostMetaData(
	                'director',
	                'Director(a)',
	                '\GutenPress\Forms\Element\InputText'
	            ),
			new Model\PostMetaData(
	                'editor_boss',
	                'Jefe(a) Editor(a)',
	                '\GutenPress\Forms\Element\InputText'
	            ),
			
			new Model\PostMetaData(
	                'period',
	                'Periodicidad',
	                '\GutenPress\Forms\Element\InputText'
	            ),
			new Model\PostMetaData(
	                'web',
	                'Sitio Web',
	                '\GutenPress\Forms\Element\InputText'
	            )
		);
    }
}
new Model\Metabox( 'MagazineMeta', 'Información de Revista', 'books', array('priority' => 'high') );