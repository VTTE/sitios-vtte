<?php
/*
Plugin Name: Tipos Programa
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: ProgramType Taxonomy
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

function registerProgramTypeTaxonomy(){
	register_taxonomy(
		'program_type',
		array( 'program' ),
		array(
			'label' => _x('Tipos Programa', 'program_type', 'custom_tax_program_type'),
			'labels' => array(
				'name' => _x('Tipos Programa', 'program_type', 'custom_tax_program_type'),
				'singular_name' => _x('Tipo Programa', 'program_type', 'custom_tax_program_type'),
				'menu_name' => _x('Tipos Programa', 'program_type', 'custom_tax_program_type'),
				'all_items' => _x('Tipos Programa', 'program_type', 'custom_tax_program_type'),
				'edit_item' => _x('Editar Tipo Programa', 'program_type', 'custom_tax_program_type'),
				'view_item' => _x('Ver Tipo Programa', 'program_type', 'custom_tax_program_type'),
				'update_item' => _x('Actualizar Tipo Programa', 'program_type', 'custom_tax_program_type'),
				'add_new_item' => _x('Agregar nuevo Tipo Programa', 'program_type', 'custom_tax_program_type'),
				'new_item_name' => _x('Nuevo nombre del Tipo Programa', 'program_type', 'custom_tax_program_type'),
				'parent_item' => _x('Tipo Programa padre', 'program_type', 'custom_tax_program_type'),
				'parent_item_colon' => _x('Tipo Programa padre:', 'program_type', 'custom_tax_program_type'),
				'search_items' => _x('Buscar Tipos Programa', 'program_type', 'custom_tax_program_type'),
				'popular_items' => _x('Tipos Programa populares', 'program_type', 'custom_tax_program_type'),
				'separate_items_with_commas' => _x('Separar los Tipos Programa con comas', 'program_type', 'custom_tax_program_type'),
				'add_or_remove_items' => _x('Agregar o eliminar Tipos Programa', 'program_type', 'custom_tax_program_type'),
				'choose_from_most_used' => _x('Elegir entre los Tipos Programa más usados', 'program_type', 'custom_tax_program_type'),
				'not_found' => _x('No se han encontrado Tipos Programa', 'program_type', 'custom_tax_program_type')
			),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_admin_column' => false,
			'hierarchical' => true,
			'query_var' => false,
			'rewrite' => array( 'slug' => __('tipo-programa', 'cpt_program_type'), 'with_front' => true, 'hierarchical' => true ),
			'sort' => true
		)
	);
}
add_action('init', 'registerProgramTypeTaxonomy');