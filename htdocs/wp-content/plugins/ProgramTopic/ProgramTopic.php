<?php
/*
Plugin Name: Áreas temáticas
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: ProgramTopic Taxonomy
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;

function registerProgramTopicTaxonomy(){
	register_taxonomy(
		'program_topic',
		array( 'program' ),
		array(
			'label' => _x('Áreas temáticas', 'program_topic', 'custom_tax_program_topic'),
			'labels' => array(
				'name' => _x('Áreas temáticas', 'program_topic', 'custom_tax_program_topic'),
				'singular_name' => _x('Área temática', 'program_topic', 'custom_tax_program_topic'),
				'menu_name' => _x('Áreas temáticas', 'program_topic', 'custom_tax_program_topic'),
				'all_items' => _x('Áreas temáticas', 'program_topic', 'custom_tax_program_topic'),
				'edit_item' => _x('Editar Área temática', 'program_topic', 'custom_tax_program_topic'),
				'view_item' => _x('Ver Área temática', 'program_topic', 'custom_tax_program_topic'),
				'update_item' => _x('Actualizar Área temática', 'program_topic', 'custom_tax_program_topic'),
				'add_new_item' => _x('Agregar nuevo Área temática', 'program_topic', 'custom_tax_program_topic'),
				'new_item_name' => _x('Nuevo nombre del Área temática', 'program_topic', 'custom_tax_program_topic'),
				'parent_item' => _x('Área temática padre', 'program_topic', 'custom_tax_program_topic'),
				'parent_item_colon' => _x('Área temática padre:', 'program_topic', 'custom_tax_program_topic'),
				'search_items' => _x('Buscar Áreas temáticas', 'program_topic', 'custom_tax_program_topic'),
				'popular_items' => _x('Áreas temáticas populares', 'program_topic', 'custom_tax_program_topic'),
				'separate_items_with_commas' => _x('Separar los Áreas temáticas con comas', 'program_topic', 'custom_tax_program_topic'),
				'add_or_remove_items' => _x('Agregar o eliminar Áreas temáticas', 'program_topic', 'custom_tax_program_topic'),
				'choose_from_most_used' => _x('Elegir entre los Áreas temáticas más usados', 'program_topic', 'custom_tax_program_topic'),
				'not_found' => _x('No se han encontrado Áreas temáticas', 'program_topic', 'custom_tax_program_topic')
			),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_admin_column' => false,
			'hierarchical' => true,
			'query_var' => false,
			'rewrite' => array( 'slug' => __('tematicas', 'cpt_program_topic'), 'with_front' => true, 'hierarchical' => true ),
			'sort' => true
		)
	);
}
add_action('init', 'registerProgramTopicTaxonomy');
add_action('admin_enqueue_scripts', 'tax_topic_admin_enqueue');

function tax_topic_admin_enqueue()
{
	global $pagenow;
	if (($pagenow == 'edit-tags.php') && ($_GET['taxonomy'] == 'program_topic')) {
		wp_enqueue_media();
	}
}
/*
	Add meta data to taxonomies
	Edition details like name, image, year and other things
 */

add_action('program_topic_edit_form_fields', 'add_program_group_field', 10, 2);
function add_program_group_field($term, $taxonomy)
{
	// $tm = get_term_meta($term->term_id);
	// echo '<pre>'; print_r($tm); echo '</pre>';
	$tax_image = get_term_meta($term->term_id, 'tax_image', true);

	$form = new Forms\MetaboxForm('edition-settings');
	$form->addElement(new Element\WPImage(
		_x('Imagen / Ícono de area', 'tax settings fields', 'vtte'),
		'tax_image',
		array(
			'value' => isset($tax_image) ? $tax_image : ''
		)
	));
	
	echo $form;
}

add_action('edited_program_topic', 'save_program_topic', 10, 2);

function save_program_topic($term_id, $tt_id)
{
	$fields = array(
		'tax_image'
	);
// echo '<pre>'; print_r($_POST); echo '</pre>';
// die();
	foreach ($fields as $field) {
		if (isset($_POST[$field])) {
			update_term_meta($term_id, $field, $_POST[$field]);
		} else {
			update_term_meta($term_id, $field, null);
		}
	}
}