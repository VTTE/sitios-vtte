<?php
/*
Plugin Name: Ediciones para artículos
Plugin URI: http://vtte.utem.cl
Description: Taxonomía de Ediciones para artículos de revistas
Version: 1
Author: Hugo Solar
Author URI: http://hugo.solar
License: A "Slug" license name e.g. GPL2
*/
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;

function registerTaxEditionTaxonomy(){
	register_taxonomy(
		'tax_edition',
		array( 'article' ),
		array(
			'label' => _x('Ediciones', 'tax_edition', 'custom_tax_tax_edition'),
			'labels' => array(
				'name' => _x('Ediciones', 'tax_edition', 'custom_tax_tax_edition'),
				'singular_name' => _x('Edición', 'tax_edition', 'custom_tax_tax_edition'),
				'menu_name' => _x('Ediciones', 'tax_edition', 'custom_tax_tax_edition'),
				'all_items' => _x('Ediciones', 'tax_edition', 'custom_tax_tax_edition'),
				'edit_item' => _x('Editar Edición', 'tax_edition', 'custom_tax_tax_edition'),
				'view_item' => _x('Ver Edición', 'tax_edition', 'custom_tax_tax_edition'),
				'update_item' => _x('Actualizar Edición', 'tax_edition', 'custom_tax_tax_edition'),
				'add_new_item' => _x('Agregar nuevo Edición', 'tax_edition', 'custom_tax_tax_edition'),
				'new_item_name' => _x('Nuevo nombre del Edición', 'tax_edition', 'custom_tax_tax_edition'),
				'parent_item' => _x('Edición padre', 'tax_edition', 'custom_tax_tax_edition'),
				'parent_item_colon' => _x('Edición padre:', 'tax_edition', 'custom_tax_tax_edition'),
				'search_items' => _x('Buscar Ediciones', 'tax_edition', 'custom_tax_tax_edition'),
				'popular_items' => _x('Ediciones populares', 'tax_edition', 'custom_tax_tax_edition'),
				'separate_items_with_commas' => _x('Separar los Ediciones con comas', 'tax_edition', 'custom_tax_tax_edition'),
				'add_or_remove_items' => _x('Agregar o eliminar Ediciones', 'tax_edition', 'custom_tax_tax_edition'),
				'choose_from_most_used' => _x('Elegir entre los Ediciones más usados', 'tax_edition', 'custom_tax_tax_edition'),
				'not_found' => _x('No se han encontrado Ediciones', 'tax_edition', 'custom_tax_tax_edition')
			),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_admin_column' => false,
			'hierarchical' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => __('ediciones', 'cpt_tax_edition'), 'with_front' => true, 'hierarchical' => true ),
			'sort' => true
		)
	);
	flush_rewrite_rules();
}
add_action('init', 'registerTaxEditionTaxonomy', 10);
add_action('admin_enqueue_scripts','tax_admin_enqueue');

function tax_admin_enqueue() {
	 global $pagenow;
	 if ( ($pagenow == 'edit-tags.php') && ( $_GET['taxonomy'] == 'tax_edition' ) ) {
	 	wp_enqueue_media();
	 }
}
/*
	Add meta data to taxonomies
	Edition details like name, image, year and other things
*/

add_action( 'tax_edition_edit_form_fields', 'add_edition_group_field', 10, 2 );
function add_edition_group_field($term, $taxonomy) {
	// $tm = get_term_meta($term->term_id);
	// echo '<pre>'; print_r($tm); echo '</pre>';
	$tax_image = get_term_meta( $term->term_id, 'tax_image', true );
	$tax_volume = get_term_meta( $term->term_id, 'volume_number', true );
	$tax_number = get_term_meta( $term->term_id, 'number', true );
	$tax_year = get_term_meta( $term->term_id, 'year', true );
	$tax_digital_issn = get_term_meta( $term->term_id, 'digital_issn', true );
	$tax_print_issn = get_term_meta( $term->term_id, 'print_issn', true );
	$tax_download_edition = get_term_meta( $term->term_id, 'download_edition', true );
	$tax_only_download = get_term_meta( $term->term_id, 'only_download', true );
	$tax_faculty = get_term_meta( $term->term_id, 'faculty', true );
	$tax_order = get_term_meta( $term->term_id, 'order', true);

	$form = new Forms\MetaboxForm('edition-settings');
	$form->addElement( new Element\WPImage(
		_x('Imagen de edición', 'tax settings fields', 'vtte'),
		'tax_image',
		array(
			'value' => isset($tax_image) ? $tax_image : ''
		)
	) )->addElement( new Element\InputNumber(
		_x('Nº Volumen', 'tax settings fields', 'vtte'),
		'volume_number',
		array(
			'value' => isset($tax_volume) ? $tax_volume : ''
		)
	) )->addElement( new Element\InputNumber(
		_x('Número', 'tax settings fields', 'vtte'),
		'number',
		array(
			'value' => isset($tax_number) ? $tax_number : ''
		)
	) )->addElement( new Element\InputNumber(
		_x('Año', 'tax settings fields', 'vtte'),
		'year',
		array(
			'value' => isset($tax_year) ? $tax_year : ''
		)
	) )->addElement( new Element\InputText(
		_x('ISSN Digital', 'tax settings fields', 'vtte'),
		'digital_issn',
		array(
			'value' => isset($tax_digital_issn) ? $tax_digital_issn : ''
		)
	) )->addElement( new Element\InputText(
		_x('ISSN Impreso', 'tax settings fields', 'vtte'),
		'print_issn',
		array(
			'value' => isset($tax_print_issn) ? $tax_print_issn : ''
		)
	) )->addElement( new Element\WPFile(
		_x('Descarga edición', 'site settings fields', 'vtte'),
		'download_edition',
		array(
			'value' => isset($tax_download_edition) ? $tax_download_edition : ''
		)
	) )->addElement( new Element\InputCheckbox(
		_x('Sólo descarga', 'site settings fields', 'vtte'),
		'only_download',
		array( '1' => 'Sí' ),
		array(
			'value' => isset($tax_only_download) ? $tax_only_download : '',
		)
	) )->addElement( new Element\InputText(
		_x('Facultad de la edición', 'tax settings fields', 'vtte'),
		'faculty',
		array(
			'value' => isset($tax_faculty) ? $tax_faculty : ''
		)
	) );
	$elements = new ArticleQuery(array(
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'tax_edition',
					'field' => 'id',
					'terms' => $term->term_id
					)
				)
		));
	$elements_list = array();
	foreach ( $elements as $element ) {
		$elements_list[$element->ID] = $element->post_title;
	}
	$form->addElement( new Element\Sortable(
		_x('Ordenar Artículos', 'tax settings fields', 'vtte'),
		'order',
		$elements_list,
		array(
            'value' => isset($tax_order) ? $tax_order : null
        	)
		) );
	echo $form;
}

add_action( 'edited_tax_edition', 'save_tax_edition', 10, 2 );
   
function save_tax_edition( $term_id, $tt_id ){
	$fields = array(
		'tax_image',
		'volume_number',
		'number',
		'year',
		'digital_issn',
		'print_issn',
		'download_edition',
		'only_download',
		'faculty',
		'order'
		);
// echo '<pre>'; print_r($_POST); echo '</pre>';
// die();
	foreach ($fields as $field) {
		if ( isset( $_POST[$field] ) ) {
			update_term_meta( $term_id, $field, $_POST[$field] );
		} else {
			update_term_meta( $term_id, $field, null );
		}
	}
}