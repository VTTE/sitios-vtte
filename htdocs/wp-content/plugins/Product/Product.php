<?php
/*
Plugin Name: Productos
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: 
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/
use \GutenPress\Model as Model;

class ProductPostType extends \GutenPress\Model\PostType{
	/**
	 * Set post_type value
	 * @return string
	 */
	protected function setPostType(){
		return 'product';
	}

	/**
	 * Set post type object properties
	 * @return array
	 */
	protected function setPostTypeObject(){
		return array(
			'label' => _x('Productos', 'product', 'cpt_product'),
			'labels' => array(
				'name' => _x('Productos', 'product', 'cpt_product'),
				'singular_name' => _x('Producto', 'product', 'cpt_product'),
				'add_new' => _x('Agregar nuevo Producto', 'product', 'cpt_product'),
				'all_items' => _x('Productos', 'product', 'cpt_product'),
				'add_new_item' => _x('Agregar nuevo Producto', 'product', 'cpt_product'),
				'edit_item' => _x('Editar Producto', 'product', 'cpt_product'),
				'new_item' => _x('Nuevo Producto', 'product', 'cpt_product'),
				'view_item' => _x('Ver Producto', 'product', 'cpt_product'),
				'search_items' => _x('Buscar Productos', 'product', 'cpt_product'),
				'not_found' => _x('No se han encontrado Productos', 'product', 'cpt_product'),
				'not_found_in_trash' => _x('No se han encontrado Productos en la papelera', 'product', 'cpt_product'),
				'parent_item_colon' => _x('Producto padre', 'product', 'cpt_product'),
				'menu_name' => _x('Productos', 'product', 'cpt_product')
			),
			'description' => _x('', 'product', 'cpt_product'),
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'menu_position' => null,
			'menu_icon' => 'dashicons-products',
			'capability_type' => array( 'product', 'products' ),
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive' => true,
			'rewrite' => array( 'slug' => __('productos', 'cpt_product'), 'with_front' => true, 'feeds' => true, 'pages' => true ),
			'query_var' => true,
			'can_export' => true
		);
	}
}

// register plugin activation hook: add capabilities for admin users
register_activation_hook( __FILE__, array('ProductPostType', 'activatePlugin') );
// register post type
add_action('init', array('ProductPostType', 'registerPostType'));

class ProductQuery extends Model\PostQuery{
	protected function setPostType(){
		return 'product';
	}
	protected function setDecorator(){
		return 'ProductObject';
	}
}

class ProductObject extends Model\PostObject{
	// controller methods
}

class ProductMeta extends Model\PostMeta {
	protected function setId() {
		return 'product';
	}

	protected function setDataModel() {
		return array(
			new Model\PostMetaData(
				'gallery',
				'Galería de producto',
				'\GutenPress\Forms\Element\WPGallery'
			)
		);
	}
}

new Model\Metabox( 'ProductMeta', 'Información de producto', 'product', array( 'priority' => 'high' ) );