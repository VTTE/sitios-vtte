<?php
/*
Plugin Name: Categorización
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: Categorization Taxonomy
Version: 1
Author: Name Of The Plugin Author
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

function registerCategorizationTaxonomy(){
	register_taxonomy(
		'categorization',
		array( 'product', 'service' ),
		array(
			'label' => _x('Categorización', 'categorization', 'custom_tax_categorization'),
			'labels' => array(
				'name' => _x('Categorización', 'categorization', 'custom_tax_categorization'),
				'singular_name' => _x('Categorización', 'categorization', 'custom_tax_categorization'),
				'menu_name' => _x('Categorización', 'categorization', 'custom_tax_categorization'),
				'all_items' => _x('Categorización', 'categorization', 'custom_tax_categorization'),
				'edit_item' => _x('Editar Categorización', 'categorization', 'custom_tax_categorization'),
				'view_item' => _x('Ver Categorización', 'categorization', 'custom_tax_categorization'),
				'update_item' => _x('Actualizar Categorización', 'categorization', 'custom_tax_categorization'),
				'add_new_item' => _x('Agregar nuevo Categorización', 'categorization', 'custom_tax_categorization'),
				'new_item_name' => _x('Nuevo nombre del Categorización', 'categorization', 'custom_tax_categorization'),
				'parent_item' => _x('Categorización padre', 'categorization', 'custom_tax_categorization'),
				'parent_item_colon' => _x('Categorización padre:', 'categorization', 'custom_tax_categorization'),
				'search_items' => _x('Buscar Categorización', 'categorization', 'custom_tax_categorization'),
				'popular_items' => _x('Categorización populares', 'categorization', 'custom_tax_categorization'),
				'separate_items_with_commas' => _x('Separar los Categorización con comas', 'categorization', 'custom_tax_categorization'),
				'add_or_remove_items' => _x('Agregar o eliminar Categorización', 'categorization', 'custom_tax_categorization'),
				'choose_from_most_used' => _x('Elegir entre los Categorización más usados', 'categorization', 'custom_tax_categorization'),
				'not_found' => _x('No se han encontrado Categorización', 'categorization', 'custom_tax_categorization')
			),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'show_admin_column' => false,
			'hierarchical' => true,
			'query_var' => false,
			'rewrite' => array( 'slug' => __('categorizacion', 'cpt_categorization'), 'with_front' => true, 'hierarchical' => true ),
			'sort' => true
		)
	);
}
add_action('init', 'registerCategorizationTaxonomy');