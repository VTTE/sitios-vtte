<?php 
	get_header(); 
	the_post();
	global $post;
	$term = get_queried_object();
?>
<div class="main-content">
	<div class="row">
		<div class="large-9 columns">
			<h3 class="secondary-title"><?php $term->name; ?></h3>
			<?php edition::list_edition($term); ?>
		</div>
		<div class="large-3 columns">
			<aside class="sidebar">
				<?php echo modules::last_magazine($term); ?>
				<?php dynamic_sidebar( 'edition' ); ?>
			</aside>
		</div>
	</div>
</div>
<?php get_footer(); ?>