<?php
get_header();
the_post();
global $post;
?>
<div class="main-content">
	<div class="row">
		<div class="large-10 columns">
			<header class="article-header hentry">
				<div class="epigraph">
					<?php $edition = revista::edition($post->ID); ?>
					<span class="secondary-title">Artículo</span> <?php echo $edition->name; ?>
				</div>
				<h2 class="entry-title"><?php the_title() ?></h2>
				<div class="author-info">
					<?php
					$author_list = revista::get_author_list($post->ID);
					$author_names =  array();
					if (!empty($author_list)) {
						echo '<h4 class="mini-title">Autor(es)</h4>';
						foreach ($author_list as $author) {
							$author_names[] = $author->post_title;
						}
						echo '<span class="author">' . implode(', ', $author_names) . '</span>';
					}
					?>
				</div>
				<div class="header-meta clearfix">
					<div class="buttons float-left">
						<?php
						$file_id = get_post_meta($post->ID, 'article_download', true);

						try {
							$file = new \GutenPress\Helpers\Attachment($file_id);
						} catch (Exception $e) {
							echo '<!-- Attachment: no existe -->';
						}

						if (!empty($file)) {
							$url_file = $file->url;
						} else {
							$url_file = '#';
						}
						$extension = (!empty($file->pathinfo->extension)) ? $file->pathinfo->extension : 'PDF';
						?>
						<?php if (!empty($author_list)) : ?>
							<a href="#author-info-meta" class="button small gray author-button">Sobre los autores</a>
						<?php endif; ?>
						<?php if (!empty($file_id)) : ?>
							<?php $nonce = wp_create_nonce('track-download-article'); ?>
							<a href="<?php echo $url_file ?>" class="button small gray download-article" download="<?php echo sanitize_title_with_dashes(get_the_title($post->ID)) . '.' . $file->pathinfo->extension ?>" onclick="trackDownload(<?php echo $file_id . ',' . get_current_blog_id() . ',\'' . $nonce . '\',\'' . $url_file . '\'' ?>);ga('send', 'event', 'Articulo', 'Descarga', '<?php echo get_the_title($post->ID) ?>', '<?php echo $post->ID ?>');">Descargar <?php echo $extension; ?></a>
						<?php endif; ?>
						<?php $xml_url = add_query_arg('vista', 'xml', get_permalink($post->ID)) ?>
						<a href="<?php echo $xml_url ?>" class="button small gray download-article" onclick="ga('send', 'event', 'Articulo', 'XML', '<?php echo get_the_title($post->ID) ?>', '<?php echo $post->ID ?>');">Versión XML</a>
					</div>
					<div class="social float-right">
						<a href="#enviar_articulo" class="send_by_mail_button has-tip hide-for-small-only" data-tooltip data-toggle="send_by_mail" title="Enviar por email"><span class="dashicons dashicons-email"></span></a>
						<?php get_template_part('inc/partials/share', 'buttons') ?>
						<div class="shortlink row collapse">
							<div class="columns large-9 small-9">
								<input type="text" id="shortlink-text" readonly class="shortlink-text" value="<?php echo wp_get_shortlink($post->ID); ?>">
								<span class="clipboard-status"></span>
							</div>
							<div class="columns large-3 small-3">
								<a href="#copy_link" data-clipboard-target="#shortlink-text" data-tooltip class="has-tip shortlink-button button tiny" title="Copiar al portapapeles"><span class="dashicons dashicons-admin-page"></span></a>

							</div>
						</div>
					</div>
				</div>
			</header>
		</div>
		<div class="large-2 columns">
			<div class="widget editions">
				<a href="<?php echo $edition->link ?>">
					<h3 class="widget-title">Ver todos los artículos</h3>
					<div class="widget-content">
						<p><?php echo $edition->name; ?></p>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="row inner-space" id="sitck-local-nav">
		<div class="large-2 columns" data-sticky-container>
			<div class="local-navigation sticky" data-sticky data-anchor="sitck-local-nav" data-options="marginTop:5">
				<h4>Secciones</h4>
				<ul class="local-nav menu vertical">
					<!-- Links added by javascript -->
				</ul>
			</div>
		</div>
		<div class="large-7 columns">
			<div id="author-info-meta" class="author-info-meta">
				<h4 class="primary-title">Sobre los autores</h4>
				<a href="#close" class="close-button"><span class="dashicons dashicons-no"></span></a>
				<?php
				if (!empty($author_list)) {
					foreach ($author_list as $author) {
						echo '<article class="entry-author hentry">';
						echo '<h5 class="entry-title">' . $author->post_title . '</h5>';
						echo apply_filters('the_content', $author->post_content);
						echo '<div class="meta-author">';
						$mail = get_post_meta($author->ID, 'person_mail', true);
						if (!empty($mail)) {
							echo '<span class="dashicons dashicons-email-alt"></span> <span class="mail"><a href="mailto:' . $mail . '">' . $mail . '</a></span>';
						}
						echo '</div>';
						echo '</article>';
					}
				}
				?>
			</div>
			<div class="std-text" id="content-navigation">
				<?php
				global $_set;
				$settings = $_set->settings;
				if ($settings['tts_active']) :
					?>
					<div class="button-container">
						<span class="container-text">Leer el artículo</span>
						<button class="button play-tts"><span class="dashicons dashicons-controls-play"></span></button>
					</div>
				<?php endif; ?>
				<div class="article-content">
					<?php the_content(); ?>
				</div>
				<div class="comments">
					<div id="disqus_thread"></div>
					<script>
						var disqus_config = function() {
							this.page.url = '<?php echo get_permalink($post->ID); ?>';
							this.page.identifier = '<?php echo $post->post_name . '-' . $post->ID ?>';
						};

						(function() { // DON'T EDIT BELOW THIS LINE
							var d = document,
								s = d.createElement('script');
							s.src = 'https://revistas-academicas.disqus.com/embed.js';
							s.setAttribute('data-timestamp', +new Date());
							(d.head || d.body).appendChild(s);
						})();
					</script>
					<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

				</div>
			</div>
		</div>
		<div class="large-3 columns">
			<aside class="sidebar">
				<?php dynamic_sidebar('article'); ?>
				<?php if (!empty($post->dublin_doi) || !empty($post->dublin_orcid)) : ?>
					<div class="widget term-info">
						<dl class="term-list">
							<?php if (!empty($post->dublin_orcid)) : ?>
								<dt class="term">ORCID</dt>
								<dd class="definition"><?php echo esc_attr($post->dublin_orcid); ?></dd>
							<?php endif; ?>
							<?php if (!empty($post->dublin_doi)) : ?>
								<dt class="term">DOI</dt>
								<dd class="definition"><?php echo esc_attr($post->dublin_doi); ?></dd>
							<?php endif; ?>
						</dl>
					</div>
				<?php endif; ?>
				<div class="widget keywords">
					<h4 class="mini-title widget-title">
						Palabras Clave
					</h4>
					<div class="widget-content">
						<?php echo revista::get_keywords($post->ID) ?>
					</div>
					<h4 class="mini-title widget-title">
						Keywords
					</h4>
					<div class="widget-content">
						<?php echo revista::get_keywords($post->ID, 'tax_keywords_en') ?>
					</div>
				</div>

			</aside>
		</div>
	</div>
</div>
<div class="reveal" id="send_by_mail" data-reveal>
	<h3>Enviar a un/a amigo(a)</h3>
	<div class="info-article">
		<small><strong>Artículo:</strong></small>
		<h6><?php echo get_the_title($post->ID) ?></h6>
	</div>
	<?php
	echo mRender::form($post->ID);
	?>
	<button class="close-button" data-close aria-label="Close reveal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<?php get_footer(); ?>