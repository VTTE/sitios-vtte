 /*
    String to slug
    http://dense13.com/blog/2009/05/03/converting-string-to-slug-javascript/
*/
function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();
  
  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to   = "aaaaeeeeiiiioooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
}

function trackDownload(id,site_id,nonce_data) {
	var storageValue = 'revista-utem-'+site_id+'-article-'+id;
	var hasValue = localStorage.getItem(storageValue);
	if (!hasValue) {
		var request = new XMLHttpRequest();
		request.open('POST', Ajax.url, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');
		request.onload = function () {
			if (this.status >= 200 && this.status < 400) {
				localStorage.setItem(storageValue,true);
			} else {
				console.log(this.response);
			}
		};
		request.onerror = function () {
			console.log('connection error');
		};
		request.send('action=track_download&attachment_id=' + id +'&nonce_data='+nonce_data );
	}
}

jQuery(document).on("forminvalid.zf.abide", function(ev,frm) {
    return false;
  }).on("formvalid.zf.abide", function(ev,frm) {
  	var name = frm.find('.name-input').val(),
  		mail = frm.find('.mail-input').val(),
  		comments = frm.find('.textarea-input').val(),
  		lastname = frm.find('.lastname-input').val(),
  		recipient = frm.find('.recipient-input').val(),
  		post_id = frm.find('.post-input').val(),
  		sec = frm.find('#send_by_mail_nonce').val();
  	jQuery.ajax({
  		url: Ajax.url,
  		type: 'POST',
  		data: {
  			action: 'send_by_mail',
  			name: name,
  			mail: mail,
  			comments: comments,
  			recipient: recipient,
  			post_id: post_id,
  			lastname: lastname,
  			sec: sec
  		},
  		beforeSend: function(){
  			jQuery('.send-form').val('ENVIANDO...');
  		},
  		success: function(data){
  			if ((data == 'mail error') || (data == 'error')) {
  				jQuery('.error-message').show();
  			} else if (data == 1) {
          jQuery('.send-form').val('Formulario enviado');  
  				jQuery('.send-form').attr('disabled','disabled');
  				jQuery('.success-message').show();
  			}
  		}
  	});
  	console.log();
    return false;
  });

(function($){
	//function by @basilio
	$.fn.av_heights = function(){
		tallest = 0;
		$(this).each(function(){
			if ( $(this).height() > tallest ) { tallest = $(this).outerHeight(); };
		})
		$(this).css({'height': tallest});
	}
	$.fn.smoothTop = function(){
		$(this).click(function(){
	    	$("html, body").animate({ scrollTop: 0 }, 600);
	    		return false;
	    });
		
	}
	$.fn.slideTarget = function() {
		$(this).on('click', function(e){
			e.preventDefault();
			var obj = $(this);
			var target = $(obj.attr('href'));

			if (target.length && (target.is(':visible'))) {

				obj.removeClass('active');
				target.slideUp('fast');
				target.removeClass('active');
			} else if (!target.is(':visible')) {
				target.removeClass('hide');
				obj.addClass('active');
				target.slideDown('fast');
				target.addClass('active');
			}
			return false;
		});
	}
	
	

})(jQuery);
jQuery(document).ready(function($){
	$('.author-button').slideTarget();
	$('#author-info-meta').hide();
	$('.author-info-meta').find('.close-button').on('click',function(){
		var obj = $(this);
		obj.parent().slideUp('fast');
		obj.parent().removeClass('active');
		$('.buttons').find('.author-button').removeClass('active');
	});
	$('.send_by_mail').on('submit',function(e) {
		e.preventDefault();
		return false;
	});
	$('.send_by_mail_button').on('click', function(e){
		$('#send_by_mail').find('.name-input').val('');
		$('#send_by_mail').find('.mail-input').val('');
		$('#send_by_mail').find('.textarea-input').val('');
		$('#send_by_mail').find('.recipient-input').val('');
		$('#send_by_mail').find('.error-message').hide();
		$('#send_by_mail').find('.send-form').val('Enviar');
		$('#send_by_mail').find('.send-form').attr('enabled','enabled');
		//return false;
	});
	$('.single-article .std-text').find('h4').each(function(){
        var obj = $(this);
        
        var slug = string_to_slug(obj.text());

        $('<a name="'+slug+'"></a>').insertBefore(obj);
        //obj.attr('id',slug);
        var h6 = $('<h6>').addClass('xs').append(obj.text());
        var link = $('<a>').attr('href','#'+slug).append(h6);
        var li = $('<li>').addClass("local-item").append(link);
        $('.local-nav').append(li);
    });
	// $('.shortlink-button').on('click', function(e){
	// 	e.preventDefault();
	// 		var $temp = $('<input>');
	// 		var text = $('.shortlink-text').val().select();
	// 		try {
	// 			var success = document.execCommand('copy'),
	// 				msg = success ? 'Enlace copiado' : 'No se pudo copiar';
	// 			console.log(msg);
	// 		} catch(err) {
	// 			console.log(err);
	// 		}
	// 	return false;
	// });
	var clipboard = new ClipboardJS('.shortlink-button', {
		text: function() {
			return $('.shortlink-text').val();
		}
	});
	clipboard.on('success', function(e) {
		$('.clipboard-status').html('<span class="success">Enlace Copiado</span>').fadeIn('fast').delay(200).fadeOut('fast');
	    //$('.shortlink-button').attr('title','Enlace Copiado');
	    e.clearSelection();
	});
	clipboard.on('error', function(e) {
	    $('.clipboard-status').html('<span class="error">Enlace Copiado</span>').fadeIn('fast').delay(200).fadeOut('fast');
	});
    $('.local-nav').find('a').on('click',function(event){
		event.preventDefault();
		var obj = $(this);
		var anchor = $('a[name='+obj.attr('href').replace('#','')+']' ).offset().top - 30;
		$('html, body').stop().animate({
            scrollTop: anchor
        }, 800);
	});
	$('.advanced-search-form').find('.close-button').on('click', function(e) {
		e.preventDefault();
		$('.advanced-search-form').addClass('hide');
		return false;
	});
	$('.advanced-search').on('click', function(e){
		e.preventDefault();
			$('.advanced-search-form').toggleClass('hide');
		return false;
	});
    $('.input-datepicker').datepicker({
    	language: 'es',
    	range: true,
    	dateFormat: 'yyyy/mm/dd',
    	autoClose: true
    })
    var input = document.getElementById('autocomplete-autores');
	var aw = new Awesomplete(input, { 
		list: userList
	});
	document.addEventListener('awesomplete-selectcomplete', function(obj){
		$('#autocomplete-autor-id').val(obj.text.value);
		$('#autocomplete-autores').val(obj.text.label);
	});
	// talkify.config = {
	//     remoteService: {
	//         active: false 
	//     }
	// }

	
     $('.play-tts').on('click', function(e){
		var player = new talkify.Html5Player().disableTextHighlighting().forceLanguage('es-ES');

		var play = new talkify.playlist()
		.begin()
		.usingPlayer(player)
		.withElements($('.article-content').get())
		.build();
		
     	var obj = $(this);
     	if (player.isPlaying()) {
     		play.pause();
     		obj.html('<span class="dashicons dashicons-controls-play"></span>');
     	} else {
     		play.play();
     		obj.html('<span class="dashicons dashicons-controls-pause"></span>');
     	}
     });
	// $('#autocomplete-autores').on('awesomplete-selectcomplete', function(event,obj){
	// 	console.log(event,obj);
	// });
	//var elem = new Foundation.Sticky($('.local-navigation'),{
	//	stickTo: 'top',
	//	anchor : 'content-navigation'
	//});
});