<?php get_header() ?>
<section class="main-content">
	<div class="row">
		<div class="large-3 columns">
			<h3 class="primary-title">Último Número</h3>
			<?php 
				echo modules::last_magazine();
			?>
		</div>
		<div class="large-9 columns">
			<?php 
				if (is_active_sidebar('magazine-home-top')) {
					dynamic_sidebar('magazine-home-top');
				} else {
					the_widget('WP_Widget_last_editions',array(
						'size' => 6,
						'offset' => 1,
						'is_link' => true,
						'grid' => 3

					));
				}
			?>

		</div>
	</div>
	<?php if (is_active_sidebar( 'sidebar-home-top' )): ?>
		<div class="row inner-space">
			<div class="large-12 small-12 columns">
				<?php dynamic_sidebar('sidebar-home-top'); ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<?php if (is_active_sidebar( 'sidebar-home-1-izq' )): ?>
		<div class="large-3 columns">
			<?php dynamic_sidebar( 'sidebar-home-1-izq' ); ?>
		</div>
		<?php endif; ?>
		<?php if (is_active_sidebar( 'sidebar-home-1-der' )): ?>
			<div class="large-9 columns horizon">
				<?php dynamic_sidebar( 'sidebar-home-1-der' ); ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if (is_active_sidebar('sidebar-home-3-izq') || is_active_sidebar('sidebar-home-3-der')) : ?>
	<div class="row inner-space remove-bottom">
		<?php if ( is_active_sidebar('sidebar-home-3-izq')): ?>
			<div class="large-3 columns footer-left">
				<?php dynamic_sidebar( 'sidebar-home-3-izq' ); ?>
			</div>
		<?php endif; ?>
		<?php if ( is_active_sidebar('sidebar-home-3-der')): ?>
			<div class="large-9 columns horizon">
				<?php dynamic_sidebar( 'sidebar-home-3-der' ); ?>
			</div>
		<?php endif; ?>
	</div>
	<?php endif; ?>
	
	<?php if (is_active_sidebar('sidebar-home-middle-2-left') || is_active_sidebar('sidebar-home-middle-2-right')): ?>
		<div class="row">
			<div class="large-9 columns">
				<?php dynamic_sidebar( 'sidebar-home-middle-2-left' ); ?>
			</div>
			<div class="large-3 columns">
				<?php dynamic_sidebar( 'sidebar-home-middle-2-right' ); ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (is_active_sidebar('sidebar-home-2')): ?>
		<div class="row space-top">
			<div class="large-12 columns">
				<?php dynamic_sidebar( 'sidebar-home-2' ); ?>
			</div>
		</div>
	<?php endif; ?>
	
	<div class="row">
		<?php if ( is_active_sidebar('sidebar-home-4-izq')): ?>
			<div class="large-3 columns footer-bottom-left">
				<?php dynamic_sidebar( 'sidebar-home-4-izq' ); ?>
			</div>
		<?php endif; ?>
		<?php if ( is_active_sidebar('sidebar-home-4-der')): ?>
			<div class="large-9 columns horizon-bottom">
				<?php dynamic_sidebar( 'sidebar-home-4-der' ); ?>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php get_footer() ?>