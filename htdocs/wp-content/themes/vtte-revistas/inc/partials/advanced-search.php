
<?php 
	$titulo = esc_attr($_POST['titulo']);
	$author = esc_attr($_POST['author_id']);
	$fasciculo = esc_attr($_POST['fasciculo']);
	$cuerpo = esc_attr($_POST['body']);
	$abstract = esc_attr($_POST['abstract']);
	$tema = esc_attr($_POST['tema']);
	$keywords = esc_attr($_POST['tag']);
	$publication = esc_attr($_POST['publication']);
	$doi = esc_attr($_POST['doi']);
	$rid = esc_attr($_POST['rid']);
	$orcid = esc_attr($_POST['orcid']);
?>
<form class="form-popup" action="<?php echo site_url('busqueda-avanzada')?>" method="post">
	<div class="row">
		<div class="large-12 columns">
			<label for="title">
				<input type="text" name="titulo" class="input-text" value="<?php echo $titulo; ?>" placeholder="Título">
			</label>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<input type="text" name="author" value="<?php echo esc_attr($_POST['author']); ?>" placeholder="Nombre de autor" id="autocomplete-autores">
			<input type="hidden" name="author_id" value="<?php echo $author; ?>" id="autocomplete-autor-id">
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<select name="fasciculo" class="select">
				<option value="">Seleccionar Fasciculo</option>
				<?php 
					$editions = get_terms('tax_edition');
					if (!empty($editions)) {
						foreach ($editions as $edition) {
							$selected = ( $fasciculo == $edition->term_id ) ? ' selected="selected"': '';
							echo '<option value="'.$edition->term_id.'"'.$selected.'>'.$edition->name.'</option>';
						}
					}
				 ?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<input type="text" name="body" value="<?php echo $cuerpo; ?>" placeholder="Cuerpo">
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<input type="text" name="abstract" value="<?php echo $abstract; ?>" placeholder="Abstract">
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<input type="text" name="tema" value="<?php echo $tema; ?>" placeholder="Tema">
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns with-description">
			<input type="text" name="tag" placeholder="Palabra(s) Clave(s)" value="<?php echo $keywords; ?>">
			<small>Valores separados por coma</small>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns with-description">
			<input type="text" name="publication" value="<?php echo $publication; ?>" class="input-datepicker" placeholder="Fecha de publicación">
			<small>Debes elegir un rango de fechas</small>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<input type="text" name="doi" value="<?php echo $doi; ?>" placeholder="DOI">
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<input type="text" name="rid" value="<?php echo $rid; ?>" placeholder="Research ID">
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<input type="text" name="orcid" value="<?php echo $orcid; ?>" placeholder="ORCID">
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<input type="submit" value="Buscar" class="button expanded">
			<input type="hidden" name="action" value="send">
		</div>
	</div>
</form>