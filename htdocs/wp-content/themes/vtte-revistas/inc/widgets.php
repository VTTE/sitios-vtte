<?php 

class WP_Widget_license extends WP_Widget {
	/** constructor */
	function __construct() {
		$widget_ops = array('classname' => 'text-license', 'description' => 'Muestra una imagen de licencia acompañado de un texto');
		$control_ops = array();
		parent::__construct('text-license', 'Licencia revista', $widget_ops, $control_ops);
	}

	function widget($args, $instance) {
		extract( $instance );
		extract( $args );
		echo  '<div class="widget license">';
			echo '<div class="widget-image">';
				echo '<a href="'.$url.'"><img src="'.$instance['image-url'].'"></a>';
			echo '</div>';
			echo '<div class="widget-content">';
				echo '<p>'.esc_attr($instance['description']).'</p>';
			echo '</div>';
		echo '</div>';
	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	function form( $instance ) {
		extract( $instance );
		echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
		echo '<p><label for="'.$this->get_field_id('description').'">Bajada: <textarea name="'. $this->get_field_name('description') .'" id="'.$this->get_field_id('description').'" class="widefat">'.$instance['description'].'</textarea></label></p>';
		echo '<p><label for="'.$this->get_field_id('url').'">Url: <input type="text" name="'. $this->get_field_name('url') .'" id="'.$this->get_field_id('url').'" value="'.$instance['url'].'" class="widefat" /></label></p>';
		echo '<p><label for="'.$this->get_field_id('image-url').'">Url imagen: <input type="text" name="'. $this->get_field_name('image-url') .'" id="'.$this->get_field_id('image-url').'" value="'.$instance['image-url'].'" class="widefat" /></label></p>';
	   } 

} // class WP_Widget_text

class WP_Widget_links_edition extends WP_Widget {
	/** constructor */
	function __construct() {
		$widget_ops = array('classname' => 'links', 'description' => 'Listado de enlaces de interés');
		$control_ops = array();
		parent::__construct('links', 'Enlaces de interés', $widget_ops, $control_ops);
	}
	function get_links($category) {
		$params = array();
		if (!empty( $category )) {
			$params['category'] = $category;
		}
		$links = get_bookmarks( $params );
		$links_img = get_option('link_img');
		$list_link = array();
		if (!empty($links)) {
			foreach ( $links as $link ) {
				$the_link = new stdClass();
				$the_link->id = $link->link_id;
				$the_link->name = $link->link_name;
				$the_link->url = $link->link_url;
				$the_link->target = $link->link_target;
				$the_link->img = $links_img[$link->link_id];
				$list_link[] = $the_link;
			}
			return $list_link;
		} else {
			return false;
		}
	}
	function widget($args, $instance) {
		extract( $instance );
		extract( $args );
		$links = $this->get_links($category);
	   echo '<div class="widget links">';
			echo '<h4 class="secondary-title">'.esc_attr($title).'</h4>';
			echo '<div class="large-up-5 medium-up-3 small-up-2 clearfix" data-equalizer>';
				if (!empty( $links )) {
					foreach ( $links as $link ) {
						echo '<div class="column">';
							echo '<a href="'.$link->url.'" title="'.$link->name.'" alt="'.$link->name. '" data-equalizer-watch>'.wp_get_attachment_image($link->img, 'links').'</a>';
						echo '</div>';
					}
				}
			echo '</div>';
		echo '</div>';
	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}
	function get_link_categories($instance) {
		$terms = get_terms('link_category');
		if (!empty($terms)) {
			foreach ( $terms as $term ) {
				$sel = ($instance['category'] == $term->slug) ? ' selected="selected"' : '';
				echo '<option value="'.$term->term_id.'"'.$sel.'>'.$term->name.'</option>';
			}
		}
	}
	function form( $instance ) {
		extract( $instance );
		echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
		 echo '<p><label>Categoria enlaces: </label>';
		echo '<select class="widefat" id="'.$this->get_field_id('category').'" name="'.$this->get_field_name('category').'">';
			echo '<option value="">Seleccione</option>';
			   $this->get_link_categories($instance);
			echo '</select>';
		echo '</p>';
	   } 

}

class WP_Widget_the_last_edition extends WP_Widget {
	/** constructor */
	function __construct() {
		$widget_ops = array('classname' => 'the-last-edition', 'description' => 'Muestra ls última edicion de la revista');
		$control_ops = array();
		parent::__construct('the-last-edition', 'La Última edición', $widget_ops, $control_ops);
	}

	function widget($args, $instance) {
		extract($instance);
		echo '<div class="widget the-last-edition">';
			echo '<h4 class="widget-title">'.$title.'</h4>';
			modules::last_magazine();
		echo '</div>';
	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	function form( $instance ) {
		extract( $instance );
		 echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
	   } 

}

class WP_Widget_last_editions extends WP_Widget {
	/** constructor */
	function __construct() {
		$widget_ops = array('classname' => 'last-editions', 'description' => 'Muestra las últimas ediciones de la revista');
		$control_ops = array();
		parent::__construct('last-editions', 'Últimas ediciones', $widget_ops, $control_ops);
	}

	function widget($args, $instance) {
		extract( $instance );
		extract( $args );
		$size = ( !empty( $instance['size'] ) ) ? $instance['size'] : 4;
		$grid = ( !empty( $instance['grid'] ) ) ? $instance['grid'] : 4;
		$offset = ( !empty( $instance['offset'] ) ) ? $instance['offset'] : 0;

		echo '<div class="widget last-editions">';
			echo '<h4 class="widget-title">'.$title.'</h4>';
			echo '<div class="large-up-'.$grid.' small-up-1 medium-up-'.$grid.' clearfix">';
				$edition_list = revista::magazine_list($size,$offset); 
				foreach ( $edition_list as $edition ) {
					 modules::home_list($edition);
				}
			echo '</div>';
			if ($is_link) {
				echo '<div class="line">';
					echo '<a href="'.site_url('ediciones').'" class="button line-button">Ver todas las ediciones</a>';
				echo '</div>';
			}
		echo '</div>';
	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	function form( $instance ) {
		extract( $instance );
		echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
		echo '<p><label for="'.$this->get_field_id('size').'">Cantidad de entradas: <input type="number" name="'. $this->get_field_name('size') .'" id="'.$this->get_field_id('size').'" value="'.$instance['size'].'"/></label></p>';

		echo '<p><label for="'.$this->get_field_id('offset').'">Offset: <input type="number" name="'. $this->get_field_name('offset') .'" id="'.$this->get_field_id('offset').'" value="'.$instance['offset'].'"/></label><br><small>Cantidad de entradas que se deberían saltar desde el principio (por ejemplo si se quiere descartar la última entrada publicada, el valor deberia ser 1)</small></p>';

		 echo '<p><label for="'. $this->get_field_name('is_link').'">Link a archivo de ediciones? </label><input type="checkbox" id="'. $this->get_field_id('is_link').'"'.( ( !empty( $is_link ) ) ? ' checked="checked" ' : '' ).' name="'.$this->get_field_name('is_link').'" value="1"></p>';

		echo '<h3>Apariencia</h3>';
		echo '<p><label>Grilla: </label>';
			echo '<select class="widefat" id="'.$this->get_field_id('grid').'" name="'.$this->get_field_name('grid').'">';
				echo '<option value="">Seleccione</option>';
				echo '<option value="1" '.(($grid == '1') ? 'selected="selected"' : '') .'>1</option>';
				echo '<option value="2" '.(($grid == '2') ? 'selected="selected"' : '') .'>2</option>';
				echo '<option value="3" '.(($grid == '3') ? 'selected="selected"' : '') .'>3</option>';
				echo '<option value="4" '.(($grid == '4') ? 'selected="selected"' : '') .'>4</option>';
			echo '</select><br>';
			echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
		echo '</p>';
	   } 

}
class WP_Widget_last_articles extends WP_Widget {
	/** constructor */
	function __construct() {
		$widget_ops = array('classname' => 'last-articles', 'description' => 'Muestra los últimos artículos al azar de las últimas 2 ediciones');
		$control_ops = array();
		parent::__construct('last-articles', 'Últimos artículos de revista', $widget_ops, $control_ops);
	}

	function widget($args, $instance) {
		extract( $instance );
		extract( $args );
		$editions = get_terms( 'tax_edition');
		$size = (!empty($instance['size'])) ? $instance['size'] : 5;
		if (!empty($editions)) {
			$last_articles = new WP_Query(array(
					'post_type' => 'article',
					'posts_per_page' => $size,
					'posts_status' => 'publish',
					'orderby' => 'rand',
					'tax_query' => array(
						array(
							'taxonomy' => 'tax_edition',
							'field' => 'term_id',
							'terms' => array( $editions[0]->term_id, $editions[1]->term_id )
							)
						),
					'meta_query' => array(
						array(
							'key' => 'article_not_article',
							'value' => 1,
							'compare' => 'NOT EXISTS'
						)
					)
				));
			if ($last_articles->have_posts()) {
				echo  '<div class="widget last-articles">';
				   echo '<div class="widget-title">'.$title.'</div>';
				   echo '<div class="row large-up-'.$size.' small-up-2">';
						foreach ($last_articles->posts as $article) {
							echo '<div class="column">';
								echo modules::article_item($article->ID, true);
							echo '</div>';
						}
				   echo '</div>';
				echo '</div>';
			}

		}
	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	function form( $instance ) {
		extract( $instance );
		echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
		echo '<p><label for="'.$this->get_field_id('size').'">Cantidad de entradas: <input type="number" name="'. $this->get_field_name('size') .'" id="'.$this->get_field_id('size').'" value="'.$instance['size'].'"/></label></p>';
	   } 

} // class WP_Widget_text

class WP_Widget_download_counter extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		$widget_ops = array('classname' => 'download-counter', 'description' => 'Muestra un contador de descarga por articulos');
		$control_ops = array();
		parent::__construct('download-counter', 'Contador de descargas', $widget_ops, $control_ops);
	}

	function widget($args, $instance)
	{
		global $post;
		$file_id = get_post_meta($post->ID, 'article_download', true);
		if (!empty($file_id)) {
			$counter = get_post_meta($file_id, 'file_download_counter', true);
			echo '<div class="widget download-counter">';
				echo '<div class="download-counter-content">';
					echo '<span class="dashicons dashicons-download"></span>';
					echo '<span class="download-label"> Descargas: </span>';
					echo '<span class="download-number">'.$counter.'</span>';
				echo '</div>';
			echo '</div>';
		}
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function form($instance)
	{
		echo '<p>Este widget no tiene configuraciones</p>';
	}
} // class WP_Widget_text

// register WP_Widgets
add_action('widgets_init', 'register_magazine_widgets');
add_action('init', 'register_magazine_widgets');
function register_magazine_widgets(){
	register_widget( 'WP_Widget_the_last_edition' );
	register_widget( 'WP_Widget_last_editions' );
	register_widget( 'WP_Widget_license' );
	register_widget( 'WP_Widget_links_edition' );
	register_widget( 'WP_Widget_last_articles');
	register_widget( 'WP_Widget_download_counter' );
}
 ?>