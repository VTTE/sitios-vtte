<?php 
global $wp_query,$post;

header("Content-type: text/xml");
echo '<metadata xmlns="http://example.org/myapp/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xsi:schemaLocation="http://example.org/myapp/ http://example.org/myapp/schema.xsd"
  xmlns:dc="http://purl.org/dc/elements/1.1/">';
echo '<?xml version="1.0"?>';
echo '<!DOCTYPE rdf:RDF SYSTEM "http://dublincore.org/documents/2000/11/dcmes-xml/dcmes-xml-dtd.dtd">';
echo '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:dc="http://purl.org/dc/elements/1.1/">';

echo '<rdf:Description about="'.get_permalink($post->ID).'">';
	echo '<dc:title>'.get_the_title($post->ID).'</dc:title>';
	if (!empty($post->dublin_editor_name)) {
		echo '<dc:creator>'.$post->dublin_editor_name.'</dc:creator>';
	}
	if (!empty($post->dublin_type)) {
		echo '<dc:type>' . $post->dublin_type . '</dc:type>';
	}
	if (!empty($post->dublin_date_created)) {
		echo '<dc:date>' . $post->dublin_date_created . '</dc:date>';
	}
	if (!empty($post->dublin_date_created)) {
		echo '<dc:date>' . $post->dublin_date_created . '</dc:date>';
	}
echo '</rdf:Description>';
echo '<article>';
	echo '<front>';
		echo '<journal-meta>';
			echo '<journal-title>'.get_the_title($post->ID).'</journal-title>';
			if (!empty($post->dublin_magazine_issn)) {
				echo '<issn>'.$post->dublin_magazine_issn.'</issn>';
			}
			if (!empty($post->dublin_editor_name)) {
				echo '<publisher>';
					echo '<publisher-name>'.$post->dublin_editor_name.'</publisher-name>';
				echo '</publisher>';
			}
		echo '</journal-meta>';
		echo '<article-meta>';
			if (!empty($post->dublin_date_created)) {
				echo '<pub-date pub-type="pub">';
					echo '<day>'.mysql2date('d',$post->dublin_date_created).'</day>';
					echo '<month>'.mysql2date('m',$post->dublin_date_created).'</month>';
					echo '<year>'.mysql2date('Y',$post->dublin_date_created).'</year>';
				echo '</pub-date>';
			}
			if (!empty($post->dublin_edition_volume)) {
				echo '<volume>'.$post->dublin_edition_volume.'</volume>';
			}
			if (!empty($post->dublin_edition_number)) {
				echo '<numero>'.$post->dublin_edition_number.'</numero>';
			}
		echo '</article-meta>';
	echo '</front>';
	echo '<body>';
		echo '<![CDATA['.htmlentities(apply_filters('the_content',$post->post_content)).']]>';
	echo '</body>';
echo '</article>';