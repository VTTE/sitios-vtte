<?php

/*
	Advanced Search filter class
*/

class advanced_search {
	public $query;
	private $post_type;
	private $taxonomies;
	private $taxonomies_or;
	private $meta_prefix = 'dublin_';
	private $meta_article_prefix = 'article_';
	private $meta_article_text = array();
	private $meta_text = array();
	private $meta_text_like = array();
	private $meta_date;
	private $meta_id;
	private $meta_date_term;
	private $meta_date_compare = 'BETWEEN';
	private $posts_per_page;
	private $page;
	private $search_text;
	private $taxonomy_name;

	function __construct($args) {
		//$this->search($args);
	}

	function get_current_taxonomy() {
		$meta = get_queried_object();
		if ( empty($this->taxonomy_name) ) {
			$tax = $meta->taxonomy;
		} else {
			$tax = $this->taxonomy_name;
		}
		return $tax;
	}
	function set_taxonomy_name($tax) {
		$this->taxonomy_name = $tax;
	}
	function default_post_type() {
		return 'post';
	}
	function set_search_text($text) {
		$this->search_text = $text;
	}
	function get_search_text() {
		return $this->search_text;
	}
	function set_page($page) {
		$this->page = $page;
	}
	function get_page() {
		if ( !empty( $this->page ) ) {
			return $this->page;
		} 
	}
	function set_meta_text($array) {
		$this->meta_text = array_merge($this->meta_text,$array);
	}
	function set_meta_article_text($array) {
		$this->meta_article_text = array_merge($this->meta_article_text,$array);
	}
	function set_meta_text_like($array) {
		$this->meta_text_like = array_merge($this->meta_text_like,$array);
	}
	function set_meta_date($value) {
		$this->meta_date = $value;
	}
	function get_query() {
		return $this->query;
	}
	function set_taxonomies($taxonomies) {
		$this->taxonomies[] = $taxonomies;
	}
	function set_taxonomies_or($taxonomies) {
		$this->taxonomies_or[] = $taxonomies;
	}
	function get_taxonomies() {
		return $this->taxonomies;
	}
	function set_post_type($post_type) {
		$this->post_type = $post_type;
	}
	function get_date() {
		return $this->date;
	}
	function set_date($date_array) {
		$this->date = $date_array;
	}
	function get_post_type() {
		if ( !empty( $this->post_type ) ) {
			return $this->post_type;
		} else {
			return $this->default_post_type();
		}
	}
	function get_default_args() {
		$meta = get_queried_object();

		$default =  array(
			'post_type' => $this->get_post_type(),
			'post_per_page' => get_option('posts_per_page'),
			'posts_status' => 'publish',
			);
		if ( count($this->meta_text) || count($this->meta_date) || count($this->meta_text_like) || count($this->meta_article_text) ) {
			$meta_query = array();

			if (!empty($this->meta_text)) {
				foreach ($this->meta_text as $index => $meta) {
					$current_meta = array(
							'key' => $this->meta_prefix.$index,
							'value' => $meta
							);
					array_push($meta_query, $current_meta);
				}
			}
			if (!empty($this->meta_text_like)) {
				foreach ($this->meta_text_like as $index => $meta) {
					$current_meta = array(
							'key' => $this->meta_prefix.$index,
							'value' => $meta,
							'compare' => 'LIKE'
							);
					array_push($meta_query, $current_meta);
				}
			}
			if (!empty($this->meta_date)) {
				$date_value = explode(',',$this->meta_date);
				$date_query = array(
					'key' => $this->meta_date_term,
					'value' => $date_value,
					'compare' => $this->meta_date_compare,
					'type' => 'DATE'
					);
				array_push($meta_query, $date_query);
			}
			if (!empty($this->meta_article_text)) {
				foreach ($this->meta_article_text as $index => $meta) {
					$current_meta = array(
							'key' => $this->meta_article_prefix.$index,
							'value' => $meta
							);
					array_push($meta_query, $current_meta);
				}
			}
			$default['meta_query'] = $meta_query;
		}
		if (!empty($this->date)) {
			 $default['date_query'] = array($this->date);
		}
		if (!empty($this->search_text)) {
			$default['s'] = $this->search_text;
		}
		if (!empty($this->page)) {
			$default['paged'] = $this->page;
		}
		if ( !empty($this->taxonomies) ) {
			$taxonomies = array();
			foreach ($this->taxonomies as $tax) {
				$taxonomies[] = array(
					'taxonomy' => $tax['taxonomy'],
					'field' => $tax['field'],
					'terms' => $tax['terms']
					);
			}
			$default['tax_query'] = $taxonomies; 
		}
		return $default;
	}
	function search($args) {
		$default = wp_parse_args($this->get_default_args(),$args=false);
		$this->query = new WP_Query($default);
		return $this->query;
	}
	function current_advanced_search($post) {
		echo '<div class="search-resume">';
		if (!empty($post['titulo'])) {
			echo '<span class="item"><strong>Título: </strong>'.esc_attr($post['titulo']).'</span> ';
		}
		if (!empty($post['author'])) {
			echo '<span class="item"><strong>Autor(a): </strong>'.esc_attr($post['author']).'</span> ';
		}
		if (!empty($post['fasciculo'])) {
			$fasciculo = get_term($post['fasciculo']);
			echo '<span class="item"><strong>Fascículo: </strong>'.$fasciculo->name.'</span> ';
		}
		if (!empty($post['cuerpo'])) {
			echo '<span class="item"><strong>Cuerpo: </strong>'.esc_attr($post['cuerpo']).'</span> ';
		}
		if (!empty($post['abstract'])) {
			echo '<span class="item"><strong>Abstract: </strong>'.esc_attr($post['abstract']).'</span> ';
		}
		if (!empty($post['tema'])) {
			echo '<span class="item"><strong>Tema: </strong>'.esc_attr($post['tema']).'</span> ';
		}
		if (!empty($post['tag'])) {
			echo '<span class="item"><strong>Tag(s): </strong>'.esc_attr($post['tag']).'</span> ';
		}
		if (!empty($post['publication'])) {
			$dates = explode(',',$post['publication']);
			$date_from = mysql2date('d, m Y', $dates[0]);
			$date_to = mysql2date('d, m Y', $dates[1]);
			echo '<span class="item"><strong>Desde: </strong>'.$date_from.' <strong>Hasta: </strong>'.$date_to.'</span> ';
		}
		if (!empty($post['doi'])) {
			echo '<span class="item"><strong>DOI: </strong>'.esc_attr($post['doi']).'</span>';
		}
		if (!empty($post['rid'])) {
			echo '<span class="item"><strong>Research ID: </strong>'.esc_attr($post['rid']).'</span>';
		}
		if (!empty($post['orcid'])) {
			echo '<span class="item"><strong>ORCID: </strong>'.esc_attr($post['orcid']).'</span>';
		}
		echo '</div>';
	}
}