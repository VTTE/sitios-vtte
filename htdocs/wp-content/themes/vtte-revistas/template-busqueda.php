<?php
/*
	Template name: Busqueda avanzada
 */
	get_header();
	/*Search params*/
	if (!empty($_POST['action'])) {
		$titulo = esc_attr($_POST['titulo']);
		$author = esc_attr($_POST['author_id']);
		$fasciculo = esc_attr($_POST['fasciculo']);
		$cuerpo = esc_attr($_POST['body']);
		$abstract = esc_attr($_POST['abstract']);
		$tema = esc_attr($_POST['tema']);
		$keywords = esc_attr($_POST['tag']);
		$publication = esc_attr($_POST['publication']);
		$doi = esc_attr($_POST['doi']);
		$rid = esc_attr($_POST['rid']);
		$orcid = esc_attr($_POST['orcid']);

		$search = new advanced_search();
		$search->set_post_type('article');
		if ( get_query_var('paged') ) {
			$search->set_page(get_query_var('paged'));
		}
		if (!empty($cuerpo)) {
			$search->set_search_text($cuerpo);
		}
		if (!empty($fasciculo)) {
			$tax_fasciculo = array(
					'taxonomy' => 'tax_edition',
					'field' => 'tern_id',
					'term' => $fasciculo
				);
			$search->set_taxonomies($tax_fasciculo);
		}
		if (!empty($keywords)) {
			$keys = explode(',',$keywords);
			$the_keys = array();
			foreach ($keys as $key) {
				$the_keys[] = sanitize_title_with_dashes(remove_accents($key));
			}
			$keywords_array_es = array(
					'taxonomy' => 'tax_keywords',
					'field' => 'slug',
					'terms' => $the_keys
				);
			$search->set_taxonomies($keywords_array_es);
			
		}
		if (!empty($publication)) {
			$search->set_meta_date($publication);
		}
		/*text meta value*/
		if (!empty($author)) {
			$search->set_meta_article_text( array( 'person' => $author ) );
		}
		if (!empty($doi)) {
			$search->set_meta_text( array( 'doi' => $doi ) );
		}
		if (!empty($orcid)) {
			$search->set_meta_text( array( 'orcid' => $orcid ) );
		}
		if (!empty($abstract)) {
			$search->set_meta_text_like( array( 'abstract_es' => $abstract ) );
			//$search->set_meta_text_like( array( 'abstract_en' => $abstract ) );
		}
		if (!empty($titulo)) {
			$search->set_meta_text_like( array( 'title' => $titulo ) );
		}

		$query = $search->search();
		//echo '<pre>'; print_r($query); echo '</pre>';
	}
?>
<section class="main-content">
	<header class="row section-header">
		<div class="large-12 columns">
			<h3 class="secondary-title">Buscando</h3>
			<?php echo $search->current_advanced_search($_POST); ?>
		</div>
	</header>
	<div class="row content">
		<div class="large-12 columns">
			<?php 
				if ( $query->have_posts() ) {
					echo '<div class="large-up-4 small-up-2 clearfix row">';
						while( $query->have_posts() ): $query->the_post();
							global $post;
							echo base::news_share();
						endwhile;
					echo '</div>';
						if ( function_exists('wp_pagenavi')) {
							wp_pagenavi(array('query' => $query));
						}
					} else {
						echo '<div class="callout warning"><h5>Lo sentimos</h5> <p>No se han encontrado resultados para lo que buscas</p> </div>';
					}
			 ?>
		</div>
	</div>
</section>
<?php get_footer() ?>