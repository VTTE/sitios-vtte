<?php 
	/*
		Template name: Listado ediciones
	*/
	get_header(); 
	the_post();
?>
<div class="main-content">
	<div class="row">
		<div class="large-12 columns">
			<header class="page-header">
				<h3 class="secondary-title"><?php the_title() ?></h3>
				<div class="page-content">
					<?php the_content() ?>
				</div>
			</header>
			<?php 
				// if ( get_query_var( 'paged' ) )
				// 	$paged = get_query_var('paged');
				// else if ( get_query_var( 'page' ) )
				// 	$paged = get_query_var( 'page' );
				// else
				// 	$paged = 1;
				// $per_page    = 3;
				// $number_of_series = count( get_terms( 'tax_edition', array('hide_empty' => 0) ) );
				// $offset      = $per_page * ( $paged - 1);

				$args = array(
						//'offset'       => $offset,
						'hide_empty'	=> 0,
						//'number'       => $per_page
				);

				$editions = get_terms( 'tax_edition', $args );

			 ?>
			<div class="row large-up-4 medium-up-4 small-up-2 clearfix edition-list">
				<?php 
					foreach($editions as $item) {
						$file = edition::get_file($item->term_id);
						$metadata = edition::get_metadata($item->term_id);

				        echo '<div class="column">';
				        	echo '<article class="hentry entry-edition">';
				        		if (!$metadata->only_download) {
					        		echo '<a href="'.edition::get_link($item->term_id).'">';
					        	}
					        		echo edition::get_image($item->term_id,'featured-magazine');
					        	if (!$metadata->only_download) {
					        		echo '</a>';
					        	}
					        	echo '<h5 class="entry-title">'.$item->name.'</h5>';
								echo '<div class="magazine-buttons">';
									if (!$metadata->only_download) {
										echo '<a href="'.edition::get_link($item->term_id).'" class="button gray">Ver Artículos</a>';
									}
									if ( is_object( $file ) ) {
										echo '<a href="'.$file->url.'" alt="'.$file->name.'" class="button gray">Descargar</a>';
									}
								echo '</div>';
							echo '</article>';
				        echo '</div>';
				}
				 ?>
			</div>
			<?php 

				// $big = 9999999;
				// echo paginate_links( array(
				// 		'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				// 		'format'  => '?paged=%#%',
				// 		'current' => $paged,
				// 		'total'   => ceil( $number_of_series / $per_page ) // 3 items per page
				// 	) );
			 ?>		 
		</div>
	</div>
</div>
<?php get_footer(); ?>