<div class="search-form">
	<form role="search" method="get" id="searchform" class="searchform" action="<?php echo site_url() ?>">
		<div class="input-group">
			<input value="" class="input-group-field" name="s" placeholder="Buscar..." id="s" type="text">
			<a href="#" class="advanced-search has-tip" data-tooltip data-position="top" title="Búsqueda Avanzada"><span class="dashicons dashicons-arrow-down"></span></a>
			<div class="input-group-button">
				<input id="searchsubmit" value="Ir" class="button" type="submit">
			</div>
		</div>
	</form>
	<div class="advanced-search-form hide">
		<button class="close-button" aria-label="Close alert" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
		<?php get_template_part('inc/partials/advanced','search') ?>
	</div>
</div>