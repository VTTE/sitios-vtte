<?php
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;

/*
	Calling some files
*/

include STYLESHEETPATH . '/inc/widgets.php';
include STYLESHEETPATH . '/inc/advanced-search.php';

/*
	Sidebars filters
*/
add_filter('vtte_base_mandatory_sidebars', array('revista','remove_base_sidebars'));
/*
	Theme sidebars
*/
$mandatory_sidebars = array(
	'Revista arriba derecha' => array(
		'name' => 'magazine-home-top',
		'size' => 9
		),
	'Articulo' => array(
		'name' => 'article',
		'size' => 3
		),
	'Edicion' => array(
		'name' => 'edition',
		'size' => 3
		),
	'Home espacio superior' => array(
		'name' => 'sidebar-home-top',
		'size' => 9
		),
	'Home fila 1 izq' => array(
		'name' => 'sidebar-home-1-izq',
		'size' => 3
		),
	'Home fila 1 der' => array(
		'name' => 'sidebar-home-1-der',
		'size' => 9
		),
	'Home fila intermedio 2 izq' => array(
		'name' => 'sidebar-home-middle-2-left',
		'size' => 9
	),
	'Home fila intermedio 2 der' => array(
		'name' => 'sidebar-home-middle-2-right',
		'size' => 3
	),
	'Home fila 2' => array(
		'name' => 'sidebar-home-2',
		'size' => 12
		),
	'Home fila 3 izq' => array(
		'name' => 'sidebar-home-3-izq',
		'size' => 3
		),
	'Home fila 3 der' => array(
		'name' => 'sidebar-home-3-der',
		'size' => 9
		),
	'Home fila 4 izq' => array(
		'name' => 'sidebar-home-4-izq',
		'size' => 3
		),
	'Home fila 4 der' => array(
		'name' => 'sidebar-home-4-der',
		'size' => 9
		),
	'Página Footer' => array(
		'name' => 'page_footer_large',
		'size' => 12
		),
	'Single Footer' => array(
		'name' => 'single_footer_large',
		'size' => 12
		)
);
foreach ( $mandatory_sidebars as $sidebar => $id_sidebar ) {
	register_sidebar( array(
		'name'          => $sidebar,
		'id'			=> $id_sidebar['name'],
		'before_widget' => '<section id="%1$s" class="widget %2$s">'."\n",
		'after_widget'  => '</section>',
		'before_title'  => '<header class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3></header>',
		'size' => $id_sidebar['size']
	) );
}

/*
	Theme specific sizes
*/
add_image_size('featured-magazine', 270, 350, true);
add_image_size('mini-magazine', 100, 130, true);
add_image_size('links', 200, 9999, false);

/*
	Activate links manager
*/

add_filter('pre_option_link_manager_enabled', '__return_true');

add_action( 'admin_enqueue_scripts', 'admin_enqueue_scripts' );
function admin_enqueue_scripts() {
	 global $pagenow;
	 if ( is_admin() && ( $pagenow == 'term.php' ) ) {
	 		wp_enqueue_media();
	 }
}

add_action( 'wp_enqueue_scripts', 'enqueue_magazine_scripts' );
function enqueue_magazine_scripts() {
	wp_enqueue_script( 'Foundation.util.motion', PRE_HOME_URI .'/vtte-revistas/js/foundation.util.motion.js', array('Foundation'), '1.0', '' );
	wp_enqueue_script( 'Foundation.reveal', PRE_HOME_URI .'/vtte-revistas/js/foundation.reveal.js', array('Foundation'), '1.0', '' );
	wp_enqueue_script( 'Foundation.tooltip', PRE_HOME_URI .'/vtte-revistas/js/foundation.tooltip.js', array('Foundation'), '1.0', '' );
	wp_enqueue_script( 'clipboard', PRE_HOME_URI .'/vtte-revistas/js/clipboard.js', array('jquery'), '1.0', '' );
	//talkify
	wp_enqueue_script( 'talkify', PRE_HOME_URI .'/vtte-revistas/js/talkify.min.js', array('jquery'), '1.0', '' );
	//Awesomeplete
	wp_enqueue_script( 'awesomeplete', PRE_HOME_URI .'/vtte-revistas/js/awesomeplete.js', array('jquery'), '1.0', false );
	wp_enqueue_style('awesomeplete', PRE_HOME_URI .'/vtte-revistas/css/awesomeplete.css');
	//Datepicker
	wp_enqueue_script( 'datepicker', PRE_HOME_URI .'/vtte-revistas/js/datepicker.js', array('jquery'), '1.0', '' );
	wp_enqueue_script( 'datepicker_es', PRE_HOME_URI .'/vtte-revistas/js/datepicker.es.js', array('datepicker'), '1.0', '' );
	wp_enqueue_style('datepicker', PRE_HOME_URI .'/vtte-revistas/css/datepicker.css');
	wp_enqueue_script( 'vtte_magazine_script', PRE_HOME_URI .'/vtte-revistas/js/script.js', array('jquery'), '1.0', '' );
	$authors = new WP_Query(array(
		'post_type' => 'person',
		'posts_per_page' => -1,
		'post_status' => 'publish'
		));
	$author_list = array();
	if ($authors->have_posts()) {
		foreach ($authors->posts as $author) {
			$obj = [];
			$obj['label'] = get_the_title($author->ID);
			$obj['value'] = $author->ID;
			$author_list[] = $obj;
		}
	}
	$users_data = $author_list;
	wp_localize_script( 'vtte_magazine_script', 'userList', $users_data );
}

class revista {
	static function remove_base_sidebars($sidebars) {
		unset($sidebars['Página footer 1']);
		unset($sidebars['Página footer 2']);
		unset($sidebars['Página footer 3']);
		unset($sidebars['Entrada footer 1']);
		unset($sidebars['Entrada footer 2']);
		unset($sidebars['Entrada footer 3']);
		return $sidebars;
	}
	static function edition($id) {
		$terms = wp_get_post_terms( $id, 'tax_edition' );
		if ( !empty( $terms ) && is_array( $terms ) ) {
			$current_term = current($terms);
			$link = get_term_link( $current_term, 'tax_edition' );
			$current_term->link = $link;
			return $current_term;
		} else {
			return false;
		}
	}
	static function get_keywords($id,$tax='tax_keywords') {
		$terms = wp_get_post_terms( $id, $tax );
		if ( !empty( $terms ) ) {
			echo '<ul class="menu vertical">';
			foreach ( $terms as $term ) {
				$term_link = ( !is_wp_error( get_term_link( $term, $tax ) ) ) ? get_term_link( $term, 'tax_keywords' ) : '#' ;
				echo '<li><a href="'.$term_link.'">'.$term->name.'</a>';
			}
			echo '</ul>';
		} else {
			return false;
		}
	}
	static function get_author_list($id) {
		$person_meta = get_post_meta($id, 'article_person');
		if (!empty($person_meta)) {
			$person_list = array();
			foreach ($person_meta as $person) {
				$object = get_post($person);
				$person_list[] = $object;
			}
			return $person_list;
		} else {
			return false;
		}
		
	}
	static function magazine_list($size, $offset) {
		$magazines = get_terms( 'tax_edition', array('hide_empty' => false) );
		if ( !empty( $magazines ) ) {
			$return = array_slice($magazines, $offset, $size);
			return $return;
		} else {
			return false;
		}
	}
	function send_mail() {
		if (check_ajax_referer( 'send_by_mail','sec' ) && ( empty( $_POST['lastname'] ) ) ) {
			global $_set;
			$name = esc_attr($_POST['name']);
			$mail = esc_attr($_POST['mail']);
			$comments = esc_attr($_POST['comments']);
			$recipient = esc_attr($_POST['recipient']);
			$post_id = esc_attr($_POST['post_id']);
			$post_name = get_the_title($post_id);
			$post_link= get_permalink($post_id);
			$array_mails = explode(',',$recipient);
			if (count($array_mails) == 1) {
				$send_mail = ( !empty( $recipient ) ) ? $recipient : get_option('admin_email');
			}
			if (!empty($name) && !empty($recipient) && !empty($mail) ) {
				add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
				$message = '<small>Este correo ha sido enviado desde el sitio web de '.get_bloginfo('name').'</small><br><br>';
				$message .= '<h1>Te acaban de recomendar un artículo</h1>';
				$message .= '<p>'.$name.' te ha recomendado el siguiente artículo:<a href="'.$post_link.'">'. $post_name.'</a></p>';
				if (!empty($comments)) {
					$message .= '<blockquote>'.$comments.'</blockquote>';
				}
				$message .= '<p>Puedes responder este correo para comentarle que te pareció</p>';

				$subject = '['.get_bloginfo('name').'] te han recomendado un artículo: '.$post_name;

				$headers = array(
					'From: "'.$name.'" <'.$mail.'>'
					);
				if (count($array_mails > 1)) {
					$send = 0;
					foreach ($array_mails as $the_mail) {
						if (wp_mail(trim($the_mail), $subject, $message,$headers)) {
							$send++;
						} 
					}
					if ($send == count($array_mails)) {
						echo 1;
					} else {
						echo 'mail error';
					}
				} else {
					$to = $send_mail;
					$try = wp_mail($to, $subject, $message,$headers);
					if ($try) {
						echo 1;
					} else {
						echo 'mail error';
					}
				}

			} else {
				echo 'error';
				exit();
			}

			exit();
		} else {
			echo 'Error ¬¬';
			exit(0);
		}

	}
	public function add_form_config($form, $data) {
		$form->addElement( new Element\InputCheckbox(
				_x('Activar Lectura de artículos', 'site settings fields', 'revology'),
				'tts_active',
				array( '1' => 'Sí'),
				array(
					'value' => isset($data['tts_active']) ? $data['tts_active'] : '',
					'description' => 'Funciona mediante la api HTML5 de Text to Speech, no se garantiza compatibilidad en todos los navegadores'
				)
			) )->addElement( new Element\InputText(
				_x('ISSN Online', 'tax settings fields', 'vtte'),
				'digital_issn',
				array(
					'value' => isset($data['digital_issn']) ? $data['digital_issn'] : ''
				)
			) )->addElement( new Element\InputText(
				_x('ISSN Impreso', 'tax settings fields', 'vtte'),
				'print_issn',
				array(
					'value' => isset($data['print_issn']) ? $data['print_issn'] : ''
				)
			) )->addElement( new Element\InputText(
				_x('ISSN (ISSN-L)', 'tax settings fields', 'vtte'),
				'issn_l',
				array(
					'value' => isset($data['issn_l']) ? $data['issn_l'] : ''
				)
			) );
		return $form;
	}
	public function add_form_config_field($fields) {
		$fields[] = 'tts_active';
		$fields[] = 'digital_issn';
		$fields[] = 'print_issn';
		$fields[] = 'issn_l';
		return $fields;
	}
	public function add_issn_info($string) {
		global $_set;
		$settings = $_set->settings;
		$digital_issn = (!empty($settings['digital_issn'])) ? '<strong class="title">ISSN (Online)</strong> <span class="number">'.$settings['digital_issn'].'</span>' : '';
		$print_issn = (!empty($settings['print_issn'])) ? '<strong class="title">ISSN (Print)</strong> <span class="number">'.$settings['print_issn'].'</span>' : '';
		$l_issn = (!empty($settings['issn_l'])) ? '<strong class="title">ISSN (ISSN-L)</strong> <span class="number">'.$settings['issn_l'].'</span>' : '';
		$string = '<span class="issn-line">'.$l_issn.$digital_issn.$print_issn.'</span>';

		return $string;
	}
	public function track_download() {
		check_ajax_referer('track-download-article', 'nonce_data');
		$attachment_id = $_POST['attachment_id'];
		if (!empty($attachment_id)) {
			$counter = get_post_meta( $attachment_id, 'file_download_counter', true );
			if (!empty($counter)) {
				$counter++;
			} else {
				$counter = 1;
			}
			update_post_meta( $attachment_id, 'file_download_counter', $counter );
		}
		wp_die();
	}
}
add_filter('vtte_main_configuration_form',array('revista','add_form_config'),10,2);
add_filter('vtte_main_configuration_fields',array('revista','add_form_config_field'),10,1);
add_filter('vtte_main_header_down_logo',array('revista','add_issn_info'),10,1);
/*
	AJAX CALLS
*/
add_action('wp_ajax_nopriv_send_by_mail', array('revista','send_mail'));
add_action('wp_ajax_send_by_mail', array('revista','send_mail'));
add_action('wp_ajax_nopriv_track_download', array('revista', 'track_download'));
add_action('wp_ajax_track_download', array('revista', 'track_download'));
class mRender {
	static function form($post_id) {
		
		$out = '<div class="info-box">';
			$out .= '<div class="program-form">';
				$out .= '<form action="" method="POST" class="send_by_mail valid-form" data-abide novalidate>';
					$out .= '<div data-abide-error class="alert callout error-message" style="display: none;">';
					    $out .= '<p><span class="dashicons dashicons-dismiss"></span> Hay errores en el envío del formulario.</p>';
					  $out .= '</div>';
					  $out .= '<div class="success callout success-message" style="display: none;">';
					    $out .= '<p><span class="dashicons dashicons-yes"></span> Articulo enviado</p>';
					  $out .= '</div>';
					$out .= '<label>';
						$out .= '<input type="text" name="name" class="input-text name-input" placeholder="Tu nombre" required>';
						$out .= '<span class="form-error">Debe especificar un nombre...</span>';
					$out .= '</label>';
					$out .= '<label>';
						$out .= '<input type="email" name="mail" class="input-text mail-input" placeholder="Tu Email" required>';
						$out .= '<span class="form-error">Debe especificar tu email...</span>';
					$out .= '</label>';
					$out .= '<label>';
						$out .= '<textarea name="comments" class="input-text textarea-input" placeholder="Tus comentarios"></textarea>';
					$out .= '</label>';
					$out .= '<label>';
						$out .= '<input type="text" name="target" class="input-text recipient-input" placeholder="Destinatario">';
					$out .= '</label>';
					$out .= '<input type="hidden" name="post_id" class="post-input" value="'.$post_id.'">';
					$out .= wp_nonce_field( 'send_by_mail', 'send_by_mail_nonce', true, false );
					$out .= '<input type="text" name="last_name" class="input-text lastname-input" placeholder="last name">';
					$out .= '<input type="submit" class="button expanded green send-form" value="Enviar">';
				$out .= '</form>';
			$out .= '</div>';
		$out .= '</div>';
		return $out;
	}
}
class edition {
	static function list_edition($term) {
		$order = get_term_meta($term->term_id, 'order', true);
		if ( !empty($order) ) {
			echo '<div class="large-up-2 medium-up-2 small-up-1">';
				foreach ($order as $item) {
					echo '<div class="column">';
						echo modules::article_item($item);
					echo '</div>';
				}
			echo '</div>';
		}
	}
	static function get_author($post_id) {
		$author = revista::get_author_list($post_id);
		$author_title = ( count($author) > 1) ? 'Autores' : 'Autor';
		echo '<h5 class="mini-title">'.$author_title.'</h5>';
		$x = 0;
		$out = '<span class="author">';
		foreach ($author as $item) {
			$x++;
			$out .= get_the_title($item->ID);
			$comma = ($x < count($author)) ? ', ':'';
			$out .= $comma;
			if (count($author) == 1) {
				$grade = get_post_meta($item->ID, 'person_grade', true);
				if (!empty($grade)) {
					$out .=  ', '.$grade;
				}
			}
		}
		$out .= '</span>';
		return $out;
	}
	static function get_image($id,$size) {
		$image_id = get_term_meta($id, 'tax_image', true);
		if ( !empty($image_id) ) {
			$image = wp_get_attachment_image( $image_id, $size );
			return $image;
		} else {
			return false;
		}
	}
	static function get_file($id) {
		$file_id = get_term_meta($id, 'download_edition', true);
		if (!empty($file_id)) {
			try {
				$file = new \GutenPress\Helpers\Attachment($file_id);
			} catch ( Exception $e ) {
				return '<!-- Attachment: no existe -->';
			}
		}
		$return = new stdClass();
		$return->url = $file->url;
		$return->size = $file->filesize;
		$return->name = $file->title;
		$return->extension = $file->pathinfo->extension;
		
		return $return;
	}
	static function get_link($id) {
		$link = get_term_link( $id, 'tax_edition' );
		if ( !is_wp_error( $link ) ) {
			return $link;
		} else {
			return '#';
		}
	}
	static function get_metadata($id) {
		$tax_volume = get_term_meta( $id, 'volume_number', true );
		$tax_number = get_term_meta( $id, 'number', true );
		$tax_year = get_term_meta( $id, 'year', true );
		$tax_digital_issn = get_term_meta( $id, 'digital_issn', true );
		$tax_print_issn = get_term_meta( $id, 'print_issn', true );
		$tax_faculty = get_term_meta( $id, 'faculty', true );
		$tax_only_download = get_term_meta( $id, 'only_download', true );

		$return = new stdClass();
		$return->volume = $tax_volume;
		$return->number = $tax_number;
		$return->year = $tax_year;
		$return->digital_issn = $tax_digital_issn;
		$return->print_issn = $tax_print_issn;
		$return->faculty = $tax_faculty;
		$return->only_download = array_pop($tax_only_download);

		return $return;
	}
	static function get_articles($id) {
		$tax_order = get_term_meta( $id, 'order', true);
	}
	static function get_meta_page($sidebar, $post) {
		if ( is_object( $sidebar ) ) {
			$post = $sidebar;
			$sidebar = '';
		}
		$icon = ( !empty( $post->page_icon ) ) ? $post->page_icon : 'info';

		if ( !empty( $post->page_info ) ) {
			$sidebar .= '<section class="page-meta">';
				$sidebar .= '<h3 class="meta-title"><span class="dashicons dashicons-'.$icon.'"></span> '.$post->page_title.' </h3>';
				$sidebar .= apply_filters( 'the_content', $post->page_info );
			$sidebar .= '</section>';
			return $sidebar;
		} else {
			return false;
		}
	}
}

/*
	Add filters
*/

add_filter('vtte_page_content_sidebar',array('edition','get_meta_page'),11,2);

class modules {
	static function article_item($post_id, $cut_title=false) {
		$metadata = get_post_meta($post_id,'article_only_download', true);
		$the_title  = ($cut_title) ? smart_substr(get_the_title($post_id),100) : get_the_title($post_id);
		echo '<article class="hentry entry-article">';
			if ($metadata) {
				$file_id = get_post_meta($post_id,'article_download',true);
			        try {
			               $file = new \GutenPress\Helpers\Attachment($file_id);
			            } catch ( Exception $e ) {
			                echo '<!-- Attachment: no existe -->';
			            }
			        
			        if (!empty($file)) {
			            $url = $file->url;
			        } else {
			            $url = '#';
			        }
			} else {
				$url = get_permalink($post_id);
			}
			echo '<h4 class="entry-title"><a href="'.$url.'">'.$the_title.'</a></h4>';
			if ($metadata) {
				echo '<a href="'.$url.'" class="button gray tiny">Descargar</a>';
			}
			echo edition::get_author($post_id);
		echo '</article>';
	}
	static function last_magazine($post=null) {
		if ( empty( $post ) ) {
			$item = current(revista::magazine_list(1,0));
		} else {
			$item = $post;
		}
		$file = edition::get_file($item->term_id);
		$metadata = edition::get_metadata($item->term_id);

		echo '<article class="hentry entry-last-magazine single">';
			echo edition::get_image($item->term_id,'featured-magazine');
			echo '<div class="content-box">';
				echo '<h4 class="entry-title">'.$item->name.'</h4>';
				echo '<div class="magazine-buttons">';
					if (!is_tax() && (!$metadata->only_download) ) {
						echo '<a href="'.edition::get_link($item->term_id).'" class="button">Ver Artículos</a>';
					}
					if ( is_object( $file ) ) {
						echo '<a href="'.$file->url.'" alt="'.$file->name.'" class="button">Descargar</a>';
					}
				echo '</div>';
			echo '</div>';
		echo '</article>';
		// echo '<div class="magazine-meta">';
		// 	echo '<ul class="menu vertical">';
		// 		if (!empty( $metadata->digital_issn )){
		// 			echo '<li><strong>ISSN Digital </strong> '.$metadata->digital_issn.'</li>';
		// 		}
		// 		if (!empty( $metadata->print_issn )) {
		// 			echo '<li><strong>ISSN Impreso </strong> '.$metadata->print_issn.'</li>';
		// 		}
		// 	echo '</ul>';
		// echo '</div>';
	}
	static function home_list($item) {
		$file = edition::get_file($item->term_id);
		$metadata = edition::get_metadata($item->term_id);


		echo '<article class="column hentry entry-publication">';
			echo '<div class="row collapse">';
				echo '<div class="large-5 columns">';
				if (!$metadata->only_download) {
					echo '<a href="'.edition::get_link($item->term_id).'">';
				}
				echo edition::get_image($item->term_id,'mini-magazine');
				if (!$metadata->only_download) {
					echo '</a>';
				}
				echo '</div>';
				echo '<div class="large-7 columns content">';
					echo '<h5 class="entry-title">'.$item->name.'</h5>';
					echo '<div class="magazine-buttons">';
						if (!$metadata->only_download) {
							echo '<a href="'.edition::get_link($item->term_id).'" class="button">Ver Artículos</a>';
						}
						if ( is_object( $file ) ) {
							echo '<a href="'.$file->url.'" alt="'.$file->name.'" class="button">Descargar</a>';
						}
					echo '</div>';
					if (!empty( $metadata->faculty )) {
						echo '<div class="disclaimer">';
							echo $metadata->faculty;
						echo '</div>';
					}
				echo '</div>';
			echo '</div>';
		echo '</article>';
	}
}
/*
	Custom link featured image
*/
	class linkMeta extends Model\PostMeta{
    protected function setId(){
        return 'link';
    }
    protected function setDataModel(){
    	global $link;
    	$options = get_option('link_img');
    	$link_image = $options[$link->link_id];
    	
        return array(
            new Model\PostMetaData(
                'image',
                '',
                '\GutenPress\Forms\Element\WPImage',
                array(
                	'value' => (!empty($link_image)) ? $link_image : ''
                )
            )
         );
    }
}
new Model\Metabox( 'LinkMeta', 'Imagen del link', 'link', array('priority' => 'high', 'context' => 'side') );
 add_action('edit_link', 'link_save_postdata', 10, 1);
 add_action('add_link', 'link_save_postdata', 10, 1);
 function link_save_postdata($link_id) {
 	if (is_admin() && (!empty($_POST['link-form']['image']))) {
 		$image = esc_attr($_POST['link-form']['image']);
 		$links = get_option( 'link_img' );
 		$links[$link_id] = $image;
 		update_option('link_img', $links);
 	}
 }

 // search filter
function make_search_filter($query) {
	$pt = $query->get('post_type');
	if ( empty( $pt ) ) {
		if ( !$query->is_admin && $query->is_search) {
			$query->set('post_type', array('post','article') ); // id of page or post
		}
	}
	return $query;
}
add_filter( 'pre_get_posts', 'make_search_filter' );

add_action('wp_head', 'vtte_magazine_article_headers');
function vtte_magazine_article_headers() {
	if (is_singular('article')) {
		global $post;
		//obtener autor desde donde corresponda
		$authors = get_post_meta($post->ID, 'article_person');

		$headers = array(
			'title' => 'Title',
			'alt_title' => 'Title.Alternative',
			'alt_title' => 'Creator.PersonalName',
			'author_email' => 'Creator.Address',
			'keyword_es' => 'Subject',
			'keyword_en' => 'Subject',
			'abstract_es' => 'Abstract',
			'abstract_en' => 'Abstract',
			'date_created' => 'Date.created',
			'date_issued' => 'Date.issued',
			'editor_name' => 'publisher.corporateName',
			'content_type' => 'content',
			'format' => 'format',
			'url' => 'identifier.URL',
			'language' => 'language',
			'edition' => 'Source',
			'magazine_issn' => 'Source.ISSN',
			'edition_number' => 'Source.Issue',
			'edition_volume' => 'Source.Volumne',
			'magazine_url' => 'Source.URI',
			'license' => 'DC.rights.license',
			'article_citation' => 'DCTERMS.bibliographicCitation',
			'title_citation' => 'citation_title',
			'author_citation' => 'citation_author',
			'year_citation' => 'citation_publication_date',
			'magazine_citation' => 'citation_journal_title',
			'issn_citation' => 'citation_issn',
			'volume_citation' => 'citation_volume',
			'number_citation' => 'citation_issue',
			'initial_page_citation' => 'citation_firstpage',
			'last_page_citation' => 'citation_lastpage',
			'pdf_url_citation' => 'citation_pdf_url',
			'orcid' => 'orcid_id',
			'doi' => 'doi_id'
			);
		echo PHP_EOL.'<!-- Inicio de cabeceras DC-->'. PHP_EOL;
		echo '<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />'.PHP_EOL;
		
		foreach ($authors as $author) {
			if (!empty($author)) {
				echo '<meta name="Creator.PersonalName" content="'.get_the_title($author).'">'. PHP_EOL;
			}
		}
		
		foreach ($headers as $key => $value) {
			$meta_value = get_post_meta($post->ID, 'dublin_'.$key, true);
			if (!empty($meta_value)) {
				echo '<meta name="'.$value.'" content="'.esc_attr($meta_value).'">'. PHP_EOL;
			}
		}
		echo '<!-- FIN de cabeceras DC-->' . PHP_EOL.PHP_EOL;
	}
}
//XML ARTICLE VIEW
// function revistas_rewrite(){
// 	global $wp_rewrite;
// 	$xml_tag = '%xml%';
// 	$wp_rewrite->add_rewrite_tag($xml_tag, '([0-9]+)', 'index.php?xml=');
// 	$xml_rewrite = array(
// 		'xml/([0-9]+)/?$' => 'index.php?xml=' . $wp_rewrite->preg_index(1)
// 	);
// 	$wp_rewrite->rules = $xml_rewrite + $wp_rewrite->rules;
// 	return $wp_rewrite->rules;
// }
// add_action('generate_rewrite_rules', 'revistas_rewrite');
function revista_custom_query_var($vars) {
  $vars[] = 'vista';
  return $vars;
}
add_filter( 'query_vars', 'revista_custom_query_var' );

function revista_template_redirect( $foo ){
	global $wp_query;
	$view = get_query_var('vista');
	if ( isset($view) && ($view == 'xml') ) {

		include STYLESHEETPATH . '/inc/xml-view.php';
		die();
	}
	return $foo;
}
add_action('template_redirect','revista_template_redirect');
add_filter('404_template', 'revista_template_redirect');

/* Update meta title on article*/
add_action( 'save_post', 'save_article_meta');
function save_article_meta($post_id) {
	if ($_POST['post_type'] == 'article') {
		update_post_meta($post_id,'dublin_title', esc_attr($_POST['post_title']));
	}
}
