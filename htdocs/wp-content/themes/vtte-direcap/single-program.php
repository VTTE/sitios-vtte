<?php 
	get_header(); 
	the_post();
	global $post;
	$type = programs::get_program_type($post);
	$date_init = get_post_meta($post->ID,'program_init',true);
	$jornada = get_post_meta($post->ID,'program_time',true);
	$duration = get_post_meta($post->ID,'program_duration',true);
	$topic = programs::get_program_topic($post);
?>
<div class="main-content">

	<div class="row">
		<div class="large-9 small-12 medium-12 columns">
			<header class="article-header hentry">
			<div class="epigraph">
				<span class="secondary-title"><?php echo $type ?></span> 
			</div>
				<div class="row">
					<?php if ( has_post_thumbnail( ) ): ?>
						<div class="large-3 small-12 medium-3 columns">
							<?php echo get_the_post_thumbnail( $post->ID, 'squared' ); ?>
						</div>
					<?php endif; ?>
					<div class="large-9 columns entry-header-title">
						<h2 class="entry-title"><?php the_title() ?></h2>
					</div>
				</div>
				<div class="header-meta clearfix">
					<div class="row meta-program">
						<?php 
							if (!empty($date_init)) {
								echo '<div class="large-3 small-3 medium-3 columns">';
									echo '<h5 class="mini-title">Fecha de inicio</h5>';
									echo $date_init;
								echo '</div>';
							}
							if (!empty($jornada)) {
								echo '<div class="large-3 small-3 medium-3 columns">';
									echo '<h5 class="mini-title">Jornada</h5>';
									echo $jornada;
								echo '</div>';
							}
							if (!empty($duration)) {
								echo '<div class="large-3 small-3 medium-3 columns">';
									echo '<h5 class="mini-title">Duración</h5>';
									echo $duration;
								echo '</div>';
							}
							if (!empty($topic)) {
								echo '<div class="large-3 small-3 medium-3 columns end">';
									echo '<h5 class="mini-title">Área temática</h5>';
									echo $topic;
								echo '</div>';
							}
						 ?>
					</div>
					<div class="buttons float-left">
					<?php
						$apply_url = get_post_meta($post->ID, 'program_apply_url', true);
						if (!empty($apply_url)) {
							echo '<a href="'.esc_url($apply_url).'" class="button">Postular</a>';
						}
						$file_id = get_post_meta($post->ID,'program_download',true);
				        try {
				                $file = new \GutenPress\Helpers\Attachment($file_id);
				            } catch ( Exception $e ) {
				                echo '<!-- Attachment: no existe -->';
				            }
				        
				        if (!empty($file)) {
				            $url_file = $file->url;
				        } else {
				            $url_file = '#';
				        }
				        $extension = (!empty($file->pathinfo->extension)) ? $file->pathinfo->extension : 'PDF';
					 ?>
						<?php if ( !empty( $file_id ) ): ?>
							<a href="<?php echo $url_file ?>" class="button green">Descargar resumen (<?php echo $extension; ?>)</a>
						<?php endif; ?>
					</div>
					<div class="social float-right">
						<?php get_template_part('inc/partials/share','buttons') ?>
					</div>
				</div>
			</header>
		
	<div class="row inner-space">
		<div class="large-3 columns" data-sticky-container>
			<div class="local-navigation sticky" data-sticky data-anchor="content-navigation" data-options="marginTop:2">
				<!-- <h4>Secciones</h4> -->
				<ul class="local-nav menu vertical">
				<!-- Links added by javascript -->
				</ul>
			</div>
		</div>
		<div class="large-9 columns">
			<div class="std-text" id="content-navigation">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</div>	
		<div class="large-3 columns">
			<?php 
				$params = array(
					'bottom_info' => false,
					'selected' => $post->ID
					);
				echo programs::get_program_form($params);
			 ?>
			 
			<?php dynamic_sidebar('program') ?>
		</div>
	</div>

</div>
<?php get_footer(); ?>