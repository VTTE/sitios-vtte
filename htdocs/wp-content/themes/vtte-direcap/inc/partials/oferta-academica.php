<div class="row">
    <div class="columns large-12 program_list">
        <div class="clearfix">
            <h2 class="primary-title float-left">Oferta de educación continua</h2>
            <a href="<?php echo site_url('programas') ?>" class="all float-right">Ver todas las ofertas</a>
        </div>
        <div class="large-4 columns">
            <aside class="program-type">
                <h4 class="program-type-name postitulos">Postítulos</h4>
                <?php programs::get_programs('postitulos'); ?>
            </aside>
        </div>
        <div class="large-4 columns">
            <aside class="program-type">
                <h4 class="program-type-name diplomados">Diplomados</h4>
                <?php programs::get_programs('diplomados'); ?>
            </aside>
        </div>
        <div class="large-4 columns">
            <aside class="program-type">
                <h4 class="program-type-name cursos">Cursos</h4>
                <?php programs::get_programs('cursos'); ?>
            </aside>
        </div>
    </div>
</div>