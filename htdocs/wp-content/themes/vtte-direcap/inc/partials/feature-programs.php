<div class="row">
    <div class="large-12 columns">
        <div class="widget sidebar-title">
            <h4 class="widget-title">Destacamos</h4>
        </div>
        <?php 
            $featured_postitle = programs::get_featured(1, 'postitulos');
            $featured_diploma = programs::get_featured(1, 'diplomados');
            $featured_course = programs::get_featured(1, 'cursos');
            echo '<div class="large-up-3 small-up-1 inline-lists clearfix">';
            if ($featured_postitle->have_posts()) {
                $current_program = current($featured_postitle->posts);
                echo '<div class="column">';
                echo pRender::program($current_program);
                echo '</div>';
            }
            if ($featured_diploma->have_posts()) {
                $current_program = current($featured_diploma->posts);
                echo '<div class="column">';
                echo pRender::program($current_program);
                echo '</div>';
            }
            if ($featured_course->have_posts()) {
                $current_program = current($featured_course->posts);
                echo '<div class="column">';
                echo pRender::program($current_program);
                echo '</div>';
            }
            echo '</div>';
            ?>
    </div>
</div>