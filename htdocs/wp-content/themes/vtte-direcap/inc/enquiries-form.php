<?php

use \GutenPress\Model as Model;

class EnquiryPostType extends \GutenPress\Model\PostType
{
    /**
     * Set post_type value
     * @return string
     */
    protected function setPostType()
    {
        return 'direcap-enquiries';
    }

    /**
     * Set post type object properties
     * @return array
     */
    protected function setPostTypeObject()
    {
        return array(
            'label' => _x('Consultas', 'enquiry', 'cpt_enquiry'),
            'labels' => array(
                'name' => _x('Consultas', 'enquiry', 'cpt_enquiry'),
                'singular_name' => _x('Consulta', 'enquiry', 'cpt_enquiry'),
                'add_new' => _x('Agregar nuevo Consulta', 'enquiry', 'cpt_enquiry'),
                'all_items' => _x('Consultas', 'enquiry', 'cpt_enquiry'),
                'add_new_item' => _x('Agregar nuevo Consulta', 'enquiry', 'cpt_enquiry'),
                'edit_item' => _x('Editar Consulta', 'enquiry', 'cpt_enquiry'),
                'new_item' => _x('Nuevo Consulta', 'enquiry', 'cpt_enquiry'),
                'view_item' => _x('Ver Consulta', 'enquiry', 'cpt_enquiry'),
                'search_items' => _x('Buscar Consultas', 'enquiry', 'cpt_enquiry'),
                'not_found' => _x('No se han encontrado Consultas', 'enquiry', 'cpt_enquiry'),
                'not_found_in_trash' => _x('No se han encontrado Consultas en la papelera', 'enquiry', 'cpt_enquiry'),
                'parent_item_colon' => _x('Consulta padre', 'enquiry', 'cpt_enquiry'),
                'menu_name' => _x('Consultas', 'enquiry', 'cpt_enquiry')
            ),
            'description' => _x('', 'direcap-enquiries', 'cpt_enquiry'),
            'public' => false,
            'exclude_from_search' => false,
            'publicly_queryable' => false,
            'show_ui' => true,
            'show_in_nav_menus' => false,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => null,
            'menu_icon' => 'dashicons-format-aside',
            'capability_type' => array('direcap-enquiry', 'direcap-enquiries'),
            'hierarchical' => true,
            'supports' => array('title'),
            'has_archive' => false,
            'rewrite' => false,
            'query_var' => true,
            'can_export' => false
        );
    }
}

add_action('init', 'ActivateEnquiryPostType', 11);
add_action('restrict_manage_posts', 'add_filter_enquiries');

function add_filter_enquiries(){
    global $typenow;
    $post_type = 'direcap-enquiries';
    if ($typenow == $post_type) {
        $query_url = add_query_arg( 'export_csv', 'true', $_SERVER['REQUEST_URI'] );
        echo '<a href="'.$query_url.'" class="button">Exportar a CSV</a>';
    }
}

function ActivateEnquiryPostType() {
    EnquiryPostType::activatePlugin();
    EnquiryPostType::registerPostType();
}
class EnquiryQuery extends Model\PostQuery
{
    protected function setPostType()
    {
        return 'enquiry';
    }
    protected function setDecorator()
    {
        return 'EnquiryObject';
    }
}

class EnquiryObject extends Model\PostObject
{
    // controller methods
}
class EnquiryMeta extends Model\PostMeta
{
    protected function setId()
    {
        return 'enquiry';
    }
    protected function setDataModel()
    {
        return array(
            new Model\PostMetaData(
                'email',
                'Email',
                '\GutenPress\Forms\Element\InputEmail',
                array(
                    'disabled' => 'disabled'
                )
            ),
            new Model\PostMetaData(
                'phone',
                'Teléfono',
                '\GutenPress\Forms\Element\InputText',
                array(
                    'disabled' => 'disabled'
                )
            ),
            new Model\PostMetaData(
                'program',
                'Programa',
                '\GutenPress\Forms\Element\InputText',
                array(
                    'disabled' => 'disabled'
                )
            )
        );
    }
}
new Model\Metabox('EnquiryMeta', 'Datos solicitud', 'direcap-enquiries', array('priority' => 'high'));

add_filter('manage_direcap-enquiries_posts_columns', 'direcap_enquiries_custom_post_type_columns');
add_action('manage_direcap-enquiries_posts_custom_column', 'direcap_enquiries_fill_custom_post_type_columns', 10, 2);

function direcap_enquiries_fill_custom_post_type_columns( $column, $post_id ) {
    switch( $column ) {
        case 'mail' :
            echo get_post_meta( $post_id, 'enquiry_email', true );
        break;
        case 'phone':
            echo get_post_meta($post_id, 'enquiry_phone', true);
        break;
        case 'program':
            echo get_post_meta($post_id, 'enquiry_program', true);
        break;
    }
}
function direcap_enquiries_custom_post_type_columns( $columns ) {
    $columns['mail'] = 'Email';
    $columns['phone'] = 'Telefono';
    $columns['program'] = 'Programa';
    return $columns;
}

add_action('admin_init', 'export_csv_enquiries');
function export_csv_enquiries() {
    if ( isset($_GET['export_csv']) && $_GET['export_csv'] == 'true') {
        if (current_user_can('manage_options')) {
            header('Content-type: text/csv');
            header('Content-Disposition: attachment; filename="users' . date('YmdHis') . '.csv"');
            header('Pragma: no-cache');
            header('Expires: 0');

            $file = fopen('php://output', 'w');
            fputcsv($file, array('Nombre', 'Email', 'Telefono', 'Programa', 'Fecha'));

            $query = new WP_Query(array(
                'post_type' => 'direcap-enquiries',
                'post_status' => 'publish'
            ));
            foreach ($query->posts as $entry) {
                $email = get_post_meta($entry->ID, 'enquiry_email', true);
                $phone = get_post_meta($entry->ID, 'enquiry_phone', true);
                $program = get_post_meta($entry->ID, 'enquiry_program', true);
                fputcsv($file, array(get_the_title($entry->ID), $email, $phone, $program, $entry->post_date));
            }
            exit();
        }
    }
}