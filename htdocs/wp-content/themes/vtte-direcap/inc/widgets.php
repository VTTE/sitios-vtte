<?php

class WP_Widget_form extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'form-info', 'description' => 'Muestra el formulario de solicitud de información');
        $control_ops = array();
        parent::__construct('form-info', 'Formulario solicitud información', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        global $post;
        extract( $instance );

       $params = array(
            'bottom_info' => $is_bottom,
            'selected' => ((get_class(get_queried_object()) == 'WP_Post') ? $post->ID : false)
            );
       if (!empty($instance['title'])) {
            $params['title'] = $instance['title'];
       }
       if (!empty($instance['subtitle'])) {
            $params['subtitle'] = $instance['subtitle'];
       }
       if (get_class(get_queried_object()) == 'WP_Term') {
        if (get_queried_object()->taxonomy == 'program_type') {
            $params['program_type'] = get_queried_object()->slug;
            $params['program_topic'] = false;
        }
        if (get_queried_object()->taxonomy == 'program_topic') {
            $params['program_topic'] = get_queried_object()->slug;
            $params['program_type'] = false;
        }
       }
        echo programs::get_program_form($params);
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
       echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
       echo '<p><label for="'.$this->get_field_id('subtitle').'">Subtitulo: <input type="text" name="'. $this->get_field_name('subtitle') .'" id="'.$this->get_field_id('subtitle').'" value="'.$instance['subtitle'].'" class="widefat" /></label></p>';
       echo '<p><label for="'. $this->get_field_name('is_bottom').'">Mostrar teléfono y mail? </label><input type="checkbox" id="'. $this->get_field_id('is_bottom').'"'.( ( !empty( $is_bottom ) ) ? ' checked="checked" ' : '' ).' name="'.$this->get_field_name('is_bottom').'" value="1"></p>';
       } 
}
class WP_Widget_program_list extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'program_list', 'description' => 'Muestra el listado de programas según su tipo');
        $control_ops = array();
        parent::__construct('program_list', 'Listado de programas', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        $terms = get_terms('program_type');
        foreach ($terms as $term) {
            if ( ( get_class( get_queried_object() ) == 'WP_Term') && (get_queried_object()->term_id == $term->term_id ) ) { continue; } 
            $term_link = get_term_link( $term, 'program_type' );
            echo '<div class="widget program_list">';
                echo '<aside class="program-type">';
                    echo '<h4 class="program-type-name '.$term->slug.'"><a href="'.$term_link.'">'.$term->name.'</a></h4>';
                    programs::get_programs($term->slug);
                echo '</aside>';
            echo '</div>';
        }
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
        echo '<p>Este widget no tiene configuraciones.</p>';
       } 
}

// register WP_Widgets
add_action('widgets_init', 'register_direcap_widgets');
add_action('init', 'register_direcap_widgets');
function register_direcap_widgets(){
    register_widget('WP_Widget_form');
    register_widget('WP_Widget_program_list');
}