<?php 
	get_header();
?>
<section class="main-content">
	<header class="row entry-header">
		<div class="large-12 columns">
			<aside class="program-type">
		    	<h4 class="program-type-name <?php echo get_queried_object()->slug ?>"><?php echo get_queried_object()->name ?></h4>
		    </aside>
	    </div>
    </header>
    <div class="row inner-space">
    	<div class="large-9 columns">
    		<?php 
    			if ( have_posts() ) {
    				echo '<div class="large-up-3 small-up-1 clearfix">';
	    			while ( have_posts() ): the_post();
	    				global $post;
	    				echo '<div class="column">';
	    					echo pRender::program_tax_list($post,'program_topic');
	    				echo '</div>';
	    			endwhile;
	    			if (function_exists('wp_pagenavi')) {
	    				wp_pagenavi();
	    			}
	    			echo '</div>';
	    		}
    		 ?>
    	</div>
    	<div class="large-3 columns">
    		<aside class="sidebar">
    			<?php dynamic_sidebar('tax_program_type'); ?>
    		</aside>
    	</div>
    </div>
</section>
<?php get_footer(); ?>