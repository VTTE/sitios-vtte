<?php
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;

add_image_size('program-list', 270, 110, true);
add_image_size('slide-feature', 780, 400, true);
/*
	Calling some files
*/

include STYLESHEETPATH . '/inc/enquiries-form.php';
include STYLESHEETPATH . '/inc/widgets.php';

add_action( 'wp_enqueue_scripts', 'enqueue_direcap_scripts' );
function enqueue_direcap_scripts() {
	wp_enqueue_script( 'direcap_script', PRE_HOME_URI .'/vtte-direcap/js/script.js', array('jquery'), '1.0', '' );
}

/*
	Sidebars filters
*/
add_filter('vtte_base_mandatory_sidebars', array('programs','remove_base_sidebars'));
/*
	Theme sidebars
*/
$mandatory_sidebars = array(
	'Archivo Programas' => array(
		'name' => 'archive_program',
		'size' => 3
		),
	'Programa sidebar' => array(
		'name' => 'program',
		'size' => 3
		),
	'Tipo de programa' => array(
		'name' => 'tax_program_type',
		'size' => 3
		),
	'Área temática' => array(
		'name' => 'tax_program_topic',
		'size' => 3
		),
	'Home fila 1' => array(
		'name' => 'sidebar-home-1',
		'size' => 12
		),
	'Home fila 2' => array(
		'name' => 'sidebar-home-2',
		'size' => 12
		),
	'Footer Página' => array(
		'name' => 'page_footer_large',
		'size' => 12
		),
	'Footer Artículo' => array(
		'name' => 'single_footer_large',
		'size' => 12
		),
);
foreach ( $mandatory_sidebars as $sidebar => $id_sidebar ) {
	register_sidebar( array(
		'name'          => $sidebar,
		'id'			=> $id_sidebar['name'],
		'before_widget' => '<section id="%1$s" class="widget %2$s">'."\n",
		'after_widget'  => '</section>',
		'before_title'  => '<header class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3></header>',
		'size' => $id_sidebar['size']
	) );
}

class programs {
	static function remove_base_sidebars($sidebars) {
		unset($sidebars['Página footer 1']);
		unset($sidebars['Página footer 2']);
		unset($sidebars['Página footer 3']);
		unset($sidebars['Entrada footer 1']);
		unset($sidebars['Entrada footer 2']);
		unset($sidebars['Entrada footer 3']);
		return $sidebars;
	}
	static function get_program_type($post) {
		$term = current(wp_get_post_terms( $post->ID, 'program_type' ));
		return $term->name;
	}
	static function get_program_topic($post) {
		$term = current(wp_get_post_terms( $post->ID, 'program_topic' ));
		$link = ( !is_wp_error( get_term_link( $term, 'program_topic' ) ) ? get_term_link( $term, 'program_topic' ) : '#');
		return '<a href="'.$link.'">'.$term->name.'</a>';
	}
	static function get_programs($type) {
		if ( !empty( $type ) ) {
			
			$topics = get_terms('program_topic', array('hide_empty' => false));
			echo '<select class="program-select" name="program">';
				echo '<option value="">Elegir área</option>';
				if ( !empty( $topics) ) {
					foreach ( $topics as $topic ) {
						$link = get_term_link($topic);
						echo '<option value="'.$link.'?oferta='.$type.'">'.$topic->name.'</option>';
					}
				}
			echo '</select>';
		}
	}
	static function get_featured($size=3,$taxonomy) {
		$programs = new WP_Query(array(
					'post_type' => 'program',
					'post_status' => 'publish',
					'posts_per_page' => $size,
					'meta_query' => array(
						array(
							'key' => 'program_featured',
							'value' => 1
							)
						),
					'tax_query' => array(
						array(
							'taxonomy' => 'program_type',
							'field' => 'slug',
							'terms' => $taxonomy
							)
						)
				));
		if (count($programs) > 0) {
			return $programs;
		} else {
			return false;
		}
	}
	static function get_topics_list($format='list') {
		$terms = get_terms('program_topic');
		$title = ( $format == 'list' ) ? '<h3 class="primary-title">Todas las áreas</h3>' : 'Áreas temáticas';
		$begin_list = ( $format == 'list' ) ? '<ul class="menu vertical">' : '<div class="large-up-8 small-up-2">';
		$end_list = ( $format == 'list' ) ? '</ul>' : '</div>';
		if (!empty($terms)) {
			echo '<div class="themes">';
				echo '<h3 class="primary-title">'.$title.'</h3>';
				echo $begin_list;
				foreach ($terms as $term) {
					echo ( ($format == 'list') ? pRender::topic_list_item($term) : pRender::topic_img_item($term) );
				}
				echo $end_list;
			echo '</div>';
		}
	}
	static function get_program_form($params=null) {
		$defaults = array(
				'title' => 'Solicitar información',
				'subtitle' => '¿Te interesa una de nuestras ofertas?',
				'button_text' => 'Solicitar',
				'bottom_info' => true,
				'phone' => '(+56 2) 2787 7758',
				'mail' => 'capacitacion@utem.cl',
				'program_type' => 'all',
				'program_topic' => false,
				'selected' => false
			);
		$params = wp_parse_args( $params, $defaults );
		return pRender::form($params);
	}
	static function get_all_programs($selected) {
		$terms = get_terms('program_type');
		$out = '';
		foreach ( $terms as $term ) {
			$out .= '<optgroup label="'.$term->name.'">';
				$query = new ProgramQuery(array(
					'posts_per_page' => -1,
					'post_status' => 'publish',
					'tax_query' => array(
						array(
							'taxonomy' => 'program_type',
							'field' => 'slug',
							'terms' => $term->slug
							)
						)
					));
				if (count($query) > 0) {
					foreach ( $query as $item ) {
						$selected_class = (!empty($selected) && ($item->ID == $selected)) ? ' selected="selected"' : '';
						$out .= '<option value="'.$item->ID.'"'.$selected_class.'>'.get_the_title($item->ID).'</option>';
					}
				}
			$out .= '</optgroup>';
		}
		return $out;
	}
	static function get_program_type_select($type) {
		$out = '';
		$term = get_term_by( 'slug', $type, 'program_type' );
		$out .= '<optgroup label="'.$term->name.'">';
			$query = new ProgramQuery(array(
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'tax_query' => array(
					array(
						'taxonomy' => 'program_type',
						'field' => 'slug',
						'terms' => $term->slug
						)
					)
				));
			if (count($query) > 0) {
				foreach ( $query as $item ) {
					$out .= '<option value="'.$item->ID.'">'.get_the_title($item->ID).'</option>';
				}
		$out .= '</optgroup>';
		}
		return $out;
	}
	static function get_program_topic_select($topic) {
		$out = '';
		$term = get_term_by('slug', $topic, 'program_topic' );
		$out .= '<optgroup label="'.$term->name.'">';
			$query = new ProgramQuery(array(
				'posts_per_page' => -1,
				'post_status' => 'publish',
				'tax_query' => array(
					array(
						'taxonomy' => 'program_topic',
						'field' => 'slug',
						'terms' => $term->slug
						)
					)
				));
			if (count($query) > 0) {
				foreach ( $query as $item ) {
					$out .= '<option value="'.$item->ID.'">'.get_the_title($item->ID).'</option>';
				}
		$out .= '</optgroup>';
		}
		return $out;
	}
	function send_mail() {
		if (check_ajax_referer( 'ask_information','sec' ) && ( empty( $_POST['lastname'] ) ) ) {
			global $_set;
			$settings = $_set->settings;
			$name = esc_attr($_POST['name']);
			$mail = esc_attr($_POST['mail']);
			$phone = esc_attr($_POST['phone']);
			$program_id = esc_attr($_POST['program']);
			$program_name = get_the_title($program_id);
			$program_link= get_permalink($program_id);
			$array_mails = explode(',',$settings['info_mail']);
			if (count($array_mails) == 1) {
				$send_mail = ( !empty( $settings['info_mail'] ) ) ? $settings['info_mail'] : get_option('admin_email');
			}
			if (!empty($name) && !empty($mail) && !empty($program_id) ) {
				add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
				$message = '<small>Este correo ha sido enviado desde el sitio web de la Direcap</small><br><br>';
				$message .= '<h1>Solicitud de información</h1>';
				$message .= '<p>'.$name.' Ha solicitado información relativa al programa:<a href="'.$program_link.'">'. $program_name.'</a></p>';
				$message .= '<p>Puede contactarlo utilzando los siguientes datos</p>';
				$message .= '<p><strong>Mail</strong>: '.$mail.'</p>';
				if (!empty($phone)) {
					$message .= '<p><strong>Teléfono</strong>: '.$phone.'</p><br><br>';
				}
				$message .= '<p>Para revisar el programa solicitado haga click <a href="'.$program_link.'">aqui</a></p>';

				$subject = '['.get_bloginfo('name').'] solicitud de información del programa: '.$program_name;

				$headers = array(
					'From: "'.$name.'" <'.$mail.'>'
					);
				if (count($array_mails > 1)) {
					$send = 0;
					foreach ($array_mails as $the_mail) {
						if (wp_mail(trim($the_mail), $subject, $message,$headers)) {
							$send++;
						} 
					}
					if ($send == count($array_mails)) {
						self::insert_enquiry($name, $phone, $mail, $program_name );
						echo 1;
					} else {
						echo 'mail error';
					}
				} else {
					$to = $send_mail;
					if (wp_mail($to, $subject, $message,$headers)) {
						self::insert_enquiry($name, $phone, $mail, $program_name);
						echo 1;
					} else {
						echo 'mail error';
					}
				}

			} else {
				echo 'error';
				exit();
			}

			exit();
		} else {
			echo 'Error ¬¬';
			exit(0);
		}

	}
	static function insert_enquiry($name, $phone, $mail, $program) {
		$array_post = array(
			'post_type' => 'direcap-enquiries',
			'post_status' => 'publish',
			'post_title' => $name
		);
		$post_id = wp_insert_post( $array_post );
		if (!empty($post_id)) {
			update_post_meta( $post_id, 'enquiry_email', $mail );
			update_post_meta( $post_id, 'enquiry_phone', $phone);
			update_post_meta( $post_id, 'enquiry_program', $program);
		}
	}
	static function get_topics($term_id = null) {
		$topics = get_terms('program_topic', array( 'hide_empty' => false ));
		if ( !empty( $topics ) ) {
			$sidebar .= '<div class="themes topic-list">';
				$sidebar .= '<h3 class="primary-title">Todas las áreas</h3>';
				$sidebar .= '<ul class="menu vertical">';
				foreach ( $topics as $topic ) {
					
					$active = ($topic->term_id == $term_id) ? true : false;
					$sidebar .= pRender::topics($topic, $active);
				}
				$sidebar .= '</ul>';
			$sidebar .= '</div>';
			return $sidebar;
		} else {
			return false;
		}
	}
	static function get_select_elements($args) {
		extract($args);
		if ( ( $args['program_type'] == 'all') && ( $args['program_topic'] == false ) ) {
			return programs::get_all_programs($selected);
		} else if ( ( !empty( $args['program_type'] ) ) && ( $args['program_topic'] == false ) ) {
			return programs::get_program_type_select($args['program_type']);
		} else if ( ( !empty( $args['program_topic'] ) ) && ( $args['program_type'] == false ) )
			return programs::get_program_topic_select($args['program_topic']);
	}

	static function get_meta_page($sidebar, $post) {
		if ( is_object( $sidebar ) ) {
			$post = $sidebar;
			$sidebar = '';
		}
		$icon = ( !empty( $post->page_icon ) ) ? $post->page_icon : 'info';

		if ( !empty( $post->page_info ) ) {

			$sidebar .= '<div class="widget-single-info margin-bottom-sm">';
				$sidebar .= '<div class="info">';
					$sidebar .= '<h5 class="bold condensed uppercase">'.$post->page_title.'</h5>';
				$sidebar .= '</div>';
			$sidebar .= '</div>';

			$sidebar .= '<div class="widget-single-info">';
				$sidebar .= '<div class="icon">';
					$sidebar .= '<i class="dashicons dashicons-'.$icon.'"></i>';
				$sidebar .= '</div>';
				$sidebar .= '<div class="info page-sidebar-text">';
					$sidebar .= apply_filters( 'the_content', $post->page_info );
				$sidebar .= '</div>';
			$sidebar .= '</div>';
			return $sidebar;
		} else {
			return false;
		}
	}
	public function add_form_config($form, $data) {
		$form->addElement( new Element\InputText(
				_x('Correo de recepción solicitudes', 'site settings fields', 'vtte'),
				'info_mail',
				array(
					'value' => isset($data['info_mail']) ? $data['info_mail'] : ''
				)
			) );
		return $form;
	}
	public function add_form_config_field($fields) {
		$fields[] = 'info_mail';
		return $fields;
	}
}
add_filter('vtte_main_configuration_form',array('programs','add_form_config'),10,2);
add_filter('vtte_main_configuration_fields',array('programs','add_form_config_field'),10,1);
add_filter('vtte_page_content_sidebar',array('programs','get_meta_page'),11,2);
/*
	AJAX CALLS
*/
add_action('wp_ajax_nopriv_ask_information', array('programs','send_mail'));
add_action('wp_ajax_ask_information', array('programs','send_mail'));

class pRender {
	static function form($args) {
		$select_elements = programs::get_select_elements($args);
		extract($args);
		$out = '<div class="info-box">';
			$out .= '<h3 class="secondary-title">'.$title.'</h3>';
			$out .= '<div class="program-form">';
				$out .= '<h5 class="form-title">'.$subtitle.'</h5>';
				$out .= '<form action="" method="POST" class="program-info valid-form" data-abide novalidate>';
					$out .= '<div data-abide-error class="alert callout error-message" style="display: none;">';
					    $out .= '<p><span class="dashicons dashicons-dismiss"></span> Hay errores en el envío del formulario.</p>';
					  $out .= '</div>';
					  $out .= '<div class="success callout success-message" style="display: none;">';
					    $out .= '<p><span class="dashicons dashicons-yes"></span> Formulario enviado, te contactaremos a la brevedad.</p>';
					  $out .= '</div>';
					$out .= '<label>';
						$out .= '<input type="text" name="name" class="input-text name-input" placeholder="Nombre" required>';
						$out .= '<span class="form-error">Debe especificar un nombre...</span>';
					$out .= '</label>';
					$out .= '<label>';
						$out .= '<input type="email" name="mail" class="input-text mail-input" placeholder="Correo" required>';
						$out .= '<span class="form-error">Debe especificar un correo para contacto...</span>';
					$out .= '</label>';
					$out .= '<label>';
						$out .= '<input type="text" name="phone" class="input-text phone-input" placeholder="Teléfono">';
					$out .= '</label>';
					$out .= '<label>';
						$out .= '<select name="program" id="program" class="program-select-form" required>';
							$out .= '<option value="">Seleccionar Oferta</option>';
							//$out .= ( $program_type == 'all') ? programs::get_all_programs($selected) : programs::get_program_type_select($program_type);
							$out .= $select_elements;
						$out .= '</select>';
					$out .= '</label>';
					$out .= wp_nonce_field( 'ask_information', 'ask_information_nonce', true, false );
					$out .= '<input type="text" name="last_name" class="input-text lastname-input" placeholder="last name">';

					$out .= '<input type="submit" class="button expanded green send-form" value="'.$button_text.'">';
					if ($bottom_info) {
						$out .= '<div class="bottom-info clearfix">';
							$out .= '<span class="float-left">';
								$out .= '<span class="dashicons dashicons-phone"></span> '.$phone;
							$out .= '</span>';
							$out .= '<span class="float-right">';
								$out .= '<span class="dashicons dashicons-email-alt"></span> '.$mail;
							$out .= '</span>';
						$out .= '</div>';
					}
				$out .= '</form>';
			$out .= '</div>';
		$out .= '</div>';
		return $out;
	}
	static function topic_img_item($term) {
		$tax_image = get_term_meta($term->term_id, 'tax_image', true);			

		$term_link = !is_wp_error(get_term_link( $term, 'program_topics' )) ? get_term_link( $term, 'program_topics' ) : '#';
		$out = '<div class="columns">';
			$out .= '<article class="entry-topic hentry">';
				$out .= '<a href="'.$term_link.'">';
					if (!empty($tax_image)) {
						$out .= wp_get_attachment_image($tax_image, 'thumbnail');
					}
					//$out .= get_the_post_thumbnail( $post->ID, 'thumbnail' );
					$out .= '<span class="wrap-content">';
						$out .= '<h5 class="entry-title">'.$term->name.'</h5>';
					$out .= '</span>';
				$out .= '</a>';
			$out .= '</article>';
		$out .= '</div>';
		return $out;
	}
	static function topic_list_item($term) {
		$term_link = get_term_link( $term, 'program_topics' );
		$out = '<li>';
			$out .= '<a href="'.$term_link.'">'.$term->name.'</a>';
		$out .= '</li>';
		return $out;
	}
	static function program($post) {
		$type = wp_get_post_terms( $post->ID, 'program_type' );
		$out = '<article class="hentry entry-publication">';
			$out .= '<a href="'.get_permalink($post->ID).'">';
				$out .= '<div class="entry-image">';
					$out .= get_the_post_thumbnail( $post->ID, 'thumbnail' );
				$out .= '</div>';
			$out .= '</a>';
			$out .= '<div class="entry-content">';
				$out .= '<a href="'.get_permalink($post->ID).'"><h4 class="entry-title">'.get_the_title($post->ID).'</h4></a>';
				if (!empty($type)) {
					$current_type = current($type);
					$link = get_term_link( $current_type, 'program_type' );
					$out .= '<div class="categories">';
						$out .= '<a href="'.$link.'">'.$current_type->name.'</a>';
					$out .= '</div>';
				}
			$out .= '</div>';
		$out .= '</article>';
		return $out;
	}
	static function program_tax_list($post,$term='program_topic') {
		$short_title = get_post_meta($post->ID,'program_short_title', true);
		$out = '<article class="hentry entry-program">';
			$out .= '<div class="row collapse">';
				$out .= '<div class="large-5 columns">';
					$out .= '<a href="'.get_permalink($post->ID).'">'.get_the_post_thumbnail( $post->ID, 'thumbnail' ).'</a>';
				$out .= '</div>';
				$out .= '<div class="large-7 columns">';
				$the_title = (!empty($short_title)) ? esc_attr($short_title) : get_the_title($post->ID);
				$out .= '<h4 class="entry-title"><a href="'.get_permalink($post->ID).'">'.$the_title.'</a></h4>';
				$topic = wp_get_post_terms( $post->ID, $term );
				if ( !empty( $topic ) ) {
					$current_term = current($topic);
					$term_link = get_term_link( $current_term, $term );
					$out .= '<div class="program-topic">';
						$out .= '<a href="'.$term_link.'">'.$current_term->name.'</a>';
					$out .= '</div>';
				}
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</article>';
		return $out;
	}
	static function topics( $item, $active ) {
		$link = ( !is_wp_error( get_term_link( $item, 'program_topic' ) ) ) ? get_term_link( $item, 'program_topic' ) : '#';
		$active = ( !empty( $active ) ) ? ' class="active"':'';
		return '<li'.$active.'><a href="'.$link.'">'.$item->name.'</a></li>';
	}
}