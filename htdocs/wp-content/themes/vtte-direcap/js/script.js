 /*
    String to slug
    http://dense13.com/blog/2009/05/03/converting-string-to-slug-javascript/
*/
function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();
  
  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to   = "aaaaeeeeiiiioooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
}

jQuery(document).on("forminvalid.zf.abide", function(ev,frm) {
    return false;
  }).on("formvalid.zf.abide", function(ev,frm) {
  	var name = frm.find('.name-input').val(),
  		mail = frm.find('.mail-input').val(),
  		phone = frm.find('.phone-input').val(),
  		program = frm.find('.program-select-form option:selected').val(),
  		lastname = frm.find('.lastname-input').val(),
  		sec = frm.find('#ask_information_nonce').val();
  	jQuery.ajax({
  		url: Ajax.url,
  		type: 'POST',
  		data: {
  			action: 'ask_information',
  			name: name,
  			mail: mail,
  			phone: phone,
  			program: program,
  			lastname: lastname,
  			sec: sec
  		},
  		beforeSend: function(){
  			jQuery('.send-form').val('ENVIANDO...');
  		},
  		success: function(data){
  			if (data == 'mail error') {
  				jQuery('.error-message').show();
  			} else if (data == 1) {
          jQuery('.send-form').val('Formulario enviado');  
  				jQuery('.send-form').attr('disabled','disabled');
  				jQuery('.success-message').show();
  			}
  		}
  	});
  	console.log();
    return false;
  });
jQuery(document).ready(function($) {
	//Scripts
	$('.program-info').on('submit',function(e) {
		e.preventDefault();
		return false;
	});
	$('.program-select').on('change',function(e){
		var url = $(this).val();
		location.href=url;
	});
	$('.single-program .std-text').find('h4').each(function(){
        var obj = $(this);
        var slug = string_to_slug(obj.text());

        $('<a name="'+slug+'"></a>').insertBefore(obj);
        //obj.attr('id',slug);
        var h6 = $('<h6>').addClass('xs').append(obj.text());
        var link = $('<a>').attr('href','#'+slug).append(h6);
        var li = $('<li>').addClass("local-item").append(link);
        $('.local-nav').append(li);
    });

    $('.local-nav').find('a').on('click',function(event){
		event.preventDefault();
		var obj = $(this);
		var anchor = $('a[name='+obj.attr('href').replace('#','')+']' ).offset().top;
		$('html, body').stop().animate({
            scrollTop: anchor
        }, 800);
	});
	$('.feature-list').slick({
		slidesToShow: 1,
		autoplay: true,
		autoplaySpeed: 5000
	});
	// var elem = new Foundation.Sticky($('.local-navigation'),{
	// 	stickTo: 'top',
	// 	anchor : 'content-navigation'
	// });

});