<?php 
	get_header();
	$search = new search_filter();
	$search->set_post_type('program');
	if (get_query_var('paged')) {
		$search->set_page(get_query_var('paged'));
	}
	if (isset($_GET['action'])) {
		$taxonomies = [];
		if (isset($_GET['program_type']) && ($_GET['program_type'] != '')) {
			$taxonomies['program_type'] = esc_attr($_GET['program_type']);
		}
		if (isset($_GET['program_topic']) && ($_GET['program_topic'] != '')) {
			$taxonomies['program_topic'] = esc_attr($_GET['program_topic']);
		}
		$search->set_array_taxonomies($taxonomies);
	}
	$the_query = $search->search();
	//echo '<pre>'; print_r($the_query); echo '</pre>';
?>
<section class="main-content">
	<header class="row entry-header">
		<div class="large-12 columns">
			  <h3 class="entry-title tax-title">Programas <?php bloginfo('name'); ?></h3>
	    </div>
    </header>
	<header class="row section-header">
		<div class="large-12 columns">
			<h3 class="secondary-title"><?php sitio::page_title() ?> </h3>
			<div class="filter-form">
				<form action="" method="GET">
					<div class="row">
						<div class="large-2 small-12 medium-2 columns">
							<h5 class="form-title">Buscar Por</h5>
						</div>
						<div class="large-4 small-12 medium-4 columns">
							<select name="program_topic" class="input-type">
								<option value="">Tipo de programa</option>
								<?php 
							$terms = get_terms('program_topic');
							foreach ($terms as $term) {
								$selected = (!empty($_GET['program_topic']) && ($_GET['program_topic'] == $term->slug)) ? ' selected="selected" ' : '';
								echo '<option value="' . $term->slug . '"' . $selected . '>' . $term->name . '</option>';
							}
							?>
							</select>
						</div>
						<div class="large-4 small-12 medium-4 columns">
							<select name="program_type" class="input-type">
								<option value="">Área temática</option>
								<?php 
							$terms = get_terms('program_type');
							foreach ($terms as $term) {
								$selected = (!empty($_GET['program_type']) && ($_GET['program_type'] == $term->slug)) ? ' selected="selected" ' : '';
								echo '<option value="' . $term->slug . '"' . $selected . '>' . $term->name . '</option>';
							}
							?>
							</select>
						</div>
						<div class="large-1 small-12 medium-1 columns end">
							<input type="submit" class="button secondary" value="Buscar">
							<input type="hidden" name="action" value="send">
						</div>
					</div>
				</form>
			</div>
		</div>
	</header>
	
    <div class="row inner-space">
    	<div class="large-9 columns">
    		<?php 
    			if ( $the_query->have_posts() ) {
    				echo '<div class="large-up-2 small-up-1 small-up-1 clearfix">';
	    			while ( $the_query->have_posts() ): $the_query->the_post();
	    				global $post;
	    				echo '<div class="column">';
	    					echo pRender::program_tax_list($post,'program_type');
	    				echo '</div>';
	    			endwhile;
	    			if (function_exists('wp_pagenavi')) {
	    				wp_pagenavi(array('query' => $the_query));
	    			}
	    			echo '</div>';
	    		} else {
						echo '<div class="callout warning">';
						echo '<h5>Lo sentimos</h5>';
						echo '<p>No existen programas para el area seleccionada</p>';
						echo '</div>';
				}
    		 ?>
    	</div>
    	<div class="large-3 columns">
    		<aside class="sidebar">
    			<?php dynamic_sidebar('archive_program'); ?>
    		</aside>
    	</div>
    </div>
</section>
<?php get_footer(); ?>