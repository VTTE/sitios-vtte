<?php get_header(); ?>
<section class="main-content">
	<?php 
		$features = sitio::get_features(5);
		if (!empty($features)):
	?>
		<div class="row">
			<div class="columns large-8 feature-list">
				<?php 
					foreach ($features as $feature) {
						echo render::feature($feature, 'slide-feature');
					}
				?>
			</div>
			<div class="columns large-4">
				<?php 
					the_widget('WP_Widget_program_list');
				 ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="large-12 columns topic-list">
			<?php programs::get_topics_list('img'); ?>
		</div>
	</div>
	<?php if (is_active_sidebar('sidebar-home-1')) : ?>
	<div class="row inner-space">
		<div class="large-12 columns sidebar-home-1">
			<?php dynamic_sidebar( 'sidebar-home-1' ); ?>
		</div>	
	</div>
	<?php endif; ?>
	<div class="row inner-space">
		<div class="large-8 columns sidebar-home-2">
			<?php dynamic_sidebar( 'sidebar-home-2' ); ?>
		</div>
		<div class="large-4 columns">
		<?php 
			echo programs::get_program_form();
		?>
		</div>
	</div>
	
</section>
<?php get_footer(); ?>