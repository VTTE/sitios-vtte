<!doctype html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700' rel='stylesheet' type='text/css'>
    <title><?php wp_title('|') ?></title>
    <?php wp_head(); ?>
</head>
<?php
global $_set;
$settings = $_set->settings;
$image = (!empty($settings['site_image'])) ? current(wp_get_attachment_image_src($settings['site_image'], 'full')) : PRE_HOME_URI . '/img/logo-vicerrectoria-transferencia-tecnologica-extension-utem.png';
$facebook_url = (!empty($settings['link_facebook'])) ? esc_url($settings['link_facebook']) : 'https://www.facebook.com/vcm.utem/';
$twitter_url = (!empty($settings['link_twitter'])) ? esc_url($settings['link_twitter']) : 'https://twitter.com/vcmutem/';
$youtube_url = (!empty($settings['link_youtube'])) ? esc_url($settings['link_youtube']) : 'https://www.youtube.com/channel/UCyvdEKb5EkDTPzM4HDjdcuw';
$linkedin_url = (!empty($settings['link_linkedin'])) ? esc_url($settings['link_linkedin']) : 'https://cl.linkedin.com/edu/universidad-tecnol%C3%B3gica-metropolitana-11004';
$instagram_url = (!empty($settings['link_instagram'])) ? esc_url($settings['link_instagram']) : 'https://www.instagram.com/vcm.utem/';
if (!empty($settings['site_image_svg'])) {
    $image = wp_get_attachment_url($settings['site_image_svg']);
}
?>

<body <?php body_class(); ?>>
    <header class="mobile-header show-for-small-only">
        <div class="top-bar">
            <div class="top-bar-left">
                <a href="<?php bloginfo('url') ?>" class="logo"><img src="<?php echo $image ?>" alt="<?php bloginfo('name') ?>"></a>
            </div>
            <div class="top-bar-right">
                <a href="#" class="mobile-nav-button"><span class="dashicons dashicons-menu"></span></a>
            </div>
        </div>
        <div class="menu-mobile-container closed">

            <a class="close" href="#"><span class="dashicons dashicons-no-alt"></span></a>
            <div class="search-mobile"><?php // get_search_form(); 
                                        ?></div>
            <?php
            $args = array(
                'theme_location' => 'principal',
                'container' => '',
                'depth' => 2,
                'items_wrap' => '<ul id = "%1$s" class = "menu vertical %2$s">%3$s</ul>'
            );

            wp_nav_menu($args);
            ?>
        </div>
    </header>

    <header class="main-header hide-for-small-only">
        <div class="row">
            <div class="large-4 small-1 columns logo">
                <a href="<?php bloginfo('url') ?>"><img src="<?php echo $image ?>" alt="<?php bloginfo('name') ?>"></a>
            </div>
            <div class="large-4 columns gob-logo">
                <ul class="menu social-media align-right">
                    <li><a href="<?php echo $facebook_url ?>"><i class="ion-social-facebook"></i></a></li>
                    <li><a href="<?php echo $twitter_url ?>"><i class="ion-social-twitter"></i></a></li>
                    <li><a href="<?php echo $youtube_url ?>"><i class="ion-social-youtube"></i></a></li>
                    <li><a href="<?php echo $linkedin_url ?>"><i class="ion-social-linkedin"></i></a></li>
                    <li><a href="<?php echo $instagram_url ?>"><i class="ion-social-instagram"></i></a></li>
                </ul>
                <?php get_search_form(); ?>
                <nav class="auxiliar">
                    <?php base::translate_google_widget('desktop'); ?>
                    <?php
                    $args = array(
                        'theme_location' => 'header_aux',
                        'container' => '',
                        'items_wrap' => '<ul id = "%1$s" class = "menu %2$s">%3$s</ul>',
                        'fallback_cb' => false
                    );

                    wp_nav_menu($args);
                    ?>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="column large-12">
                <?php
                $string = '';
                echo apply_filters('vtte_main_header_down_logo', $string);
                ?>
            </div>
        </div>
        <div class="sticky-menu" id="sticky-menu" data-sticky-container>
            <nav class="main-navigation sticky" data-sticky data-top-anchor="sticky-menu" data-btm-anchor="stick-footer:bottom" data-options="marginTop:0;">
                <div class="row">
                    <div class="large-12 columns">
                        <?php
                        $args = array(
                            'theme_location' => 'principal',
                            'container' => '',
                            'items_wrap' => '<ul id = "%1$s" class = "menu %2$s">%3$s</ul>'
                        );

                        wp_nav_menu($args);
                        ?>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <div id="stick-content">