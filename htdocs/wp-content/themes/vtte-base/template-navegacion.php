<?php 
/*
	Template name: Navegación lateral
*/
	get_header(); 
	the_post();
	global $post;
?>
<section class="main-content">
	<header class="row content-header">
		<div class="large-12 columns">
			<div class="row">
				<?php if ( has_post_thumbnail() ): ?>
					<div class="large-12 columns entry-image">
						<?php the_post_thumbnail( 'page-featured' ); ?>
						<div class="content-wrap">
							<h3 class="entry-title"><?php the_title(); ?></h3>
						</div>
					</div>
				<?php else: ?>
					<div class="large-12 columns">
						<h3 class="entry-title"><?php the_title(); ?></h3>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</header>
	<div class="row">
		<div class="large-12 columns">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
				?>
		</div>
	</div>
	<div class="row content">
		<div class="large-3 columns">
			<aside class="sidebar sidebar-left">
				<nav class="side-navigation">
					<?php 
						if ($post->post_parent != 0) {
							$post_id = $post->post_parent;
						} else {
							$post_id = $post->ID;
						}
						$nav_title = get_post_meta($post_id, 'page_title',true);
						if (!empty($nav_title)) {
							echo '<h4 class="nav-title">'.$nav_title.'</h4>';
						}

					?>
					<div class="content-mobile-nav show-for-small-only">
						<a href="#" class="content-nav-menu">Contenido asociado <span class="dashicons dashicons-arrow-down"></span></a>
						<ul class="menu vertical hide clearfix content-nav-mobile-menu">
							<?php 
								wp_list_pages(array(
									'child_of' => $post_id,
									'show_date' => '',
									'title_li' => ''
									));
							 ?>
						</ul>
					</div>
					<ul class="menu vertical clearfix hide-for-small-only">
						<?php 
							wp_list_pages(array(
								'child_of' => $post_id,
								'show_date' => '',
								'title_li' => ''
								));
						 ?>
					 </ul>
				</nav>

			</aside>

		</div>
		<div class="large-9 columns">
			<div class="std-text">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<?php get_template_part('inc/partials/footer','page'); ?>
</section>
<?php get_footer(); ?>