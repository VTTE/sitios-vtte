<?php 
	get_header(); 
?>
<section class="main-content">
	<header class="row section-header">
		<div class="large-12 columns">
			<h3 class="secondary-title">Buscando: <?php echo get_search_query( ); ?></h3>
		</div>
	</header>
	<div class="row content">
		<div class="large-12 columns">
			<?php 
				if ( have_posts() ) {
					echo '<div class="large-up-4 small-up-2 clearfix">';
						while( have_posts() ): the_post();
							global $post;
							echo base::news_share();
						endwhile;
					echo '</div>';
						if ( function_exists('wp_pagenavi')) {
							wp_pagenavi();
						}
					} else {
						echo '<div class="callout warning"><h5>Lo sentimos</h5> <p>No se han encontrado resultados para lo que buscas</p> </div>';
					}
			 ?>
		</div>
	</div>
</section>
<?php get_footer() ?>