<?php
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;


class videos {
	/* function get_video
	*	Extract and return video id from youtbe & vimeo videos
	*	@param url : Video url
	*/
	static function get_video($url,$width=560,$height=315) {
		if (strstr($url,'youtube')){
			$parse_url=parse_url($url);
			parse_str($parse_url['query']);
			$id=$v;
			$iframe='<iframe width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';
			return $iframe;
		}
		if (strstr($url,'vimeo')) {
			if (!empty($url)) {
				$id=array_pop(explode('/',$url));
				$iframe='<iframe src="http://player.vimeo.com/video/'.$id.'" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
				return $iframe;
			}
		}
		return false;

	}
	static function render_video_button($video,$active=false) {
		$active_class = ($active) ? ' active' : '';
		$url = get_post_meta($video->ID,'_video_url',true);
		echo '<figure class="hentry entry-video clearfix'.$active_class.'">';
            echo '<a href="'.$url.'">';
                echo '<img src="'.self::get_video_thumb($url,'big').'" alt="'.$video->post_title.'">';
                echo '<div class="video-info">';
                    echo '<h4 class="entry-title">'.$video->post_title.'</h4>';
                    if (!empty($video->post_content)) {
	                    echo '<div class="entry-description">';
	                        echo apply_filters( 'the_content', $video->post_content );
	                    echo '</div>';
                	}
                echo '</div>';
            echo '</a>';
        echo '</figure>';
	}
	static function get_video_class($url) {
		if (strstr($url,'youtube')){
			return 'youtube-lightbox';
		}
		if (strstr($url,'vimeo')){
			return 'vimeo-lightbox';
		}
	}
	/**
	* function get_video_thumb
	* Extract and return video id from youtbe & vimeo videos
	* @param url : Video url
	* @param size: small, medium
	*/
	static function get_video_thumb($url,$size) {
		if (strstr($url,'youtube')){
			$parse_url=parse_url($url);
			parse_str($parse_url['query']);
			$id=$v;
			$def_size = ($size=='small')?1:0;
			$thumb='http://img.youtube.com/vi/'.$id.'/'.$def_size.'.jpg';
			return $thumb;
		}
		if (strstr($url,'vimeo')) {
			$thumb = '';
			if (!empty($url)) {
				if ( false === ( $thumb = get_transient( 'vimeo_'.$id ) ) ) {

				$id=array_pop(explode('/',$url));
				$request = new WP_Http;
				$result = $request->request( 'http://vimeo.com/api/v2/video/'.$id.'.php' , $args );
				//$iframe='<iframe src="http://player.vimeo.com/video/'.$id.'?title=0&amp;byline=0&amp;portrait=0" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
				if (!is_wp_error( $result )) {
					$body = maybe_unserialize($result['body']);
					if ($size == 'medium') 
						$thumb = $body[0]['thumbnail_medium'];
					else
						$thumb = $body[0]['thumbnail_large'];
					set_transient('vimeo_'.$id,$thumb,60*60*24);
				}
			}
				return $thumb;
			}
		}
		return false;

	}
	static function get_last_videos($size=3) {
		$video = query_posts(array(
				'post_type' => 'videos',
				'posts_per_page' => $size,
				'orderby' => 'date',
				'order' => 'DESC'
			));
		return $video;
	}
}

// helpers

/**
 * Generate a exceprt from whatever argument it's passed to it
 * @param $post object|string Post object with excerpt and/or content OR string
 * @param $args object|array|string Arguments: length, echo, strict mode
 * @return string Formatted excerpt
 * @author Felipe Lavín <felipe@yukei.net>
 * */
function do_excerpt($post, $args=null){
	$params = wp_parse_args($args, array(
		'length' => 255,
		'echo' => false,
		'strict' => false,
		'wrap' => null,
		'wrap_id' => null,
		'wrap_class' => 'entry-excerpt',
		'hellip' => false,
		'append' => null
	));
	$out = $wrap = '';
	if ( $params['wrap'] ) {
		$wrap_id = $params['wrap_id'] ? ' id="'. esc_attr( $params['wrap_id'] ) .'"' : null;
		$wrap_class = $params['wrap_class'] ? ' class="'. esc_attr( $params['wrap_class'] ) .'"' : null;
		$wrap = '<'. $params['wrap'] . $wrap_id . $wrap_class .'>';
	}
	if ( is_string($post) ) {
		$excerpt = strip_shortcodes($post);
		$excerpt = strip_tags($excerpt);
		if ( strlen($excerpt) > $params['length'] ) {
			$excerpt = smart_substr($excerpt, $params['length']);
			if ( $params['hellip'] ) $excerpt .= ' '. $params['hellip'];
		}
		if ( $params['append'] ) $excerpt .= ' '. $params['append'];
		$out .= apply_filters('the_excerpt', $excerpt);
	} elseif ( is_object($post) ) {
		if ( isset($post->post_excerpt) && !empty($post->post_excerpt) ) {
			if ( $params['strict'] && strlen($post->post_excerpt) > $params['length'] ) {
				$buff = smart_substr($post->post_excerpt, $params['length']);
				if ($params['hellip'] ) $buff .= ' '. $params['hellip'];
				if ( $params['append'] ) $buff .= ' '. $params['append'];
				$out .= apply_filters('the_excerpt', $buff);
			} else {
				if ( $params['append'] ) $post->post_excerpt .= ' '. $params['append'];
				$out .= apply_filters('the_excerpt', $post->post_excerpt);
			}
		} elseif ( isset($post->post_content) && !empty($post->post_content) ) {
			return do_excerpt($post->post_content, $params);
		}
	}

	if ( !$out ) return false;

	if ( $out ) {
		if ( $params['wrap'] ) {
		    $wrap .= $out . '</'. $params['wrap'] .'>';
		    $out = $wrap;
		}
		if( $params['echo'] ) echo $out;
		else return $out;
	}
}

/**
 * Smarter text cutting
 * @param $str string contenido a cortar
 * @param string cantidad de caracteres que se mostraran
 * @return string
 * @author Basilio Cáceres <bcaceres@ayerviernes.com>
 */
function smart_substr($str,$n,$hellip=true){
	if ( strlen($str) > $n ) {
		$out = substr( strip_tags($str), 0, $n );
		$out = explode(" ",$out);
		array_pop( $out );
		$out = implode(" ",$out);
		if ( $hellip ) $out .= ' [&hellip;]';
	} else {
		$out = $str;
	}
	return $out;
}

// METABOX
class MpostMeta extends Model\PostMeta{
    protected function setId(){
        return 'page';
    }
    protected function setDataModel(){
        return array(
	        new Model\PostMetaData(
	                'title',
	                'Título de información',
	                '\GutenPress\Forms\Element\InputText',
	                array(
	                )
	          ),
			new Model\PostMetaData(
                'icon',
                'Ícono de título',
                '\GutenPress\Forms\Element\InputText',
                array(
                	'description' => 'escoger desde alguno de los íconos de <a href="https://developer.wordpress.org/resource/dashicons/" target="_blank">Dashicons</a> y especificar sólamente la clase que utiliza (por defecto se asume el ícono info)'
                )
            ),    	
        	new Model\PostMetaData(
                'info',
                'Información extra para sidebar',
                '\GutenPress\Forms\Element\WPEditor',
                array(
                	'textarea_rows' => 7,
					'media_buttons' => false,
                	'description' => 'alguna información asociada que se quiera mostrar en el sidebar de la página, si no se detalla no aparecerá'
                )
            )
        );
    }
}
new Model\Metabox( 'MpostMeta', 'Información extra de la Página', 'page', array('priority' => 'high') );

/*
	Super PHP Plugin to add Full SVG Media support to WordPress
	Author URI: http://www.lewiscowles.co.uk/
*/

class SVGSupport {
	function __construct() {
		add_action( 'admin_init', [ $this, 'add_svg_upload' ] );

		add_action( 'load-post.php', [ $this, 'add_editor_styles' ] );
		add_action( 'load-post-new.php', [ $this, 'add_editor_styles' ] );
	}

	public function add_svg_upload() {
		ob_start();
		add_action( 'wp_ajax_adminlc_mce_svg.css', [ $this, 'tinyMCE_svg_css' ] );
		add_filter( 'image_send_to_editor', [ $this, 'remove_dimensions_svg' ], 10 );
		
		add_filter( 'upload_mimes', [ $this, 'filter_mimes' ] );
		add_action( 'shutdown', [ $this, 'on_shutdown' ], 0 );
		add_filter( 'final_output', [ $this, 'fix_template' ] );
	}

	public function add_editor_styles() {
		add_filter( 'mce_css', [ $this, 'filter_mce_css' ] );
	}

	public function filter_mce_css( $mce_css ) {
		global $current_screen;
		$mce_css .= ', ' . '/wp-admin/admin-ajax.php?action=adminlc_mce_svg.css';
		return $mce_css;
	}

	public function remove_dimensions_svg( $html = '' ) {
		return str_ireplace( [ " width=\"1\"", " height=\"1\"" ], "", $html );
	}
	
	public function tinyMCE_svg_css() {
		header( 'Content-type: text/css' );
		echo 'img[src$=".svg"] { width: 100%; height: auto; }';
		exit();
	}

	public function filter_mimes( $mimes = [] ){
		$mimes[ 'svg' ] = 'image/svg+xml';
		return $mimes;
	}

	public function on_shutdown() {
		$final = '';
		$ob_levels = count( ob_get_level() );
		for ( $i = 0; $i < $ob_levels; $i++ ) {
			$final .= ob_get_clean();
		}
		echo apply_filters( 'final_output', $final );
	}

	public function fix_template( $content = '' ) {
		$content = str_replace(
			'<# } else if ( \'image\' === data.type && data.sizes && data.sizes.full ) { #>',
			'<# } else if ( \'svg+xml\' === data.subtype ) { #>
				<img class="details-image" src="{{ data.url }}" draggable="false" />
			<# } else if ( \'image\' === data.type && data.sizes && data.sizes.full ) { #>',
			$content
		);
		$content = str_replace(
			'<# } else if ( \'image\' === data.type && data.sizes ) { #>',
			'<# } else if ( \'svg+xml\' === data.subtype ) { #>
				<div class="centered">
					<img src="{{ data.url }}" class="thumbnail" draggable="false" />
				</div>
			<# } else if ( \'image\' === data.type && data.sizes ) { #>',
			$content
		);
		return $content;
	}
}
new SVGSupport();



//uploads filter
add_filter('sanitize_file_name', 'sa_sanitize_spanish_chars', 10);
function sa_sanitize_spanish_chars ($filename) {
	return remove_accents( $filename );
}