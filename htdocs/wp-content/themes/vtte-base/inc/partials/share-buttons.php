<?php global $post; ?>
<div class="share-buttons">
	<ul class="menu horizontal">
		<li class="fb"><div class="fb-like" data-href="<?php echo get_permalink($post->ID) ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
		<li class="fb-share">
			<div class="fb-share-button" data-href="<?php echo get_permalink($post->ID) ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Compartir</a></div>
		</li>
		<li class="tw"><a href="https://twitter.com/share" class="twitter-share-button"{count} data-via="utem">Tweet</a></li>
	</ul>
</div>