<div class="light-gray">
	<footer class="row article-footer inner-space">
		<div class="large-6 columns">
			<?php dynamic_sidebar( 'page_footer_1' );  ?>
		</div>
		<div class="large-3 columns">
			<?php dynamic_sidebar( 'page_footer_2' );  ?>
		</div>
		<div class="large-3 columns">
			<?php dynamic_sidebar( 'page_footer_3' );  ?>
		</div>
	</footer>
</div>