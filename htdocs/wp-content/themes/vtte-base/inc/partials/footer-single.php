<footer class="row article-footer">
	<div class="large-6 columns related-books">
		<?php dynamic_sidebar( 'single_footer_1' ); ?>
	</div>
	<div class="large-3 columns">
		<?php dynamic_sidebar( 'single_footer_2' ); ?>
	</div>
	<div class="large-3 columns">
		<?php dynamic_sidebar( 'single_footer_3' ); ?>
	</div>
</footer>