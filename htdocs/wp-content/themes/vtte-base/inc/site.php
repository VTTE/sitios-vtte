<?php 
/*
	Functiones generales de sitios
*/

class sitio {
	/*
		Get last 5 elements from post type feature
	*/
	static function get_features($size=5) {
		$args = array(
				'post_type' => 'utemfeature',
				'posts_per_page' => $size,
				'post_status' => 'publish'
			);
		$features = new WP_Query($args);
		return $features->posts;
	}
	/*
		Get posts from certain category
	*/
	static function get_posts_by_category($category,$limit=-1) {
		$args = array(
				'post_type' => 'post',
				'posts_per_page' => $limit,
				'post_status' => 'publish',
				'category_name' => $category
			);
		$features = new WP_Query($args);
		return $features->posts;
	}
	/*
		Return child navigation from current page hierarchy
	*/
	static function child_navigation($post) {
		if ($post->post_parent != 0) {
			$post_id = $post->post_parent;
		} else {
			$post_id = $post->ID;
		}
		wp_list_pages(array(
			'child_of' => $post_id,
			'show_date' => '',
			'title_li' => ''
			));
	}
	/*
		Generic page title name
	*/
	static function page_title(){
		$get= get_queried_object();
		if (is_post_type_archive())
			return 'Archivo: '.$get->labels->name;
		if (is_category())
			return 'Categoría '.$get->name;
		if (is_tag())
			return 'Tag "'.$get->name.'"';
		if (is_tax())
			return 'Archivo '.$get->name;
		if (is_search())
			return 'Búsqueda por: &#8220;'. get_search_query() .'&#8221;';
		if (is_404())
			return 'Ups, Página no encontrada';
	}
	/*
		Meta information for post 
	*/
	static function get_meta_post($post_id) {
		$categories = wp_get_post_categories( $post_id );
		$tags = wp_get_post_tags( $post_id );
		echo '<section class="widget meta_post">';
			echo '<h4 class="category-title"><span class="dashicons dashicons-tag"></span> Categorías</h4>';
			if ( !empty( $categories ) ) {
				echo '<ul class="menu vertical categories">';
				foreach ( $categories as $category ) {
					$link = ( !is_wp_error( get_category_link( $category ) ) ) ? get_category_link( $category ) : '#';
					$category_name = get_category( $category );
					echo '<li><a href="'.$link.'">'.$category_name->name.'</a></li>';
				}
				echo '</ul>';
			}
			if ( !empty( $tags ) ) {
				echo '<ul class="menu vertical tags">';
				foreach ( $tags as $tag ) {
					$link = ( !is_wp_error( get_tag_link( $tag ) ) ) ? get_tag_link( $tag ) : '#';
					echo '<li><a href="'.$link.'">'.$tag->name.'</a></li>';
				}
				echo '</ul>';
			}

		echo '</section>';
	}
	/*
		Get featured image url from post
	*/
	static function get_post_thumbnail_url($post,$size) {
		$attachment_id = get_post_thumbnail_id( $post->ID );
		return current(wp_get_attachment_image_src( $attachment_id, $size));
	}
	/*
		Get file from post meta
	 */
	static function get_file($id, $meta = 'document_download')
	{
		$file_id = get_post_meta($id, $meta, true);
		if (!empty($file_id)) {
			try {
				$file = new \GutenPress\Helpers\Attachment($file_id);
			} catch (Exception $e) {
				return '<!-- Attachment: no existe -->';
			}
			$return = new stdClass();
			$return->url = $file->url;
			$return->size = $file->filesize;
			$return->name = $file->title;
			$return->extension = $file->pathinfo->extension;

			return $return;
		} else {
			return false;
		}


	}
	/*
		Build facebook link to share with feed dialog
	*/
	static function get_facebook_link($post) {
		$text = do_excerpt(wp_strip_all_tags($post->post_content),array('length' => 150));
		$caption = get_the_title($post->ID);
		$url = get_permalink($post->ID);
		$img = self::get_post_thumbnail_url($post,'event-big-home');
		$out = '<a href="#" class="facebook-share" data-text="'.$text.'" data-caption="'.$caption.'" data-url="'.$url.'" data-img="'.$img.'"><i class="dashicons dashicons-facebook-alt"></i></a> ';
		return $out;
	}
	/*
		Build twitter link to share with twitter intent
	*/
	static function get_tweet_link($post) {
		$text = urlencode($post->post_title);
		$url = urlencode(wp_get_shortlink($post->ID));
		$out = '<a href="https://twitter.com/intent/tweet?text='.$text.'&url='.$url.'" class="tweet-share share-tw"><span class="dashicons dashicons-twitter"></span></a>';
		return $out;
	}
}