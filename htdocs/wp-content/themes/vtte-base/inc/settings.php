<?php
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;

class ThemeSettings{
	private $flash;
	public $settings;
	public function __construct(){
		$this->init();
		$this->flash = array(
			'updated' => __('Configuraciones del sitio guardadas', 'vtte'),
			'error'   => __('Hubo un problema guardando las opciones del sitio', 'vtte')
		);
		$this->settings = get_option( 'site_theme_settings');
	}
	public function init(){
		add_action('admin_menu', array($this, 'addAdminMenu'));
		add_action('admin_init', array($this, 'saveSettings'));
	}
	public function addAdminMenu(){
		add_submenu_page( 'index.php' , _x('Configuraciones', 'site settings title', 'vtte'), _x('Configuraciones', 'site settings menu', 'vtte'), 'edit_theme_options', 'vtte-site-settings', array($this, 'adminMenuScreen'));
	}
	public function adminMenuScreen(){
		echo '<div class="wrap">';
			screen_icon('index');
			echo '<h2>'. _x('Configuraciones', 'site settings title', 'vtte') .'</h2>';
			if ( ! empty($_GET['msg']) && isset($this->flash[ $_GET['msg'] ]) ) :
				echo '<div class="updated">';
					echo '<p>'. $this->flash[ $_GET['msg'] ] .'</p>';
				echo '</div>';
			endif;
			$data = get_option( 'site_theme_settings' );
			$form = new Forms\Form('site-settings');
			$form->addElement( new Element\WPImage(
				_x('Logo del sitio', 'site settings fields', 'vtte'),
				'site_image',
				array(
					'value' => isset($data['site_image']) ? $data['site_image'] : '',
					'description' => 'Tamaño recomendado 310x55 px'
				)
			) )->addElement( new Element\WPFile(
				_x('Fallback Logo (svg)', 'site settings fields', 'vtte'),
				'site_image_svg',
				array(
					'value' => isset($data['site_image_svg']) ? $data['site_image_svg'] : ''
				)
			) )->addElement( new Element\InputCheckbox(
				_x('Activar traducción automatica de sitio', 'site settings fields', 'revology'),
				'translator_active',
				array( '1' => 'Sí'),
				array(
					'value' => isset($data['translator_active']) ? $data['translator_active'] : '',
					'description' => 'Se activa el widget de traducción automática de google'
				)
			) )->addElement( new Element\InputText(
				_x('Título columna 1 footer', 'site settings fields', 'vtte'),
				'footer_col_1',
				array(
					'value' => isset($data['footer_col_1']) ? $data['footer_col_1'] : ''
				)
			) )->addElement( new Element\InputText(
				_x('Título columna 2 footer', 'site settings fields', 'vtte'),
				'footer_col_2',
				array(
					'value' => isset($data['footer_col_2']) ? $data['footer_col_2'] : ''
				)
			) )->addElement( new Element\InputText(
				_x('Título columna 3 footer', 'site settings fields', 'vtte'),
				'footer_col_3',
				array(
					'value' => isset($data['footer_col_3']) ? $data['footer_col_3'] : ''
				)
			) )->addElement( new Element\WPEditor(
				_x('Texto del footer', 'site settings fields', 'vtte'),
				'footer_text',
				array(
					'value' => isset($data['footer_text']) ? $data['footer_text'] : '',
					'textarea_rows' => 7,
					'media_buttons' => false
				)
			))->addElement(new Element\InputText(
				_x('Link facebook', 'site settings fields', 'vtte'),
				'link_facebook',
				array(
					'value' => isset($data['link_facebook']) ? $data['link_facebook'] : ''
				)
			))->addElement(new Element\InputText(
				_x('Link Twitter', 'site settings fields', 'vtte'),
				'link_twitter',
				array(
					'value' => isset($data['link_twitter']) ? $data['link_twitter'] : ''
				)
			))->addElement(new Element\InputText(
				_x('Link Youtube', 'site settings fields', 'vtte'),
				'link_youtube',
				array(
					'value' => isset($data['link_youtube']) ? $data['link_youtube'] : ''
				)
			))->addElement(new Element\InputText(
				_x('Link Linkedin', 'site settings fields', 'vtte'),
				'link_linkedin',
				array(
					'value' => isset($data['link_linkedin']) ? $data['link_linkedin'] : ''
				)
			))->addElement(new Element\InputText(
				_x('Link instagram', 'site settings fields', 'vtte'),
				'link_instagram',
				array(
					'value' => isset($data['link_instagram']) ? $data['link_instagram'] : ''
				)
			));
			$form = apply_filters( 'vtte_main_configuration_form', $form, $data );
			/*
			Fin opciones newsletter
			*/
			$form->addElement( new Element\InputSubmit(
				_x('Guardar', 'site settings fields', 'vtte')
			) )->addElement( new Element\WPNonce(
				'update_site_settings',
				'_site_settings_nonce'
			) )->addElement( new Element\InputHidden(
				'action',
				'update_site_settings'
			) );
			echo '<h3>'._x('Home settings', 'site settings fields', 'vtte').'</h3>';
			echo $form;
		echo '</div>';
	}


	public function saveSettings(){
		if ( empty($_POST['action']) )
			return;
		if ( $_POST['action'] !== 'update_site_settings' )
			return;
		if ( ! wp_verify_nonce( $_POST['_site_settings_nonce'], 'update_site_settings' ) )
			wp_die( _x("You are not supposed to do that", 'site settings error', 'vtte') );
		if ( ! current_user_can( 'edit_theme_options' ) )
			wp_die( _x("You are not allowed to edit this options", 'site settings error', 'vtte') );
		$fields = array(
			'site_image',
			'site_image_svg',
			'translator_active',
			'footer_text',
			'footer_col_1',
			'footer_col_2',
			'footer_col_3',
			'link_facebook',
			'link_twitter',
			'link_youtube',
			'link_linkedin',
			'link_instagram'
		);
		$fields = apply_filters('vtte_main_configuration_fields',$fields);
		$raw_post = stripslashes_deep( $_POST );
		$data = array_intersect_key($raw_post, array_combine($fields, $fields) );
		update_option( 'site_theme_settings' , $data );
		wp_redirect( admin_url('admin.php?page=vtte-site-settings&msg=updated', 303) );
		exit;
	}
}
$_set = new ThemeSettings;