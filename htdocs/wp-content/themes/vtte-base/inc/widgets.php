<?php 
class WP_Widget_text_banner_simple extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'text-banner', 'description' => 'Muestra un banner de texto con un link');
        $control_ops = array();
        parent::__construct('text-banner', 'Banner de texto', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        extract( $instance );
        extract( $args );
        $color = ( !empty( $color ) ) ? $color : 'light';
        echo '<div class="widget text '.$color.'">';
            echo '<a href="'.$url.'">';
                echo '<h4 class="widget-title">'.$title.'</h4>';
                if ( !empty( $instance['description'] ) ) {
                    echo '<p>'.esc_attr($instance['description']).'</p>';
                }
                if ( !empty( $instance['mini-text'] ) ) {
                    echo '<span class="widget-link">'.esc_attr($instance['mini-text']).'</span>';
                }
            echo '</a>';
        echo '</div>';
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
        echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'.$this->get_field_id('description').'">Bajada: <textarea name="'. $this->get_field_name('description') .'" id="'.$this->get_field_id('description').'" class="widefat">'.$instance['description'].'</textarea></label></p>';
        echo '<p><label for="'.$this->get_field_id('mini-text').'">Texto pequeño (opcional): <textarea name="'. $this->get_field_name('mini-text') .'" id="'.$this->get_field_id('mini-text').'" class="widefat">'.$instance['mini-text'].'</textarea></label></p>';
        echo '<p><label for="'.$this->get_field_id('url').'">Url: <input type="text" name="'. $this->get_field_name('url') .'" id="'.$this->get_field_id('url').'" value="'.$instance['url'].'" class="widefat" /></label></p>';
        echo '<h3>Apariencia</h3>';
        echo '<p><label>Color: </label>';
        echo '<select class="widefat" id="'.$this->get_field_id('color').'" name="'.$this->get_field_name('color').'">';
            echo '<option value="">Seleccione</option>';
                echo '<option value="blue"'. (($color == 'blue') ? 'selected="selected"' : '') .'>Azul</option>';
                echo '<option value="green" '.(($color == 'green') ? 'selected="selected"' : '') .'>Verde</option>';
                echo '<option value="dark" '.(($color == 'dark') ? 'selected="selected"' : '') .'>Oscuro</option>';
                echo '<option value="light" '.(($color == 'light') ? 'selected="selected"' : '') .'>Claro</option>';
            echo '</select>';
        echo '</p>';
       } 
}

class WP_Widget_sidebar_title extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'sidebar-title', 'description' => 'Muestra un texto en formato de titulo en el sidebar');
        $control_ops = array();
        parent::__construct('sidebar-title', 'Título en sidebar', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        extract( $instance );
        extract( $args );
        echo '<div class="widget sidebar-title">';
            echo '<h4 class="widget-title">'.$title.'</h4>';
        echo '</div>';
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
        echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
       } 
}

class WP_Widget_news extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'news', 'description' => 'Muestra las últimas noticias publicadas en el sitio');
        $control_ops = array();
        parent::__construct('news', 'Últimas noticias', $widget_ops, $control_ops);
    }
    function get_last_news($size, $category) {
        $args = array(
                'post_type' => 'post',
                'posts_per_page' => $size,
                'post_status' => 'publish'
            );
        if ( is_single() ) {
            global $post;
            $args['post__not_in'] = array($post->ID);
        }
        if (!empty($category)) {
            $args['cat'] = $category;
        }
        $news = new WP_Query($args);
        if ( $news->have_posts() ) {
            return $news->posts;
        } else {
            return false;
        }
    }
    function widget($args, $instance) {
        global $post;
        extract( $instance );
        extract( $args );
        $size = ( !empty( $instance['size'] ) ) ? $instance['size'] : 1;
        $grid = ( !empty( $instance['grid'] ) ) ? $instance['grid'] : 1;
        $the_category = ( !empty( $instance['category'] ) ) ? $instance['category'] : null;
        $link_text = ( !empty( $instance['link_text'] ) ) ? $instance['link_text'] : 'Más Noticias';
        $news = $this->get_last_news($size, $the_category);
        if ( !empty( $news ) ) {
            echo '<div class="widget news">';
                echo '<h4 class="widget-title">'.$title.'</h4>';
                echo '<div class="widget-content clearfix">';
                    echo '<div class="row large-up-'.$grid.' medium-up-'.$grid.' small-up-2 clearfix">';
                        foreach ($news as $item) {
                            echo '<div class="column">';
                                echo '<article class="hentry entry-news">';
                                    echo '<a href="'.get_permalink($item->ID).'">';
                                        echo get_the_post_thumbnail( $item->ID, 'squared' );
                                        echo '<div class="entry-info">';
                                            echo '<h4 class="entry-title">'.get_the_title($item->ID).'</h4>';
                                        echo '</div>';
                                    echo '</a>';
                                echo '</article>';
                            echo '</div>';
                        }

                    echo '</div>';
                    if ( !empty( $instance['is_link'] ) ) {
                        $link = get_category_link( get_cat_ID( 'Noticias' ) );
                        echo '<a href="'.$link.'" class="button secondary">'.$link_text.'</a>';
                    }
                echo '</div>';
            echo '</div>';
        }
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
       echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'. $this->get_field_name('is_link').'">Link a archivo de noticias? </label><input type="checkbox" id="'. $this->get_field_id('is_link').'"'.( ( !empty( $is_link ) ) ? ' checked="checked" ' : '' ).' name="'.$this->get_field_name('is_link').'" value="1"></p>';
        echo '<p><label for="'.$this->get_field_id('link_text').'">Texto link: <input type="text" name="'. $this->get_field_name('link_text') .'" id="'.$this->get_field_id('link_text').'" value="'.$instance['link_text'].'" class="widefat"/></label><br><small>Si no se especifica será: "Más noticias"</small></p>';
        echo '<p><label for="'.$this->get_field_id('size').'">Cantidad de entradas: <input type="number" name="'. $this->get_field_name('size') .'" id="'.$this->get_field_id('size').'" value="'.$instance['size'].'"/></label></p>';
        echo '<p><label for="' . $this->get_field_id('category') . '">Categoría: ';
            wp_dropdown_categories(array('show_option_none' => 'Seleccione', 'selected' => $instance['category'], 'class' => 'widefat', 'name' => $this->get_field_name('category'), 'id' => $this->get_field_id('category') ));
        echo '</label></p>';
        echo '<h3>Apariencia</h3>';
        echo '<p><label>Grilla: </label>';
            echo '<select class="widefat" id="'.$this->get_field_id('grid').'" name="'.$this->get_field_name('grid').'">';
                echo '<option value="">Seleccione</option>';
                echo '<option value="1" '.(($grid == '1') ? 'selected="selected"' : '') .'>1</option>';
                echo '<option value="2" '.(($grid == '2') ? 'selected="selected"' : '') .'>2</option>';
                echo '<option value="3" '.(($grid == '3') ? 'selected="selected"' : '') .'>3</option>';
                echo '<option value="4" '.(($grid == '4') ? 'selected="selected"' : '') .'>4</option>';
            echo '</select>';
            echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
        echo '</p>';
       } 
}
class WP_Widget_custom_form extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'custom-form', 'description' => 'Muestra un formulario de Contact form 7');
        $control_ops = array();
        parent::__construct('custom-form', 'Formulario personalizado', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        extract( $instance );
        extract( $args );

        echo '<div class="widget widget-newsletter">';
            echo '<div class="row newsletter">';
                echo '<div class="large-12 columns">';
                    echo '<strong>'.$title.'</strong><br>';
                    if ( !empty( $instance['bajada'] ) ) {
                        echo '<span>'.esc_attr($instance['bajada']).'</span>';
                    }
                    if ( !empty( $instance['description'] ) ) {
                        echo '<p>'.esc_attr($instance['description']).'</p>';
                    }
                    if ( !empty( $instance['shortcode'] ) ) {
                        echo '<div class="form">';
                            echo do_shortcode( $instance['shortcode'] );
                        echo '</div>';
                    }
                echo '</div>';
            echo '</div>';
        echo '</div>';

    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
        if (!empty($instance['shortcode'])){
            preg_match_all( '/' . get_shortcode_regex() . '/', $instance['shortcode'], $matches, PREG_SET_ORDER );
            if (empty($matches)) {
                echo '<div class="notice notice-error">';
                    echo '<p>el shortcode ingresado no es válido</p>';
                echo '</div>';
            }
        }
        echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'.$this->get_field_id('bajada').'">Subtitulo: <textarea name="'. $this->get_field_name('bajada') .'" id="'.$this->get_field_id('bajada').'" class="widefat">'.$instance['bajada'].'</textarea></label></p>';
        echo '<p><label for="'.$this->get_field_id('description').'">Bajada: <textarea name="'. $this->get_field_name('description') .'" id="'.$this->get_field_id('description').'" class="widefat">'.$instance['description'].'</textarea></label></p>';
        echo '<p><label for="'.$this->get_field_id('shortcode').'">Shortcode Formulario: <input type="text" name="'. $this->get_field_name('shortcode') .'" id="'.$this->get_field_id('shortcode').'" value="'.esc_attr($instance['shortcode']).'" class="widefat" /></label></p>';
       } 
}

class WP_Widget_twitter_timeline extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'twitter-timeline', 'description' => 'Muestra un timeline de twitter creado desde profile de twitter');
        $control_ops = array();
        parent::__construct('twitter-timeline', 'Twitter timeline', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        extract( $instance );
        extract( $args );
        $html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $instance['description']);
        
        echo '<div class="widget timeline-twitter">';
            echo $html;
        echo '</div>';
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
        echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'.$this->get_field_id('description').'">Pegar código de widget: <textarea name="'. $this->get_field_name('description') .'" id="'.$this->get_field_id('description').'" class="widefat">'.$instance['description'].'</textarea></label></p>';
       } 

}

class ButtonSpecials extends WP_Widget{
    function __construct() {
        $widget_ops = array('classname' => 'widget-button', 'description' => 'Muestra un banner con un estilo especifico');
        $control_ops = array('width' => 400);
        parent::__construct('widget-button', 'Banner Múltiple', $widget_ops, $control_ops);
    }
    function form($instance){
    extract($instance);
?>
        <p>
            <label for="<?php echo $this->get_field_name('title'); ?>">Título</label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php if(!empty($instance['title'])){ echo $instance['title']; }?>"/>
        </p>
        <?php if ($instance['estilo'] == 6): ?>
            <p>
                <label for="<?php echo $this->get_field_name('subtitle'); ?>">Subtítulo</label>
                <input class="widefat" id="<?php echo $this->get_field_id('subtitle'); ?>" name="<?php echo $this->get_field_name('subtitle'); ?>" type="text" value="<?php if(!empty($instance['subtitle'])){ echo $instance['subtitle']; }?>"/>
            </p>
        <?php endif; ?>
        <p>
            <label for="<?php echo $this->get_field_name('bajada'); ?>">Bajada</label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('bajada'); ?>" name="<?php echo $this->get_field_name('bajada'); ?>" ><?php if(!empty($instance['bajada'])){ echo $instance['bajada']; }?></textarea>
        </p>
        <p>
            <?php
                $img_selected = '';
                if (!empty($instance['attachment_id'])) {
                    $img_selected = '<img src="'.wp_get_attachment_thumb_url( $instance['attachment_id'] ).'" width="150">';
                }
             ?>
            <div><?php echo $img_selected; ?></div>
            <a href="#" id="<?php echo $this->get_field_id('attach_button'); ?>" onClick="bindEventWidgetImage(this.id);return false;" data-targetid="<?php echo $this->get_field_id('attachment_id'); ?>" data-button-text="Seleccionar" data-uploader-title="selecciona la imagen del widget" class="button widget_custom_media_upload">Subir una imagen</a>
            <input type="hidden" id="<?php echo $this->get_field_id('attachment_id'); ?>" name="<?php echo $this->get_field_name('attachment_id'); ?>" value="<?php echo $instance['attachment_id'] ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_name('color'); ?>">Color</label>
            <select class="widefat" id="<?php echo $this->get_field_id('color'); ?>" name="<?php echo $this->get_field_name('color'); ?>">
                <option value="">Seleccione</option>
                <option value="blue" <?php echo ($color == 'blue') ? 'selected="selected"' : ''; ?>>Azul</option>
                <option value="green" <?php echo ($color == 'green') ? 'selected="selected"' : ''; ?>>Verde</option>
                <option value="dark" <?php echo ($color == 'dark') ? 'selected="selected"' : ''; ?>>Oscuro</option>
                <option value="light" <?php echo ($color == 'light') ? 'selected="selected"' : ''; ?>>Claro</option>
            </select>
        </p>
         <p>
            <label for="<?php echo $this->get_field_name('estilo'); ?>">Estilo</label>
            <select class="widefat" id="<?php echo $this->get_field_id('estilo'); ?>" name="<?php echo $this->get_field_name('estilo'); ?>">
                <option value="">Seleccione</option>
                <option value="1" <?php echo ($estilo == 1) ? 'selected="selected"' : ''; ?>>Titulo - imagen - bajada</option>
                <option value="2" <?php echo ($estilo == 2) ? 'selected="selected"' : ''; ?>>Imagen alta - titulo - bajada</option>
                <option value="8" <?php echo ($estilo == 8) ? 'selected="selected"' : ''; ?>>Imagen apaisada - titulo - bajada</option>
                <option value="3" <?php echo ($estilo == 3) ? 'selected="selected"' : ''; ?>>Sólo imagen</option>
                <option value="4" <?php echo ($estilo == 4) ? 'selected="selected"' : ''; ?>>Imagen izquierda (cita)</option>
                <option value="5" <?php echo ($estilo == 5) ? 'selected="selected"' : ''; ?>>Imagen de fondo con texto encima</option>
                <option value="6" <?php echo ($estilo == 6) ? 'selected="selected"' : ''; ?>>Columna Imagen / Columna contenido</option>
                <option value="7" <?php echo ($estilo == 7) ? 'selected="selected"' : ''; ?>>Imagen cuadrada (redondeada) - Título</option>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_name('rounded_img'); ?>">Redondear imagen? </label>
            <input type="checkbox" id="<?php echo $this->get_field_id('rounded_img'); ?>" <?php echo ( (!empty($rounded_img)) ? ' checked="checked" ' : '' ) ?> name="<?php echo $this->get_field_name('rounded_img') ?>" value="1">
        </p>
        <p>
            <label for="<?php echo $this->get_field_name('url'); ?>">Url</label>
            <input class="widefat" id="<?php echo $this->get_field_id('url'); ?>" name="<?php echo $this->get_field_name('url'); ?>" type="text" value="<?php if(!empty($instance['url'])){ echo $instance['url']; }?>"/>
        </p>


<?php
    }
    function update($new_instance, $old_instance){
        return $new_instance;
    }
    function widget($args, $instance){
        extract($instance);        
        if (!empty($url)) {
           echo $this->render_widget($instance);
        }
    }
    function render_widget($instance) {
        extract($instance);
        switch($estilo) {
            case 1:
                return $this->render_title_up($instance,'large');
            break;
            case 2:
                 return $this->render_image_top($instance);
            break;
            case 3:
                return $this->render_image_only($instance,'small');
            break;
            case 4:
            return $this->render_image_left($instance);
            break;
            case 5:
                return $this->render_image_background($instance);
            break;
            case 6:
                return $this->render_mini_image_side($instance);
            break;
             case 7:
                return $this->render_title_squared_img($instance);
            break;
            case 8:
                return $this->render_image_top_landscape($instance);
            break;
        }
    }
    function render_image_left($instance) {
        extract($instance);
        $out = '';
        $color = ( !empty( $color ) ) ? $color : 'green';
        $rounded = ( !empty( $rounded_img )) ? ' rounded': '';
        $out .= '<div class="widget img testimonial '.$color.' clearfix">';
            $out .= '<div class="widget-image float-left'.$rounded.'">';
                $out .= wp_get_attachment_image( $attachment_id, 'thumbnail' );
            $out .= '</div>';
            $out .= '<div class="widget-content">';
                $out .= '<p>'.esc_attr($bajada).'</p>';
                $out .= '<span class="author">'.esc_attr($title).'</span>';
            $out .= '</div>';
        $out .= '</div>';
        return $out;
    }
    function render_title_up($instance) {
        extract($instance);
        $out = '';
        $color = ( !empty( $color ) ) ? $color : ' light';
        $rounded = ( !empty( $rounded_img )) ? ' rounded': '';

        $out .= '<div class="widget img title-up '.$color.'">';
            $out .= '<h4 class="widget-title">'.esc_attr($title).'</h4>';
            $out .= '<div class="widget-content">';
                $out .= '<a href="'.$url.'">';
                   $out .= '<span class="widget-image '.$rounded.'">'.wp_get_attachment_image( $attachment_id, 'squared' ).'</span>';
                $out .= '</a>';
                $out .= '<p class="entry-summary">'.esc_attr($bajada).'</p>';
            $out .= '</div>';
        $out .= '</div>';
        return $out;

    }
    function render_title_squared_img($instance) {
        extract($instance);
        $out = '';
        $rounded = ( !empty( $rounded_img )) ? ' rounded': '';

        $out .= '<div class="widget img squared-title">';
            $out .= '<div class="widget-content">';
                $out .= '<a href="'.$url.'">';
                   $out .= '<span class="widget-image '.$rounded.'">'.wp_get_attachment_image( $attachment_id, 'squared' ).'</span>';
                   $out .= '<h4 class="widget-title">'.esc_attr($title).'</h4>';
                $out .= '</a>';
            $out .= '</div>';
        $out .= '</div>';
        return $out;

    }
    function render_image_top($instance) {
        extract($instance);
        $out = '';
        $color = ( !empty( $color ) ) ? $color : 'light';
        $rounded = ( !empty( $rounded_img )) ? ' rounded': '';
        $out .= '<div class="widget '.$color.' img simple">';
            $out .= '<article class="entry-magazine">';
                $out .= '<a href="'.$url.'">';
                    $out .= '<span class="widget-image'.$rounded.'">'.wp_get_attachment_image( $attachment_id, 'letter' ).'</span>';
                    $out .= '<h4 class="entry-title">'.esc_attr($title).'</h4>';
                    $out .= '<p class="entry-summary">'.esc_attr($bajada).'</p>';
                $out .= '</a>';
            $out .= '</article>';
        $out .= '</div>';
        return $out;
    }
    function render_image_top_landscape($instance) {
        extract($instance);
        $out = '';
        $color = ( !empty( $color ) ) ? $color : 'light';
        $rounded = ( !empty( $rounded_img )) ? ' rounded': '';
        $out .= '<div class="widget '.$color.' img simple">';
            $out .= '<article class="entry-magazine">';
                $out .= '<a href="'.$url.'">';
                    $out .= '<span class="widget-image'.$rounded.'">'.wp_get_attachment_image( $attachment_id, 'landscape-small' ).'</span>';
                    $out .= '<h4 class="entry-title">'.esc_attr($title).'</h4>';
                    $out .= '<p class="entry-summary">'.esc_attr($bajada).'</p>';
                $out .= '</a>';
            $out .= '</article>';
        $out .= '</div>';
        return $out;
    }
    function render_image_only($instance, $size) {
        extract($instance);
        $rounded = ( !empty( $rounded_img )) ? ' rounded': '';
        $out .= '<div class="widget banner image-only">';
            $out .= '<a href="'.$url.'">';
                $out .= '<div class="image'.$rounded.'">'.wp_get_attachment_image( $attachment_id, 'full' ).'</div>';
            $out .= '</a>';
        $out .= '</div>';
        return $out;
    }
    function render_image_background($instance) {
        extract($instance);

        $out .= '<div class="widget img news background">';
            $out .= '<div class="widget-content">';
                $out .= '<article class="hentry entry-news">';
                    $out .= '<a href="'.$url.'">';
                        $out .= wp_get_attachment_image( $attachment_id, 'squared' );
                        $out .= '<div class="entry-info">';
                            $out .= '<div class="wrap-content">';
                            $out .= '<h4 class="entry-title">'.esc_attr($title).'</h4>';
                            if (!empty($bajada)) {
                                $out .= '<p class="entry-summary">'.esc_attr($bajada).'</p>';
                            }
                            $out .= '</div>';
                        $out .= '</div>';
                    $out .= '</a>';
                $out .= '</article>';
            $out .= '</div>';
        $out .= '</div>';
        return $out;
    }
    function render_mini_image_side($instance) {
        extract($instance);

        $out .= '<div class="widget img mini-side">';
            $out .= '<h4 class="widget-title">'.$title.'</h4>';
            $out .= '<div class="widget-content">';
                $out .= '<div class="widget-image">';
                    $out .= '<a href="'.$url.'">';
                        $out .= wp_get_attachment_image( $attachment_id, 'side-img' );
                    $out .= '</a>';
                $out .= '</div>';
                $out .= '<div class="widget-side-content end">';
                    if (!empty($subtitle)) {
                        $out .= '<h5 class="entry-subtitle"><a href="'.$url.'">'.esc_attr($subtitle).'</a></h5>';
                    }
                    if (!empty($bajada)) {
                        $out .= '<p class="entry-summary">'.esc_attr($bajada).'</p>';
                    }
                $out .= '</div>';
            $out .= '</div>';
        $out .= '</div>';
        return $out;
    }
}
class WP_Widget_page_content extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'page_content', 'description' => 'Muestra el contenido de una página');
        $control_ops = array();
        parent::__construct('page_content', 'Contenido página', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        extract($instance);
        if (!empty($page)) {
            $the_page = get_post($page);
            echo '<div class="widget page_content">';
                echo '<h4 class="widget-title secondary-title">'.get_the_title($page).'</h4>';
                echo '<div class="widget-content">';
                    if (!empty($the_page->post_excerpt)) {
                        echo apply_filters( 'the_content', $the_page->post_excerpt );
                    } else {
                        echo do_excerpt($the_page->post_content);
                    }
                echo '</div>';
                echo '<a href="'.get_permalink($page).'" class="link-more"><span>Conoce más</span></a>';
            echo '</div>';
        }
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
        echo '<p><label for="'.$this->get_field_id('page').'">Página:';
        wp_dropdown_pages( array(
            'selected' => $instance['page'],
            'name' => $this->get_field_name('page'),
            'id' => $this->get_field_id('page'),
            'show_option_none' => 'Seleccionar página'
            ) );
        echo '</label></p>';
       } 

}
class WP_Widget_event_navigator extends WP_Widget
{
    
    /** constructor */
    function __construct()
    {
        $widget_ops = array('classname' => 'event-navigator', 'description' => 'Muestra un listado de meses y años para visualizar actividades de la fecha seleccionada');
        $control_ops = array();
        parent::__construct('event-navigator', 'Navegador de eventos', $widget_ops, $control_ops);
    }
    function get_years() {
        $from = '2012';
        $years= array();
        for ( $i = date('Y'); $i >= $from ; $i--) { 
            $years[] = $i;
        }
        return $years;
    }
    function widget($args, $instance)
    {
        extract($instance);
        extract($args);
        $years = $this->get_years();
        echo '<div class="widget event-navigator">';
        echo '<ul class="accordion" data-accordion>';
            foreach ($years as $year):
                $active = ($year == date('Y')) ? ' is-active' : '';
                echo '<li class="accordion-item '.$active.'" data-accordion-item>';
                    echo '<a href="#" class="accordion-title">'.$year.'</a>';
                    echo '<div class="accordion-content" data-tab-content>';
                        $start = (new DateTime($year.'-01-01'));
                        $end = (new DateTime($year.'-12-30'))->modify('last day of this month');
                        $interval = DateInterval::createFromDateString('1 month');
                        $period = new DatePeriod($start, $interval, $end);
                        foreach ($period as $month) {
                            echo '<a class="month-name" href="'.site_url('actividades/?date_month='.$month->format('m').'&date_year='.$year).'">'.mysql2date('F',$month->format("d-m-Y")) .'</a>';
                        }
                    echo '</div>';
                echo '</li>';
            endforeach;
        echo '</ul>';

        echo '</div>';
    }

    function update($new_instance, $old_instance)
    {
        return $new_instance;
    }

    function form($instance)
    {
        extract($instance);
        echo '<p>Este widget no tiene configuraciones</p>';
    }
}

// register WP_Widgets
add_action('widgets_init', 'register_widgets');
add_action('init', 'register_widgets');
function register_widgets(){
    /*
        Unregister unnecesary widget
    */

    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    unregister_widget('WP_Widget_Search');
    unregister_widget('WP_Widget_Categories');
    unregister_widget('WP_Widget_Recent_Posts');
    unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_Tag_Cloud');
    //unregister_widget('WP_Nav_Menu_Widget');

    register_widget('WP_Widget_text_banner_simple');
    register_widget('WP_Widget_page_content');
    register_widget( 'ButtonSpecials' );
    register_widget( 'WP_Widget_twitter_timeline' );
    register_widget('WP_Widget_custom_form');
    register_widget('WP_Widget_news');
    register_widget( 'WP_Widget_sidebar_title' );
    register_widget('WP_Widget_event_navigator');
}

/*
// WIDGETS COLUMNAS
*/

class WP_Widget_Open_row extends WP_Widget {
    public $widget_id = 'widget-open-row';
    public $widget_name = '---Abrir Fila de widgets----';
    public $widget_options = array(
        'description' => 'Permite abrir una fila donde insertar widgets en columnas'
    );
    public function __construct( ) {
        parent::__construct(
            $this->widget_id, // Base ID
            $this->widget_name, // Name
            $this->widget_options // Args
        );
    }
    public function form( $instance ) {
        // outputs the options form on admin
        echo '<p class="description">';
            echo 'Este widget no tiene opciones configurables.';
        echo '</p>';
        echo '<p>Para usar widgets en columnas, debes utilizar:</p>';
        echo '<ol>';
            echo '<li>Abrir fila de widgets (este widget)</li>';
            echo '<li>{widget}</li>';
            echo '<li><strong>Separador de columnas</strong></li>';
            echo '<li>{otro widget}</li>';
            echo '<li>Cerrar fila de widgets</li>';
        echo '</ol>';
    }
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        return $new_instance;
    }
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        global $sidebar_sizes;
        $this->get_column_count();
        if ( $this->columns > 1 ) :
        echo '<!--comienza fila-->';
        echo '<div class="row medium-up-'.$this->column_grid.' small-up-2 large-up-'. $this->column_grid .'">';
            echo '<div class="column">';
        endif;
    }
    public function get_column_count(){
        global $wp_registered_sidebars, $wp_registered_widgets;
        // widgets x sidebar
        $sidebars_widgets = wp_get_sidebars_widgets();
        // buscar en qué sidebar estamos de acuerdo al id de esta intancia del widget
        foreach ( $sidebars_widgets as $sidebars => $widgets ) {
            if ( in_array($this->id, $widgets) ) {
                $present_sidebar = $sidebars;
                $present_widgets = $widgets;
                break;
            }
        }

        // si no encontramos el widget, chao pescao
        if ( ! isset($present_sidebar,$present_widgets, $wp_registered_sidebars[$present_sidebar], $wp_registered_sidebars[$present_sidebar]['size']) )
            return;

        // el tamaño total de la grilla para este sidebar (o sea, el total a dividir)
        $grid_size = $wp_registered_sidebars[$present_sidebar]['size'];

        // contar en cuántas columnas se está intentando separar
        $cols = 1;
        //$this_position = array_search($this->id, $present_widgets);
        $this_position = 0;
        $widgets_count = count( $present_widgets );
        for ( $i = $this_position; $i < $widgets_count; $i++ ) {
            // cuenta la cantidad de separadores
            if ( stripos($present_widgets[$i], 'widget-separate-columns') !== false ) ++$cols;
            // si encuentra un widget de cierre, se acabó la fila
            if ( stripos($present_widgets[$i], 'widget-close-row') !== false ) break;
        }
        $this->columns = $cols;
        $this->column_grid = $grid_size / ($grid_size / $cols);
    }
}
// register Foo_Widget widget
add_action( 'widgets_init', create_function( '', 'register_widget( "WP_Widget_Open_row" );' ) );

class WP_Widget_Column_Separator extends WP_Widget_Open_row {
    public $widget_id = 'widget-separate-columns';
    public $widget_name = '---Separador de columnas----';
    public $widget_options = array(
        'description' => 'Permite separar una fila de widgets en columnas'
    );
    public function form( $instance ) {
        // outputs the options form on admin
    }
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        return $new_instance;
    }
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        $this->get_column_count();
        if ( $this->columns > 1 ) :
        echo '</div><!--separacion de columnas--><div class="column">';
        endif;
    }
}
// register Foo_Widget widget
add_action( 'widgets_init', create_function( '', 'register_widget( "WP_Widget_Column_Separator" );' ) );

class WP_Widget_Close_Row extends WP_Widget_Open_row {
    public $widget_id = 'widget-close-row';
    public $widget_name = '---Cerrar fila de widgets-----';
    public $widget_options = array(
        'description' => 'Permite cerrar un fila de widgets'
    );
    public function form( $instance ) {
        // outputs the options form on admin
    }
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        return $new_instance;
    }
    public function widget( $args, $instance ) {
        // outputs the content of the widget
            echo '</div>';
        echo '</div><!--cierre fila-->';
    }
}
// register Foo_Widget widget
add_action( 'widgets_init', create_function( '', 'register_widget( "WP_Widget_Close_Row" );' ) );
 ?>