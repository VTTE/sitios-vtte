	<?php 
	get_header(); 
	the_post();
	global $post;
?>
<section class="main-content">
	<header class="row content-header">
		<div class="large-12 columns entry-meta">
			<div class="header-metadata">
				<h3 class="entry-title"><?php the_title(); ?></h3>
				<?php get_template_part('inc/partials/share','buttons'); ?>
				<span class="published">Publicada el <?php echo get_the_date('d - m - Y') ?></span>
			</div>
		</div>
	</header>
	<div class="row content">
		<div class="large-12 columns">
			<div class="std-text">
				<?php the_excerpt(); ?>
			</div>
			
			<?php  if (!empty($post->portafolio_gallery)): ?>
				<div class="large-up-4 small-up-2 medium-up-4 row gallery-list">
					<?php 
						foreach ($post->portafolio_gallery as $item) {
							$img_url = wp_get_attachment_image_src($item,'full');
							echo '<div class="column">';
								echo '<a href="'.$img_url[0].'" class="box">';
									echo wp_get_attachment_image($item,'squared');
								echo '</a>';
							echo '</div>';
						} 
					?>	
				</div>
			<?php endif; ?>
			<a href="<?php echo get_post_type_archive_link( 'gallery' ); ?>" class="button primary big"> Ver todas las galerías</a>
		</div>
	</div>
	<?php get_template_part('inc/partials/footer','single'); ?>
</section>
<?php get_footer(); ?>