<form role="search" method="get" id="searchform" class="searchform" action="<?php echo site_url() ?>">
	<div class="input-group">
		<input value="" class="input-group-field" name="s" placeholder="Buscar..." id="s" type="text">
		<div class="input-group-button">
			<input id="searchsubmit" value="Ir" class="button" type="submit">
		</div>
	</div>
</form>