<?php 
	get_header(); 
		$search = new search_filter();
		$tax = get_taxonomy(get_queried_object()->taxonomy);
		$post_types = $tax->object_type;
		
		$search->set_post_type($post_types);
		if ( get_query_var('paged') ) {
			$search->set_page(get_query_var('paged'));
		}
		if ( isset($_GET['action']) ) {
			if (isset($_GET['search'])) {
				$search->set_search_text( esc_attr( $_GET['search'] ) ) ;
			}
			$date = array();
			if ( !empty($_GET['date_month'])) {
				$date['month'] = esc_attr( $_GET['date_month'] );
			}
			if ( !empty( $_GET['date_year'] ) ) {
				$date['year'] = esc_attr( $_GET['date_year'] );
			}
			if ( !empty($date) ) {
				$search->set_date($date);
			}
		}
		$query = $search->search();
?>
<section class="main-content">
	<header class="row section-header">
		<div class="large-12 columns">
			<h3 class="secondary-title"><?php echo sitio::page_title() ?></h3>
			<div class="filter-form">
				<form action="" method="GET">
					<div class="row">
						<div class="large-2 columns">
							<h5>Buscar Por</h5>
						</div>
						<div class="large-4 columns">
							<input type="text" placeholder="Palabra clave" value="<?php echo esc_attr($_GET['search']) ?>" class="input-type" name="search">
						</div>
						<div class="large-1 columns">
							<?php echo $search->get_months_select(array( 
								'name' => 'date_month',
								'class' => 'input-type'
							), $_GET['date_month']); ?>
						</div>
						<div class="large-1 columns">
							<?php echo $search->get_years_select(array( 
								'name' => 'date_year',
								'class' => 'input-type'
							), $_GET['date_year']); ?>
						</div>
						<div class="large-1 columns end">
							<input type="submit" class="button secondary" value="Buscar">
							<input type="hidden" name="action" value="send">
						</div>
					</div>
				</form>
			</div>
		</div>
	</header>
	<div class="row content">
		<div class="large-12 columns">
			<?php 
				if ( $query->have_posts() ) {
					echo '<div class="large-up-4 small-up-2 clearfix">';
						while( $query->have_posts() ): $query->the_post();
							global $post;
							echo base::news_share($post);
						endwhile;
					echo '</div>';
						if ( function_exists('wp_pagenavi')) {
							wp_pagenavi(array('query' => $query));
						}
					} else {
						echo '<div class="callout warning"><h5>Lo sentimos</h5> <p>No se han encontrado resultados para lo que buscas</p> </div>';
					}
			 ?>
		</div>
	</div>
</section>
<?php get_footer() ?>