<?php
	/**
	* Functions: lista de funciones del theme vtte-editorial
	* Funciones nuevas para el sitio
	* Editorial UTEM
	* @version 2.0
	* @package vtte
	*/
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;
/* Theme Constants (to speed up some common things) ------*/
define('HOME_URI', get_bloginfo( 'url' ));
define('PRE_HOME_URI',get_bloginfo('url').'/wp-content/themes');
define('SITE_NAME', get_bloginfo( 'name' ));
define('THEME_URI', get_template_directory_uri());
define('THEME_IMG', THEME_URI . '/img');
define('THEME_CSS', THEME_URI . '/css');
define('THEME_FONTS', THEME_URI . '/fonts');
define('PRE_THEME_JS', PRE_HOME_URI . '/js');
define('PRE_THEME_CSS', PRE_HOME_URI . '/vtte-base/css');
define('THEME_JS', THEME_URI. '/js');
define('MULTILINGUAL', function_exists( 'qtrans_use' ));
/*
	calling related files
*/

	include TEMPLATEPATH . '/inc/widgets.php';
	include TEMPLATEPATH . '/inc/settings.php';
	include TEMPLATEPATH . '/inc/site.php';
	include TEMPLATEPATH . '/inc/search_filter.php';
	include TEMPLATEPATH . '/inc/helpers.php';

/**
 * Images
 * ------
 * */
// Add theme suppor for post thumbnails
add_theme_support( 'post-thumbnails' );
// Define the default post thumbnail size

// set_post_thumbnail_size( 200, 130, true );

// Define custom thumbnail sizes
// add_image_size( $name, $width, $height, $crop );
add_image_size('squared', 300, 300, true);
add_image_size('featured-page', 570, 385, true);
add_image_size('letter', 315, 390, true);
add_image_size('side-img', 150, 9999, false);
add_image_size('page-featured', 1170, 383, true);
add_image_size('landscape-small', 215, 100, true);
// add_image_size('thumbnail-home', 290, 180, true);
// add_image_size('post-image', 640, 300, true);
// add_image_size('main-feature', 727, 317, true);
/*
	REGISTER SIDEBARS
*/
/*
	Theme sidebars
*/
$mandatory_sidebars = array(
	'Página' => array(
		'name' => 'page',
		'size' => 4
		),
	'Página contenido' => array(
		'name' => 'page_contenido',
		'size' => 4
		),
	'Página footer 1' => array(
		'name' => 'page_footer_1',
		'size' => 6
		),
	'Página footer 2' => array(
		'name' => 'page_footer_2',
		'size' => 3
		),
	'Página footer 3' => array(
		'name' => 'page_footer_3',
		'size' => 3
		),
	'Entrada' => array(
		'name' => 'single',
		'size' => 4
		),
	'Entrada footer 1' => array(
		'name' => 'single_footer_1',
		'size' => 6
		),
	'Entrada footer 2' => array(
		'name' => 'single_footer_2',
		'size' => 3
		),
	'Entrada footer 3' => array(
		'name' => 'single_footer_3',
		'size' => 3
		),
);
$mandatory_sidebars = apply_filters('vtte_base_mandatory_sidebars',$mandatory_sidebars);
foreach ( $mandatory_sidebars as $sidebar => $id_sidebar ) {
	register_sidebar( array(
		'name'          => $sidebar,
		'id'			=> $id_sidebar['name'],
		'before_widget' => '<section id="%1$s" class="widget %2$s">'."\n",
		'after_widget'  => '</section>',
		'before_title'  => '<header class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3></header>',
		'size' => $id_sidebar['size']
	) );
}

/**
 * Theme specific stuff
 * --------------------
 * */

/**
 * Theme singleton class
 * ---------------------
 * Stores various theme and site specific info and groups custom methods
 **/
class site {
	private static $instance;

	protected $settings;

	const id = __CLASS__;
	const theme_ver = '20140624';
	const theme_settings_permissions = 'edit_theme_options';
	private function __construct(){
		/**
		 * Get our custom theme options so we can easily access them
		 * on templates or other admin pages
		 * */
		// $this->settings = get_option( __CLASS__ .'_theme_settings' );

		$this->actions_manager();

	}
	public function __get($key){
		return isset($this->$key) ? $this->$key : null;
	}
	public function __isset($key){
		return isset($this->$key);
	}
	public static function get_instance(){
		if ( !isset(self::$instance) ){
			$c = __CLASS__;
			self::$instance = new $c;
		}
		return self::$instance;
	}
	public function __clone(){
		trigger_error( 'Clone is not allowed.', E_USER_ERROR );
	}
	/**
	 * Setup theme actions, both in the front and back end
	 * */
	public function actions_manager(){
		if ( is_admin() ) {
			//
		} else {
			//add_filter('wp_title', array($this, 'original_title'), 1, 1);
			//add_filter('wp_title', array($this, 'wp_title'), 99, 3);
			//add_filter('wpseo_canonical', array($this, 'canonical'), 99, 1);
			//add_filter('request', array($this, 'filter_request'));
		}
		add_action( 'after_setup_theme', array($this, 'setup_theme') );
		add_action( 'wp_enqueue_scripts', array($this, 'enqueue_styles') );
		add_action( 'wp_enqueue_scripts', array($this, 'enqueue_scripts') );
		add_action( 'enqueue_scripts', array($this, 'enqueue_scripts') );
		add_action( 'admin_enqueue_scripts', array($this, 'admin_enqueue_scripts') );
		add_action('init', array($this, 'init_functions') );
		add_action('init', array($this,'register_menus_locations') );

	}
	public function init_functions() {
		add_post_type_support( 'page', 'excerpt' );
		register_post_status( 'suspended', array( 
			'label' => 'Suspendida',
			'public' => true,
			'show_in_admin_all_list' => true,   
			'show_in_admin_status_list' => true,
			'exclude_from_search' => false
		) );
		add_action('do_faviconico', array($this,'set_default_favicon'), 10);
	}
	public function set_default_favicon() {
		wp_redirect(PRE_HOME_URI.'/img/favicon-utem-vcm.png');
		exit;
	}
	/**
	 * habilitar funcionalidades del tema
	 * @return void
	 */
	public function setup_theme(){
		// habilitar post formats
		add_theme_support('post-formats', array('gallery', 'image', 'video', 'audio'));
		add_theme_support('post-thumbnails');
		add_theme_support('automatic_feed_links');
		add_theme_support('menus');
	}

	public function register_menus_locations(){
		register_nav_menus(array(
			'principal' => 'Menú Principal',
			'header_aux' => 'Auxiliar',
			'footer_1' => 'Footer Col 1',
			'footer_2' => 'Footer Col 2',
			'footer_3' => 'Footer Col 3'
		));
	}

	public function get_post_thumbnail_url( $postid = null, $size = 'landscape-medium' ){
		if ( is_null($postid) ){
			global $post;
			$postid = $post->ID;
		}
		$thumb_id = get_post_thumbnail_id( $postid );
		$img_src  = wp_get_attachment_image_src( $thumb_id, $size );
		return $img_src ? current( $img_src ) : '';
	}

	public function enqueue_styles(){
		// Front-end styles
		//wp_enqueue_style( 'vtte_font', THEME_URI.'/font/stylesheet.css' );
		
		wp_enqueue_style( 'vtte_style_base', PRE_HOME_URI.'/vtte-base/style.css' );
		wp_enqueue_style( 'vtte_style', get_stylesheet_uri() );
		wp_enqueue_style( 'slick', THEME_JS .'/slick/slick.css');
		// wp_enqueue_style( 'slick', THEME_JS .'/slick/slick.css');
		wp_enqueue_style( 'slick-theme', THEME_JS .'/slick/slick-theme.css');
		wp_enqueue_style( 'swipebox', PRE_THEME_CSS .'/swipebox.min.css');
		wp_enqueue_style( 'ionicons', 'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' );
		wp_enqueue_style( 'dashicons' );
	}

	function admin_enqueue_scripts(){

		// admin scripts
		 global $pagenow;
		// wp_enqueue_script( 'theme_admin_scripts', THEME_JS .'/admin_scripts.js', array('jquery'), true );
		 if ( is_admin() && (($pagenow == 'index.php' ) || ($pagenow == 'admin.php')) ) {
		 	if ($_GET['page'] == 'vtte-site-settings')
		 		wp_enqueue_media();
		 	//wp_enqueue_script( 'script-admin', THEME_JS . '/admin_scripts.js', array('jquery'), THEME_VERSION );
		 }
		 if (is_admin() && ( ($pagenow == 'link-add.php') || ( $pagenow == 'link.php' ) ) ) {
		 	wp_enqueue_media();
		 }
		 if ( is_admin() && ($pagenow == 'widgets.php' ) ) {
		 	wp_enqueue_media();
		 	wp_enqueue_script( 'script-admin', THEME_JS . '/admin_scripts.js', array('jquery'), THEME_VERSION );
		 }
		// if ($pagenow == 'nav-menus.php' || $pagenow == 'edit-tags.php') {
		// 	wp_enqueue_media();
		// }
	}

	function enqueue_scripts(){
		// front-end scripts
		wp_enqueue_script( 'jquery' );

		wp_enqueue_script( 'modernizr', PRE_THEME_JS .'/modernizr.js', array('jquery'), self::theme_ver, '' );
		/*foundation JS*/
		wp_enqueue_script( 'Foundation', THEME_JS .'/foundation.core.js', array('jquery'), self::theme_ver, '' );
		wp_enqueue_script( 'foundation-timer-image', THEME_JS.'/foundation.util.timerAndImageLoader.js', 
			array('jquery'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-mediaquery', THEME_JS .'/foundation.util.mediaQuery.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-util-box', THEME_JS .'/foundation.util.box.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-util-keyboard', THEME_JS .'/foundation.util.keyboard.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-util-nest', THEME_JS .'/foundation.util.nest.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-equalizer', THEME_JS .'/foundation.equalizer.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-dropdown-menu', THEME_JS .'/foundation.dropdownMenu.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-accordion', THEME_JS . '/foundation.accordion.js', array('Foundation'), self::theme_ver, '');
		wp_enqueue_script( 'Foundation-accordion-menu', THEME_JS .'/foundation.accordionMenu.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-drilldown', THEME_JS .'/foundation.drilldown.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-dropdown', THEME_JS .'/foundation.dropdown.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-responsiveMenu', THEME_JS .'/foundation.responsiveMenu.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-responsiveToggle', THEME_JS .'/foundation.responsiveToggle.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-triggers', THEME_JS .'/foundation.util.triggers.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-sticky', THEME_JS .'/foundation.sticky.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script( 'Foundation-abide', THEME_JS .'/foundation.abide.js', array('Foundation'), self::theme_ver, '' );
		wp_enqueue_script('Foundation-tabs', THEME_JS . '/foundation.tabs.js', array('Foundation'), self::theme_ver, '');

		wp_enqueue_script( 'Slick', PRE_THEME_JS .'/slick.min.js', array('jquery'), self::theme_ver, '' );
		wp_enqueue_script( 'Swipebox', PRE_THEME_JS .'/swipebox.min.js', array('jquery'), self::theme_ver, '' );
		//wp_enqueue_script( 'isotope', PRE_THEME_JS .'/masonry.js', array('jquery'), self::theme_ver, '' );
		wp_enqueue_script( 'vtte_script', PRE_THEME_JS .'/script.js', array('jquery'), self::theme_ver, '' );

		//attach data to script.js
		$ajax_data = array(
			'url' => admin_url( 'admin-ajax.php' )
		);
		wp_localize_script( 'vtte_script', 'Ajax', $ajax_data );
	}
}

/**
 * Instantiate the class object
 * You can access its methods or variables by globalizing $wpbp or
 * using the get_instance() method (it's a singleton class, so it won't
 * create a new instance)
 * */

$_s = site::get_instance();


class base {
	static function sidebar_page($post) {
		$sidebar_content = '';
		$sidebar = apply_filters('vtte_page_sidebar', $sidebar_content, $post);
		echo $sidebar;
	}
	static function get_meta_page($post) {
		$sidebar_content = '';
		$sidebar = apply_filters('vtte_page_content_sidebar', $sidebar_content, $post);
		echo $sidebar;	
	}
	static function translate_google_widget($mode='text') {
		global $_set;
		$settings = $_set->settings;
		if (isset($settings['translator_active'])) {
			$selected_mode = ($mode != 'text') ? ', layout: google.translate.TranslateElement.InlineLayout.SIMPLE' : '';
			echo '<div id="google_translate_element"></div>';
			echo '<script type="text/javascript">';
				echo 'function googleTranslateElementInit() {';
		  			echo 'new google.translate.TranslateElement({pageLanguage: \'es\''.$selected_mode.'}, \'google_translate_element\');';
				echo '}';
			echo '</script>';
			echo '<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>';
		}
	}
	static function news_share($post) {
		$has_thumb = ( !has_post_thumbnail( $post->ID ) ) ? ' no-thumb' : '';
		$out = '<div class="column">';
			$out .= '<article class="hentry entry-article'.$has_thumb.'">';
				$out .= '<div class="entry-content">';
					if (has_post_thumbnail( $post->ID )) {
						$out .= '<div class="entry-image">';
							$out .= '<a href="'.get_permalink($post->ID).'">';
								$out .= get_the_post_thumbnail( $book->ID, 'squared' );
								$out .= '<span class="wrap">';
								$out .= '</span>';
							$out .= '</a>';
							$out .= '<div class="social">';
								$out .= sitio::get_facebook_link($post);
								$out .= sitio::get_tweet_link($post);
							$out .= '</div>';
						$out .= '</div>';	
					} else {
						$out .= '<div class="wrap">';
							$out .= '<div class="social">';
								$out .= sitio::get_facebook_link($post);
								$out .= sitio::get_tweet_link($post);
							$out .= '</div>';							
						$out .= '</div>';
					}

					$out .= '</div>';
				$out .= '<h4 class="entry-title"><a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a></h4>';
			$out .= '</article>';
		$out .= '</div>';
        return $out;
	}
}
add_filter( 'wpseo_opengraph_url', 'my_opengraph_url' );

function my_opengraph_url( $url ) {
        return str_replace( 'https://', 'http://', $url );
}
add_filter( 'wpseo_canonical', 'yoast_remove_canonical_search' );

function yoast_remove_canonical_search( $canonical ) {;
	return str_replace( 'https://', 'http://', $canonical );
}
class render {
	static function feature($item, $thumb='featured-page') 	{
		$out = '<div class="hentry entry-feature">';
		$out .= get_the_post_thumbnail($item->ID, $thumb);
		$out .= '<div class="wrap">';
			$out .= '<div class="wrap-content">';
				if (!empty($item->feature_subtitle)) {
					$out .= '<h4 class="subtitle">' . esc_attr($item->feature_subtitle) . '</h4>';
				}
				$out .= '<h3 class="entry-title">' . get_the_title($item->ID) . '</h3>';
				if (!empty($item->post_excerpt)) {
					$out .= '<div class="entry-summary">' . apply_filters('the_content', $item->post_excerpt) . '</div>';
				}
				if (!empty($item->feature_button_url)) {
					$out .= '<a href="' . esc_attr($item->feature_button_url) . '" class="button primary">' . esc_attr($item->feature_button_text) . '</a>';
				}
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</div>';
		return $out;
	}
}
// Disable dashboard updates
add_action('after_setup_theme', 'remove_core_updates');
function remove_core_updates()
{
	if (!current_user_can('update_core')) {
		return;
	}
	add_action('init', create_function('$a', "remove_action( 'init', 'wp_version_check' );"), 2);
	add_filter('pre_option_update_core', '__return_null');
	add_filter('pre_site_transient_update_core', '__return_null');
	add_filter('auto_update_theme', '__return_false');
	add_filter('auto_update_plugin', '__return_false');

	remove_action('load-update-core.php', 'wp_update_plugins');	
}
