<?php 
	get_header(); 
	the_post();
	global $post;
?>
<section class="main-content">
	<div class="row content-header">
		<div class="large-12 columns">
			<?php get_template_part('inc/partials/share','buttons'); ?>
		</div>
	</div>
	<div class="row content">
		<div class="large-8 columns">
			<div class="entry-image default">
				<?php the_post_thumbnail( 'page-default' ); ?>
				<div class="content-wrap">
					<h3 class="entry-title"><?php the_title(); ?></h3>
				</div>
			</div>
			<div class="std-text">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="large-4 columns">
			<aside class="sidebar">
				<?php 
					base::sidebar_page($post);
				?>
				<?php dynamic_sidebar( 'page' ); ?>

			</aside>

		</div>
	</div>
	<?php get_template_part('inc/partials/footer','page'); ?>
</section>
<?php get_footer(); ?>