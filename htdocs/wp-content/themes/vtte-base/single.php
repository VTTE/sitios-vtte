	<?php 
	get_header(); 
	the_post();
?>
<section class="main-content">
	<header class="row content-header">
		<div class="large-12 columns">
			<h4 class="secondary-title">Noticias</h4>
		</div>
		<div class="row">
			<div class="large-6 columns entry-image">
				<?php the_post_thumbnail( 'featured-page' ); ?>
			</div>
			<div class="large-6 columns entry-meta">
				<div class="header-metadata">
					<h3 class="entry-title"><?php the_title(); ?></h3>
					<?php get_template_part('inc/partials/share','buttons'); ?>
					<span class="published">Publicado el <?php echo get_the_date('d - m - Y') ?></span>
				</div>
			</div>
		</div>
	</header>
	<div class="row content">
		<div class="large-8 columns">
			<div class="std-text">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="large-4 columns">
			<aside class="sidebar">
				<?php echo sitio::get_meta_post(get_the_ID()) ?>
				<?php dynamic_sidebar( 'single' ); ?>
			</aside>

		</div>
	</div>
	<?php get_template_part('inc/partials/footer','single'); ?>
</section>
<?php get_footer(); ?>