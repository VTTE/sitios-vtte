(function($){
	//function by @basilio
	$.fn.av_heights = function(){
		tallest = 0;
		$(this).each(function(){
			if ( $(this).height() > tallest ) { tallest = $(this).outerHeight(); };
		})
		$(this).css({'height': tallest});
	}
	$.fn.smoothTop = function(){
		$(this).click(function(){
	    	$("html, body").animate({ scrollTop: 0 }, 600);
	    		return false;
	    });
		
	}
	
	

})(jQuery);

jQuery(document).ready(function($){
	$(document).foundation();
	$('.box').swipebox();
	$('.facebook-share').on('click',function(event){
		/*
			Facebook share api
			- Text
			- Url
			- Caption
			- img
		*/
		event.preventDefault();
		var obj = $(this);
		var text = obj.data('text');
		FB.ui({
		  method: 'feed',
		  link: obj.data('url'),
		  caption: obj.data('caption'),
		  name: obj.data('caption'),
		  description: text,
		  picture: obj.data('img')
		}, function(response){});
	});
});