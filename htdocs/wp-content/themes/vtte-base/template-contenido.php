<?php 
/*
	Template name: Contenido
*/
	get_header(); 
	the_post();
	global $post;
?>
<section class="main-content">
	<header class="row content-header">
		<div class="large-12 columns">
			<nav class="content-nav">
				<div class="content-mobile-nav show-for-small-only">
					<a href="#" class="content-nav-menu">Contenido asociado <span class="dashicons dashicons-arrow-down"></span></a>
					<ul class="menu vertical hide clearfix content-nav-mobile-menu">
						<?php 
							if ($post->post_parent != 0) {
								$post_id = $post->post_parent;
							} else {
								$post_id = $post->ID;
							}
							wp_list_pages(array(
								'child_of' => $post_id,
								'show_date' => '',
								'title_li' => ''
								));
						 ?>
					</ul>
				</div>
				<ul class="menu horizontal clearfix hide-for-small-only">
					<?php 
						if ($post->post_parent != 0) {
							$post_id = $post->post_parent;
						} else {
							$post_id = $post->ID;
						}
						wp_list_pages(array(
							'child_of' => $post_id,
							'show_date' => '',
							'title_li' => ''
							));
					 ?>
				 </ul>
			</nav>
		
		<div class="row">
			<?php if ( has_post_thumbnail() ): ?>
				<div class="large-12 columns entry-image">
					<?php the_post_thumbnail( 'page-featured' ); ?>
					<div class="content-wrap">
						<h3 class="entry-title"><?php the_title(); ?></h3>
					</div>
				</div>
			<?php else: ?>
				<div class="large-12 columns">
					<h3 class="entry-title"><?php the_title(); ?></h3>
				</div>
			<?php endif; ?>
		</div>
		</div>
	</header>
	<div class="row content">
		<div class="large-8 columns">
			<div class="std-text">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="large-4 columns">
			<aside class="sidebar">
				<?php echo base::get_meta_page($post) ?>
				<?php dynamic_sidebar( 'page_contenido' ); ?>

			</aside>

		</div>
	</div>
	<?php get_template_part('inc/partials/footer','page'); ?>
</section>
<?php get_footer(); ?>