</div>
<?php
	global $_set;
	$settings = $_set->settings;
	$facebook_url = (!empty($settings['link_facebook'])) ? esc_url($settings['link_facebook']) : 'https://www.facebook.com/vcm.utem/';
	$twitter_url = (!empty($settings['link_twitter'])) ? esc_url($settings['link_twitter']) : 'https://twitter.com/vcmutem/';
	$youtube_url = (!empty($settings['link_youtube'])) ? esc_url($settings['link_youtube']) : 'https://www.youtube.com/channel/UCyvdEKb5EkDTPzM4HDjdcuw';
	$linkedin_url = (!empty($settings['link_linkedin'])) ? esc_url($settings['link_linkedin']) : 'https://cl.linkedin.com/edu/universidad-tecnol%C3%B3gica-metropolitana-11004';
	$instagram_url = (!empty($settings['link_instagram'])) ? esc_url($settings['link_instagram']) : 'https://www.instagram.com/vcm.utem/';
?>
<footer class="main-footer" id="stick-footer">
	<div class="dark-blue">
		<div class="row top">
			<div class="large-5 columns social clearfix">
				<strong class="float-left">Conéctate con Nosotros: </strong>
				<ul class="menu float-left">
					<li><a href="<?php echo $facebook_url; ?>" target="_blank"><img src="<?php echo PRE_HOME_URI.'/img/rs-facebook.png' ?>" alt="Facebook" title="Facebook" /></a></li>
					<li><a href="<?php echo $twitter_url; ?>" target="_blank"><img src="<?php echo PRE_HOME_URI.'/img/rs-twitter.png' ?>" alt="Twitter" title="Twitter" /></a></li>
					<li><a href="<?php echo $youtube_url; ?>" target="_blank"><img src="<?php echo PRE_HOME_URI.'/img/rs-youtube.png' ?>" alt="Youtube" title="Youtube" /></a></li>
					<li><a href="<?php echo $linkedin_url; ?>" target="_blank"><img src="<?php echo PRE_HOME_URI.'/img/rs-linkedin.png' ?>" alt="Linkedin" title="Linkedin" /></a></li>
					<li><a href="<?php echo $instagram_url; ?>" target="_blank"><img src="<?php echo PRE_HOME_URI.'/img/rs-instagram.png' ?>" alt="Instagram" title="Instagram" /></a></li>
				</ul>
			</div>
			<div class="large-3 columns">
				<img src="<?php echo PRE_HOME_URI.'/img/utem-acreditada-2016-2020-cna.png' ?>" alt="Acreditada por 3 años" />
			</div>
		</div>
	</div>
	<div class="blue">
		<div class="row middle" data-equalizer>
			<div class="large-3 columns" data-equalizer-watch>
				<?php 
					$current_title = ( !empty($settings['footer_col_1']) ) ? $settings['footer_col_1'] : 'Nuestros Sitios:';
				?>
				<h4 class="footer-title"><?php echo $current_title ?></h4>
				 <?php 
	                $args = array(
	                  'theme_location' => 'footer_1',
	                  'container' => '',
	                  'items_wrap' => '<ul id = "%1$s" class = "menu vertical %2$s">%3$s</ul>',
	                  'fallback_cb' => false
	                  );

	              wp_nav_menu( $args );
              ?>
			</div>
			<div class="large-3 columns" data-equalizer-watch>
				<?php $current_title = ( !empty($settings['footer_col_2']) ) ? $settings['footer_col_2'] : 'Servicios:'; ?>
				<h4 class="footer-title"><?php echo $current_title ?></h4>
				<?php 
	                $args = array(
	                  'theme_location' => 'footer_2',
	                  'container' => '',
	                  'items_wrap' => '<ul id = "%1$s" class = "menu vertical %2$s">%3$s</ul>',
	                  'fallback_cb' => false
	                  );

	              wp_nav_menu( $args );
              ?>
			</div>
			<div class="large-3 columns" data-equalizer-watch>
				<?php $current_title = ( !empty($settings['footer_col_3']) ) ? $settings['footer_col_3'] : 'Nuestro Sitio:'; ?>
				<h4 class="footer-title"><?php echo $current_title ?></h4>
				<?php 
	                $args = array(
	                  'theme_location' => 'footer_3',
	                  'container' => '',
	                  'items_wrap' => '<ul id = "%1$s" class = "menu vertical %2$s">%3$s</ul>',
	                  'fallback_cb' => false
	                  );

	              wp_nav_menu( $args );
              ?>
			</div>
			<div class="large-3 columns form" data-equalizer-watch>
				<h4 class="footer-title">Acceso Intranet:</h4>
				<form class="footer_form" action="https://intranet.utem.cl/intranet/inicio.php" method="post">
					<input type="text" class="wide" name="usuario" placeholder="Usuario">
					<input type="password" class="wide" name="clave" placeholder="Contraseña">
					<input type="submit" class="submit" class="button" value="Entrar">
				</form>
			</div>
		</div>
	</div>
	<div class="dark-blue">
		<div class="row bottom">
			<div class="large-6 columns info">
				<?php 
					if (!empty($settings['footer_text'])) {
						echo apply_filters( 'the_content', $settings['footer_text'] );
					} else {
				 ?>
					<address>Dieciocho 161 - Santiago, Chile. Metro Moneda- Fono:227877500 </address>
					<span>Sitio diseñado y desarrollado por la Vicerrectoria de Transferencia Tecnologica y Extensión - UTEM </span>
					<span>Soporte informático VTTE y SISEI</span>
				<?php } ?>
			</div>
			<div class="large-2 columns logo">
				<span class="gob-logo">Gobierno de Chile</span>
			</div>
		</div>
	</div>
</footer>
<!-- twitter SDK -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<!-- facebook SDK -->

<div id="fb-root"></div>
<script>
 window.fbAsyncInit = function() {
    FB.init({
      appId      : '186807828361521',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5&appId=226051430767222";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

(function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/es_LA/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<?php wp_footer(); ?>
</body>
</html>