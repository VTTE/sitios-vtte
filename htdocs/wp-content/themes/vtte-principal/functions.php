<?php 

use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;
/*
	Image Sizes
*/
add_image_size('event-big-home', 568, 383, true);
add_image_size('event-small-home', 270, 180, true);
add_image_size('news-home', 270, 232, true);
add_image_size('news-single', 568, 383, true);
add_image_size('medium-large', 770, 383, true);
add_image_size('large', 1170, 383, true);
add_image_size('height-full', 143,9999,false);

/*
	Calling some files
*/

include STYLESHEETPATH . '/inc/widgets.php';


/*
	Sidebars filters
*/
add_filter('vtte_base_mandatory_sidebars', array('vtte','remove_base_sidebars'));


/*
	Theme sidebars
*/
$mandatory_sidebars = array(
	'Actividad sidebar' => array(
		'name' => 'event',
		'size' => 4
		),
	'Actividad footer izq' => array(
		'name' => 'event_footer_1',
		'size' => 9
		),
	'Actividad footer der' => array(
		'name' => 'event_footer_2',
		'size' => 3
		),
	'Home fila 1' => array(
		'name' => 'sidebar-home-1',
		'size' => 12
		),
	'Home fila 2 izq' => array(
		'name' => 'sidebar-home-2-izq',
		'size' => 6
		),
	'Home fila 2 der' => array(
		'name' => 'sidebar-home-2-der',
		'size' => 6
		),
	'Home arriba' => array(
		'name' => 'home_top',
		'size' => 12
		)
);
$mandatory_sidebars = apply_filters('vtte_base_mandatory_sidebars',$mandatory_sidebars);
foreach ( $mandatory_sidebars as $sidebar => $id_sidebar ) {
	register_sidebar( array(
		'name'          => $sidebar,
		'id'			=> $id_sidebar['name'],
		'before_widget' => '<section id="%1$s" class="widget %2$s">'."\n",
		'after_widget'  => '</section>',
		'before_title'  => '<header class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3></header>',
		'size' => $id_sidebar['size']
	) );
}

class vtte {
	static function remove_base_sidebars($sidebars) {
		unset($sidebars['Página footer 3']);
		unset($sidebars['Entrada footer 3']);
		return $sidebars;
	}
	static function get_last_event($ppp) {
		$events = new WP_Query(array(
				'post_type' => 'event',
				'post_status' => 'publish',
				'posts_per_page' => $ppp
			));
		if ( $events->have_posts() ) {
			return $events->posts;
		} else {
			return false;
		}
	}
	static function day_count($date, $date_end) {
		$date_1 = strtotime($date);
		$date_2 = strtotime($date_end);
		$date_calc = $date_2 - $date_1;

		return '('.floor($date_calc / (60*60*24)).' Días)';

	}
	static function get_first_category_name($post_id){
		$categories = wp_get_post_terms( $post_id, 'sellos' );
		if ( !empty($categories) ) {
			return $categories[0]->name;
		} else {
			return false;
		}
	}
	static function get_meta_event($post_id) {
		$categories = wp_get_post_terms( $post_id, 'sellos' );
		$tags = wp_get_post_terms( $post_id, 'post_tag' );
		echo '<section class="widget meta_post">';
			echo '<h5 class="condensed bold">Categorías</h5>';
			if ( !empty( $categories ) ) {
				foreach ( $categories as $category ) {
					$link = ( !is_wp_error( get_category_link( $category ) ) ) ? get_term_link( $category, 'sellos' ) : '#';
					echo '<label class="category dark-grey"><a href="'.$link.'">'.$category->name.'</a></label><br>';
				}
			}
			if ( !empty( $tags ) ) {
				foreach ( $tags as $tag ) {
					$link = ( !is_wp_error( get_term_link( $tag, 'event_tags') ) ) ? get_tag_link( $tag, 'event_tags' ) : '#';
					echo '<label class="category grey"><a href="'.$link.'">'.$tag->name.'</a></label><br>';
				}
			}

		echo '</section>';
	}
	static function get_last_news($ppp=4) {
		$news = new WP_Query(array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => $ppp,
			'category_name' => 'noticias'
			));
		if ( $news->have_posts() ) {
			foreach ( $news->posts as $item ) {
				echo vRender::news($item);
			}
		} else {
			return null;
		}
	}
	static function get_meta_page($sidebar, $post) {
		if ( is_object( $sidebar ) ) {
			$post = $sidebar;
			$sidebar = '';
		}
		$icon = ( !empty( $post->page_icon ) ) ? $post->page_icon : 'info';

		if ( !empty( $post->page_info ) ) {

			$sidebar .= '<div class="widget-single-info margin-bottom-sm">';
				$sidebar .= '<div class="info">';
					$sidebar .= '<h5 class="bold condensed uppercase">'.$post->page_title.'</h5>';
				$sidebar .= '</div>';
			$sidebar .= '</div>';

			$sidebar .= '<div class="widget-single-info">';
				$sidebar .= '<div class="icon">';
					$sidebar .= '<i class="dashicons dashicons-'.$icon.'"></i>';
				$sidebar .= '</div>';
				$sidebar .= '<div class="info page-sidebar-text">';
					$sidebar .= apply_filters( 'the_content', $post->page_info );
				$sidebar .= '</div>';
			$sidebar .= '</div>';
			return $sidebar;
		} else {
			return false;
		}
	}
	static function letter_tag($slug) {
		switch ($slug) {
			case 'Deporte':
				return 'dep';
				break;
			case 'Desarrollo Cultural':
				return 'C';
			break;
			default:
				return substr($slug,0,1);
				break;
		}
	}
	static function color_tag($slug) {
		switch ($slug) {
			case 'deporte':
				return '#000000';
			break;
			case 'desarrollo-cultural':
				return '#eb1e97';
			break;
			case 'editorial':
				return '#ec0606';
			break;
			case 'sustentabilidad':
				return '#abcc37';
			break;
			case 'responsabilidad-social-universitaria':
				return '#eca71e';
			break;
			case 'tecnologia':
				return '#0086cd';
			break;
			default:
				return 'transparent';
			break;
		}
	}
}

/*
	Add filters
*/

add_filter('vtte_page_content_sidebar',array('vtte','get_meta_page'),11,2);

class vRender {
	static function bigHome($post) {
		$out = '';
		$topics = wp_get_post_terms( $post->ID, 'sellos' );
		$link = ( !is_wp_error( get_term_link( $topics[0], 'sellos' ) ) ) ? get_term_link( $topics[0], 'sellos' ) : '#';
		$color = vtte::color_tag($topics[0]->slug);
		$status = get_post_meta($post->ID, 'event_status', true);
		$date = get_post_meta($post->ID,'event_date',true);
		$out .= '<div class="widget-post widget-lg gradient">';
			$out .= '<div class="widget-img">';
				$out .= '<a href="'.get_permalink($post->ID).'">';
					$out .= get_the_post_thumbnail( $post->ID, 'event-big-home' );
				$out .= '</a>';
			$out .= '</div>';
			$out .= '<div class="widget-header">';
				$out .= '<div class="top-left">';
					$out .= '<a href="'.$link.'">';
						$out .= '<label class="category category-blue" style="background-color:'.$color.'"><span>'.vtte::letter_tag($topics[0]->name).'</span></label>';
					$out .= '</a>';
					$out .= '<div class="widget-social">';
						$out .= sitio::get_facebook_link($post);
						$out .= sitio::get_tweet_link($post);
					$out .= '</div>';
				$out .= '</div>';
				$out .= '<div class="widget-date">';
					$out .= '<div class="day">';
						$out .= '<h3>'.mysql2date('d',$date).'</h3>';
						$out .= '<p>'.mysql2date('l',$date).'</p>';
					$out .= '</div>';
					$out .= '<div class="month">';
						$out .= '<h3>'.mysql2date('F',$date).'</h3>';
						$out .= '<p>'.mysql2date('Y',$date).'</p>';
					$out .= '</div>';
				$out .= '</div>';
			$out .= '</div>';
			// if the event was cancelled
			if ($status == 2) {
				$out .= '<div class="status-label">';
					$out .= '<span class="label alert">Evento Cancelado</span>';
				$out .= '</div>';
			}	
			$out .= '<div class="widget-body">';
				$out .= '<p><a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a></p>';

				$out .= '<span><a href="'.$link.'">'.$topics[0]->name.'</a></span>';
			$out .= '</div>';
		$out .= '</div>';
		return $out;
	}
	static function mediumHome($post) {
		$out = '';
		$topics = wp_get_post_terms( $post->ID, 'sellos' );
		$link = ( !is_wp_error( get_term_link( $topics[0], 'sellos' ) ) ) ? get_term_link( $topics[0], 'sellos' ) : '#';
		$color = vtte::color_tag($topics[0]->slug);
		$date = get_post_meta($post->ID,'event_date',true);
		$status = get_post_meta($post->ID, 'event_status', true);
		$out .= '<div class="widget-post gradient">';
			$out .= '<div class="widget-img">';
				$out .= '<a href="'.get_permalink($post->ID).'">'.get_the_post_thumbnail($post->ID, 'event-small-home').'</a>';
			$out .= '</div>';
			$out .= '<div class="widget-header">';
				$out .= '<a href="'.$link.'"><label class="category category-purple" style="background-color:'.$color.'"><span>'.vtte::letter_tag($topics[0]->name).'</span></label></a>';
				if ($status == 2) {
					$out .= '<div class="status-label">';
						$out .= '<span class="label alert">Evento Cancelado</span>';
					$out .= '</div>';
				}	
				$out .= '<div class="widget-social">';
					$out .= sitio::get_facebook_link($post);
					$out .= sitio::get_tweet_link($post);
				$out .= '</div>';
			$out .= '</div>';
			$out .= '<div class="widget-body">';
				$out .= '<p><a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a></p>';
			$out .= '</div>';
			$out .= '<div class="widget-footer">';
				$out .= '<p><a href="'.get_permalink($post->ID).'">'.mysql2date('d F Y', $date).'</a></p>';
				$out .= '<span><a href="'.$link.'">'.$topics[0]->name.'</a></span>';
			$out .= '</div>';
		$out .= '</div>';
		return $out;
	}
	static function miniHome($post) {
		$out = '';
		$topics = wp_get_post_terms( $post->ID, 'sellos' );
		$color = vtte::color_tag($topics[0]->slug);
		$link = ( !is_wp_error( get_term_link( $topics[0], 'sellos' ) ) ) ? get_term_link( $topics[0], 'sellos' ) : '#';
		$date = get_post_meta($post->ID,'event_date',true);
		$end_date = get_post_meta($post->ID,'event_date_end', true);
		$class_date = (!empty($end_date)) ? ' has-end-date' : '';
		$status = get_post_meta($post->ID, 'event_status', true);
		$out .= '<div class="widget-post event list">';
			$out .= '<div class="widget-img">';
				$out .= get_the_post_thumbnail($post->ID, 'event-small-home');
			$out .= '</div>';
			$out .= '<div class="widget-header">';
				$out .= '<a href="'.$link.'"><label class="category category-yellow" style="background-color:'.$color.'"><span>'.vtte::letter_tag($topics[0]->name).'</span></label></a>';
				$out .= '<div class="widget-date'.$class_date.'">';
					$out .= '<a href="'.get_permalink($post->ID).'">';
						$out .= '<div class="day">';
						$out .= '<h3>'.mysql2date('d',$date).'</h3>';
						if (!empty($end_date)) {
							$out .= '<p>'.mysql2date('d',$end_date).'</p>';
						} else {
							$out .= '<p>'.mysql2date('l',$date).'</p>';
						}
					$out .= '</div>';
					$out .= '<div class="month">';
						$out .= '<h3>'.mysql2date('F',$date).'</h3>';
						if (!empty($end_date)) {
							$out .= '<p>'.mysql2date('F',$end_date).'</p>';
							$out .= '<span class="al">al</span>';
						} else {
							$out .= '<p>'.mysql2date('Y',$date).'</p>';
						}
					$out .= '</div>';
					$out .= '</a>';
				$out .= '</div>';
				if ($status == 2) {
					$out .= '<div class="status-label">';
						$out .= '<span class="label alert">Evento Cancelado</span>';
					$out .= '</div>';
				}	
			$out .= '</div>';
			$out .= '<div class="widget-body">';
				$out .= '<p><a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a></p>';
			$out .= '</div>';
			$out .= '<div class="widget-footer">';
				$out .= '<span><a href="'.$link.'">'.$topics[0]->name.'</a></span>';
			$out .= '</div>';
		$out .= '</div>';
		return $out;
	}
	static function news($post) {
		$out = '';
		$out .= '<article class="column hentry entry-news">';
			$out .= '<div class="widget-news">';
				$out .= '<div class="widget-img">';
					$out .= '<a href="'.get_permalink($post->ID).'" class="entry-image">'.get_the_post_thumbnail($post->ID,'post-home').'</a>';
				$out .= '</div>';
				$out .= '<div class="widget-body">';
					$out .= '<p><a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a></p>';
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</article>';
		return $out;
	}
	static function gallery($post) {
		$out = '';
		$out .= '<article class="column hentry entry-news entry-gallery">';
			$out .= '<div class="widget-news widget-gallery">';
				$out .= '<div class="widget-img">';
					$out .= '<a href="'.get_permalink($post->ID).'" class="entry-image">'.get_the_post_thumbnail($post->ID,'squared').'</a>';
				$out .= '</div>';
				$out .= '<div class="entry-icon"><span class="dashicons dashicons-format-gallery"></span></div>';
				$out .= '<div class="widget-body">';
					$out .= '<p><a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a></p>';
				$out .= '</div>';
			$out .= '</div>';
		$out .= '</article>';
		return $out;
	}
	static function mini_gallery($post) {
		$out = '';
		$out .= '<article class="column hentry entry-news entry-gallery mini-gallery">';
			$out .= '<div class="widget-news widget-gallery">';
				$out .= '<div class="widget-img">';
					$out .= '<a href="'.get_permalink($post->ID).'" class="entry-image">'.get_the_post_thumbnail($post->ID,'squared').'</a>';
				$out .= '</div>';
				$out .= '<div class="entry-icon"><span class="dashicons dashicons-format-gallery"></span></div>';
			$out .= '</div>';
			$out .= '<div class="widget-body">';
				$out .= '<p><a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a></p>';
			$out .= '</div>';
		$out .= '</article>';
		return $out;
	}
	static function document($post) {
		$document = get_post($post);
		if (!empty( $document->document_download )) {
			try {
				$file = new \GutenPress\Helpers\Attachment( $document->document_download );
			} catch ( Exception $e ) {
				$out .= '<!-- Attachment: no existe -->';
			}
		}
		$url_file = ( !empty( $file ) ) ? $file->url : $document->document_url;
		
		$out = '';
		$out .= '<div class="widget-documents">';
			$out .= '<div class="widget-img">';
				$out .= '<a href="'.$url_file.'">'.get_the_post_thumbnail($document->ID, 'height-full').'</a>';
			$out .= '</div>';
			$out .= '<div class="widget-body">';
				$out .= '<p>'.get_the_title($document->ID).'</p>';
				$extension = ( !empty( $file->pathinfo->extension ) ) ? $file->pathinfo->extension : 'PDF';
				$out .= '<span><a href="'.$url_file.'">DESCARGAR formato '.strtoupper($extension).'</a></span>';
			$out .= '</div>';
		$out .= '</div>';

		return $out;
	}
}


// search filter
function make_search_filter($query) {
	$pt = $query->get('post_type');
	if ( empty( $pt ) ) {
		if ( !$query->is_admin && $query->is_search) {
			$query->set('post_type', array('post','event', 'document') ); // id of page or post
		}
	}
	return $query;
}
add_filter( 'pre_get_posts', 'make_search_filter' );

add_action( 'init', 'add_tag_to_event' );
function add_tag_to_event() {
    register_taxonomy_for_object_type( 'post_tag', 'event' );
};