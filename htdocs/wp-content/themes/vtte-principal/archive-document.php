<?php get_header(); ?>
<section class="main-content">
	<header class="row section-header">
		<div class="large-12 columns">
			<h3 class="secondary-title"><?php echo sitio::page_title() ?></h3>
		</div>
	</header>
	<div class="row content">
		<div class="large-12 columns">
			<div class="std-text">
				<?php the_content(); ?>
			</div>
			<?php 
				if (have_posts()): 
					echo '<div class="large-up-3 medium-up-2 small-up-1 clearfix">';
					while ( have_posts() ): the_post();
						global $post;
						$file_id = $post->document_download;
						$file_url = $post->document_url;
						$file = null;

				        try {
				                $file = new \GutenPress\Helpers\Attachment($file_id);
				            } catch ( Exception $e ) {
				                echo '<!-- Attachment: no existe -->';
				            }
				        if (!empty($file)) {
				            $url_file = $file->url;
				        } else {
				            $url_file = $file_url;
				        }
				        $size = ( $file->filesize != '' ) ? ' ( '.$file->filesize.' )' : '';
				        $extension = (!empty($file->pathinfo->extension)) ? $file->pathinfo->extension : 'PDF';

						echo '<div class="column">';
							echo '<article class="hentry entry-document">';
								echo '<div class="row" data-equalizer>';
									echo '<div class="large-6 columns">';
										echo '<div class="document-img" data-equalizer-watch>';
											the_post_thumbnail( 'squared' );
										echo '</div>';
									echo '</div>';
									echo '<div class="large-6 columns">';
										echo '<div class="document-content" data-equalizer-watch>';
											echo '<h5 class="entry-title"><a href="'.$url_file.'">'.get_the_title().'</a></h5>';
											echo '<div class="link-file">';
												echo '<a href="'.$url_file.'">';
													echo '<strong>DESCARGAR</strong>';
													echo '<em>formato '.$extension.'</em>';
												echo '</a>';
											echo '</div>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
							echo '</article>';
						echo '</div>';
					endwhile;
					echo '</div>';
					if (function_exists( 'wp_pagenavi' )) {
						wp_pagenavi();
					}
				endif;
			?>
		</div>
	</div>
</section>
<?php get_footer(); ?>