<?php get_header() ?>
<?php 
	$search = new search_filter();
	$search->set_post_type('event');
	if ( get_query_var('paged') ) {
		$search->set_page(get_query_var('paged'));
	}
	if ( isset($_GET['action']) ) {
		if (isset($_GET['seal'])) {
			$search->set_taxonomies(array(esc_attr($_GET['seal'])));
		}
		$date = array();
		if ( !empty($_GET['date_month'])) {
			$date['month'] = esc_attr( $_GET['date_month'] );
		}
		if ( !empty( $_GET['date_year'] ) ) {
			$date['year'] = esc_attr( $_GET['date_year'] );
		}
		if ( !empty($date) ) {
			$search->set_date($date);
		}
	}
	$query = $search->search();
 ?>
<section class="main-content">
	<header class="row section-header">
		<div class="large-12 columns">
			<h3 class="secondary-title"><?php sitio::page_title() ?> <?php echo get_queried_object()->name ?> </h3>
			<div class="filter-form">
				<form action="" method="GET">
					<div class="row">
						<div class="large-2 columns">
							<h5>Buscar Por</h5>
						</div>
						<div class="large-4 columns">
							<select name="seal" class="input-type">
								<option value="">Seleccionar</option>
								<?php 
									$terms = get_terms('sellos');
									foreach ($terms as $term) {
										$selected = ( !empty($_GET['seal']) && ($_GET['seal'] == $term->slug) ) ? ' selected="selected" ' : '';
										echo '<option value="'.$term->slug.'"'.$selected.'>'.$term->name.'</option>';
									}
								 ?>
							</select>
						</div>
						<div class="large-1 columns">
							<?php echo $search->get_months_select(array( 
								'name' => 'date_month',
								'class' => 'input-type'
							), $_GET['date_month']); ?>
						</div>
						<div class="large-1 columns">
							<?php echo $search->get_years_select(array( 
								'name' => 'date_year',
								'class' => 'input-type'
							), $_GET['date_year']); ?>
						</div>
						<div class="large-1 columns end">
							<input type="submit" class="button secondary" value="Buscar">
							<input type="hidden" name="action" value="send">
						</div>
					</div>
				</form>
			</div>
		</div>
	</header>
	<div class="row content">
		<div class="large-12 columns">
			<?php 
				if ( $query->have_posts() ):
					echo '<div class="large-up-4 small-up-2">';
						while( $query->have_posts() ): $query->the_post();
							global $post;
							echo '<div class="column">';
								echo vRender::miniHome($post);
							echo '</div>';
						endwhile;
						if ( function_exists('wp_pagenavi')) {
							wp_pagenavi(array('query' => $query));
						}
					echo '</div>';
					else:
						echo '<div class="callout warning"><h5>Lo sentimos</h5> <p>No se han encontrado resultados para lo que buscas</p> </div>';
				endif;
			 ?>
		</div>
	</div>
</section>
<?php get_footer() ?>