<?php 
	get_header();
	get_post();
	global $post;
	setup_postdata( $post );
	$topics = wp_get_post_terms( $post->ID, 'sellos' );
	$link = ( !is_wp_error( get_term_link( $topics[0], 'sellos' ) ) ) ? get_term_link( $topics[0], 'sellos' ) : '#';
	$color = vtte::color_tag($topics[0]->slug);
?>
<section class="main">
	<div class="row columns">
		<h3 class="heading slab"><span class="info cyan no-margin-left">Actividades de <?php echo vtte::get_first_category_name($post->ID) ?></span></h3>
	</div>
	<div class="row">
		<div class="columns large-6 medium-6">
			<div class="widget-post widget-lg">
				<div class="widget-img">
					<?php the_post_thumbnail( 'news-single' ); ?>
				</div>
				<div class="widget-header">
					<div class="top-left">
						<a href="<?php echo $link ?>"><label class="category category-blue" style="background-color:<?php echo $color ?>"><span><?php echo vtte::letter_tag($topics[0]->name) ?></span></label></a>
					</div>
				</div>
			</div>
		</div>
		<div class="columns large-6 medium-6">
			<?php  
				if ($post->event_status == 2) {
					echo '<span class="label alert">Evento Cancelado</span>';
				}
			?>
			<h2 class="heading heading-lg"><?php the_title() ?></h2>
			<?php get_template_part('inc/partials/share','buttons'); ?>
			<span class="additional-info">
				 <?php // echo 'Publicado el'. get_the_date('d - m - Y') ?>
			</span>
		</div>
	</div>
	<div class="row">
		<div class="columns large-8 medium-7 content std-text">
			<?php the_content(); ?>
		</div>
		<div class="columns large-4 medium-5">
			<aside class="sidebar">
				<div class="widget-single-info">
					<div class="icon">
						<i class="dashicons dashicons-calendar-alt"></i>
					</div>
					<div class="info date cyan">
						<div class="day">
							<h3><?php echo mysql2date('d',$post->event_date) ?></h3>
							<p><?php echo mysql2date('l',$post->event_date) ?></p>
						</div>
						<div class="month">
							<h3><?php echo mysql2date('F',$post->event_date) ?></h3>
							<p><?php echo mysql2date('Y',$post->event_date) ?></p>
						</div>
					</div>
					<?php if (!empty($post->event_date_end)): ?>
						<div class="info date cyan end-date">
							<div class="and-date">Al</div>
							<div class="day">
								<h3><?php echo mysql2date('d',$post->event_date_end) ?></h3>
								<p><?php echo mysql2date('l',$post->event_date_end) ?></p>
							</div>
							<div class="month">
								<h3><?php echo mysql2date('F',$post->event_date_end) ?></h3>
								<p><?php echo mysql2date('Y',$post->event_date_end) ?></p>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<div class="widget-single-info">
					<div class="icon">
						<i class="dashicons dashicons-clock"></i>
					</div>
					<div class="info">
						<h5 class="condensed"><?php echo $post->event_hour ?></h5>
						<?php if (!empty($post->event_hour_end)): ?>
							<div class="hour-end">
								<div class="and-hour">Hasta las</div>
								<h5 class="condensed"><?php echo $post->event_hour_end ?></h5>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="widget-single-info">
					<div class="icon">
						<i class="dashicons dashicons-location"></i>
					</div>
					<div class="info">
						<?php if ( !empty( $post->event_location ) ): ?>
							<h5 class="condensed bold blue"><?php echo $post->event_location ?></h5>
						<?php endif; ?>
						<?php if ( !empty( $post->event_campus ) ): ?>
							<h5 class="condensed bold blue"><?php echo $post->event_campus ?></h5>
						<?php endif; ?>
						<?php if ( !empty( $post->event_address ) ): ?>
							<p class="condensed light"><?php echo $post->event_address ?></p>
						<?php endif; ?>
					</div>
				</div>
				<?php 
					if ( !empty( $post->event_program ) ): 
					
							$file = '';
							try {
								$file = new \GutenPress\Helpers\Attachment($post->event_program);
							} catch ( Exception $e ) {
								echo '<!-- Attachment: no existe -->';
							}
					?>
					<div class="widget-single-info">
						<div class="icon">
							<i class="dashicons dashicons-admin-page"></i>
						</div>
						<div class="info cyan">
							<h5 class="condensed bold"><a href="<?php echo $file->url  ?>">Descargar programa</a></h5>
						</div>
					</div>
				<?php endif; ?>
				
				<div class="widget-single-info">
					<div class="icon">
						<i class="dashicons dashicons-tag"></i>
					</div>
					<div class="info">
						<?php echo vtte::get_meta_event(get_the_ID()) ?>
					</div>
				</div>
				<?php dynamic_sidebar( 'event' ); ?>
			</aside>

		</div>
	</div>
	<div class="row">
		<div class="columns large-4 end">
			<hr>
		</div>
	</div>
	<footer class="row event-footer inner-space">
		<div class="columns large-9 medium-6 small-12 clear">
			<?php dynamic_sidebar( 'event_footer_1' ); ?>
		</div>
		<div class="columns large-3 medium-6 small-12 clear">
			<?php dynamic_sidebar( 'event_footer_2' ); ?>
		</div>
	</footer>
</section>
<?php get_footer(); ?>