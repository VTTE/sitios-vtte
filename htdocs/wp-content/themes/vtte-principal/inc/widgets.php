<?php

class WP_Widget_events extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'events', 'description' => 'Muestra los últimos eventos del sitio');
        $control_ops = array();
        parent::__construct('events', 'Últimos eventos', $widget_ops, $control_ops);
    }
    function get_last_events($size, $seal=null) {
    	$args = array(
                'post_type' => 'event',
                'posts_per_page' => $size,
                'post_status' => 'publish'
            );
    	if ( !empty( $seal ) ) {
    		$args['tax_query'] = array(
    				array(
    					'taxonomy' => 'sellos',
    					'field' => 'slug',
    					'terms' => $seal
     					)
    			);
    	}
    	if ( is_single() ) {
            global $post;
            $args['post__not_in'] = array($post->ID);
        }
        $events = new WP_Query($args);
        if ( $events->have_posts() ) {
            return $events->posts;
        } else {
            return false;
        }
    }
    function get_seal() {
    	if (is_single()) {
    		global $post;
    		$terms = wp_get_post_terms( $post->ID, 'sellos' );
    		if ( !empty( $terms ) ) {
    			$term = current( $terms );
    			return $term->slug;
    		} else {
    			return null;
    		}
    	} else {
    		return null;
    	}
    }
    function widget($args, $instance) {
        global $post;
        extract( $instance );
        extract( $args );
        $size = ( !empty( $instance['size'] ) ) ? $instance['size'] : 1;
        $grid = ( !empty( $instance['grid'] ) ) ? $instance['grid'] : 1;
        $link_text = ( !empty( $instance['link_text'] ) ) ? $instance['link_text'] : 'Más actividades';
        $seal = $this->get_seal();
        $events = $this->get_last_events($size, $seal);
        if ( !empty( $events ) ) {
            echo '<div class="widget events">';
                echo '<h4 class="widget-title">'.$title.'</h4>';
                echo '<div class="widget-content clearfix">';
                    echo '<div class="large-up-'.$grid.' medium-up-'.$grid.' small-up-2 event-list">';
                        foreach ($events as $item) {
                        	$topics = wp_get_post_terms( $item->ID, 'sellos' );
							$color = vtte::color_tag($topics[0]->slug);
							$link = ( !is_wp_error( get_term_link( $topics[0], 'sellos' ) ) ) ? get_term_link( $topics[0], 'sellos' ) : '#';
                        	echo '<div class="column">';
                            	echo vRender::miniHome($item);
                            echo '</div>';
                        }

                    echo '</div>';
                    if ( !empty( $instance['is_link'] ) ) {
                        $link = get_post_type_archive_link( 'event' );
				        echo '<div class="btn-more line">';
							echo '<a href="'.$link.'" class="btn">';
								echo '<span>'.$link_text.'</span>';
							echo '</a>';
						echo '</div>';
                    }
                echo '</div>';
            echo '</div>';
        }
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
       echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'. $this->get_field_name('is_link').'">Link a archivo de noticias? </label><input type="checkbox" id="'. $this->get_field_id('is_link').'"'.( ( !empty( $is_link ) ) ? ' checked="checked" ' : '' ).' name="'.$this->get_field_name('is_link').'" value="1"></p>';
        echo '<p><label for="'.$this->get_field_id('link_text').'">Texto link: <input type="text" name="'. $this->get_field_name('link_text') .'" id="'.$this->get_field_id('link_text').'" value="'.$instance['link_text'].'" class="widefat"/></label><br><small>Si no se especifica será: "Más actividades"</small></p>';
        echo '<p><label for="'.$this->get_field_id('size').'">Cantidad de entradas: <input type="number" name="'. $this->get_field_name('size') .'" id="'.$this->get_field_id('size').'" value="'.$instance['size'].'"/></label></p>';
        echo '<h3>Apariencia</h3>';
        echo '<p><label>Grilla: </label>';
            echo '<select class="widefat" id="'.$this->get_field_id('grid').'" name="'.$this->get_field_name('grid').'">';
                echo '<option value="">Seleccione</option>';
                echo '<option value="1" '.(($grid == '1') ? 'selected="selected"' : '') .'>1</option>';
                echo '<option value="2" '.(($grid == '2') ? 'selected="selected"' : '') .'>2</option>';
                echo '<option value="3" '.(($grid == '3') ? 'selected="selected"' : '') .'>3</option>';
                echo '<option value="4" '.(($grid == '4') ? 'selected="selected"' : '') .'>4</option>';
            echo '</select>';
            echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
        echo '</p>';
       } 
}
class WP_Widget_document extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'document', 'description' => 'Muestra un documento con su información relacionada');
        $control_ops = array();
        parent::__construct('document', 'Documento', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        extract( $instance );
        extract( $args );
        $file_id = get_post_meta($document,'document_download',true);
        $file_url = get_post_meta($document,'document_url',true);

        try {
                $file = new \GutenPress\Helpers\Attachment($file_id);
            } catch ( Exception $e ) {
                echo '<!-- Attachment: no existe -->';
            }
        
        if (!empty($file)) {
            $url_file = $file->url;
        } else {
            $url_file = $file_url;
        }
        $size = ( $file->filesize != '' ) ? ' ( '.$file->filesize.' )' : '';
        $extension = (!empty($file->pathinfo->extension)) ? $file->pathinfo->extension : 'PDF';

        $out .= '<div class="widget img mini-side">';
            $out .= '<div class="widget-content">';
                $out .= '<div class="widget-image">';
                    $out .= '<a href="'.$url_file.'">';
                        $out .= get_the_post_thumbnail( $document, 'side-img' );
                    $out .= '</a>';
                $out .= '</div>';
                $out .= '<div class="widget-side-content end">';
                    $out .= '<h5 class="entry-subtitle"><a href="'.$url_file.'">'.get_the_title($document).'</a></h5>';
                    if (!empty($instance['description'])) {
                        $out .= '<p class="entry-summary">'.$instance['description'].'</p>';
                    } else if (has_excerpt( $document )) {
                        $out .= '<p class="entry-summary">'.apply_filters('the_excerpt', get_post_field('post_excerpt', $document)).'</p>';
                    }
                $out .= '</div>';
            $out .= '</div>';
        $out .= '</div>';

        echo $out;
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }
    function get_documents($sel){
        $pub = new WP_Query(array(
                'post_type' => 'document',
                'posts_per_page' => -1
            ));
        $out = '<p><label>Documentos <select name='.$this->get_field_name('document').' class="widefat">';
        $out .= '<option value="">Seleccione</option>';
        foreach ($pub->posts as $graphic) {
            $class = ($sel == $graphic->ID) ? ' selected="selected"' : '';
            $out .= '<option value="'.$graphic->ID.'"'.$class.'>'.$graphic->post_title.'</option>';
        }
        $out .= '</select></label></p>';

        return $out;
    }
    function form( $instance ) {
        extract( $instance );
        echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'.$this->get_field_id('description').'">Bajada: <textarea name="'. $this->get_field_name('description') .'" id="'.$this->get_field_id('description').'" class="widefat">'.$instance['description'].'</textarea></label></p>';
        echo $this->get_documents($document);
        echo '</p>';
       } 

}
class WP_Widget_galeries extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'galeries', 'description' => 'Muestra las últimas galerías del sitio');
        $control_ops = array();
        parent::__construct('galeries', 'Últimas galerias', $widget_ops, $control_ops);
    }
    function get_last_galleries($size) {
    	$args = array(
                'post_type' => 'gallery',
                'posts_per_page' => $size,
                'post_status' => 'publish'
            );
    	if ( is_single() ) {
            global $post;
            $args['post__not_in'] = array($post->ID);
        }
        $galleries = new WP_Query($args);
        if ( $galleries->have_posts() ) {
            return $galleries->posts;
        } else {
            return false;
        }
    }
    function widget($args, $instance) {
        global $post;
        extract( $instance );
        extract( $args );
        $size = ( !empty( $instance['size'] ) ) ? $instance['size'] : 1;
        $grid = ( !empty( $instance['grid'] ) ) ? $instance['grid'] : 1;
        $link_text = ( !empty( $instance['link_text'] ) ) ? $instance['link_text'] : 'Más Galerías';
        $galleries = $this->get_last_galleries($size);
        if ( !empty( $galleries ) ) {
            echo '<div class="widget events galleries">';
                echo '<h4 class="widget-title">'.$title.'</h4>';
                echo '<div class="widget-content clearfix">';
                    echo '<div class="row large-up-'.$grid.' medium-up-'.$grid.' small-up-2 event-list">';
                        foreach ($galleries as $item) {
                                if ($grid > 4) {
                                    echo vRender::mini_gallery($item);
                                } else {
                                    echo vRender::gallery($item);
                                }
                        }

                    echo '</div>';
                    if ( !empty( $instance['is_link'] ) ) {
                        $link = get_post_type_archive_link( 'gallery' );
				        echo '<div class="btn-more line">';
							echo '<a href="'.$link.'" class="btn">';
								echo '<span>'.$link_text.'</span>';
							echo '</a>';
						echo '</div>';
                    }
                echo '</div>';
            echo '</div>';
        }
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
       echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'. $this->get_field_name('is_link').'">Link a archivo de galerías? </label><input type="checkbox" id="'. $this->get_field_id('is_link').'"'.( ( !empty( $is_link ) ) ? ' checked="checked" ' : '' ).' name="'.$this->get_field_name('is_link').'" value="1"></p>';
        echo '<p><label for="'.$this->get_field_id('link_text').'">Texto link: <input type="text" name="'. $this->get_field_name('link_text') .'" id="'.$this->get_field_id('link_text').'" value="'.$instance['link_text'].'" class="widefat"/></label><br><small>Si no se especifica será: "Más actividades"</small></p>';
        echo '<p><label for="'.$this->get_field_id('size').'">Cantidad de entradas: <input type="number" name="'. $this->get_field_name('size') .'" id="'.$this->get_field_id('size').'" value="'.$instance['size'].'"/></label></p>';
        echo '<h3>Apariencia</h3>';
        echo '<p><label>Grilla: </label>';
            echo '<select class="widefat" id="'.$this->get_field_id('grid').'" name="'.$this->get_field_name('grid').'">';
                echo '<option value="">Seleccione</option>';
                echo '<option value="1" '.(($grid == '1') ? 'selected="selected"' : '') .'>1</option>';
                echo '<option value="2" '.(($grid == '2') ? 'selected="selected"' : '') .'>2</option>';
                echo '<option value="3" '.(($grid == '3') ? 'selected="selected"' : '') .'>3</option>';
                echo '<option value="4" '.(($grid == '4') ? 'selected="selected"' : '') .'>4</option>';
                echo '<option value="5" ' . (($grid == '5') ? 'selected="selected"' : '') . '>5</option>';
                echo '<option value="6" ' . (($grid == '6') ? 'selected="selected"' : '') . '>6</option>';
            echo '</select>';
            echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
        echo '</p>';
       } 
}
// register WP_Widgets
add_action('widgets_init', 'register_vtte_widgets');
add_action('init', 'register_vtte_widgets');
function register_vtte_widgets(){
    register_widget('WP_Widget_events');
    register_widget('WP_Widget_document');
    register_widget('WP_Widget_galeries');
}