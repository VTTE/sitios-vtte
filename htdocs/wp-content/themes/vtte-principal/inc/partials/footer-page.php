<footer class="row article-footer">
	<div class="large-9 columns">
		<?php dynamic_sidebar( 'page_footer_1' );  ?>
	</div>
	<div class="large-3 columns">
		<?php dynamic_sidebar( 'page_footer_2' );  ?>
	</div>
</footer>