<?php get_header(); ?>
<section class="main main-content">
	<?php 
		if ( is_active_sidebar( 'home_top' ) ) {
			echo '<div class="row home_top">';
				echo '<div class="large-12 columns">';
					dynamic_sidebar( 'home_top' );
				echo '</div>';
			echo '</div>';
		}
	 ?>
	<div class="row columns">
		<h3 class="heading slab"><span class="primary-title"> Actividades de Vinculación con el Medio</span> <span class="float-right info cyan uppercase no-margin-right no-margin-bottom"><a href="<?php echo site_url('actividades') ?>">REVISA TODAS LAS ACTIVIDADES</a></span></h3>
	</div>
	<?php 
		$last_events = vtte::get_last_event(7);
	 ?>
	<div class="row">
		<div class="columns large-6 medium-6 small-12">
			<?php 
				$current = $last_events[0];
				echo vRender::bigHome( $current ) 
			?>

		</div>
		<div class="columns large-3 medium-3 small-6 mobile-featured-medium">
			<?php 
				$current = $last_events[1];
				echo vRender::mediumHome( $current ) 
			?>
		</div>
		<div class="columns large-3 medium-3 small-6 mobile-featured-medium">
			<?php 
				$current = $last_events[2];
				echo vRender::mediumHome( $current ) 
			?>
		</div>
	</div>
	<div class="row">
		<?php 
			for( $x=3; $x<=6; $x++) {
				echo '<div class="columns large-3 small-6 medium-6 clear event-item-in-list">';
					echo vRender::miniHome($last_events[$x]);
				echo '</div>';
			}
		 ?>
	</div>

	<div class="row inner-space">
		<div class="large-12 columns sidebar-home-1">
			<?php dynamic_sidebar( 'sidebar-home-1' ); ?>
		</div>
	</div>
	<div class="row inner-space remove-bottom">
		<div class="column large-6 medium-6 small-12 sidebar-home-2-izq">
			<?php dynamic_sidebar( 'sidebar-home-2-izq' ); ?>
		</div>
		<div class="columns large-6 medium-6 small-12 sidebar-home-2-der">
			<?php dynamic_sidebar( 'sidebar-home-2-der' ); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>