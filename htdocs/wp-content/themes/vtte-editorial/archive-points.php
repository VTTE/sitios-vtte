<?php get_header(); ?>
<section class="main-content">
	<div class="row inner-space">
		<div class="large-12 columns">
			<h3 class="secondary-title">Puntos de Venta</h3>
			<div class="point-list large-up-3 small-up-1">
				<?php while( have_posts() ): the_post(); global $post; ?>
					<div class="column">
						<article class="entry-point">
							<h4 class="entry-title"><?php the_title() ?></h4>
							<address><?php echo $post->point_address ?></address>
							<span class="phone"><?php echo $post->point_phone ?></span>
							<span class="city"><?php echo $post->point_city ?></span>

							<div class="map">
								<?php echo $post->point_map ?>							
							</div>
						</article>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
<?php get_template_part('inc/partials/footer','page'); ?>
</section>
<?php get_footer(); ?>