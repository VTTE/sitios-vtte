<?php 
/*
	Editorial theme widgets
*/

class WP_Widget_related_books extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'related-books', 'description' => 'Muestra un listado contextual de libros relacionados, debe no ser posicionado dentro de un sidebar de libros, mostrará libros al azar');
        $control_ops = array();
		parent::__construct('related-books', 'Libros relacionados', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        extract( $instance );
        extract( $args );
        //static function get_related_books( $post, $grid=4, $title='Más Publicaciones', $link=true ) {
        global $post;
        $size = (!empty($instance['size'])) ? $instance['size'] : 4;
		$link_text = ( !empty( $instance['link_text'] ) ) ? $instance['link_text'] : 'Revisa todos los libros';
		$terms = editorial::get_book_terms($post);
		if (!empty($terms)) {
			$params = array(
				'post_status' => 'publish',
				'posts_per_page' => $size,
				'orderby' => 'rand',
				'post__not_in' => array($post->ID),
				'tax_query' => array(
					array(
						'taxonomy' => 'tax_topics',
						'field' => 'id',
						'terms' => $terms
					)
				)
			);
			$books = new BooksQuery($params);
		} else {
			$books = new BooksQuery(array(
				'post_status' => 'publish',
				'orderby' => 'rand',
				'posts_per_page' => $size
			));
		}
		if (!empty($books) && (count($books) > 2)) {
        	echo '<div class="widget related-books">';
				echo '<div class="related-title clearfix">';
					echo '<h3 class="secondary-title float-left">'.$title.'</h3>';
					if ( !empty( $is_link ) ) {
						echo '<a href="'.site_url('libros').'" class="all float-right">'.$link_text.'</a>';
					}
				echo '</div>';
				echo '<div class="large-up-'.$grid.' medium-up-'.$grid.' small-up-1 clearfix">';
				foreach ( $books as $book ) {
					echo edRender::related_book($book);
				}
				echo '</div>';
			echo '</div>';
		}

    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
        echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'. $this->get_field_name('is_link').'">Link a archivo de libros? </label><input type="checkbox" id="'. $this->get_field_id('is_link').'"'.( ( !empty( $is_link ) ) ? ' checked="checked" ' : '' ).' name="'.$this->get_field_name('is_link').'" value="1"></p>';
        echo '<p><label for="'.$this->get_field_id('link_text').'">Texto link: <input type="text" name="'. $this->get_field_name('link_text') .'" id="'.$this->get_field_id('link_text').'" value="'.$instance['link_text'].'" class="widefat"/></label><br><small>Si no se especifica será: "Revisa todos los libros"</small></p>';
        echo '<p><label for="'.$this->get_field_id('size').'">Cantidad de entradas: <input type="number" name="'. $this->get_field_name('size') .'" id="'.$this->get_field_id('size').'" value="'.$instance['size'].'"/></label></p>';
        echo '<h3>Apariencia</h3>';
        echo '<p><label>Grilla: </label>';
	        echo '<select class="widefat" id="'.$this->get_field_id('grid').'" name="'.$this->get_field_name('grid').'">';
	        	echo '<option value="">Seleccione</option>';
	            echo '<option value="1" '.(($grid == '1') ? 'selected="selected"' : '') .'>1</option>';
	            echo '<option value="2" '.(($grid == '2') ? 'selected="selected"' : '') .'>2</option>';
	            echo '<option value="3" '.(($grid == '3') ? 'selected="selected"' : '') .'>3</option>';
	            echo '<option value="4" '.(($grid == '4') ? 'selected="selected"' : '') .'>4</option>';
	        echo '</select>';
	        echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
        echo '</p>';
       } 
}

class WP_Widget_random_magazine extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'random-magazine', 'description' => 'muestra un enlace a la temática de revistas mostrando una portada al azar');
        $control_ops = array();
		parent::__construct('random-magazine', 'Revistas académicas', $widget_ops, $control_ops);
    }
    function get_random_magazine() {
    	$magazines = new BooksQuery( array( 
					'post_status' => 'publish', 
					'posts_per_page' => 1,
					'orderby' => 'rand',
					'tax_query' => array(
							array(
								'taxonomy' => 'tax_topics',
								'field' => 'slug',
								'terms' => 'revistas-academicas-utem'
								)
						) 
				) );
    	foreach ($magazines as $magazine) {
    		return $magazine;
    	}
    }
    function widget($args, $instance) {
        extract( $instance );
        extract( $args );
       $magazine = $this->get_random_magazine();
       echo '<article class="hentry entry-magazine">';
			echo '<a href="'.site_url('tematica/revistas-academicas-utem').'">';
				echo get_the_post_thumbnail( $magazine->ID, 'letter' );
				echo '<h3 class="entry-title">'.$title.'</h3>';
				echo '<p class="entry-summary">'.esc_attr($instance['description']).'</p>';
			echo '</a>';
		echo '</article>';
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
        echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
    	
       } 
}
class WP_Widget_select_book extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'select-book', 'description' => 'Permite seleccionar una publicación especifica del listado');
        $control_ops = array();
		parent::__construct('select-book', 'Seleccionar Publicación', $widget_ops, $control_ops);
    }
    function get_books() {
		$return_books = array();
    	$magazines = new BooksQuery( array( 
					'post_status' => 'publish', 
					'posts_per_page' => -1,	
				) );
    	foreach ($magazines as $magazine) {
			$return_books[$magazine->ID] = $magazine->post_title;
		}
		return $return_books;
    }
    function widget($args, $instance) {
        extract( $instance );
		extract( $args );
	   $book = $instance['book'];
	   if (!empty($book)) {
			$book_post = get_post($book);
			echo edRender::book($book_post);
	   }
       
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
		extract( $instance );
		$selected_book = $instance['book'];
        echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
    	 echo '<p><label>Publicación: </label>';
	        echo '<select class="widefat" id="'.$this->get_field_id('book').'" name="'.$this->get_field_name('book').'">';
				echo '<option value="">Seleccione</option>';
				$books = $this->get_books();
				foreach ($books as $key => $book) {
					echo '<option value="'.$key.'" ' . (($selected_book == $key) ? 'selected="selected"' : '') . '>'.$book.'</option>';
				}
	        echo '</select>';
        echo '</p>';
       } 
}
// register WP_Widgets
add_action('widgets_init', 'register_editorial_widgets');
add_action('init', 'register_editorial_widgets');
function register_editorial_widgets(){
    
    register_widget('WP_Widget_related_books');
	register_widget( 'WP_Widget_random_magazine' );
	register_widget('WP_Widget_select_book' );
    // register_widget( 'WP_Widget_twitter_timeline' );
    // register_widget('WP_Widget_custom_form');
}