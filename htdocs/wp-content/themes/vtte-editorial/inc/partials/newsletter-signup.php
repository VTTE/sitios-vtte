<div class="row newsletter">
	<div class="large-6 large-centered columns">
		<p>Nuevas publicaciones, ferias, presentaciones, etc.</p>
		<div class="row collapse">
			<div class="columns large-4">
				<strong>Recibe Información</strong>
				<span>Sobre Ediciones UTEM</span>
			</div>
			<div class="large-8 columns form">
				<iframe class="fidelizador-embed" style="width: 100%; height: 215px;" src="https://publiccl1.fidelizador.com/utem/form/4D985F23A03E1BA4983959D540C3219565225F2C/subscription" width="100%" height="215"></iframe>
			</div>
		</div>
	</div>
</div>