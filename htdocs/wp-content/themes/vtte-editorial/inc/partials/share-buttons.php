<?php global $post; ?>
<div class="share-buttons">
	<ul class="menu horizontal">
		<li class="fb"><div class="fb-like" data-href="<?php echo get_permalink($post->ID) ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
		<li class="tw"><a href="https://twitter.com/share" class="twitter-share-button"{count} data-via="utem">Tweet</a></li>
	</ul>
</div>