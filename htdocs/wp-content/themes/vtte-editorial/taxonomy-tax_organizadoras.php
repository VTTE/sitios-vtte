<?php
get_header();
$author_term = get_queried_object();
$author_img = get_term_meta($author_term->term_id, 'tax_image', true);
?>
<section class="main-content">
    <header class="author-header">
        <div class="row">
            <?php if (!empty($author_img)) : ?>
                <div class="columns large-3">
                    <div class="img rounded">
                        <?php echo wp_get_attachment_image($author_img, 'squared');
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="columns large-7 end">
                <h1 class="entry-title"><?php echo $author_term->name ?></h1>
                <?php
                if (!empty($author_term->description)) {
                    echo '<div class="author_description">' . apply_filters('the_content', $author_term->description) . '</div>';
                }
                ?>
                <div class="text-right">
                    <a href="<?php echo site_url('organizadoras') ?>">Ver más organizadores(as)</a>
                </div>
            </div>
        </div>
    </header>
    <div class="row">
        <div class="large-12 columns">
            <h4 class="secondary-title">Publicaciones con Ediciones UTEM</h4>
            <?php
            if (have_posts()) {
                echo '<div class="book-list author-taxonomy">';
                while (have_posts()) : the_post();
                    global $post;
                    echo edRender::book_list($post, false);
                endwhile;
                echo '</div>';

                if (function_exists('wp_pagenavi')) {
                    wp_pagenavi();
                }
            } else {
                echo '<div class="callout warning"><h5>Lo sentimos</h5> <p>No se han encontrado resultados para lo que buscas</p> </div>';
            }
            ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>