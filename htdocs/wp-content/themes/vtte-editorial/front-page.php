<?php get_header() ?>
<section class="main-content">
	<div class="row">
		<div class="large-3 columns themes">
			<div class="hide-for-small-only">
				<?php echo editorial::get_topics(); ?>
			</div>
			<div class="show-for-small-only">
				<a href="#" class="theme-mobile-menu">Temáticas <span class="dashicons dashicons-arrow-down"></a>
				<div class="themes-mobile-list clearfix hide">
					<?php echo editorial::get_topics(); ?>	
				</div>
			</div>

		</div>
		<div class="large-6 columns">
			<h3 class="secondary-title float-left">Últimas Publicaciones</h3>
			<a href="<?php echo site_url('publicaciones') ?>" class="all float-right">Revisa todos los libros</a>
			
			<?php 
				if (is_active_sidebar('featured_books' )) {
					echo '<div class="row large-up-2 small-up-1">';
						dynamic_sidebar('featured_books');
					echo '</div>';
				} else {
					editorial::get_books();
				}
			 ?>
			
		</div>
		<div class="large-3 columns">
			<?php dynamic_sidebar( 'home_top_right' ); ?>
			
		</div>
	</div>
	<?php if (is_active_sidebar('sidebar-home-top')): ?>
		<div class="row">
			<div class="large-12 columns">
				<?php dynamic_sidebar( 'sidebar-home-top' ) ?>
			</div>	
		</div>
	<?php endif; ?>
	<div class="light-gray">
		<div class="row home-sidebars sidebar-home-center">
			<?php dynamic_sidebar( 'sidebar-home-center' ); ?>
		</div>
	</div>
	<?php get_template_part('inc/partials/newsletter','signup') ?>
</section>
<?php get_footer() ?>