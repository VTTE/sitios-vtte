<?php
get_header();
/* Template name: Listado de Organizadoras(es) */
global $post;
?>
<section class="main-content">
    <header class="row">
        <div class="columns large-12">
            <h4 class="secondary-title">Organizadoras y Organizadores</h4>
        </div>
    </header>
    <div class="row">
        <div class="large-12 columns">
            <?php if ($post->post_content != '') : ?>
                <div class="std-text">
                    <?php the_content(); ?>
                </div>
            <?php endif; ?>
            <?php
            $terms = get_terms('tax_organizadoras');
            if (!empty($terms)) {
                echo '<div class="large-up-5 small-up-2 row">';
                foreach ($terms as $term) {
                    echo '<div class="column">';
                    echo edRender::author($term);
                    echo '</div>';
                }
                echo '</div>';
            } else {
                echo '<div class="callout warning"><h5>Lo sentimos</h5> <p>No se han encontrado resultados para lo que buscas</p> </div>';
            }
            ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>