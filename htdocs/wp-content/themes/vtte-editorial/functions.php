<?php 
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;
/*
	Image Sizes
*/
add_image_size('publication-home', 270, 350, true);
add_image_size('publication-gallery', 290, 195, true);
add_image_size('publication-related', 100, 130, true);
add_image_size('page-default', 770, 383, true);
/*
	CALL RELATED FILES
*/
include STYLESHEETPATH.'/inc/widgets.php';
/*
	SIDEBARS
*/

$mandatory_sidebars = array(
	'Libro' => array(
		'name' =>'book',
		'size' => 3
	),
	'Home centro superior' => array(
		'name' => 'sidebar-home-top',
		'size' => 12
	),
	'Home Centro' => array(
		'name' => 'sidebar-home-center',
		'size' => 12
	),
	'Home superior derecha' => array(
		'name' => 'home_top_right',
		'size' => 3
	),
	'Libros destacados' => array(
		'name' => 'featured_books',
		'size' => 6
	)
);
foreach ( $mandatory_sidebars as $sidebar => $id_sidebar ) {
	register_sidebar( array(
		'name'          => $sidebar,
		'id'			=> $id_sidebar['name'],
		'before_widget' => '<section id="%1$s" class="widget %2$s">'."\n",
		'after_widget'  => '</section>',
		'before_title'  => '<header class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3></header>',
		'size' => $id_sidebar['size']
	) );
}

// Theme classes

class editorial {
	public static function get_book_terms($post) {
		$term_list = array();
		if ( !empty( $post ) ) {
			$terms = wp_get_post_terms( $post->ID, 'tax_topics' );
			foreach ($terms as $term) {
				$term_list[] = $term->term_id;
			}
		}
		return $term_list;
	}
	static function get_topic_list($post=null) {
		$term_list = ( !empty( $post ) ) ? self::get_book_terms($post) : array();
		$topics = get_terms('tax_topics', array( 'hide_empty' => false ));
		if ( !empty( $topics ) ) {
				$sidebar .= '<ul class="menu vertical">';
				foreach ( $topics as $topic ) {
					$active = ( in_array($topic->term_id, $term_list) ) ? true : false;
					$sidebar .= edRender::topics($topic, $active);
				}
				$sidebar .= '</ul>';
			return $sidebar;
		} else {
			return false;
		}
	}
	static function get_topics($sidebar=null, $post=null) {
		if (is_object($sidebar)) {
			$post = $sidebar;
			$sidebar = '';
		}
		$term_list = ( !empty( $post ) ) ? self::get_book_terms($post) : array();
		$topics = get_terms('tax_topics', array( 'hide_empty' => false ));
		if ( !empty( $topics ) ) {
			$sidebar .= '<div class="themes">';
				$sidebar .= '<h3 class="primary-title">Temáticas</h3>';
				$sidebar .= '<ul class="menu vertical">';
				foreach ( $topics as $topic ) {
					$active = ( in_array($topic->term_id, $term_list) ) ? true : false;
					$sidebar .= edRender::topics($topic, $active);
				}
				$sidebar .= '</ul>';
			$sidebar .= '</div>';
			return $sidebar;
		} else {
			return false;
		}
	}
	static function book_title($post) {
		$terms = wp_get_post_terms( $post->ID, 'tax_topics' );
		foreach ($terms as $term) {
			if ($term->slug == 'revistas-academicas-utem' ) {
				return 'Revista';
			} else {
				return 'Publicación';
			}
		}
	}
	static function get_books() {
		$books = new BooksQuery( array( 
			'post_status' => 'publish',
			'posts_per_page' => 2,
			'tax_query' => array(
				array(
				'taxonomy' => 'tax_topics',
				'field' => 'slug',
				'terms' => 'revistas-academicas-utem',
				'operator' => 'NOT IN'
				)
			)
			));

		if ( !empty( $books ) ) {
			echo '<div class="row large-up-2 small-up-1">';
			foreach ( $books as $book )	{
				echo edRender::book($book);
			}
			echo '</div>';
		}
	}
	static function get_magazine_detail($post) {
		$out = '';
		if ( !empty( $post->magazine_director ) ) {
			$out .= '<h5>Director</h5>';
			$out .= '<span class="director">'.$post->magazine_director.'</span>';

		}
		if ( !empty( $post->magazine_editor_boss ) ) {
			$out .= '<h5>Jefe Editor</h5>';
			$out .= '<span class="editor_boss">'.$post->magazine_editor_boss.'</span>';

		}
		if ( !empty( $post->magazine_issni ) || !empty( $post->magazine_issnd ) ) {
			$out .= '<h5>ISSN</h5>';
			if ( !empty( $post->magazine_issni ) ) {
				$out .= '<span class="issn" itemprop="issn">'.$post->magazine_issni.' (Impresa)</span><br>';
			}
			if ( !empty( $post->magazine_issnd ) ) {
				$out .= '<span class="issn" itemprop="issn">'.$post->magazine_issnd.' (Digital)</span>';
			}
		}
		if (!empty($post->magazine_issnl)) {
			$out .= '<h5>ISSN-L</h5>';
			$out .= '<span class="issn" itemprop="issn">' . $post->magazine_issnl . '</span>';
		}
		if ( !empty( $post->magazine_period ) ) {
			$out .= '<h5>Periodicidad</h5>';
			$out .= '<span class="period">'.$post->magazine_period.'</span>';
		}
		if ( !empty( $post->magazine_web ) ) {
			$out .= '<h5>Sitio Web</h5>';
			$out .= '<span class="web" itemprop="url"><a href="'.$post->magazine_web.'" target="_blank">'.$post->magazine_web.'</a></span>';
		}
		return $out;
	}
	static function get_book_details($post) {
		$post_author = wp_get_post_terms( $post->ID, 'tax_autores' );
		if (!empty($post_author)) {
			$author_title = (!empty($post->book_author_type)) ? $post->book_author_type : 'Autores(as)';
			echo '<h5>' . $author_title . '</h5>';
			
			echo get_the_term_list($post->ID,'tax_autores','',', ', '');
			echo '</ul>';
		} else {
			$author_list = get_post_meta( $post->ID, 'book_author' );
			if ( !empty( $author_list ) ) {
				$author_title = ( ( count( $author_list ) > 1 ) ? 'Autores(as)' : 'Autor(a)');
				echo  '<h5>'.$author_title.'</h5>';
				echo '<ul class="menu vertical bold-list">';
				foreach ( $author_list as $author ) {
					echo '<li itemprop="author">'.$author.'</li>';
				}
				echo '</ul>';
			}
		}
		$post_editor = wp_get_post_terms($post->ID, 'tax_editoras');
		if (!empty($post_editor)) {
			$editor_title = ((count($post_editor) > 1) ? 'Editores(as)' : 'Editor(a)');
			echo '<h5>' . $editor_title . '</h5>';

			echo get_the_term_list($post->ID, 'tax_editoras', '', ', ', '');
			echo '</ul>';
		} else {
			$editor_list = get_post_meta( $post->ID, 'book_editor' );
			if ( !empty( $editor_list ) ) {
				$author_title = ( ( count( $editor_list ) > 1 ) ? 'Editores(as)' : 'Editor(a)');
				echo  '<h5>'.$author_title.'</h5>';
				echo '<ul class="menu vertical bold-list">';
				foreach ( $editor_list as $editor ) {
					echo '<li itemprop="editor">'.$editor.'</li>';
				}
				echo '</ul>';
			}
		}
		$post_director = wp_get_post_terms($post->ID, 'tax_directoras');
		if (!empty($post_director)) {
			$editor_title = ((count($post_director) > 1) ? 'Directores(as)' : 'Director(a)');
			echo '<h5>' . $editor_title . '</h5>';

			echo get_the_term_list($post->ID, 'tax_directoras', '', ', ', '');
			echo '</ul>';
		} else {
			$director_list = get_post_meta( $post->ID, 'book_director' );
			if ( !empty( $director_list ) ) {
				$author_title = ( ( count( $director_list ) > 1 ) ? 'Directores(as)' : 'Director(a)');
				echo  '<h5>'.$author_title.'</h5>';
				echo '<ul class="menu vertical bold-list">';
				foreach ( $director_list as $director ) {
					echo '<li>'.$director.'</li>';
				}
				echo '</ul>';
			}
		}
		$post_compiler = wp_get_post_terms($post->ID, 'tax_compiladoras');
		if (!empty($post_compiler)) {
			$compiler_title = ((count($post_compiler) > 1) ? 'Compiladores(as)' : 'Compilador(a)');
			echo '<h5>' . $compiler_title . '</h5>';

			echo get_the_term_list($post->ID, 'tax_compiladoras', '', ', ', '');
			echo '</ul>';
		} else {
			$compiler_list = get_post_meta( $post->ID, 'book_compiler' );
			if ( !empty( $compiler_list ) ) {
				$author_title = ( ( count( $compiler_list ) > 1 ) ? 'Compiladores(as)' : 'Compilador(a)');
				echo  '<h5>'.$author_title.'</h5>';
				echo '<ul class="menu vertical bold-list">';
				foreach ( $compiler_list as $compiler ) {
					echo '<li>'.$compiler.'</li>';
				}
				echo '</ul>';
			}
		}
		$post_coordinator = wp_get_post_terms($post->ID, 'tax_coordinadoras');
		if (!empty($post_coordinator)) {
			$coordinator_title = ((count($post_coordinator) > 1) ? 'Coordinadores(as)' : 'Coordinador(a)');
			echo '<h5>' . $coordinator_title . '</h5>';

			echo get_the_term_list($post->ID, 'tax_coordinadoras', '', ', ', '');
			echo '</ul>';
		} else {
			$coordinator_list = get_post_meta( $post->ID, 'book_coordinator' );
			if ( !empty( $coordinator_list ) ) {
				$author_title = ( ( count( $coordinator_list ) > 1 ) ? 'Coordinadores(as)' : 'Coordinador(a)');
				echo  '<h5>'.$author_title.'</h5>';
				echo '<ul class="menu vertical bold-list">';
				foreach ( $coordinator_list as $coordinator ) {
					echo '<li>'.$coordinator.'</li>';
				}
				echo '</ul>';
			}
		}
		$post_redaction = wp_get_post_terms($post->ID, 'tax_redactoras');
		if (!empty($post_redaction)) {
			$redaction_title = ((count($post_redaction) > 1) ? 'Redactores(as)' : 'Redactor(a)');
			echo '<h5>' . $redaction_title . '</h5>';

			echo get_the_term_list($post->ID, 'tax_redactoras', '', ', ', '');
			echo '</ul>';
		}
		$post_organizers = wp_get_post_terms($post->ID, 'tax_organizadoras');
		if (!empty($post_organizers)) {
			$organizers_title = ((count($post_organizers) > 1) ? 'Organizadores(as)' : 'Organizador(a)');
			echo '<h5>' . $organizers_title . '</h5>';

			echo get_the_term_list($post->ID, 'tax_organizadoras', '', ', ', '');
			echo '</ul>';
		}
		if ( !empty( $post->book_isbn ) ) {
			echo '<h5>ISBN</h5>';
			echo '<span class="isbn" itemprop="isbn">'.$post->book_isbn.'</span>';
		}
		if ( !empty( $post->book_edition_date ) ) {
			echo '<h5>Año de Publicación</h5>';
			echo '<span class="created" itemprop="datePublished">'.$post->book_edition_date.'</span>';
		}
		if ( !empty( $post->book_coedition ) ) {
			echo '<h5>Coedición</h5>';
			echo '<span>'.$post->book_coedition.'</span>';
		}
		if ( !empty( $post->book_pages ) || !empty( $post->book_size ) || !empty( $post->book_weight ) ) {
			echo '<h5>Formato</h5>';
			echo '<ul class="menu vertical">';
				if ( !empty( $post->book_pages ) ) {
					echo '<li itemprop="numberOfPages">'.$post->book_pages.'</li>';
				}
				if ( !empty( $post->book_size ) ) {
					echo '<li>'.$post->book_size.'</li>';
				}
				if ( !empty( $post->book_weight ) ) {
					echo '<li>'.$post->book_weight.'</li>';
				}
			echo '</ul>';
		}
	}
	static function get_book_gallery( $post ) {
		$images = get_children( array(
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'post_parent' => $post->ID,
			'order' => 'ASC'
		 ) );
		 $book_gallery = $post->book_gallery;
		 if (!empty($book_gallery)) {
			echo '<h3 class="middle-title"><span>Galería</span></h3>';
			echo '<div class="large-up-4 small-up-2">';
			foreach ($book_gallery as $image) {
				echo '<div class="column">';
					echo '<a href="' . current(wp_get_attachment_image_src($image, 'full')) . '" class="box">' . wp_get_attachment_image($image, 'publication-gallery') . '</a>';
				echo '</div>';
			}
			echo '</div>';
		 } else {
			$featured_id = get_post_thumbnail_id( $post->ID );
			if ( count( $images ) > 1 ) {
				echo '<h3 class="middle-title"><span>Galería</span></h3>';
				echo '<div class="large-up-4 small-up-2">';
				foreach ( $images as $image ) {
					if ( $image->ID  != $featured_id ) {
						echo '<div class="column">';
							echo '<a href="'.current(wp_get_attachment_image_src( $image->ID, 'full')).'" class="box">'.wp_get_attachment_image( $image->ID, 'publication-gallery' ).'</a>';
						echo '</div>';
					}
				}
				echo '</div>';
			}
		}
	}
	static function get_related_books( $post, $grid=4, $title='Más Publicaciones', $link=true ) {
		echo '<div class="related-title clearfix">';
			echo '<h3 class="secondary-title float-left">'.$title.'</h3>';
			if ( $link ) {
				echo '<a href="#" class="all float-right">Revisa todos los libros</a>';
			}
		echo '</div>';
		$terms = self::get_book_terms($post);
		if ( !empty($terms) ) {
			$books = new BooksQuery( array( 
				'post_status' => 'publish', 
				'posts_per_page' => 4,
				'tax_query' => array(
						array(
							'taxonomy' => 'tax_topics',
							'field' => 'id',
							'terms' => $terms
							)
					) 
			) );
		} else {
			$books = new BooksQuery( array( 
				'post_status' => 'publish', 
				'posts_per_page' => 4,
				
			) );
		}
			if ( !empty( $books ) ) {
				echo '<div class="large-up-'.$grid.' small-up-1 clearfix">';
				foreach ( $books as $book ) {
					if ( $book->ID != $post->ID ) {
						echo edRender::related_book($book);
					}
				}
				echo '</div>';
			}
	}
	static function get_meta_page($sidebar, $post) {
		if ( is_object( $sidebar ) ) {
			$post = $sidebar;
			$sidebar = '';
		}
		$icon = ( !empty( $post->page_icon ) ) ? $post->page_icon : 'info';

		if ( !empty( $post->page_info ) ) {
			$sidebar .= '<section class="page-meta">';
				$sidebar .= '<h3 class="meta-title"><span class="dashicons dashicons-'.$icon.'"></span> '.$post->page_title.' </h3>';
				$sidebar .= apply_filters( 'the_content', $post->page_info );
			$sidebar .= '</section>';
			return $sidebar;
		} else {
			return false;
		}
	}
}
/*
	Filters and actions 
*/
add_filter('vtte_page_sidebar',array('editorial','get_topics'),11,2);
add_filter('vtte_page_content_sidebar',array('editorial','get_meta_page'),11,2);
/*
	Render functions
*/
class edRender {
	static function topics( $item, $active ) {
		$link = ( !is_wp_error( get_term_link( $item, 'tax_topics' ) ) ) ? get_term_link( $item, 'tax_topics' ) : '#';
		$active = ( !empty( $active ) ) ? ' class="active"':'';
		return '<li'.$active.'><a href="'.$link.'">'.$item->name.'</a></li>';
	}
	static function book( $item, $cat=true ) {
		$out = '';
		if ( !empty( $item ) ):
			$out .= '<div class="column">';
				$out .= '<article class="hentry entry-publication">';
					$out .= '<a href="'.get_permalink($item->ID).'">';
						$out .= get_the_post_thumbnail( $item->ID, 'publication-home' );
						$out .= '<h4 class="entry-title">'.get_the_title( $item->ID ).'</h4>';
						if ( $cat ) {
							$out .= '<div class="categories">';
								$topics = wp_get_post_terms( $item->ID, 'tax_topics' );
								$link = ( !is_wp_error( get_term_link( $topics[0], 'tax_topics' ) ) ) ? get_term_link( $topics[0], 'tax_topics' ) : '#';
								$out .= '<a href="'.$link.'">'.$topics[0]->name.'</a>';
							$out .= '</div>';
						}
					$out .= '</a>';
				$out .= '</article>';
			$out .= '</div>';
		endif;
		return $out;
	}
	static function author( $item ) {
		$out = '';
		if ( !empty( $item ) ):
			$author_img = get_term_meta($item->term_id, 'tax_image', true);
			$out .= '<article class="hentry entry-author">';
				$out .= '<div class="entry-image rounded">';
					$out .= '<a href="'.get_term_link($item,'tax_autores').'">';
						$out .= wp_get_attachment_image( $author_img, 'squared' );
					$out .= '</a>';
				$out .= '</div>';
				$out .= '<div class="entry-content">';
					$out .= '<a href="' . get_term_link($item, 'tax_autores') . '"><h4 class="entry-title">'.$item->name.'</h4></a>';
				$out .= '</div>';
			$out .= '</article>';
		endif;
		return $out;
	}
	static function book_list( $item, $cat=true ) {
		$out = '';
		if ( !empty( $item ) ):
			$out .= '<article class="hentry entry-publication">';
				$out .= '<div class="row">';
					$out .= '<div class="large-3 column">';
						$out .= get_the_post_thumbnail( $item->ID, 'publication-home' );
					$out .= '</div>';
					$out .= '<div class="large-9 columns">';
						$out .= '<a href="' . get_permalink($item->ID) . '">';
							$out .= '<h4 class="entry-title">'.get_the_title( $item->ID ).'</h4>';
						$out .= '</a>';
						$out .= do_excerpt($item);
						$out .= '<a href="'.get_permalink($item->ID).'" class="button primary">Ver libro</a>';
						if ( $cat ) {
							$out .= '<div class="categories">';
								$topics = wp_get_post_terms( $item->ID, 'tax_topics' );
								$link = ( !is_wp_error( get_term_link( $topics[0], 'tax_topics' ) ) ) ? get_term_link( $topics[0], 'tax_topics' ) : '#';
								$out .= '<a href="'.$link.'">'.$topics[0]->name.'</a>';
							$out .= '</div>';
						}
					$out .= '</div>';
				
			$out .= '</article>';
		endif;
		return $out;
	}
	static function magazine( $item ) {
		$out = '';
		if ( !empty( $item ) ):
			$out .= '<div class="column">';
				$out .= '<article class="hentry entry-publication">';
					$out .= '<div class="row collapse">';
						$out .= '<div class="large-6 columns">';
							$out .= '<a href="'.get_permalink($item->ID).'">';
								$out .= get_the_post_thumbnail( $item->ID, 'publication-home' );
							$out .= '</a>';
						$out .= '</div>';						
							$out .= '<div class="large-6 columns book-details">';
								$out .= '<h4 class="entry-title"><a href="'.get_permalink($item->ID).'">'.get_the_title( $item->ID ).'</a></h4>';
								$out .= editorial::get_magazine_detail($item);
							$out .= '</div>';
					$out .= '</div>';
				$out .= '</article>';
			$out .= '</div>';
		endif;
		return $out;
	}
	static function related_book( $book ) {
		$out = '';
		$out .= '<div class="column">';
			$out .= '<article class="hentry entry-publication">';
				$out .= '<a href="'.get_permalink($book->ID).'">';
					$out .= '<div class="entry-image">';
						$out .= get_the_post_thumbnail( $book->ID, 'publication-related' );
					$out .= '</div>';
					$out .= '<div class="entry-content">';
						$out .= '<h4 class="entry-title">'.get_the_title( $book->ID ).'</h4>';
						$out .= '<div class="categories">';
							$topics = wp_get_post_terms( $book->ID, 'tax_topics' );
							$link = ( !is_wp_error( get_term_link( $topics[0], 'tax_topics' ) ) ) ? get_term_link( $topics[0], 'tax_topics' ) : '#';
							$out .= '<a href="'.$link.'">'.$topics[0]->name.'</a>';
						$out .= '</div>';
					$out .= '</div>';
				$out .= '</a>';
			$out .= '</article>';
		$out .= '</div>';
		return $out;
	}
	static function news_share($post) {
		$out = '';
		$out .= '<div class="column">';
			$out .= '<article class="hentry entry-article">';
					$out .= '<div class="entry-image">';
						$out .= get_the_post_thumbnail( $book->ID, 'publication-gallery' );
						$out .= '<div class="wrap">';
							$out .= '<div class="social">';
								$out .= '<a href="#" class="share-fb"><span class="dashicons dashicons-facebook-alt"></span></a>';
								$out .= '<a href="#" class="share-tw"> <span class="dashicons dashicons-twitter"></span></a>';
							$out .= '</div>';
							$out .= '<a href="'.get_permalink($post->ID).'" class="link-entry"><span class="dashicons dashicons-admin-links"></span></a>';
						$out .= '</div>';
					$out .= '</div>';
				$out .= '<h4 class="entry-title"><a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a></h4>';
			$out .= '</article>';
		$out .= '</div>';
		return $out;
	}
}

// search filter
function make_search_filter($query) {
	$pt = $query->get('post_type');
	if ( empty( $pt ) ) {
		if ( !$query->is_admin && $query->is_search) {
			$query->set('post_type', array('post','books') ); // id of page or post
		}
	}
	return $query;
}
add_filter( 'pre_get_posts', 'make_search_filter' );