<?php 
	get_header(); 
?>
<section class="main-content">
	<header class="row">
		<div class="columns large-12">
			<h4 class="secondary-title"><?php echo get_queried_object()->name ?></h4>
		</div>
	</header>
	<div class="row">
		<div class="large-9 columns">
			<?php if ( !empty( get_queried_object()->description) ): ?>
				<div class="tax-description">
					<?php echo apply_filters('the_content',get_queried_object()->description); ?>
				</div>
			<?php endif; ?>
			<?php 
				if ( have_posts() ):
					echo '<div class="large-up-3 small-up-2">';
						while( have_posts() ): the_post();
							global $post;
							echo edRender::book($post,false);
						endwhile;
					echo '</div>';
					if ( function_exists( 'wp_pagenavi' ) ) {
						wp_pagenavi();
					}
				endif;
			 ?>
		</div>
		<div class="large-3 columns">
			<?php echo editorial::get_topics(''); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>