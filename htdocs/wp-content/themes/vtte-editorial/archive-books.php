<?php 
	get_header(); 
	$search = new search_filter();
	$search->set_post_type('books');
	if ( get_query_var('paged') ) {
		$search->set_page(get_query_var('paged'));
	}
	if ( isset($_GET['action']) ) {
		if (isset($_GET['search'])) {
			$search->set_search_text( esc_attr( $_GET['search'] ) ) ;
		}
		if (isset($_GET['tematica'])) {
			$search->set_taxonomy_name('tax_topics');
			$search->set_taxonomies(array(esc_attr($_GET['tematica'])));
		}
	}
	$query = $search->search();
?>
<section class="main-content">
	<header class="row">
		<div class="columns large-12">
			<h4 class="secondary-title"><?php echo get_queried_object()->labels->name ?></h4>
			<div class="filter-form">
				<form action="" method="GET">
					<div class="row">
						<div class="large-2 small-12 medium-2 columns">
							<h5>Buscar Por</h5>
						</div>
						<div class="large-4 small-12 medium-4 columns">
							<input type="text" placeholder="Palabra clave" value="<?php echo esc_attr($_GET['search']) ?>" class="input-type" name="search">
						</div>
						<div class="large-4 small-12 medium-4 columns">
							<select name="tematica" class="input-type">
								<option value="">Temática</option>
								<?php 
									$terms = get_terms('tax_topics');
									foreach ($terms as $term) {
										$selected = ( !empty($_GET['tematica']) && ($_GET['tematica'] == $term->slug) ) ? ' selected="selected" ' : '';
										echo '<option value="'.$term->slug.'"'.$selected.'>'.$term->name.'</option>';
									}
								 ?>
							</select>
						</div>
						<div class="large-1 small-12 medium-1 columns end">
							<input type="submit" class="button secondary" value="Buscar">
							<input type="hidden" name="action" value="send">
						</div>
					</div>
				</form>
			</div>
		</div>
	</header>
	<div class="row">
		<div class="large-9 columns">
			<?php 
				if ( $query->have_posts() ) {
					echo '<div class="large-up-3 small-up-2">';
						while( $query->have_posts() ): $query->the_post();
							global $post;
							echo edRender::book($post,false);
						endwhile;
					echo '</div>';
					if ( function_exists( 'wp_pagenavi' ) ) {
						wp_pagenavi(array('query' => $query));
					}
				} else {
					echo '<div class="callout warning"><h5>Lo sentimos</h5> <p>No se han encontrado resultados para lo que buscas</p> </div>';
				}
			 ?>
		</div>
		<div class="large-3 columns">
			<?php echo editorial::get_topics(''); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>