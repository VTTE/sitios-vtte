<?php 
	get_header(); 
	// get_post();
	global $post;
	setup_postdata( $post );
?>
<section class="main-content">
	<div class="row">
		<div class="large-12 columns header-books">
			<h4 class="secondary-title"><?php echo editorial::book_title($post) ?></h4>
		</div>
	</div>
	<div class="row hentry entry-book" itemscope itemtype="http://schema.org/Book">
		<div class="large-3 columns entry-image">
			<?php 
				the_post_thumbnail('publication-home', array( 'itemprop', 'image' )); 

				$file_id = get_post_meta($post->ID,'upload_pdf',true);
				$file_url = get_post_meta($post->ID,'upload_url',true);
				$url_file = false;
				if (empty($file_url) && !empty($file_id)) {
			        try {
			                $file = new \GutenPress\Helpers\Attachment($file_id);
			            } catch ( Exception $e ) {
			                echo '<!-- Attachment: no existe -->';
			            }
			        
			        if (!empty($file)) {
			            $url_file = $file->url;
			        } else {
			            $url_file = false;
			        }
			    } else {
			    	$url_file = $file_url;
				}
				echo '<div class="bottom-buttons">';
					if (!empty($url_file)) {
						echo '<a href="'. $url_file .'" class="button download primary">Descargar</a>';
					} else {
						echo '<a href="'. site_url('puntos-de-venta') .'" class="button green">Adquirir</a>';
					}
				echo '</div>';

			?>
			
		</div>
		<div class="large-4 columns">
			<h3 class="entry-title" itemprop="name"><?php the_title(); ?></h3>
			<?php the_content(); ?>
		</div>
		<div class="large-2 columns">
			<aside class="book-details">
			<?php 
				global $post;
				editorial::get_book_details($post);
				echo editorial::get_magazine_detail($post);
			 ?>
			 </aside>
		</div>
		<div class="large-3 columns themes">
			<?php echo editorial::get_topics($post); ?>
		</div>
	</div>
	<div class="row entry-gallery">
		<div class="large-12 columns">
			<?php 
				editorial::get_book_gallery($post);
			 ?>
		</div>
	</div>
	<?php 
		$prensa = new WP_Query(array(
			'post_type' => 'press',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' => 'press_publication',
					'value' => $post->ID
					)
				)
			));
		if ($prensa->have_posts()):
	 ?>
	<div class="row entry-press">
		<div class="large-12 columns">
			<h3 class="middle-title"><span>La Publicación en la prensa</span></h3>
			<table>
				<?php foreach ($prensa->posts as $item) {
						echo '<tr>';
							echo '<td>'.get_the_title($item->ID).'</td>';
							echo '<td>'.esc_attr($item->press_medio).'</td>';
							echo '<td><a href="'.$item->press_url.'" class="button secondary" target="_blank">VER</a></td>';
						echo '</tr>';
					} 
					?>
			</table>
		</div>
	</div>
<?php endif; ?>
	<div class="light-gray">
		<div class="row inner-space related-books">
			<div class="large-12 columns">
				<?php dynamic_sidebar('book') ?>
				<?php // editorial::get_related_books($post); ?>
				<?php get_template_part('inc/partials/newsletter','signup') ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>