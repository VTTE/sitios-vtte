<?php
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;

add_image_size('program-list', 270, 110, true);
add_image_size('publication-home', 270, 350, true);
add_image_size('publication-gallery', 290, 195, true);
/*
	calling related files
*/

	include STYLESHEETPATH . '/inc/widgets.php';
/*
	Activate links manager
*/

//add_filter('pre_option_link_manager_enabled', '__return_true');


/*
	Sidebars filters
*/
add_filter('vtte_base_mandatory_sidebars', array('programs','remove_base_sidebars'));

/*
	Theme sidebars
*/
$mandatory_sidebars = array(
	'Proyecto sidebar' => array(
		'name' => 'project',
		'size' => 3
		),
	'Home fila 1 Izquierda' => array(
		'name' => 'sidebar-home-1-left',
		'size' => 9
		),
	'Home fila 1 Derecha' => array(
		'name' => 'sidebar-home-1-right',
		'size' => 3
		),
	'Home fila 2' => array(
		'name' => 'sidebar-home-2',
		'size' => 12
		),
	'Footer Página' => array(
		'name' => 'page_footer_large',
		'size' => 12
		),
	'Footer Artículo' => array(
		'name' => 'single_footer_large',
		'size' => 12
		),
);

foreach ( $mandatory_sidebars as $sidebar => $id_sidebar ) {
	register_sidebar( array(
		'name'          => $sidebar,
		'id'			=> $id_sidebar['name'],
		'before_widget' => '<section id="%1$s" class="widget %2$s">'."\n",
		'after_widget'  => '</section>',
		'before_title'  => '<header class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3></header>',
		'size' => $id_sidebar['size']
	) );
}

class programs {
	const blog_feed_id = 2;
	static function remove_base_sidebars($sidebars) {
		unset($sidebars['Página footer 1']);
		unset($sidebars['Página footer 2']);
		unset($sidebars['Página footer 3']);
		unset($sidebars['Entrada footer 1']);
		unset($sidebars['Entrada footer 2']);
		unset($sidebars['Entrada footer 3']);
		return $sidebars;
	}
	static function get_projects($size, $offset=0) {
		$projects = new WP_Query(array(
			'post_type' => 'proyect',
			'posts_per_page' => $size,
			'offset' => $offset,
			'post_status' => 'publish'
			));
		if ($projects->have_posts()) {
			return $projects->posts;
		} else {
			return false;
		}

	}
	static function get_book_gallery( $post ) {
		$images = get_children( array(
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'post_parent' => $post->ID,
			'order' => 'ASC'
		 ) );
		$featured_id = get_post_thumbnail_id( $post->ID );
		if ( count( $images ) > 1 ) {
			echo '<h3 class="middle-title"><span>Galería</span></h3>';
			echo '<div class="large-up-4 small-up-2">';
			foreach ( $images as $image ) {
				if ( $image->ID  != $featured_id ) {
					echo '<div class="column">';
						echo '<a href="'.current(wp_get_attachment_image_src( $image->ID, 'full')).'" class="box">'.wp_get_attachment_image( $image->ID, 'publication-gallery' ).'</a>';
					echo '</div>';
				}
			}
			echo '</div>';
		}
	}
	public static function get_categorization_terms($post) {
		$term_list = array();
		if ( !empty( $post ) ) {
			$terms = wp_get_post_terms( $post->ID, 'categorization' );
			foreach ($terms as $term) {
				$term_list[] = $term->term_id;
			}
		}
		return $term_list;
	}
	static function get_categorization($sidebar, $post=null) {
		if (is_object($sidebar)) {
			$post = $sidebar;
			$sidebar = '';
		}
		$term_list = ( !empty( $post ) ) ? self::get_categorization_terms($post) : array();
		$topics = get_terms('categorization', array( 'hide_empty' => false ));
		if ( !empty( $topics ) ) {
			$sidebar .= '<div class="themes">';
				$sidebar .= '<h3 class="primary-title">Temáticas</h3>';
				$sidebar .= '<ul class="menu vertical">';
				foreach ( $topics as $topic ) {
					$active = ( in_array($topic->term_id, $term_list) ) ? true : false;
					$sidebar .= prRender::topics($topic, $active);
				}
				$sidebar .= '</ul>';
			$sidebar .= '</div>';
			return $sidebar;
		} else {
			return false;
		}
	}
	static function get_posts_main_site($post_type) {
		global $_set;
		$settings = $_set->settings;
		//delete_transient('main_site_'.$post_type);
		$slug = $settings['slug_content'];
		if (!empty($slug)) {
			if ( false === ( $site_posts = get_transient( 'main_site_'.$post_type ) ) ) {
				switch_to_blog( self::blog_feed_id );
				$query = new WP_Query(array(
					'post_type' => $post_type,
					'posts_per_page' => 4,
					'post_status' => 'publish',
					'tax_query' => array(
						array(
							'taxonomy' => 'post_tag',
							'field' => 'slug',
							'terms' => $slug
							)
						)
					));
				if ($query->have_posts()) {
					$site_posts = array();
					foreach ($query->posts as $item) {

						$item_post = new stdClass();
						$item_post->ID = $item->ID;
						$item_post->post_type = $item->post_type;
						$item_post->title = $item->post_title;
						$item_post->date = $item->post_date;
						$item_post->permalink = get_permalink($item->ID);
						$item_post->short_permalink = wp_get_shortlink($item->ID );
						$item_post->content = do_excerpt($item);
						$item_post->image = current(wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), 'news-home' ));
						$item_post->image_full = current(wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), 'page-featured' ));
						$item_post->categories = get_the_category_list( ', ', '', $item->ID );
						$item_post->archive_link = site_url('tag/'.$slug);
						$item_post->author_link = $author_link;
						$item_post->author_name = $author_name;
						$site_posts[] = $item_post;
					}
				} 
				restore_current_blog();
				set_transient( 'main_site_'.$post_type, $site_posts, 60*60 );
			}
			return $site_posts;
		} else {
			return null;
		}
	}
	public function add_form_config($form, $data) {
		switch_to_blog( self::blog_feed_id );
		$tags = get_terms('post_tag', array('hide_empty' => false) );
		restore_current_blog();
		$array_terms = array('' => 'Seleccione');
		foreach ($tags as $tag) {
			$array_terms[$tag->slug] = $tag->name;
		}
		$form->addElement( new Element\Select(
				_x('Tag de Noticias y Actividades', 'site settings fields', 'vtte'),
				'slug_content',
				$array_terms,
				array(
					'value' => isset($data['slug_content']) ? $data['slug_content'] : ''
				)
			) );
		return $form;
	}
	public function add_form_config_field($fields) {
		$fields[] = 'slug_content';
		return $fields;
	}
}

add_filter('vtte_main_configuration_form',array('programs','add_form_config'),10,2);
add_filter('vtte_main_configuration_fields',array('programs','add_form_config_field'),10,1);


class prRender {
	static function topics( $item, $active ) {
		$link = ( !is_wp_error( get_term_link( $item, 'categorization' ) ) ) ? get_term_link( $item, 'categorization' ) : '#';
		$active = ( !empty( $active ) ) ? ' class="active"':'';
		return '<li'.$active.'><a href="'.$link.'">'.$item->name.'</a></li>';
	}
	static function featured_project($post) {
		$out = '';
		$out .= '<div class="widget news">';
            $out .= '<div class="widget-content clearfix">';
                    $out .= '<article class="hentry entry-news">';
                        $out .= '<a href="'.get_permalink($post->ID).'">';
                            $out .= get_the_post_thumbnail( $post->ID, 'squared' );
                            $out .= '<div class="entry-info">';
                                $out .= '<h4 class="entry-title">'.get_the_title($post->ID).'</h4>';
                            $out .= '</div>';
                        $out .= '</a>';
                    $out .= '</article>';
                $out .= '</div>';
        $out .= '</div>';
        return $out;
}
	static function project($post) {
		$out = '<article class="hentry entry-publication">';
			$out .= '<a href="'.get_permalink($post->ID).'">';
				$out .= '<div class="entry-image">';
					$out .= get_the_post_thumbnail( $post->ID, 'thumbnail' );
				$out .= '</div>';
			$out .= '</a>';
			$out .= '<div class="entry-content">';
				$out .= '<a href="'.get_permalink($post->ID).'"><h4 class="entry-title">'.get_the_title($post->ID).'</h4></a>';
				if (!empty($post->program_career)) {
					$out .= '<span class="career">'.$post->program_career.'</span>';
				}
			$out .= '</div>';
		$out .= '</article>';
		return $out;
	}
	static function product($post) {
		$out = '<article class="hentry entry-product">';
			$out .= '<a href="'.get_permalink($post->ID).'">';
				$out .= '<div class="entry-image">';
					$out .= get_the_post_thumbnail( $post->ID, 'product-list' );
				$out .= '</div>';
			$out .= '</a>';
				$out .= '<a href="'.get_permalink($post->ID).'"><h4 class="entry-title">'.get_the_title($post->ID).'</h4></a>';
		$out .= '</article>';
		return $out;
	}
	static function event($post) {
		$out = '<article class="hentry entry-product">';
			$out .= '<a href="'.$post->permalink.'">';
				$out .= '<div class="entry-image">';
					$out .= '<img src="'.$post->image.'" alt="">';
				$out .= '</div>';
			$out .= '</a>';
				$out .= '<a href="'.$post->permalink.'"><h4 class="entry-title">'.$post->title.'</h4></a>';
		$out .= '</article>';
		return $out;
	}
}