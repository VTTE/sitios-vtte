<?php
class WP_Widget_news_multisite extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'news-vtte', 'description' => 'Muestra las últimas noticias publicadas en el sitio de la VTTE con el tag especificado en la configuración');
        $control_ops = array();
        parent::__construct('news-vtte', 'Últimas noticias de VTTE', $widget_ops, $control_ops);
    }
  
    function widget($args, $instance) {
        global $post;
        extract( $instance );
        extract( $args );
        $size = ( !empty( $instance['size'] ) ) ? $instance['size'] : 3;
        $grid = ( !empty( $instance['grid'] ) ) ? $instance['grid'] : 1;
        $link_text = ( !empty( $instance['link_text'] ) ) ? $instance['link_text'] : 'Más Noticias';
        $news = programs::get_posts_main_site('post');
        if ( !empty( $news ) ) {
            echo '<div class="widget news">';
                echo '<h4 class="widget-title">'.$title.'</h4>';
                echo '<div class="widget-content clearfix">';
                    echo '<div class="large-up-'.$grid.' medium-up-'.$grid.' small-up-2 row">';
                        for($x = 0; $x+1 <= $size; $x++) {
                            echo '<div class="column">';
                                echo '<article class="hentry entry-news">';
                                    echo '<a href="'.$news[$x]->permalink.'">';
                                        echo '<img src="'.$news[$x]->image.'" alt="">';
                                        echo '<div class="entry-info">';
                                            echo '<h4 class="entry-title">'.$news[$x]->title.'</h4>';
                                        echo '</div>';
                                    echo '</a>';
                                echo '</article>';
                            echo '</div>';
                        }

                    echo '</div>';
                    if ( !empty( $instance['is_link'] ) ) {
                        $link = $news[0]->archive_link;
                        echo '<a href="'.$link.'" class="button secondary">'.$link_text.'</a>';
                    }
                echo '</div>';
            echo '</div>';
        }
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
       echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'. $this->get_field_name('is_link').'">Link a archivo de noticias? </label><input type="checkbox" id="'. $this->get_field_id('is_link').'"'.( ( !empty( $is_link ) ) ? ' checked="checked" ' : '' ).' name="'.$this->get_field_name('is_link').'" value="1"></p>';
        echo '<p><label for="'.$this->get_field_id('link_text').'">Texto link: <input type="text" name="'. $this->get_field_name('link_text') .'" id="'.$this->get_field_id('link_text').'" value="'.$instance['link_text'].'" class="widefat"/></label><br><small>Si no se especifica será: "Más noticias"</small></p>';
        echo '<p><label for="'.$this->get_field_id('size').'">Cantidad de entradas: <input type="number" name="'. $this->get_field_name('size') .'" id="'.$this->get_field_id('size').'" value="'.$instance['size'].'"/></label></p>';
        echo '<h3>Apariencia</h3>';
        echo '<p><label>Grilla: </label>';
            echo '<select class="widefat" id="'.$this->get_field_id('grid').'" name="'.$this->get_field_name('grid').'">';
                echo '<option value="">Seleccione</option>';
                echo '<option value="1" '.(($grid == '1') ? 'selected="selected"' : '') .'>1</option>';
                echo '<option value="2" '.(($grid == '2') ? 'selected="selected"' : '') .'>2</option>';
                echo '<option value="3" '.(($grid == '3') ? 'selected="selected"' : '') .'>3</option>';
                echo '<option value="4" '.(($grid == '4') ? 'selected="selected"' : '') .'>4</option>';
            echo '</select>';
            echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
        echo '</p>';
       } 
}

add_action('widgets_init', 'register_centros_widgets');
add_action('init', 'register_centros_widgets');
function register_centros_widgets(){
	register_widget('WP_Widget_news_multisite');
}