<?php 
get_header();
the_post();
global $post;
$thumb = sitio::get_post_thumbnail_url($post, 'full');
?>
<section class="main-content">
	<div class="row feature" style="background-image:url(<?php echo $thumb ?>),linear-gradient(rgba(255,255,255,0.50) 0%, rgba(0,111,191,0.47) 48%, rgba(1,132,132,0.47) 95%);">
		<div class="large-12 columns">
			<div class="wrap-content">
				<div class="feature-text">
					<span class="subtitle">Tecnología</span>
					<span class="feature-title"><?php the_title(); ?></span>
				</div>
			</div>
			<?php if (!empty($post->technology_status)) : ?>
				<div class="project-status">
					<strong><?php echo $post->technology_status ?></strong>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="row move-up">
		
		<div class="large-4 columns">
			<div class="white box-content">
				<?php get_template_part('inc/partials/share', 'buttons'); ?>
			</div>
		</div>
	</div>
	<div class="row inner-space">
		<div class="large-12 columns">
			<?php if (!empty($post->technology_download)) : ?>
				<div class="download-project">
					<?php $file = sitio::get_file($post->ID, 'technology_download'); ?>
					<a href="<?php echo $file->url ?>" class="button primary">Descargar <em>(<?php echo $file->size; ?>)</em> </a>
				</div>
			<?php endif; ?>
			<ul class="tabs" data-tabs id="project-tabs">
				<li class="tabs-title is-active">
					<a href="#proyecto" aria-selected="trie">Proyecto</a>
				</li>
				<?php if (!empty($post->technology_gallery)) : ?>
					<li class="tabs-title">
						<a href="#imagenes">Imágenes</a>
					</li>
				<?php endif; ?>
			</ul>
			<div class="tabs-content" data-tabs-content="project-tabs">
				<div class="tabs-panel is-active" id="proyecto">
					<div class="row">
						<div class="large-9 large-centered">
							<div class="std-text">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
				<?php if (!empty($post->technology_gallery)) : ?>
					<div class="tabs-panel" id="imagenes">
						<div class="entry-gallery">
							<div class="row large-up-4">
								<?php 
							foreach ($post->technology_gallery as $item) {
								$img_url = wp_get_attachment_image_src($item, 'full');
								echo '<div class="column">';
								echo '<a href="' . $img_url[0] . '" class="box">';
								echo wp_get_attachment_image($item, 'squared');
								echo '</a>';
								echo '</div>';
							}
							?>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer() ?>