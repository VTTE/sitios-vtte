<?php  
	get_header();
	global $_set;
	$settings = $_set->settings;	
?>

<section class="main-content">
	<div class="row inner-space remove-bottom">
		<div class="large-12 columns project-list">
			<div class="titled clearfix">
				<h2 class="primary-title float-left">Proyectos</h2>
				<a href="<?php echo site_url('proyectos') ?>" class="secondary-title float-right">Ver todos los proyectos</a>
			</div>
			<div class="large-up-3 small-up-1 medium-up-3 row cleafix inline-lists">
				<?php 
					$programs = programs::get_projects(6,0);
					foreach ( $programs as $program ) {
						echo '<div class="column">';
							echo prRender::project($program);
						echo '</div>';
					}
				 ?>
			</div>
		</div>
	</div>
	<?php 
		$events = programs::get_posts_main_site('event');
		if (!empty( $events )):
	 ?>
		<div class="row inner-space remove-top">
			<div class="large-12 columns">
				<div class="widget sidebar-title clearfix">
					<div class="float-left">
						<h3 class="widget-title">Actividades</h3>
					</div>
					<div class="float-right">
						<a href="https://vtte.utem.cl/tag/<?php echo $settings['slug_content'] ?>" class="all-link-gray">Todas las actividades</a>
					</div>
				</div>
					<div class="large-up-4 small-up2 medium-up-4 row clearfix">
					 <?php 
						 foreach ( $events as $event) {
						 	echo '<div class="column">';
						 		echo prRender::event($event);
						 	echo '</div>';
						 } 
					 ?>
					 </div>
					 <div class="line-button"><span><a href="https://vtte.utem.cl/tag/<?php echo $settings['slug_content'] ?>" class="button green">Ver todo</a></span></div>
			</div>
		</div>
	<?php endif; ?>
	<div class="row inner-space remove-top">
		<?php if (is_active_sidebar( 'sidebar-home-1-left' )): ?>
			<div class="large-9 columns sidebar-home-1 left">
				<?php dynamic_sidebar( 'sidebar-home-1-left' ); ?>
			</div>
		<?php endif; ?>
		<?php if (is_active_sidebar( 'sidebar-home-1-right' )): ?>
			<div class="large-3 columns sidebar-home-1 right">
				<?php dynamic_sidebar( 'sidebar-home-1-right' ); ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if (is_active_sidebar( 'sidebar-home-2' )): ?>
		<div class="row inner-space remove-top">
			<div class="large-12 columns sidebar-home-2">
				<?php dynamic_sidebar( 'sidebar-home-2' ); ?>
			</div>
		</div>
	<?php endif; ?>
</section>

<?php get_footer(); ?>