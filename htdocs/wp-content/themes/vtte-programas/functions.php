<?php
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;

add_image_size('program-list', 270, 110, true);
add_image_size('product-list', 270, 180, true);
add_image_size('publication-home', 270, 350, true);
add_image_size('publication-gallery', 290, 195, true);
add_image_size('links', 200, 9999, false);

/*
	Calling some files
*/

include STYLESHEETPATH . '/inc/widgets.php';
include STYLESHEETPATH . '/inc/shortcodes.php';

/*
	Activate links manager
*/

add_filter('pre_option_link_manager_enabled', '__return_true');


/*
	Sidebars filters
*/
add_filter('vtte_base_mandatory_sidebars', array('programs','remove_base_sidebars'));

/*
	Theme sidebars
*/
$mandatory_sidebars = array(
	'Portada arriba izquierda' => array(
		'name' => 'home-top-left',
		'size' => 4
	),
	'Portada arriba derecha' => array(
		'name' => 'home-top-right',
		'size' => 8
	),
	'Home Principal' => array(
		'name' => 'sidebar-home-principal',
		'size' => 12
	),
	'Home fila 1' => array(
		'name' => 'sidebar-home-1',
		'size' => 12
		),
	'Home intermedio izquierda' => array(
		'name' => 'sidebar-home-middle-left',
		'size' => 4
	),
	'Home intermedio derecha' => array(
		'name' => 'sidebar-home-middle-right',
		'size' => 8
	),
	'Home fila 2' => array(
		'name' => 'sidebar-home-2',
		'size' => 12
		),
	'Proyecto sidebar' => array(
		'name' => 'project',
		'size' => 3
	),
	'Sidebar Producto' => array(
		'name' => 'product_service',
		'size' => 3
	),
	'Footer Producto' => array(
		'name' => 'Product',
		'size' => 3
	),
	'Menú Servicio' => array(
		'name' => 'service_menu',
		'size' => 3
	),
	'Sidebar Servicio' => array(
		'name' => 'service',
		'size' => 12
	),
	'Footer Servicio' => array(
		'name' => 'footer_service',
		'size' => 12
	),
	'Footer Página' => array(
		'name' => 'page_footer_large',
		'size' => 12
		),
	'Footer Artículo' => array(
		'name' => 'single_footer_large',
		'size' => 12
		),
);

foreach ( $mandatory_sidebars as $sidebar => $id_sidebar ) {
	register_sidebar( array(
		'name'          => $sidebar,
		'id'			=> $id_sidebar['name'],
		'before_widget' => '<section id="%1$s" class="widget %2$s">'."\n",
		'after_widget'  => '</section>',
		'before_title'  => '<header class="widget-header"><h3 class="widget-title">',
		'after_title'   => '</h3></header>',
		'size' => $id_sidebar['size']
	) );
}
add_action('wp_enqueue_scripts', 'enqueue_program_scripts');
function enqueue_program_scripts()
{
	wp_enqueue_script('Foundation.util.motion', PRE_HOME_URI . '/vtte-programas/js/foundation.tabs.js', array('Foundation'), '1.0', '');
	wp_enqueue_script('vtte_programs_script', PRE_HOME_URI . '/vtte-programas/js/script.js', array('jquery'), '1.0', '');
}
class programs {
	static function remove_base_sidebars($sidebars) {
		unset($sidebars['Página footer 1']);
		unset($sidebars['Página footer 2']);
		unset($sidebars['Página footer 3']);
		unset($sidebars['Entrada footer 1']);
		unset($sidebars['Entrada footer 2']);
		unset($sidebars['Entrada footer 3']);
		return $sidebars;
	}
	static function get_projects($size, $offset=0) {
		$projects = new WP_Query(array(
			'post_type' => 'proyect',
			'posts_per_page' => $size,
			'offset' => $offset,
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key' => 'program_featured',
					'compare' => 'NOT EXISTS'
				)
			)
			));
		if ($projects->have_posts()) {
			return $projects->posts;
		} else {
			return false;
		}

	}
	static function get_featured_projects($size) {
		$projects = new WP_Query(array(
			'post_type' => 'proyect',
			'posts_per_page' => $size,
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key' => 'program_featured',
					'value' => 1
				)
			)
			));
		if ($projects->have_posts()) {
			return $projects->posts;
		} else {
			return false;
		}

	}
	static function get_products($size, $offset=0) {
		$projects = new WP_Query(array(
			'post_type' => 'product',
			'posts_per_page' => $size,
			'offset' => $offset,
			'post_status' => 'publish'
			));
		if ($projects->have_posts()) {
			return $projects->posts;
		} else {
			return false;
		}

	}
	static function get_technologies($size, $offset = 0)
	{
		$technology = new WP_Query(array(
			'post_type' => 'technology',
			'posts_per_page' => $size,
			'offset' => $offset,
			'post_status' => 'publish'
		));
		if ($technology->have_posts()) {
			return $technology->posts;
		} else {
			return false;
		}
	}
	static function get_services($size, $offset=0) {
		$projects = new WP_Query(array(
			'post_type' => 'service',
			'posts_per_page' => $size,
			'offset' => $offset,
			'post_status' => 'publish'
			));
		if ($projects->have_posts()) {
			return $projects->posts;
		} else {
			return false;
		}

	}
	public static function get_product_terms($post) {
		$term_list = array();
		if ( !empty( $post ) ) {
			$terms = wp_get_post_terms( $post->ID, 'categorization' );
			foreach ($terms as $term) {
				$term_list[] = $term->term_id;
			}
		}
		return $term_list;
	}
	static function get_product_gallery( $post ) {
		$images = $post->product_gallery;
		$featured_id = get_post_thumbnail_id( $post->ID );
		if ( count( $images ) > 1 ) {
			echo '<h3 class="middle-title"><span>Galería</span></h3>';
			echo '<div class="large-up-4 small-up-2">';
			foreach ( $images as $image ) {
				echo '<div class="column">';
					echo '<a href="'.current(wp_get_attachment_image_src( $image, 'full')).'" class="box">'.wp_get_attachment_image( $image, 'publication-gallery' ).'</a>';
				echo '</div>';
			}
			echo '</div>';
		}
	}
	public static function get_categorization_terms($post) {
		$term_list = array();
		if ( !empty( $post ) ) {
			$terms = wp_get_post_terms( $post->ID, 'categorization' );
			foreach ($terms as $term) {
				$term_list[] = $term->term_id;
			}
		}
		return $term_list;
	}
	static function get_categorization($sidebar, $post=null) {
		if (is_object($sidebar)) {
			$post = $sidebar;
			$sidebar = '';
		}
		$term_list = ( !empty( $post ) ) ? self::get_categorization_terms($post) : array();
		$topics = get_terms('categorization', array( 'hide_empty' => false ));
		if ( !empty( $topics ) ) {
			$sidebar .= '<div class="themes">';
				$sidebar .= '<h3 class="primary-title">Temáticas</h3>';
				$sidebar .= '<ul class="menu vertical">';
				foreach ( $topics as $topic ) {
					$active = ( in_array($topic->term_id, $term_list) ) ? true : false;
					$sidebar .= prRender::topics($topic, $active);
				}
				$sidebar .= '</ul>';
			$sidebar .= '</div>';
			return $sidebar;
		} else {
			return false;
		}
	}
	
	static function get_select_elements($args)
	{
		extract($args);
		$query = new ServiceQuery(array(
			'posts_per_page' => -1,
			'post_status' => 'publish'
		));
		if (count($query) > 0) {
			foreach ($query as $item) {
				$selected_class = (!empty($selected) && ($item->ID == $selected)) ? ' selected="selected"' : '';
				$out .= '<option value="' . $item->ID . '"' . $selected_class . '>' . get_the_title($item->ID) . '</option>';
			}
		}

		return $out;
	}
	static function get_program_form($params = null)
	{
		$defaults = array(
			'title' => 'Solicitar información',
			'subtitle' => '¿Te interesa una de nuestras ofertas?',
			'button_text' => 'Solicitar',
			'bottom_info' => true,
			'phone' => '(+56 2) 2787 7758',
			'mail' => 'capacitacion@utem.cl',
			'program_type' => 'all',
			'program_topic' => false,
			'selected' => false
		);
		$params = wp_parse_args($params, $defaults);


		return prRender::form($params);
	}
	function send_mail()
	{
		if (check_ajax_referer('ask_information', 'sec') && (empty($_POST['lastname']))) {
			global $_set;
			$settings = $_set->settings;
			$name = esc_attr($_POST['name']);
			$mail = esc_attr($_POST['mail']);
			$phone = esc_attr($_POST['phone']);
			$program_id = esc_attr($_POST['program']);
			$program_name = get_the_title($program_id);
			$program_link = get_permalink($program_id);
			$array_mails = explode(',', $settings['info_mail']);
			if (count($array_mails) == 1) {
				$send_mail = (!empty($settings['info_mail'])) ? $settings['info_mail'] : get_option('admin_email');
			}
			if (!empty($name) && !empty($mail) && !empty($program_id)) {
				add_filter('wp_mail_content_type', create_function('', 'return "text/html"; '));
				$message = '<small>Este correo ha sido enviado desde el sitio web de '.get_bloginfo('name').'</small><br><br>';
				$message .= '<h1>Solicitud de información</h1>';
				$message .= '<p>' . $name . ' Ha solicitado información relativa al servicio de: <a href="' . $program_link . '">' . $program_name . '</a></p>';
				$message .= '<p>Puede contactarl utilzando los siguientes datos</p>';
				$message .= '<p><strong>Mail</strong>: ' . $mail . '</p>';
				if (!empty($phone)) {
					$message .= '<p><strong>Teléfono</strong>: ' . $phone . '</p><br><br>';
				}
				$message .= '<p>Para revisar el servicio solicitado haga click <a href="' . $program_link . '">aqui</a></p>';

				$subject = '[' . get_bloginfo('name') . '] solicitud de información del servicio: ' . $program_name;

				$headers = array(
					'From: "' . $name . '" <' . $mail . '>'
				);
				if (count($array_mails > 1)) {
					$send = 0;
					foreach ($array_mails as $the_mail) {
						if (wp_mail(trim($the_mail), $subject, $message, $headers)) {
							$send++;
						}
					}
					if ($send == count($array_mails)) {
						echo 1;
					} else {
						echo 'mail error';
					}
				} else {
					$to = $send_mail;
					if (wp_mail($to, $subject, $message, $headers)) {
						echo 1;
					} else {
						echo 'mail error';
					}
				}

			} else {
				echo 'error';
				exit();
			}

			exit();
		} else {
			echo 'Error ¬¬';
			exit(0);
		}

	}
	public function add_form_config($form, $data)
	{
		$form->addElement(new Element\InputText(
			_x('Correo de recepción solicitudes', 'site settings fields', 'vtte'),
			'info_mail',
			array(
				'value' => isset($data['info_mail']) ? $data['info_mail'] : ''
			)
		));
		return $form;
	}
	public function add_form_config_field($fields)
	{
		$fields[] = 'info_mail';
		return $fields;
	}
}
add_filter('vtte_main_configuration_form', array('programs', 'add_form_config'), 10, 2);
add_filter('vtte_main_configuration_fields', array('programs', 'add_form_config_field'), 10, 1);

add_action('wp_ajax_nopriv_ask_information', array('programs', 'send_mail'));
add_action('wp_ajax_ask_information', array('programs', 'send_mail'));

class prRender {
	public static function person($id)
	{
		$person = get_post($id);
		$out = '';
		$out .= '<article class="hentry entry-person">';
		$out .= '<div class="img round">';
		$out .= get_the_post_thumbnail($id, 'squared');
		$out .= '</div>';
		$position = get_post_meta($id, 'person_position', true);
		$mail = get_post_meta($id, 'person_mail', true);

		$out .= '<h3 class="entry-title">' . get_the_title($id) . '</h3>';
		if (!empty($position)) {
			$out .= '<span class="position">' . $position . '</span>';
		}
		if (!empty($mail)) {
			$out .= '<span class="mail"><a href="mailto:' . $mail . '">' . antispambot($mail) . '</a></span>';
		}
		if (!empty($person->post_content)) {
			$out .= '<div class="entry-summary">' . apply_filters('the_content', $person->post_content) . '</div>';
		}

		$out .= '</article>';
		return $out;
	}
	static function form($args)
	{
		$select_elements = programs::get_select_elements($args);
		extract($args);
		$out = '<div class="info-box">';
		$out .= '<h3 class="secondary-title">' . $title . '</h3>';
		$out .= '<div class="program-form">';
		$out .= '<h5 class="form-title">' . $subtitle . '</h5>';
		$out .= '<form action="" method="POST" class="program-info valid-form" data-abide novalidate>';
		$out .= '<div data-abide-error class="alert callout error-message" style="display: none;">';
		$out .= '<p><span class="dashicons dashicons-dismiss"></span> Hay errores en el envío del formulario.</p>';
		$out .= '</div>';
		$out .= '<div class="success callout success-message" style="display: none;">';
		$out .= '<p><span class="dashicons dashicons-yes"></span> Formulario enviado, te contactaremos a la brevedad.</p>';
		$out .= '</div>';
		$out .= '<label>';
		$out .= '<input type="text" name="name" class="input-text name-input" placeholder="Nombre" required>';
		$out .= '<span class="form-error">Debe especificar un nombre...</span>';
		$out .= '</label>';
		$out .= '<label>';
		$out .= '<input type="email" name="mail" class="input-text mail-input" placeholder="Correo" required>';
		$out .= '<span class="form-error">Debe especificar un correo para contacto...</span>';
		$out .= '</label>';
		$out .= '<label>';
		$out .= '<input type="text" name="phone" class="input-text phone-input" placeholder="Teléfono">';
		$out .= '</label>';
		$out .= '<label>';
		$out .= '<select name="program" id="program" class="program-select-form" required>';
		$out .= '<option value="">Seleccionar Servicio</option>';
							//$out .= ( $program_type == 'all') ? programs::get_all_programs($selected) : programs::get_program_type_select($program_type);
		$out .= $select_elements;
		$out .= '</select>';
		$out .= '</label>';
		$out .= wp_nonce_field('ask_information', 'ask_information_nonce', true, false);
		$out .= '<input type="text" name="last_name" class="input-text lastname-input" placeholder="last name">';

		$out .= '<input type="submit" class="button expanded green send-form" value="' . $button_text . '">';
		if ($bottom_info) {
			$out .= '<div class="bottom-info clearfix">';
			$out .= '<span class="float-left">';
			$out .= '<span class="dashicons dashicons-phone"></span> ' . $phone;
			$out .= '</span>';
			$out .= '<span class="float-right">';
			$out .= '<span class="dashicons dashicons-email-alt"></span> ' . $mail;
			$out .= '</span>';
			$out .= '</div>';
		}
		$out .= '</form>';
		$out .= '</div>';
		$out .= '</div>';
		return $out;
	}
	static function topics( $item, $active ) {
		$link = ( !is_wp_error( get_term_link( $item, 'categorization' ) ) ) ? get_term_link( $item, 'categorization' ) : '#';
		$active = ( !empty( $active ) ) ? ' class="active"':'';
		return '<li'.$active.'><a href="'.$link.'">'.$item->name.'</a></li>';
	}
	static function featured_project($post) {
		$out = '';
		$out .= '<div class="widget news">';
            $out .= '<div class="widget-content clearfix">';
                    $out .= '<article class="hentry entry-news">';
                        $out .= '<a href="'.get_permalink($post->ID).'">';
                            $out .= get_the_post_thumbnail( $post->ID, 'squared' );
                            $out .= '<div class="entry-info">';
                                $out .= '<h4 class="entry-title">'.get_the_title($post->ID).'</h4>';
                            $out .= '</div>';
                        $out .= '</a>';
                    $out .= '</article>';
                $out .= '</div>';
        $out .= '</div>';
        return $out;
}
	static function project($post) {
		$out = '<article class="hentry entry-publication">';
			$out .= '<a href="'.get_permalink($post->ID).'">';
				$out .= '<div class="entry-image">';
					$out .= get_the_post_thumbnail( $post->ID, 'thumbnail' );
				$out .= '</div>';
			$out .= '</a>';
			$out .= '<div class="entry-content">';
				$out .= '<a href="'.get_permalink($post->ID).'"><h4 class="entry-title">'.get_the_title($post->ID).'</h4></a>';
			$out .= '</div>';
		$out .= '</article>';
		return $out;
	}
	static function project_vertical($post)
	{
		$out = '<article class="hentry entry-publication vertical">';
			$out .= '<a href="' . get_permalink($post->ID) . '">';
				$out .= '<div class="entry-image">';
					$out .= get_the_post_thumbnail($post->ID, 'thumbnail');
				$out .= '</div>';
			$out .= '</a>';
			$out .= '<div class="entry-content">';
				$out .= '<a href="' . get_permalink($post->ID) . '"><h4 class="entry-title">' . get_the_title($post->ID) . '</h4></a>';
			$out .= '</div>';
		$out .= '</article>';
		return $out;
	}
	static function product($post) {
		$out = '<article class="hentry entry-product">';
			$out .= '<a href="'.get_permalink($post->ID).'">';
				$out .= '<span class="entry-image">';
					$out .= get_the_post_thumbnail( $post->ID, 'product-list' );
				$out .= '</span>';
				$out .= '<span class="overlay">';
					$out .= '<h4 class="entry-title">'.get_the_title($post->ID).'</h4>';
				$out .= '</span>';
			$out .= '</a>';
		$out .= '</article>';
		return $out;
	}
}

/*
	Custom link featured image
*/
	class linkMeta extends Model\PostMeta{
    protected function setId(){
        return 'link';
    }
    protected function setDataModel(){
    	global $link;
    	$options = get_option('link_img');
    	$link_image = $options[$link->link_id];
    	
        return array(
            new Model\PostMetaData(
                'image',
                '',
                '\GutenPress\Forms\Element\WPImage',
                array(
                	'value' => (!empty($link_image)) ? $link_image : ''
                )
            )
         );
    }
}
new Model\Metabox( 'LinkMeta', 'Imagen del link', 'link', array('priority' => 'high', 'context' => 'side') );
 add_action('edit_link', 'link_save_postdata', 10, 1);
 add_action('add_link', 'link_save_postdata', 10, 1);
 function link_save_postdata($link_id) {
 	if (is_admin() && (!empty($_POST['link-form']['image']))) {
 		$image = esc_attr($_POST['link-form']['image']);
 		$links = get_option( 'link_img' );
 		$links[$link_id] = $image;
 		update_option('link_img', $links);
 	}
 }