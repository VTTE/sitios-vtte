<?php 

/*
	Editorial theme widgets
*/

class WP_Widget_related_products extends WP_Widget {
    /** constructor */
    function __construct() {
        $widget_ops = array('classname' => 'related-products', 'description' => 'Muestra un listado contextual de productos / servicios relacionados a la entrada');
        $control_ops = array();
        parent::__construct('related-products', 'Productos / servicios relacionados', $widget_ops, $control_ops);
    }

    function widget($args, $instance) {
        extract( $instance );
        extract( $args );
        //static function get_related_books( $post, $grid=4, $title='Más Publicaciones', $link=true ) {
        global $post;
        $size = (!empty($instance['size'])) ? $instance['size'] : 4;
        $link_text = ( !empty( $instance['link_text'] ) ) ? $instance['link_text'] : 'Revisar todos';
        
		$terms = programs::get_product_terms($post);
		if ( !empty($terms) ) {
			$params = array( 
				'post_status' => 'publish', 
				'posts_per_page' => $size,
				'orderby' => 'rand',
				'post__not_in' => array($post->ID),
				'tax_query' => array(
						array(
							'taxonomy' => 'categorization',
							'field' => 'id',
							'terms' => $terms
							)
						) 
					);

				$products = new ProductQuery( $params );

			} else {
				$products = new ProductQuery( array( 
					'post_status' => 'publish', 
					'orderby' => 'rand',
					'posts_per_page' => $size
				) );
			}
			if ( !empty( $products ) ) {
				echo '<div class="widget related-products">';
					echo '<div class="related-title clearfix">';
						echo '<h3 class="secondary-title float-left">'.$title.'</h3>';
						if ( !empty( $is_link ) ) {
							echo '<a href="'.site_url('productos').'" class="all float-right">'.$link_text.'</a>';
						}
					echo '</div>';
					echo '<div class="large-up-'.$grid.' medium-up-'.$grid.' small-up-1 clearfix">';
					$current_post_id = $post->ID;
					foreach ( $products as $product ) {
							echo prRender::product($book);
					}
					echo '</div>';
				}

		echo '</div>';
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form( $instance ) {
        extract( $instance );
        echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
        echo '<p><label for="'. $this->get_field_name('is_link').'">Link a archivo de productos/servicios? </label><input type="checkbox" id="'. $this->get_field_id('is_link').'"'.( ( !empty( $is_link ) ) ? ' checked="checked" ' : '' ).' name="'.$this->get_field_name('is_link').'" value="1"></p>';
        echo '<p><label for="'.$this->get_field_id('link_text').'">Texto link: <input type="text" name="'. $this->get_field_name('link_text') .'" id="'.$this->get_field_id('link_text').'" value="'.$instance['link_text'].'" class="widefat"/></label><br><small>Si no se especifica será: "Revisar todos"</small></p>';
        echo '<p><label for="'.$this->get_field_id('size').'">Cantidad de entradas: <input type="number" name="'. $this->get_field_name('size') .'" id="'.$this->get_field_id('size').'" value="'.$instance['size'].'"/></label></p>';
        echo '<h3>Apariencia</h3>';
        echo '<p><label>Grilla: </label>';
	        echo '<select class="widefat" id="'.$this->get_field_id('grid').'" name="'.$this->get_field_name('grid').'">';
	        	echo '<option value="">Seleccione</option>';
	            echo '<option value="1" '.(($grid == '1') ? 'selected="selected"' : '') .'>1</option>';
	            echo '<option value="2" '.(($grid == '2') ? 'selected="selected"' : '') .'>2</option>';
	            echo '<option value="3" '.(($grid == '3') ? 'selected="selected"' : '') .'>3</option>';
	            echo '<option value="4" '.(($grid == '4') ? 'selected="selected"' : '') .'>4</option>';
	        echo '</select>';
	        echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
        echo '</p>';
       } 
}
class WP_Widget_project_list extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		$widget_ops = array('classname' => 'project-list', 'description' => 'Muestra un listado de proyectos destacados');
		$control_ops = array();
		parent::__construct('project-list', 'Listado de Proyectos destacados', $widget_ops, $control_ops);
	}

	function widget($args, $instance)
	{
		extract($instance);
		extract($args);
        //static function get_related_books( $post, $grid=4, $title='Más Publicaciones', $link=true ) {
		global $post;
		$size = (!empty($instance['size'])) ? $instance['size'] : 4;
		$grid = (!empty($grid)) ? $grid : 3;
		$projects = programs::get_featured_projects($size);
		if (!empty($projects)) {
			echo '<div class="row inner-space remove-top project-list">';
				echo '<div class="large-12 columns">';
					if (!empty($title)) {
						echo '<div class="widget sidebar-title">';
							echo '<h3 class="widget-title">' . $title . '</h3>';
						echo '</div>';
					}
					echo '<div class="large-up-' . $grid . ' small-up2 medium-up-' . $grid . ' row clearfix">';
						foreach ($projects as $project) {
							echo '<div class="column">';
								echo prRender::project_vertical($project);
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';
			echo '</div>';
		}
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function form($instance)
	{
		extract($instance);
		echo '<p><label for="' . $this->get_field_id('title') . '">Titulo: <input type="text" name="' . $this->get_field_name('title') . '" id="' . $this->get_field_id('title') . '" value="' . $instance['title'] . '" class="widefat" /></label></p>';
		echo '<p><label for="' . $this->get_field_id('size') . '">Cantidad de entradas: <input type="number" name="' . $this->get_field_name('size') . '" id="' . $this->get_field_id('size') . '" value="' . $instance['size'] . '"/></label></p>';
		echo '<h3>Apariencia</h3>';
		echo '<p><label>Grilla: </label>';
		echo '<select class="widefat" id="' . $this->get_field_id('grid') . '" name="' . $this->get_field_name('grid') . '">';
		echo '<option value="">Seleccione</option>';
		echo '<option value="1" ' . (($grid == '1') ? 'selected="selected"' : '') . '>1</option>';
		echo '<option value="2" ' . (($grid == '2') ? 'selected="selected"' : '') . '>2</option>';
		echo '<option value="3" ' . (($grid == '3') ? 'selected="selected"' : '') . '>3</option>';
		echo '<option value="4" ' . (($grid == '4') ? 'selected="selected"' : '') . '>4</option>';
		echo '</select>';
		echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
		echo '</p>';
	}
}
class WP_Widget_products_list extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		$widget_ops = array('classname' => 'products-list', 'description' => 'Muestra un listado de los últimos productos');
		$control_ops = array();
		parent::__construct('products-list', 'Listado de Productos', $widget_ops, $control_ops);
	}

	function widget($args, $instance)
	{
		extract($instance);
		extract($args);
        //static function get_related_books( $post, $grid=4, $title='Más Publicaciones', $link=true ) {
		global $post;
		$size = (!empty($instance['size'])) ? $instance['size'] : 4;
		$grid = (!empty($grid)) ? $grid : 3;
		$products = programs::get_products ($size, 0);
		if (!empty($products)) {
			echo '<div class="row inner-space remove-top product-list">';
				echo '<div class="large-12 columns">';
					echo '<div class="widget sidebar-title">';
						echo '<h3 class="widget-title">'.$title.'</h3>';
					echo '</div>';
						echo '<div class="large-up-'.$grid.' small-up2 medium-up-'.$grid.' row clearfix">';
						foreach ($products as $product) {
							echo '<div class="column">';
							echo prRender::product($product);
							echo '</div>';
						}
						echo '</div>';
				echo '</div>';
			echo '</div>';
		}
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function form($instance)
	{
		extract($instance);
		echo '<p><label for="' . $this->get_field_id('title') . '">Titulo: <input type="text" name="' . $this->get_field_name('title') . '" id="' . $this->get_field_id('title') . '" value="' . $instance['title'] . '" class="widefat" /></label></p>';
		echo '<p><label for="' . $this->get_field_id('size') . '">Cantidad de entradas: <input type="number" name="' . $this->get_field_name('size') . '" id="' . $this->get_field_id('size') . '" value="' . $instance['size'] . '"/></label></p>';
		echo '<h3>Apariencia</h3>';
		echo '<p><label>Grilla: </label>';
		echo '<select class="widefat" id="' . $this->get_field_id('grid') . '" name="' . $this->get_field_name('grid') . '">';
		echo '<option value="">Seleccione</option>';
		echo '<option value="1" ' . (($grid == '1') ? 'selected="selected"' : '') . '>1</option>';
		echo '<option value="2" ' . (($grid == '2') ? 'selected="selected"' : '') . '>2</option>';
		echo '<option value="3" ' . (($grid == '3') ? 'selected="selected"' : '') . '>3</option>';
		echo '<option value="4" ' . (($grid == '4') ? 'selected="selected"' : '') . '>4</option>';
		echo '</select>';
		echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
		echo '</p>';
	}
}
class WP_Widget_services_list extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		$widget_ops = array('classname' => 'services-list', 'description' => 'Muestra un listado de los últimos servicios');
		$control_ops = array();
		parent::__construct('services-list', 'Listado de Servicios', $widget_ops, $control_ops);
	}

	function widget($args, $instance)
	{
		extract($instance);
		extract($args);
        //static function get_related_books( $post, $grid=4, $title='Más Publicaciones', $link=true ) {
		global $post;
		$size = (!empty($instance['size'])) ? $instance['size'] : 4;
		$grid = (!empty($grid)) ? $grid : 3;
		$products = programs::get_services($size, 0);
		if (!empty($products)) {
			echo '<div class="row inner-space remove-top product-list">';
			echo '<div class="large-12 columns">';
			echo '<div class="widget sidebar-title">';
			echo '<h3 class="widget-title">' . $title . '</h3>';
			echo '</div>';
			echo '<div class="large-up-' . $grid . ' small-up2 medium-up-' . $grid . ' row clearfix">';
			foreach ($products as $product) {
				echo '<div class="column">';
				echo prRender::product($product);
				echo '</div>';
			}
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function form($instance)
	{
		extract($instance);
		echo '<p><label for="' . $this->get_field_id('title') . '">Titulo: <input type="text" name="' . $this->get_field_name('title') . '" id="' . $this->get_field_id('title') . '" value="' . $instance['title'] . '" class="widefat" /></label></p>';
		echo '<p><label for="' . $this->get_field_id('size') . '">Cantidad de entradas: <input type="number" name="' . $this->get_field_name('size') . '" id="' . $this->get_field_id('size') . '" value="' . $instance['size'] . '"/></label></p>';
		echo '<h3>Apariencia</h3>';
		echo '<p><label>Grilla: </label>';
		echo '<select class="widefat" id="' . $this->get_field_id('grid') . '" name="' . $this->get_field_name('grid') . '">';
		echo '<option value="">Seleccione</option>';
		echo '<option value="1" ' . (($grid == '1') ? 'selected="selected"' : '') . '>1</option>';
		echo '<option value="2" ' . (($grid == '2') ? 'selected="selected"' : '') . '>2</option>';
		echo '<option value="3" ' . (($grid == '3') ? 'selected="selected"' : '') . '>3</option>';
		echo '<option value="4" ' . (($grid == '4') ? 'selected="selected"' : '') . '>4</option>';
		echo '</select>';
		echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
		echo '</p>';
	}
}
class WP_Widget_links_edition extends WP_Widget {
	/** constructor */
	function __construct() {
		$widget_ops = array('classname' => 'links', 'description' => 'Listado de enlaces de interés');
		$control_ops = array();
		parent::__construct('links', 'Enlaces de interés', $widget_ops, $control_ops);
	}
	function get_links($category) {
		$params = array();
		if (!empty( $category )) {
			$params['category_name'] = $category;
		}
		$links = get_bookmarks( $params );
		$links_img = get_option('link_img');
		$list_link = array();
		if (!empty($links)) {
			foreach ( $links as $link ) {
				$the_link = new stdClass();
				$the_link->id = $link->link_id;
				$the_link->name = $link->link_name;
				$the_link->url = $link->link_url;
				$the_link->target = $link->link_target;
				$the_link->img = $links_img[$link->link_id];
				$list_link[] = $the_link;
			}
			return $list_link;
		} else {
			return false;
		}
	}
	function widget($args, $instance) {
		extract( $instance );
		extract( $args );
		$links = $this->get_links($category);
	   echo '<div class="widget links">';
			echo '<h4 class="secondary-title">'.esc_attr($title).'</h4>';
			echo '<div class="large-up-5 medium-up-3 small-up-2 clearfix">';
				if (!empty( $links )) {
					foreach ( $links as $link ) {
						echo '<div class="column">';
							echo '<a href="'.$link->url.'" title="'.$link->name.'" alt="'.$link->name.'">'.wp_get_attachment_image($link->img, 'links').'</a>';
						echo '</div>';
					}
				}
			echo '</div>';
		echo '</div>';
	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}
	function get_link_categories($instance) {
		$terms = get_terms('link_category');
		if (!empty($terms)) {
			foreach ( $terms as $term ) {
				$sel = ($instance['category'] == $term->slug) ? ' selected="selected"' : '';
				echo '<option value="'.$term->slug.'"'.$sel.'>'.$term->name.'</option>';
			}
		}
	}
	function form( $instance ) {
		extract( $instance );
		echo '<p><label for="'.$this->get_field_id('title').'">Titulo: <input type="text" name="'. $this->get_field_name('title') .'" id="'.$this->get_field_id('title').'" value="'.$instance['title'].'" class="widefat" /></label></p>';
		 echo '<p><label>Categoria enlaces: </label>';
		echo '<select class="widefat" id="'.$this->get_field_id('category').'" name="'.$this->get_field_name('category').'">';
			echo '<option value="">Seleccione</option>';
			   $this->get_link_categories($instance);
			echo '</select>';
		echo '</p>';
	   } 

}

class WP_Widget_Program_form extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		$widget_ops = array('classname' => 'form-program-info', 'description' => 'Muestra el formulario de solicitud de información');
		$control_ops = array();
		parent::__construct('form-program-info', 'Formulario solicitud información', $widget_ops, $control_ops);
	}

	function widget($args, $instance)
	{
		global $post;
		extract($instance);

		$params = array(
			'bottom_info' => $is_bottom,
			'selected' => ((get_class(get_queried_object()) == 'WP_Post') ? $post->ID : false)
		);
		if (!empty($instance['title'])) {
			$params['title'] = $instance['title'];
		}
		if (!empty($instance['subtitle'])) {
			$params['subtitle'] = $instance['subtitle'];
		}
		if (get_class(get_queried_object()) == 'WP_Term') {
			if (get_queried_object()->taxonomy == 'program_type') {
				$params['program_type'] = get_queried_object()->slug;
				$params['program_topic'] = false;
			}
			if (get_queried_object()->taxonomy == 'program_topic') {
				$params['program_topic'] = get_queried_object()->slug;
				$params['program_type'] = false;
			}
		}
		echo programs::get_program_form($params);
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function form($instance)
	{
		extract($instance);
		echo '<p><label for="' . $this->get_field_id('title') . '">Titulo: <input type="text" name="' . $this->get_field_name('title') . '" id="' . $this->get_field_id('title') . '" value="' . $instance['title'] . '" class="widefat" /></label></p>';
		echo '<p><label for="' . $this->get_field_id('subtitle') . '">Subtitulo: <input type="text" name="' . $this->get_field_name('subtitle') . '" id="' . $this->get_field_id('subtitle') . '" value="' . $instance['subtitle'] . '" class="widefat" /></label></p>';
		echo '<p><label for="' . $this->get_field_name('is_bottom') . '">Mostrar teléfono y mail? </label><input type="checkbox" id="' . $this->get_field_id('is_bottom') . '"' . ((!empty($is_bottom)) ? ' checked="checked" ' : '') . ' name="' . $this->get_field_name('is_bottom') . '" value="1"></p>';
	}
}

class WP_Widget_tecnologies_list extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		$widget_ops = array('classname' => 'technologies-list', 'description' => 'Muestra un listado de tecnologias destacados');
		$control_ops = array();
		parent::__construct('technologies-list', 'Listado de Tecnologías destacados', $widget_ops, $control_ops);
	}

	function widget($args, $instance)
	{
		extract($instance);
		extract($args);
		//static function get_related_books( $post, $grid=4, $title='Más Publicaciones', $link=true ) {
		global $post;
		$size = (!empty($instance['size'])) ? $instance['size'] : 4;
		$grid = (!empty($grid)) ? $grid : 3;
		$projects = programs::get_technologies($size);
		if (!empty($projects)) {
			echo '<div class="row inner-space remove-top project-list technologies-list">';
			echo '<div class="large-12 columns">';
			if (!empty($title)) {
				echo '<div class="widget sidebar-title">';
				echo '<h3 class="widget-title">' . $title . '</h3>';
				echo '</div>';
			}
			echo '<div class="large-up-' . $grid . ' small-up2 medium-up-' . $grid . ' row clearfix">';
			foreach ($projects as $project) {
				echo '<div class="column">';
				echo prRender::project_vertical($project);
				echo '</div>';
			}
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function form($instance)
	{
		extract($instance);
		echo '<p><label for="' . $this->get_field_id('title') . '">Titulo: <input type="text" name="' . $this->get_field_name('title') . '" id="' . $this->get_field_id('title') . '" value="' . $instance['title'] . '" class="widefat" /></label></p>';
		echo '<p><label for="' . $this->get_field_id('size') . '">Cantidad de entradas: <input type="number" name="' . $this->get_field_name('size') . '" id="' . $this->get_field_id('size') . '" value="' . $instance['size'] . '"/></label></p>';
		echo '<h3>Apariencia</h3>';
		echo '<p><label>Grilla: </label>';
		echo '<select class="widefat" id="' . $this->get_field_id('grid') . '" name="' . $this->get_field_name('grid') . '">';
		echo '<option value="">Seleccione</option>';
		echo '<option value="1" ' . (($grid == '1') ? 'selected="selected"' : '') . '>1</option>';
		echo '<option value="2" ' . (($grid == '2') ? 'selected="selected"' : '') . '>2</option>';
		echo '<option value="3" ' . (($grid == '3') ? 'selected="selected"' : '') . '>3</option>';
		echo '<option value="4" ' . (($grid == '4') ? 'selected="selected"' : '') . '>4</option>';
		echo '</select>';
		echo '<small>El tamaño de la grilla corresponde en cuantas columnas se divide el widget considerando el espacio donde está</small>';
		echo '</p>';
	}
}

// register WP_Widgets
add_action('widgets_init', 'register_editorial_widgets');
add_action('init', 'register_editorial_widgets');
function register_editorial_widgets(){
	register_widget('WP_Widget_program_form');
	register_widget('WP_Widget_project_list' );
    register_widget( 'WP_Widget_products_list' );
    register_widget( 'WP_Widget_services_list' );
    register_widget( 'WP_Widget_related_products' );
    register_widget( 'WP_Widget_links_edition' );
    register_widget('WP_Widget_tecnologies_list');
}