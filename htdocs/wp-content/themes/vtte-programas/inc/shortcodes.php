<?php
use GutenPress\Forms as Forms;
use GutenPress\Forms\Element as Element;

class PersonList extends \GutenPress\Model\Shortcode
{
    public function setTag()
    {
        $this->tag = 'personas';
    }
    public function setFriendlyName()
    {
        $this->friendly_name = 'Listado de Personas';
    }
    public function setDescription()
    {
        $this->description = 'Permite generar un listado de persona';
    }

    public function display($atts, $content)
    {
        $size = ($atts['size']) ? $atts['size'] : 3;
        $title = $atts['title'];
        $entries = new WP_Query(array(
            'post_type' => 'person',
            'posts_per_page' => $size,
            'orderby' => 'menu_order',
            'order' => 'ASC'
        ));
        $category_link = get_term_link($the_term, 'person_type');
        $out = '';
        if ($entries->have_posts()) {
            $out .= '<div class="module person-list">';
            if (!empty($title)) {
                $out .= '<h4 class="entries-title">' . $title . '</h4>';
            }
            $out .= '<div class="row large-up-3 small-up-1 entries-list">';
            foreach ($entries->posts as $entry) {
                $out .= '<div class="column">';
                $out .= prRender::person($entry->ID);
                $out .= '</div>';
            }
            $out .= '</div>';
            $out .= '</div>';
        }
        return $out;
    }
    public function configForm()
    {
        wp_enqueue_media();
        $form = new Forms\MetaboxForm('parallax-title-shortcode');
        $form->addElement(
            new Element\InputText(
                'Cantidad de entradas',
                'size'
            )
        );
        echo $form;
        exit;
    }
}
\GutenPress\Model\ShortcodeFactory::create('PersonList');