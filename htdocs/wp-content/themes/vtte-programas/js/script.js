jQuery(document).on("forminvalid.zf.abide", function (ev, frm) {
    return false;
}).on("formvalid.zf.abide", function (ev, frm) {
    var name = frm.find('.name-input').val(),
        mail = frm.find('.mail-input').val(),
        phone = frm.find('.phone-input').val(),
        program = frm.find('.program-select-form option:selected').val(),
        lastname = frm.find('.lastname-input').val(),
        sec = frm.find('#ask_information_nonce').val();
    jQuery.ajax({
        url: Ajax.url,
        type: 'POST',
        data: {
            action: 'ask_information',
            name: name,
            mail: mail,
            phone: phone,
            program: program,
            lastname: lastname,
            sec: sec
        },
        beforeSend: function () {
            jQuery('.send-form').val('ENVIANDO...');
        },
        success: function (data) {
            console.log(data);
            if (data == 'mail error') {
                jQuery('.error-message').show();
            } else if (data == 1) {
                jQuery('.send-form').val('Formulario enviado');
                jQuery('.send-form').attr('disabled', 'disabled');
                jQuery('.success-message').show();
            }
        }
    });
    
    return false;
});
jQuery(document).ready(function($){
    $(document).foundation();
    $('.program-info').on('submit', function (e) {
        e.preventDefault();
        return false;
    });
});