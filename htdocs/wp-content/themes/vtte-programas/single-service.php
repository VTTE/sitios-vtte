<?php 
	get_header(); 
	// get_post();
	global $post;
	setup_postdata( $post );
?>
<section class="main-content">
	<div class="row hentry entry-book" itemscope itemtype="http://schema.org/Book">
		<div class="large-3 columns side-navigation">
			<?php dynamic_sidebar('service_menu') ?>
		</div>
		<div class="large-6 columns">
			<h3 class="entry-title" itemprop="name"><?php the_title(); ?></h3>
			<div class="std-text">
				<?php the_post_thumbnail('landscape-medium', array('itemprop', 'image')); ?>
				<?php the_content(); ?>
			</div>
		</div>
		
		<div class="large-3 columns themes">
			<?php 
				if (is_active_sidebar( 'service' )) {
					dynamic_sidebar ('service');
				} else {
					echo programs::get_categorization($post);
				}
				?>
		</div>
	</div>
	<?php if (is_active_sidebar('footer_service')) : ?>
		<div class="light-gray">
			<div class="row inner-space related-books">
				<div class="large-12 columns">
					<?php dynamic_sidebar('footer_service'); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php get_footer(); ?>