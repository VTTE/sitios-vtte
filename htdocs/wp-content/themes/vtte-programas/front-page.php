<?php  get_header(); ?>

<section class="main-content">
	<div class="row inner-space remove-bottom">
		<div class="large-4 columns">
			<?php 
				if (is_active_sidebar('home-top-left')) {
					dynamic_sidebar('home-top-left');
				} else {
					$featured_project = current(programs::get_featured_projects(1));
					echo prRender::featured_project($featured_project);
				}
			?>
		</div>
		<div class="large-8 columns project-list">
			<?php 
				if (is_active_sidebar('home-top-right')) {
					dynamic_sidebar('home-top-right');
				} else {
			 ?>
				<h2 class="primary-title">Proyectos</h2>
				<div class="large-up-2 small-up-1 medium-up-2 row cleafix inline-lists">
					<?php 
						$programs = programs::get_projects(4,0);
						foreach ( $programs as $program ) {
							echo '<div class="column">';
								echo prRender::project($program);
							echo '</div>';
						}
					?>
				</div>
				<a href="<?php echo site_url('proyectos') ?>" class="button green">Ver todos los proyectos</a>
			<?php } ?>
		</div>
	</div>
	<?php if (is_active_sidebar('sidebar-home-principal')) : ?>
		<div class="row inner-space remove-top">
			<div class="large-12 columns sidebar-home-principal">
				<?php dynamic_sidebar('sidebar-home-principal'); ?>
			</div>
		</div>
	<?php endif; ?>

	<?php if (is_active_sidebar( 'sidebar-home-1' )): ?>
		<div class="row inner-space remove-top">
			<div class="large-12 columns sidebar-home-1">
				<?php dynamic_sidebar( 'sidebar-home-1' ); ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (is_active_sidebar('sidebar-home-middle-left') || is_active_sidebar('sidebar-home-middle-right')) : ?>
		<div class="row inner-space remove-top">
			<div class="large-4 columns sidebar-home-middle-left">
				<?php dynamic_sidebar('sidebar-home-middle-left'); ?>
			</div>
			<div class="large-8 columns sidebar-home-middle-right">
				<?php dynamic_sidebar('sidebar-home-middle-right'); ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (is_active_sidebar( 'sidebar-home-2' )): ?>
		<div class="row inner-space remove-top">
			<div class="large-12 columns sidebar-home-2">
				<?php dynamic_sidebar( 'sidebar-home-2' ); ?>
			</div>
		</div>
	<?php endif; ?>
</section>

<?php get_footer(); ?>