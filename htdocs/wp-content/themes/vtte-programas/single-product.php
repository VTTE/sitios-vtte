<?php 
	get_header(); 
	// get_post();
	global $post;
	setup_postdata( $post );
?>
<section class="main-content">
	<div class="row">
		<div class="large-12 columns header-books">
			<h4 class="secondary-title">Producto</h4>
		</div>
	</div>
	<div class="row hentry entry-product">
		<div class="large-3 columns entry-image">
			<?php the_post_thumbnail('publication-home', array( 'itemprop', 'image' )); ?>
		</div>
		<div class="large-6 columns">
			<h3 class="entry-title" itemprop="name"><?php the_title(); ?></h3>
			<?php the_content(); ?>
		</div>
		
		<div class="large-3 columns themes">
			<aside class="single-sidebar">
			<?php 
				if (is_active_sidebar( 'product_service' )) {
					dynamic_sidebar('product_service'); 
				} else {
					echo programs::get_categorization($post);
				}
				?>
			</aside>
		</div>
	</div>
	<div class="row entry-gallery">
		<div class="large-12 columns">
			<?php 
				programs::get_product_gallery($post);
			 ?>
		</div>
	</div>
	<?php if (is_active_sidebar('product_service')) : ?>
	<div class="light-gray">
		<div class="row inner-space related-books">
			<div class="large-12 columns">
				<?php dynamic_sidebar('product') ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
</section>
<?php get_footer(); ?>