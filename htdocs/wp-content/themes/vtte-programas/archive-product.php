<?php 
	get_header(); 
		$search = new search_filter();
		$search->set_post_type('product');
		if ( get_query_var('paged') ) {
			$search->set_page(get_query_var('paged'));
		}
		if ( isset($_GET['action']) ) {
			if (isset($_GET['search'])) {
				$search->set_search_text( esc_attr( $_GET['search'] ) ) ;
			}
			$taxonomies = [];
			if (isset($_GET['categorizacion']) && ($_GET['categorizacion'] != '')) {
				$taxonomies['categorization'] = esc_attr($_GET['categorizacion']);
			}
			
			$search->set_array_taxonomies($taxonomies);
		}
		$query = $search->search();
?>
<section class="main-content">
	<header class="row section-header">
		<div class="large-12 columns">
			<h3 class="secondary-title"><?php echo sitio::page_title() ?></h3>
			<div class="filter-form">
				<form action="" method="GET">
					<div class="row">
						<div class="large-2 small-12 medium-2 columns">
							<h5>Buscar Por</h5>
						</div>
						<div class="large-4 small-12 medium-4 columns">
							<input type="text" placeholder="Palabra clave" value="<?php echo esc_attr($_GET['search']) ?>" class="input-type" name="search">
						</div>
						<div class="large-4 small-12 medium-4 columns">
							<select name="categorizacion" class="input-type">
								<option value="">Todas las temáticas</option>
								<?php 
							$terms = get_terms('categorization');
							foreach ($terms as $term) {
								$selected = (!empty($_GET['categorizacion']) && ($_GET['categorizacion'] == $term->slug)) ? ' selected="selected" ' : '';
								echo '<option value="' . $term->slug . '"' . $selected . '>' . $term->name . '</option>';
							}
							?>
							</select>
						</div>
						<div class="large-1 medium-1 small-12 columns end">
							<input type="submit" class="button secondary" value="Buscar">
							<input type="hidden" name="action" value="send">
						</div>
					</div>
				</form>
			</div>
		</div>
	</header>
	<div class="row content">
		<div class="large-12 columns">
			<?php 
				if ( $query->have_posts() ) {
					echo '<div class="large-up-4 small-up-2 clearfix row">';
						while( $query->have_posts() ): $query->the_post();
							global $post;
							echo base::news_share($post);
						endwhile;
					echo '</div>';
						if ( function_exists('wp_pagenavi')) {
							wp_pagenavi(array('query' => $query));
						}
					} else {
						echo '<div class="callout warning"><h5>Lo sentimos</h5> <p>No se han encontrado resultados para lo que buscas</p> </div>';
					}
			 ?>
		</div>
	</div>
</section>
<?php get_footer() ?>