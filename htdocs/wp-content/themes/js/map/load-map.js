jQuery(document).ready(function($){
	var data = [
        {
            "hc-key": "cl-2730",
            "value": 1
        },
        {
            "hc-key": "cl-bi",
            "value": 1
        },
        {
            "hc-key": "cl-ll",
            "value": 1
        },
        {
            "hc-key": "cl-li",
            "value": 1
        },
        {
            "hc-key": "cl-ai",
            "value": 1
        },
        {
            "hc-key": "cl-ma",
            "value": 1
        },
        {
            "hc-key": "cl-co",
            "value": 1
        },
        {
            "hc-key": "cl-at",
            "value": 1
        },
        {
            "hc-key": "cl-vs",
            "value": 1
        },
        {
            "hc-key": "cl-rm",
            "value": 1
        },
        {
            "hc-key": "cl-ar",
            "value": 1
        },
        {
            "hc-key": "cl-ml",
            "value": 1
        },
        {
            "hc-key": "cl-ta",
            "value": 1
        },
        {
            "hc-key": "cl-2740",
            "value": 1
        },
        {
            "hc-key": "cl-an",
            "value": 1
        }
    ];

    $('.map-content').highcharts('Map', {
    	chart: {
    		width: 500,
    		height: 1000
    	},
        title : {
            text : ''
        },

        mapNavigation: {
            enabled: false,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },
        legend: {
        	enabled: false
        },
        plotOptions: {
        	map: {
        		color: 'white',
        		allowPointSelect: true,
        		states : {
        			select: { color: '#F84444' }
        		}
        	},
        	series: {
        		point: {
        			events: {
        				select : function() {
        					//console.log(this.series.chart);
        				},
        				click : function(event) {
        					//console.log(event.point);
        					$.ajax({
        						url: Ajax.url,
        						method: 'POST',
        						data: {
        							action: 'get_location',
        							territorio: event.point.name
        						},
		        				beforeSend: function() {
		        					$('.map-target').html('<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>');
		        				}
        					}).done(function(data){
                                $('html, body').stop().animate({
                                    scrollTop: 0
                                }, 800);
                                
		        				$('.map-target').html(data);
		        			});
        				}
        			}
        		}
        	}
        },
        series : [{
           	data : data,
            mapData: Highcharts.maps['countries/cl/cl-all'],
            joinBy: 'hc-key',
            events : {
            	click : function(event) {
            		//console.log(event.point);
            	}
            },
            name: 'Union nacional Estudiantil',
            states: {
                hover: {
                    color: '#4A90E2'
                }
            },
            tooltip: {
            	headerFormat: '',
            	pointFormat: '{point.name}'
            },
            dataLabels: {
                enabled: false,
                format: '{point.name}'
            }
        }]
    });
});