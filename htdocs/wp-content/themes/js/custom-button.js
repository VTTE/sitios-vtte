(function(){
	tinymce.PluginManager.add('custom_button_centered_content', function( editor, url ) {
		editor.addButton( 'custom_button_centered_content', {
            text: '',
            icon: 'icon dashicons-list-view',
            title: 'Contenido Centrado',
            onclick : function(){
            	selected = tinyMCE.activeEditor.selection.getContent();
            	if ( selected ) {
            		content = '[contenido-centrado]'+selected+'[/contenido-centrado]';
            	} else {
            		content = '[contenido-centrado][/contenido-centrado]';
            	}
            	tinyMCE.activeEditor.selection.setContent(content);
            }
           });
	});
})();