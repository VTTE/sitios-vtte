(function($){
	//function by @basilio
	$.fn.av_heights = function(){
		tallest = 0;
		$(this).each(function(){
			if ( $(this).height() > tallest ) { tallest = $(this).outerHeight(); };
		})
		$(this).css({'height': tallest});
	}
	$.fn.smoothTop = function(){
		$(this).click(function(){
	    	$("html, body").animate({ scrollTop: 0 }, 600);
	    		return false;
	    });
		
	}
	
	

})(jQuery);

jQuery(document).ready(function($){
	$(document).foundation();
	$('.box').swipebox();
	$('.facebook-share').on('click',function(event){
		/*
			Facebook share api
			- Text
			- Url
			- Caption
			- img
		*/
		event.preventDefault();
		var obj = $(this);
		var text = obj.data('text');
		FB.ui({
		  method: 'feed',
		  link: obj.data('url'),
		  caption: obj.data('caption'),
		  name: obj.data('caption'),
		  description: text,
		  picture: obj.data('img')
		}, function(response){});
	});
	$('.content-nav-menu').on('click', function(e) {
		e.preventDefault();
		$('.content-nav-mobile-menu').toggleClass('hide');
		$(this).toggleClass('active');
		return false;
	});
	$('.theme-mobile-menu').on('click', function(e) {
		e.preventDefault();
		$('.themes-mobile-list').toggleClass('hide');
		$(this).toggleClass('active');
		return false;
	});
	$('.mobile-nav-button').on('click',function(e) {
		e.preventDefault();
		$('.menu-mobile-container').toggleClass('closed');
		return false;
	});
	$('.menu-mobile-container').find('.close').on('click', function(e) {
		e.preventDefault();
		$(this).parent().addClass('closed');
		return false;
	});
	$('.menu-mobile-container').find('.menu-item-has-children > a').on('click', function(e){
		e.preventDefault();
		var obj = $(this);
		obj.parent().toggleClass('opened');
		obj.parent().find('.sub-menu').slideToggle('fast');
		return false;
	});
	$('.top-nav').on('click',function(event){
		event.preventDefault();
		$('.top-menu-content').slideToggle('fast');
		$('.top-menu-content').toggleClass('active');
	});

	$('.left-image').each(function(){
		var banner = $(this);
		var height = banner.outerHeight();
		banner.find('.image').css('height',height);
	});
	$('.to-top').on('click',function(event){
		event.preventDefault();
		$('html, body').stop().animate({
            scrollTop: 0
        }, 800);
	});
});