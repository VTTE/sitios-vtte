# Red de sitios VTTE

Este es el repositorio de la red de sitios de la VTTE, el cual está compuesto por los siguientes elementos

### Front

El frontend de cada sitio está construido en base al framework llamado Foundation ( http://foundation.zurb.com ) el cual genera un ambiente de desarrollo que utiliza Sass como preprocesador de CSS, Gulp como disparador de tareas, Bower como gestor de dependencias frontend y node como el lenguaje base en el que esta construido el entorno. Foundation, además, provee una herramienta CLI para ejecutar las tareas.
Las carpetas que contienen entornos de frontend son:

* Direcap-front 
* Editorial-front
* Revistas-front
* VTTE-base
* VTTE-front

Cada carpeta está configurada para compilar el css generado y enviarlo a la carpeta correspondiente en la instalación de wordpress

Para ejecutar el watch de foundation que observará los archivos .scss cuando cambien es:

> foundation watch
 
Esto lanzará la tarea en Gulp, la cual observará los archivos y cuando cambien disparará la tarea de compilar el archivo Sass y enviará el css a la carpeta correspondiente de Wordpress.

* _Nota: no es necesario realizar cambios en estos archivos de configuración de gulp o de bower o node, cualquier cambio que se haga, debe ser con conocimiento de lo que se está haciendo._

#### Instalación

Para instalar correctamente el entorno es necesario tener instalado lo siguiente:

* Node
* Foundation-cli

_Para detalles de instalación ver: http://foundation.zurb.com/sites/docs/installation.html_


luego, entrar a la carpeta de cada frontend y ejecutar lo siguiente

> bower install

> npm install
 

### Wordpress multisite

La instalación del multisitio viene en la siguiente carpeta

* htdocs

Por defecto el repositorio no guarda los archivos que deben generarse localmente para cada instalación como por ejemplo el _wp-config.php_ por lo tanto, si se busca realizar modificaciones se debe generar una instalación local de wordpress  utilizando este repositorio y toda su estructura.

## A considerar

El repositorio administra toda la instalación de wordpress, por lo tanto hay cosas que no se pueden realizar directamente en el sitio en producción como por ejemplo

	* Actualizar la versión de Wordpress
	* Instalar o actualizar plugins

Para realizar estas tareas es necesario tener siempre una instalación local de wordpress donde se pueda probar el cambio antes realizar un commit

--

Para realizar cambios de mayor envergadura o agregar nuevas funcionalidades que impliquen agregar archivos o hacer cambios sustanciales en el código que puedan afectar la estabilidad del mismo se recomienda la creación de una rama en el repositorio a fin de no generar problemas con la rama _master_